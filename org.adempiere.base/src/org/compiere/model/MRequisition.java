/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.io.File;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.process.DocAction;
import org.compiere.process.DocOptions;
import org.compiere.process.DocumentEngine;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

import com.uns.model.IUNSApprovalInfo;

/**
 *	Requisition Model
 *	
 *  @author Jorg Janke
 *
 *  @author victor.perez@e-evolution.com, e-Evolution http://www.e-evolution.com
 * 			<li> FR [ 2520591 ] Support multiples calendar for Org 
 *			@see http://sourceforge.net/tracker2/?func=detail&atid=879335&aid=2520591&group_id=176962 
 *  @version $Id: MRequisition.java,v 1.2 2006/07/30 00:51:05 jjanke Exp $
 *  @author red1
 *  		<li>FR [ 2214883 ] Remove SQL code and Replace for Query  
 *  @author Teo Sarca, www.arhipac.ro
 *  		<li>FR [ 2744682 ] Requisition: improve error reporting
 */
public class MRequisition extends X_M_Requisition implements DocAction, IUNSApprovalInfo, DocOptions
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 898606565778668659L;

	/**
	 * 	Standard Constructor
	 *	@param ctx context
	 *	@param M_Requisition_ID id
	 */
	public MRequisition (Properties ctx, int M_Requisition_ID, String trxName)
	{
		super (ctx, M_Requisition_ID, trxName);
		if (M_Requisition_ID == 0)
		{
		//	setDocumentNo (null);
		//	setAD_User_ID (0);
		//	setM_PriceList_ID (0);
		//	setM_Warehouse_ID(0);
			setDateDoc(new Timestamp(System.currentTimeMillis()));
			setDateRequired (new Timestamp(System.currentTimeMillis()));
			setDocAction (DocAction.ACTION_Complete);	// CO
			setDocStatus (DocAction.STATUS_Drafted);		// DR
			setPriorityRule (PRIORITYRULE_Medium);	// 5
			setTotalLines (Env.ZERO);
			setIsApproved (false);
			setPosted (false);
			setProcessed (false);
		}
	}	//	MRequisition

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 */
	public MRequisition (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MRequisition
	
	/** Lines						*/
	private MRequisitionLine[]		m_lines = null;
	
	/**
	 * 	Get Lines
	 *	@return array of lines
	 */
	public MRequisitionLine[] getLines()
	{
		if (m_lines != null) {
			set_TrxName(m_lines, get_TrxName());
			return m_lines;
		}
		
		//red1 - FR: [ 2214883 ] Remove SQL code and Replace for Query  
 	 	final String whereClause = I_M_RequisitionLine.COLUMNNAME_M_Requisition_ID+"=?";
	 	List <MRequisitionLine> list = new Query(getCtx(), I_M_RequisitionLine.Table_Name, whereClause, get_TrxName())
			.setParameters(get_ID())
			.setOrderBy(I_M_RequisitionLine.COLUMNNAME_Line)
			.list();
	 	//  red1 - end -

		m_lines = new MRequisitionLine[list.size ()];
		list.toArray (m_lines);
		return m_lines;
	}	//	getLines
	
	/**
	 * 	String Representation
	 *	@return info
	 */
	public String toString ()
	{
		StringBuilder sb = new StringBuilder ("MRequisition[");
		sb.append(get_ID()).append("-").append(getDocumentNo())
			.append(",Status=").append(getDocStatus()).append(",Action=").append(getDocAction())
			.append ("]");
		return sb.toString ();
	}	//	toString
	
	/**
	 * 	Get Document Info
	 *	@return document info
	 */
	public String getDocumentInfo()
	{
		return Msg.getElement(getCtx(), "M_Requisition_ID") + " " + getDocumentNo();
	}	//	getDocumentInfo
	
	/**
	 * 	Create PDF
	 *	@return File or null
	 */
	public File createPDF ()
	{
		try
		{
			File temp = File.createTempFile(get_TableName()+get_ID()+"_", ".pdf");
			return createPDF (temp);
		}
		catch (Exception e)
		{
			log.severe("Could not create PDF - " + e.getMessage());
		}
		return null;
	}	//	getPDF

	/**
	 * 	Create PDF file
	 *	@param file output file
	 *	@return file if success
	 */
	public File createPDF (File file)
	{
	//	ReportEngine re = ReportEngine.get (getCtx(), ReportEngine.INVOICE, getC_Invoice_ID());
	//	if (re == null)
			return null;
	//	return re.getPDF(file);
	}	//	createPDF

	/**
	 * 	Set default PriceList
	 */
//	public void setM_PriceList_ID()
//	{
//		MPriceList defaultPL = MPriceList.getDefault(getCtx(), false);
//		if (defaultPL == null)
//			defaultPL = MPriceList.getDefault(getCtx(), true);
//		if (defaultPL != null)
//			setM_PriceList_ID(defaultPL.getM_PriceList_ID());
//	}	//	setM_PriceList_ID()
	
	/**
	 * 	Before Save
	 *	@param newRecord new
	 *	@return true
	 */
	protected boolean beforeSave (boolean newRecord)
	{
//		if (getM_PriceList_ID() == 0)
//			setM_PriceList_ID();
		return true;
	}	//	beforeSave
	
	@Override
	protected boolean beforeDelete() {
		for (MRequisitionLine line : getLines()) {
			line.deleteEx(true);
		}
		return true;
	}

	/**************************************************************************
	 * 	Process document
	 *	@param processAction document action
	 *	@return true if performed
	 */
	public boolean processIt (String processAction)
	{
		m_processMsg = null;
		DocumentEngine engine = new DocumentEngine (this, getDocStatus());
		return engine.processIt (processAction, getDocAction());
	}	//	process
	
	/**	Process Message 			*/
	private String			m_processMsg = null;
	/**	Just Prepared Flag			*/
	private boolean 		m_justPrepared = false;

	/**
	 * 	Unlock Document.
	 * 	@return true if success 
	 */
	public boolean unlockIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("unlockIt - " + toString());
		setProcessing(false);
		return true;
	}	//	unlockIt
	
	/**
	 * 	Invalidate Document
	 * 	@return true if success 
	 */
	public boolean invalidateIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("invalidateIt - " + toString());
		return true;
	}	//	invalidateIt
	
	/**
	 *	Prepare Document
	 * 	@return new status (In Progress or Invalid) 
	 */
	public String prepareIt()
	{
		if (log.isLoggable(Level.INFO)) log.info(toString());
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		MRequisitionLine[] lines = getLines();
		
		//	Invalid
		if (getAD_User_ID() == 0 
//			|| getM_PriceList_ID() == 0
			|| getM_Warehouse_ID() == 0)
		{
			return DocAction.STATUS_Invalid;
		}
		
		if(lines.length == 0)
		{
			throw new AdempiereException("@NoLines@");
		}
		
		//	Std Period open?
		MPeriod.testPeriodOpen(getCtx(), getDateDoc(), MDocType.DOCBASETYPE_PurchaseRequisition, getAD_Org_ID());
		MPeriod.testPeriodOpen(getCtx(), getDateDoc(), MDocType.DOCBASETYPE_PackingListRequisition, getAD_Org_ID());
		
		//	Add up Amounts
//		int precision = MPriceList.getStandardPrecision(getCtx(), getM_PriceList_ID());
//		BigDecimal totalLines = Env.ZERO;
//		for (int i = 0; i < lines.length; i++)
//		{
//			MRequisitionLine line = lines[i];
//			BigDecimal lineNet = line.getQty().multiply(line.getPriceActual());
//			lineNet = lineNet.setScale(precision, BigDecimal.ROUND_HALF_UP);
//			if (lineNet.compareTo(line.getLineNetAmt()) != 0)
//			{
//				line.setLineNetAmt(lineNet);
//				line.saveEx();
//			}
//			totalLines = totalLines.add (line.getLineNetAmt());
//		}
//		if (totalLines.compareTo(getTotalLines()) != 0)
//		{
//			setTotalLines(totalLines);
//			saveEx();
//		}
		
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_PREPARE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		m_justPrepared = true;
		setProcessed(true);
		return DocAction.STATUS_InProgress;
	}	//	prepareIt
	
	/**
	 * 	Approve Document
	 * 	@return true if success 
	 */
	public boolean  approveIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("approveIt - " + toString());
		setIsApproved(true);
		return true;
	}	//	approveIt
	
	/**
	 * 	Reject Approval
	 * 	@return true if success 
	 */
	public boolean rejectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("rejectIt - " + toString());
		setIsApproved(false);
		setProcessed(false);
		return true;
	}	//	rejectIt
	
	/**
	 * 	Complete Document
	 * 	@return new status (Complete, In Progress, Invalid, Waiting ..)
	 */
	public String completeIt()
	{
		//	Re-Check
		if (!m_justPrepared)
		{
			String status = prepareIt();
			m_justPrepared = false;
			if (!DocAction.STATUS_InProgress.equals(status))
				return status;
		}

		// Set the definite document number after completed (if needed)
		setDefiniteDocumentNo();

		m_processMsg = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_BEFORE_COMPLETE);
		if (m_processMsg != null)
			return DocAction.STATUS_Invalid;
		
		if(isPackingList())
		{
			MMovement mv = null;
			for(MRequisitionLine line : getLines())
			{
				if(mv == null)
				{ 
					mv = new MMovement(getCtx(), 0, get_TrxName());
					mv.setAD_Org_ID(line.getM_LocatorTo().getAD_Org_ID());
					mv.setMovementDate(getDateRequired());
					mv.setDestinationWarehouse_ID(getM_Warehouse_ID());
					mv.setIsInTransit(true);
					mv.setC_DocType_ID(DB.getSQLValue(get_TrxName(), "SELECT C_DocType_ID FROM C_DocType WHERE IsInTransit='Y'"
							+ " AND DocBaseType=?",
							MDocType.DOCBASETYPE_MaterialMovement));
					mv.setPOReference(DB.getSQLValueString(get_TrxName(), "SELECT DocumentNo FROM"
							+ " UNS_PackingList WHERE UNS_PackingList_ID=?", getUNS_PackingList_ID()));
					mv.saveEx();
				}
				
				MMovementLine mvl = new MMovementLine(mv);
				mvl.setM_RequisitionLine_ID(line.get_ID());
				mvl.setM_Locator_ID(line.getM_LocatorTo_ID());
				mvl.setM_LocatorTo_ID(line.getM_Locator_ID());
				mvl.setM_Product_ID(line.getM_Product_ID());
				mvl.setMovementQty(line.getQty());
				mvl.saveEx();
				line.setMovementQty(line.getMovementQty().add(mvl.getMovementQty()));
				line.saveEx();
			}
			
			if(!mv.processIt(DocAction.ACTION_Prepare) || !mv.save())
			{
				m_processMsg = "Failed when create Inventory Move. " + mv.getProcessMsg();
				return DocAction.STATUS_Invalid;
			}
		}
		//	Implicit Approval
		if (!isApproved())
			approveIt();
		if (log.isLoggable(Level.INFO)) log.info(toString());
		
		//	User Validation
		String valid = ModelValidationEngine.get().fireDocValidate(this, ModelValidator.TIMING_AFTER_COMPLETE);
		if (valid != null)
		{
			m_processMsg = valid;
			return DocAction.STATUS_Invalid;
		}

		//
		setProcessed(true);
		setDocAction(ACTION_Close);
		return DocAction.STATUS_Completed;
	}	//	completeIt
	
	/**
	 * 	Set the definite document number after completed
	 */
	private void setDefiniteDocumentNo() {
		MDocType dt = MDocType.get(getCtx(), getC_DocType_ID());
		if (dt.isOverwriteDateOnComplete()) {
			setDateDoc(new Timestamp (System.currentTimeMillis()));
			MPeriod.testPeriodOpen(getCtx(), getDateDoc(), MDocType.DOCBASETYPE_PurchaseRequisition, getAD_Org_ID());
		}
		if (dt.isOverwriteSeqOnComplete()) {
			String value = DB.getDocumentNo(getC_DocType_ID(), get_TrxName(), true, this);
			if (value != null)
				setDocumentNo(value);
		}
	}

	/**
	 * 	Void Document.
	 * 	Same as Close.
	 * 	@return true if success 
	 */
	public boolean voidIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("voidIt - " + toString());
		// Before Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_VOID);
		if (m_processMsg != null)
			return false;
		
		if(null != checkPO(true))
		{
			m_processMsg = "Cannot void this document, " + m_processMsg;
			return false;
		}
		
		if (!closeIt())
			return false;
		
		// After Void
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_VOID);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	voidIt
	
	/**
	 * 	Close Document.
	 * 	Cancel not delivered Qunatities
	 * 	@return true if success 
	 */
	public boolean closeIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("closeIt - " + toString());
		// Before Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_CLOSE);
		if (m_processMsg != null)
			return false;
		
		//	Close Not delivered Qty
		MRequisitionLine[] lines = getLines();
		BigDecimal totalLines = Env.ZERO;
		for (int i = 0; i < lines.length; i++)
		{
			MRequisitionLine line = lines[i];
			BigDecimal finalQty = line.getQty();
			if (line.getC_OrderLine_ID() == 0)
				finalQty = Env.ZERO;
			else
			{
				MOrderLine ol = new MOrderLine (getCtx(), line.getC_OrderLine_ID(), get_TrxName());
				finalQty = ol.getQtyOrdered();
			}
			//	final qty is not line qty
			if (finalQty.compareTo(line.getQty()) != 0)
			{
				String description = line.getDescription();
				if (description == null)
					description = "";
				description += " [" + line.getQty() + "]"; 
				line.setDescription(description);
				line.setQty(finalQty);
				line.setLineNetAmt();
				line.saveEx();
			}
			totalLines = totalLines.add (line.getLineNetAmt());
		}
		if (totalLines.compareTo(getTotalLines()) != 0)
		{
			setTotalLines(totalLines);
			saveEx();
		}
		// After Close
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_CLOSE);
		if (m_processMsg != null)
			return false;
		
		return true;
	}	//	closeIt
	
	/**
	 * 	Reverse Correction
	 * 	@return true if success 
	 */
	public boolean reverseCorrectIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseCorrectIt - " + toString());
		// Before reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		// After reverseCorrect
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSECORRECT);
		if (m_processMsg != null)
			return false;

		return false;
	}	//	reverseCorrectionIt
	
	/**
	 * 	Reverse Accrual - none
	 * 	@return true if success 
	 */
	public boolean reverseAccrualIt()
	{
		if (log.isLoggable(Level.INFO)) log.info("reverseAccrualIt - " + toString());
		// Before reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_BEFORE_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;

		// After reverseAccrual
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,ModelValidator.TIMING_AFTER_REVERSEACCRUAL);
		if (m_processMsg != null)
			return false;				
		
		return false;
	}	//	reverseAccrualIt
	
	/** 
	 * 	Re-activate
	 * 	@return true if success 
	 */
	public boolean reActivateIt()
	{
		log.info(toString());
		// Before reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_BEFORE_REACTIVATE);
		if (m_processMsg != null)
			return false;
		
		if(null != checkPO(false))
		{
			m_processMsg = "Cannot reactivate this document, " + m_processMsg;
			return false;
		}
		
		// After reActivate
		m_processMsg = ModelValidationEngine.get().fireDocValidate(this,
				ModelValidator.TIMING_AFTER_REACTIVATE);
		if (m_processMsg != null)
			return false;

		setProcessed(false);
		setDocAction(DOCACTION_Complete);
		return true;
	}	//	reActivateIt
	
	/*************************************************************************
	 * 	Get Summary
	 *	@return Summary of Document
	 */
	public String getSummary()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getDocumentNo());
		//	 - Organization
		MOrg org = new MOrg(getCtx(), getAD_Org_ID(), get_TrxName());
		sb.append(" #Org : ").append(org.getName());
		//	 - Date
		sb.append(" #Date : ").append(getDateDoc());
		//	 - User
		sb.append(" #User : ").append(Env.getContext(getCtx(), "#AD_User_Name"));
		//	 - Priority
		sb.append(" #Priority : ").append(getPriorityRule());
		//	 - Warehouse
		sb.append(" #Warehouse : ").append(getM_Warehouse().getName());
		
		//	 - Description
		if (getDescription() != null && getDescription().length() > 0)
			sb.append(" - ").append(getDescription());
		return sb.toString();
	}	//	getSummary
	
	/**
	 * 	Get Process Message
	 *	@return clear text error message
	 */
	public String getProcessMsg()
	{
		return m_processMsg;
	}	//	getProcessMsg
	
	/**
	 * 	Get Document Owner
	 *	@return AD_User_ID
	 */
	public int getDoc_User_ID()
	{
		return getAD_User_ID();
	}
	
	/**
	 * 	Get Document Currency
	 *	@return C_Currency_ID
	 */
	public int getC_Currency_ID()
	{
		MPriceList pl = MPriceList.get(getCtx(), getM_PriceList_ID(), get_TrxName());
		return pl.getC_Currency_ID();
	}

	/**
	 * 	Get Document Approval Amount
	 *	@return amount
	 */
	public BigDecimal getApprovalAmt()
	{
		return getTotalLines();
	}
	
	/**
	 * 	Get User Name
	 *	@return user name
	 */
	public String getUserName()
	{
		return MUser.get(getCtx(), getAD_User_ID()).getName();
	}	//	getUserName

	/**
	 * 	Document Status is Complete or Closed
	 *	@return true if CO, CL or RE
	 */
	public boolean isComplete()
	{
		String ds = getDocStatus();
		return DOCSTATUS_Completed.equals(ds) 
			|| DOCSTATUS_Closed.equals(ds)
			|| DOCSTATUS_Reversed.equals(ds);
	}	//	isComplete

	public String checkPO(boolean isVoid)
	{
		m_processMsg = null;
		
		for(MRequisitionLine rl : getLines())
		{
			if(rl.getC_OrderLine_ID() > 0)
			{
				m_processMsg = "Document Purchase Order has created.";
				break;
			}
			
			String m_UserName = Env.getContext(getCtx(), "#AD_User_Name");
			
			if(isVoid)
			{	
				String uomconversion = DB.getSQLValueString(get_TrxName(),
						"SELECT uomconversiontostr(M_Product_ID, Qty) FROM M_RequisitionLine"
								+ " WHERE M_RequisitionLine_ID=?" , rl.get_ID());
				rl.setQty(Env.ZERO);
				
				String m_AddDescription = "Voided By " + m_UserName + " - (" + uomconversion + ")";
				rl.setDescription(Util.isEmpty(rl.getDescription()) ? m_AddDescription : rl.getDescription() + " || " + m_AddDescription);
				if(!rl.save())
				{
					m_processMsg = "Failed when trying update requisition line";
				}
			}
		}
		
		return m_processMsg;
	}
	@Override
	public List<Object[]> getApprovalInfoColumnClassAccessable() {
		
		List<Object[]> list = new ArrayList<>();	
		list.add(new Object[]{String.class, true}); 			//Name BPartner
		list.add(new Object[]{String.class, true}); 			//Name Product OR Charge
		list.add(new Object[]{String.class, true}); 			//Quantity - UOM (1)
		list.add(new Object[]{String.class, true}); 			//Quantity - UOM (2)
		list.add(new Object[]{String.class, true}); 			//Quantity - UOM (3)
		list.add(new Object[]{String.class, true}); 			//Quantity - UOM (4)
		list.add(new Object[]{String.class, true});				//Description
		if(isPackingList())
			list.add(new Object[]{String.class, true});
		
		return list;
	}

	@Override
	public String[] getDetailTableHeader()
	{	
		String[] def = null;
		if(isPackingList())
			def = new String[]{"Locator Request", "Locator Requested", "Product/Charge", "Qty Packing", "Qty Loc Request"
				, "Qty Loc Requested", "Qty Difference", "Qty Request"};
		else
			def = new String[]{"Vendor/Supplier", "Product/Charge", "Quantity - UOM (1)",
					"Quantity - UOM (2)", "Quantity - UOM (3)", "Quantity - UOM (4)", "Price", "Total", "Description"};
		
		return def;
	}

	@Override
	public List<Object[]> getDetailTableContent() {
		
		List<Object[]> list = new ArrayList<>();
		String sql = null;
		if(isPackingList())
		{
			sql = "SELECT Master.LocRequest, Master.LocRequested, Master.Product,"
					+ " UOMConversionToSTR(ProductID, Master.QtyPacking) AS QtyPacking,"
					+ " UOMConversionToSTR(ProductID, Master.QtyLocRequest) AS QtyLocRequest,"
					+ " UOMConversionToSTR(ProductID, Master.QtyLocRequested) AS QtyLocRequested,"
					+ " UOMConversionToSTR(ProductID, (Master.QtyLocRequest - Master.QtyPacking)) AS QtyDiff,"
					+ " UOMConversionToSTR(ProductID, Master.QtyRequest) AS QtyRequest FROM"
					+ " (SELECT ml.Value AS LocRequest, mll.Value AS LocRequested, p.Name AS Product,"
					+ " (SELECT SUM(pll.MovementQty) FROM UNS_PackingList_Line pll WHERE pll.M_Product_ID = p.M_Product_ID"
					+ " AND pll.M_Locator_ID = ml.M_Locator_ID AND EXISTS"
					+ " (SELECT 1 FROM UNS_PackingList_Order plo WHERE plo.UNS_PackingList_Order_ID"
					+ " = pll.UNS_PackingList_Order_ID AND plo.UNS_PackingList_ID=mr.UNS_PackingList_ID)) AS QtyPacking,"
					+ " TotalStockByLocator(p.M_Product_ID, ml.M_Locator_ID, null) AS QtyLocRequest,"
					+ " TotalStockByLocator(p.M_Product_ID, mll.M_Locator_ID, null) AS QtyLocRequested,"
					+ " rl.Qty AS QtyRequest, p.M_Product_ID AS ProductID"
					+ " FROM M_Requisition mr"
					+ " INNER JOIN M_RequisitionLine rl ON rl.M_Requisition_ID = mr.M_Requisition_ID"
					+ " INNER JOIN M_Product p ON p.M_Product_ID = rl.M_Product_ID"
					+ " INNER JOIN M_Locator ml ON ml.M_Locator_ID = rl.M_Locator_ID"
					+ " INNER JOIN M_Locator mll ON mll.M_Locator_ID = rl.M_LocatorTo_ID"
					+ " WHERE mr.M_Requisition_ID=?) AS Master";
		}
		else
		{
			sql = "SELECT COALESCE(bp.Name, '--') AS VendorSupplier, COALESCE(p.Name, c.Name) AS ProductORCharge," //1, 2 
					+ " CASE WHEN u1.C_UOM_ID > 0 THEN ROUND(rl.UOMQtyL1,2) || ' ' || u1.UOMSymbol ELSE '--' END AS QtyUOM1," //3
					+ " CASE WHEN u2.C_UOM_ID > 0 THEN ROUND(rl.UOMQtyL2,2) || ' ' || u2.UOMSymbol ELSE '--' END AS QtyUOM2," //4
					+ " CASE WHEN u3.C_UOM_ID > 0 THEN ROUND(rl.UOMQtyL3,2) || ' ' || u3.UOMSymbol ELSE '--' END AS QtyUOM3," //5
					+ " CASE WHEN u4.C_UOM_ID > 0 THEN ROUND(rl.UOMQtyL4,2) || ' ' || u4.UOMSymbol ELSE '--' END AS QtyUOM4," //6
					+ " rl.PriceActual AS Price, rl.LineNetAmt AS LineAmt,"
					+ " COALESCE(rl.Description, '--') AS Description" // 7
					+ " FROM M_RequisitionLine rl"
					+ " LEFT JOIN C_BPartner bp ON bp.C_BPartner_ID = rl.C_BPartner_ID"
					+ " LEFT JOIN M_Product p ON p.M_Product_ID = rl.M_Product_ID"
					+ " LEFT JOIN C_Charge c ON c.C_Charge_ID = rl.C_Charge_ID"
					+ " LEFT JOIN C_UOM u1 ON u1.C_UOM_ID = rl.UOMConversionL1_ID"
					+ " LEFT JOIN C_UOM u2 ON u2.C_UOM_ID = rl.UOMConversionL2_ID"
					+ " LEFT JOIN C_UOM u3 ON u3.C_UOM_ID = rl.UOMConversionL3_ID"
					+ " LEFT JOIN C_UOM u4 ON u4.C_UOM_ID = rl.UOMConversionL4_ID"
					+ " WHERE rl.M_Requisition_ID=?"
					+ " AND CASE WHEN (SELECT COUNT(*) FROM M_RequisitionLine l WHERE l.M_Requisition_ID = rl.M_Requisition_ID"
					+ " AND l.SalesRep_ID = " + Env.getContextAsInt(getCtx(), "#AD_User_ID") + ") > 0 THEN rl.SalesRep_ID = "
					+ Env.getContextAsInt(getCtx(), "#AD_User_ID") + " ELSE 1=1 END";
		}
	
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try
		{
			st = DB.prepareStatement(sql, get_TrxName());
			st.setInt(1, get_ID());
			rs = st.executeQuery();
			
			while (rs.next())
			{
				int count = 0;
				Object[] rowData = null;
				if(isPackingList())
				{
					rowData = new Object[8];
					rowData[count] = rs.getObject("LocRequest");
					rowData[++count] = rs.getObject("LocRequested");
					rowData[++count] = rs.getObject("Product");
					rowData[++count] = rs.getObject("QtyPacking");
					rowData[++count] = rs.getObject("QtyLocRequest");
					rowData[++count] = rs.getObject("QtyLocRequested");
					rowData[++count] = rs.getObject("QtyDiff");
					rowData[++count] = rs.getObject("QtyRequest");
				}
				else
				{
					rowData = new Object[9];
					rowData[count] = rs.getObject("VendorSupplier");
					rowData[++count] = rs.getObject("ProductORCharge");
					rowData[++count] = rs.getObject("QtyUOM1");
					rowData[++count] = rs.getObject("QtyUOM2");
					rowData[++count] = rs.getObject("QtyUOM3");
					rowData[++count] = rs.getObject("QtyUOM4");
					rowData[++count] = rs.getObject("Price");
					rowData[++count] = rs.getObject("LineAmt");
					rowData[++count] = rs.getObject("Description");
				}
				
				list.add(rowData);
			}
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		finally
		{
			DB.close(rs, st);
		}
		
		return list;
	}

	@Override
	public int customizeValidActions(String docStatus, Object processing,
			String orderType, String isSOTrx, int AD_Table_ID,
			String[] docAction, String[] options, int index) {
		
		if (docStatus.equals(DocumentEngine.STATUS_Completed))
		{
			options[index++] = DocumentEngine.ACTION_ReActivate;
			options[index++] = DocumentEngine.ACTION_Void;
		}
		if(docStatus.equals(DocumentEngine.STATUS_NotApproved))
		{
			options[index++] = DocumentEngine.ACTION_Complete;
		}
		return index;
	}
	
	public static MRequisition getByPL(Properties ctx, int UNS_PackingList_ID, 
										int M_Warehouse_ID, String trxName)
	{
		MRequisition req = null;
		String whereClause = COLUMNNAME_UNS_PackingList_ID + "=? AND " + COLUMNNAME_M_Warehouse_ID + "=?";
		req = new Query(ctx, Table_Name, whereClause, trxName).setParameters(UNS_PackingList_ID, M_Warehouse_ID).first();
		
		return req;
	}
	
	public boolean isPackingList()
	{
		if(getC_DocType_ID() == MDocType.getDocType(MDocType.DOCBASETYPE_PackingListRequisition))
			return true;
		return false;
	}

	@Override
	public boolean isShowAttachmentDetail() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getTableIDDetail() {
		// TODO Auto-generated method stub
		return 0;
	}
}	//	MRequisition

/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for M_MovementLine
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
public interface I_M_MovementLine 
{

    /** TableName=M_MovementLine */
    public static final String Table_Name = "M_MovementLine";

    /** AD_Table_ID=324 */
    public static final int Table_ID = 324;

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name ConfirmedQty */
    public static final String COLUMNNAME_ConfirmedQty = "ConfirmedQty";

	/** Set Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public void setConfirmedQty (BigDecimal ConfirmedQty);

	/** Get Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public BigDecimal getConfirmedQty();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name DD_OrderLine_ID */
    public static final String COLUMNNAME_DD_OrderLine_ID = "DD_OrderLine_ID";

	/** Set Distribution Order Line	  */
	public void setDD_OrderLine_ID (int DD_OrderLine_ID);

	/** Get Distribution Order Line	  */
	public int getDD_OrderLine_ID();

	public org.eevolution.model.I_DD_OrderLine getDD_OrderLine() throws RuntimeException;

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name M_AttributeSetInstance_ID */
    public static final String COLUMNNAME_M_AttributeSetInstance_ID = "M_AttributeSetInstance_ID";

	/** Set Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID);

	/** Get Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID();

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException;

    /** Column name M_AttributeSetInstanceTo_ID */
    public static final String COLUMNNAME_M_AttributeSetInstanceTo_ID = "M_AttributeSetInstanceTo_ID";

	/** Set Attribute Set Instance To.
	  * Target Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstanceTo_ID (int M_AttributeSetInstanceTo_ID);

	/** Get Attribute Set Instance To.
	  * Target Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstanceTo_ID();

	public I_M_AttributeSetInstance getM_AttributeSetInstanceTo() throws RuntimeException;

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public org.compiere.model.I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_LocatorTo_ID */
    public static final String COLUMNNAME_M_LocatorTo_ID = "M_LocatorTo_ID";

	/** Set Locator To.
	  * Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID);

	/** Get Locator To.
	  * Location inventory is moved to
	  */
	public int getM_LocatorTo_ID();

	public I_M_Locator getM_LocatorTo() throws RuntimeException;

    /** Column name M_Movement_ID */
    public static final String COLUMNNAME_M_Movement_ID = "M_Movement_ID";

	/** Set Inventory Move.
	  * Movement of Inventory
	  */
	public void setM_Movement_ID (int M_Movement_ID);

	/** Get Inventory Move.
	  * Movement of Inventory
	  */
	public int getM_Movement_ID();

	public org.compiere.model.I_M_Movement getM_Movement() throws RuntimeException;

    /** Column name M_MovementLine_ID */
    public static final String COLUMNNAME_M_MovementLine_ID = "M_MovementLine_ID";

	/** Set Move Line.
	  * Inventory Move document Line
	  */
	public void setM_MovementLine_ID (int M_MovementLine_ID);

	/** Get Move Line.
	  * Inventory Move document Line
	  */
	public int getM_MovementLine_ID();

    /** Column name M_MovementLine_UU */
    public static final String COLUMNNAME_M_MovementLine_UU = "M_MovementLine_UU";

	/** Set M_MovementLine_UU	  */
	public void setM_MovementLine_UU (String M_MovementLine_UU);

	/** Get M_MovementLine_UU	  */
	public String getM_MovementLine_UU();

    /** Column name MovementQty */
    public static final String COLUMNNAME_MovementQty = "MovementQty";

	/** Set Movement Quantity.
	  * Quantity of a product moved.
	  */
	public void setMovementQty (BigDecimal MovementQty);

	/** Get Movement Quantity.
	  * Quantity of a product moved.
	  */
	public BigDecimal getMovementQty();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name M_RequisitionLine_ID */
    public static final String COLUMNNAME_M_RequisitionLine_ID = "M_RequisitionLine_ID";

	/** Set Requisition Line.
	  * Material Requisition Line
	  */
	public void setM_RequisitionLine_ID (int M_RequisitionLine_ID);

	/** Get Requisition Line.
	  * Material Requisition Line
	  */
	public int getM_RequisitionLine_ID();

	public org.compiere.model.I_M_RequisitionLine getM_RequisitionLine() throws RuntimeException;

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ReversalLine_ID */
    public static final String COLUMNNAME_ReversalLine_ID = "ReversalLine_ID";

	/** Set Reversal Line.
	  * Use to keep the reversal line ID for reversing costing purpose
	  */
	public void setReversalLine_ID (int ReversalLine_ID);

	/** Get Reversal Line.
	  * Use to keep the reversal line ID for reversing costing purpose
	  */
	public int getReversalLine_ID();

	public org.compiere.model.I_M_MovementLine getReversalLine() throws RuntimeException;

    /** Column name ScrappedQty */
    public static final String COLUMNNAME_ScrappedQty = "ScrappedQty";

	/** Set Scrapped Quantity.
	  * The Quantity scrapped due to QA issues
	  */
	public void setScrappedQty (BigDecimal ScrappedQty);

	/** Get Scrapped Quantity.
	  * The Quantity scrapped due to QA issues
	  */
	public BigDecimal getScrappedQty();

    /** Column name TargetQty */
    public static final String COLUMNNAME_TargetQty = "TargetQty";

	/** Set Target Quantity.
	  * Target Movement Quantity
	  */
	public void setTargetQty (BigDecimal TargetQty);

	/** Get Target Quantity.
	  * Target Movement Quantity
	  */
	public BigDecimal getTargetQty();

    /** Column name UOMConfirmedQtyL1 */
    public static final String COLUMNNAME_UOMConfirmedQtyL1 = "UOMConfirmedQtyL1";

	/** Set Confirmed Qty L1	  */
	public void setUOMConfirmedQtyL1 (BigDecimal UOMConfirmedQtyL1);

	/** Get Confirmed Qty L1	  */
	public BigDecimal getUOMConfirmedQtyL1();

    /** Column name UOMConfirmedQtyL2 */
    public static final String COLUMNNAME_UOMConfirmedQtyL2 = "UOMConfirmedQtyL2";

	/** Set Confirmed Qty L2	  */
	public void setUOMConfirmedQtyL2 (BigDecimal UOMConfirmedQtyL2);

	/** Get Confirmed Qty L2	  */
	public BigDecimal getUOMConfirmedQtyL2();

    /** Column name UOMConfirmedQtyL3 */
    public static final String COLUMNNAME_UOMConfirmedQtyL3 = "UOMConfirmedQtyL3";

	/** Set Confirmed Qty L3	  */
	public void setUOMConfirmedQtyL3 (BigDecimal UOMConfirmedQtyL3);

	/** Get Confirmed Qty L3	  */
	public BigDecimal getUOMConfirmedQtyL3();

    /** Column name UOMConfirmedQtyL4 */
    public static final String COLUMNNAME_UOMConfirmedQtyL4 = "UOMConfirmedQtyL4";

	/** Set Confirmed Qty L4	  */
	public void setUOMConfirmedQtyL4 (BigDecimal UOMConfirmedQtyL4);

	/** Get Confirmed Qty L4	  */
	public BigDecimal getUOMConfirmedQtyL4();

    /** Column name UOMConversionL1_ID */
    public static final String COLUMNNAME_UOMConversionL1_ID = "UOMConversionL1_ID";

	/** Set UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID);

	/** Get UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException;

    /** Column name UOMConversionL2_ID */
    public static final String COLUMNNAME_UOMConversionL2_ID = "UOMConversionL2_ID";

	/** Set UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID);

	/** Get UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException;

    /** Column name UOMConversionL3_ID */
    public static final String COLUMNNAME_UOMConversionL3_ID = "UOMConversionL3_ID";

	/** Set UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID);

	/** Get UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException;

    /** Column name UOMConversionL4_ID */
    public static final String COLUMNNAME_UOMConversionL4_ID = "UOMConversionL4_ID";

	/** Set UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID);

	/** Get UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException;

    /** Column name UOMMovementQtyL1 */
    public static final String COLUMNNAME_UOMMovementQtyL1 = "UOMMovementQtyL1";

	/** Set Movement Qty L1	  */
	public void setUOMMovementQtyL1 (BigDecimal UOMMovementQtyL1);

	/** Get Movement Qty L1	  */
	public BigDecimal getUOMMovementQtyL1();

    /** Column name UOMMovementQtyL2 */
    public static final String COLUMNNAME_UOMMovementQtyL2 = "UOMMovementQtyL2";

	/** Set Movement Qty L2	  */
	public void setUOMMovementQtyL2 (BigDecimal UOMMovementQtyL2);

	/** Get Movement Qty L2	  */
	public BigDecimal getUOMMovementQtyL2();

    /** Column name UOMMovementQtyL3 */
    public static final String COLUMNNAME_UOMMovementQtyL3 = "UOMMovementQtyL3";

	/** Set Movement Qty L3	  */
	public void setUOMMovementQtyL3 (BigDecimal UOMMovementQtyL3);

	/** Get Movement Qty L3	  */
	public BigDecimal getUOMMovementQtyL3();

    /** Column name UOMMovementQtyL4 */
    public static final String COLUMNNAME_UOMMovementQtyL4 = "UOMMovementQtyL4";

	/** Set Movement Qty L4	  */
	public void setUOMMovementQtyL4 (BigDecimal UOMMovementQtyL4);

	/** Get Movement Qty L4	  */
	public BigDecimal getUOMMovementQtyL4();

    /** Column name UOMScrappedQtyL1 */
    public static final String COLUMNNAME_UOMScrappedQtyL1 = "UOMScrappedQtyL1";

	/** Set Scrapped Qty L1	  */
	public void setUOMScrappedQtyL1 (BigDecimal UOMScrappedQtyL1);

	/** Get Scrapped Qty L1	  */
	public BigDecimal getUOMScrappedQtyL1();

    /** Column name UOMScrappedQtyL2 */
    public static final String COLUMNNAME_UOMScrappedQtyL2 = "UOMScrappedQtyL2";

	/** Set Scrapped Qty L2	  */
	public void setUOMScrappedQtyL2 (BigDecimal UOMScrappedQtyL2);

	/** Get Scrapped Qty L2	  */
	public BigDecimal getUOMScrappedQtyL2();

    /** Column name UOMScrappedQtyL3 */
    public static final String COLUMNNAME_UOMScrappedQtyL3 = "UOMScrappedQtyL3";

	/** Set Scrapped Qty L3	  */
	public void setUOMScrappedQtyL3 (BigDecimal UOMScrappedQtyL3);

	/** Get Scrapped Qty L3	  */
	public BigDecimal getUOMScrappedQtyL3();

    /** Column name UOMScrappedQtyL4 */
    public static final String COLUMNNAME_UOMScrappedQtyL4 = "UOMScrappedQtyL4";

	/** Set Scrapped Qty L4	  */
	public void setUOMScrappedQtyL4 (BigDecimal UOMScrappedQtyL4);

	/** Get Scrapped Qty L4	  */
	public BigDecimal getUOMScrappedQtyL4();

    /** Column name UOMTargetQtyL1 */
    public static final String COLUMNNAME_UOMTargetQtyL1 = "UOMTargetQtyL1";

	/** Set Target Qty L1	  */
	public void setUOMTargetQtyL1 (BigDecimal UOMTargetQtyL1);

	/** Get Target Qty L1	  */
	public BigDecimal getUOMTargetQtyL1();

    /** Column name UOMTargetQtyL2 */
    public static final String COLUMNNAME_UOMTargetQtyL2 = "UOMTargetQtyL2";

	/** Set Target Qty L2	  */
	public void setUOMTargetQtyL2 (BigDecimal UOMTargetQtyL2);

	/** Get Target Qty L2	  */
	public BigDecimal getUOMTargetQtyL2();

    /** Column name UOMTargetQtyL3 */
    public static final String COLUMNNAME_UOMTargetQtyL3 = "UOMTargetQtyL3";

	/** Set Target Qty L3	  */
	public void setUOMTargetQtyL3 (BigDecimal UOMTargetQtyL3);

	/** Get Target Qty L3	  */
	public BigDecimal getUOMTargetQtyL3();

    /** Column name UOMTargetQtyL4 */
    public static final String COLUMNNAME_UOMTargetQtyL4 = "UOMTargetQtyL4";

	/** Set Target Qty L4	  */
	public void setUOMTargetQtyL4 (BigDecimal UOMTargetQtyL4);

	/** Get Target Qty L4	  */
	public BigDecimal getUOMTargetQtyL4();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}

/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for M_InventoryLine
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
public interface I_M_InventoryLine 
{

    /** TableName=M_InventoryLine */
    public static final String Table_Name = "M_InventoryLine";

    /** AD_Table_ID=322 */
    public static final int Table_ID = 322;

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_Charge_ID */
    public static final String COLUMNNAME_C_Charge_ID = "C_Charge_ID";

	/** Set Charge.
	  * Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID);

	/** Get Charge.
	  * Additional document charges
	  */
	public int getC_Charge_ID();

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name CurrentCostPrice */
    public static final String COLUMNNAME_CurrentCostPrice = "CurrentCostPrice";

	/** Set Current Cost Price.
	  * The currently used cost price
	  */
	public void setCurrentCostPrice (BigDecimal CurrentCostPrice);

	/** Get Current Cost Price.
	  * The currently used cost price
	  */
	public BigDecimal getCurrentCostPrice();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name InventoryType */
    public static final String COLUMNNAME_InventoryType = "InventoryType";

	/** Set Inventory Type.
	  * Type of inventory difference
	  */
	public void setInventoryType (String InventoryType);

	/** Get Inventory Type.
	  * Type of inventory difference
	  */
	public String getInventoryType();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name IsUPCBasedCounter */
    public static final String COLUMNNAME_IsUPCBasedCounter = "IsUPCBasedCounter";

	/** Set UPC Based Counter.
	  * To indicate physical inventories are counted based on UPC existance.
	  */
	public void setIsUPCBasedCounter (boolean IsUPCBasedCounter);

	/** Get UPC Based Counter.
	  * To indicate physical inventories are counted based on UPC existance.
	  */
	public boolean isUPCBasedCounter();

    /** Column name Line */
    public static final String COLUMNNAME_Line = "Line";

	/** Set Line No.
	  * Unique line for this document
	  */
	public void setLine (int Line);

	/** Get Line No.
	  * Unique line for this document
	  */
	public int getLine();

    /** Column name M_AttributeSetInstance_ID */
    public static final String COLUMNNAME_M_AttributeSetInstance_ID = "M_AttributeSetInstance_ID";

	/** Set Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID);

	/** Get Attribute Set Instance.
	  * Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID();

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException;

    /** Column name M_Inventory_ID */
    public static final String COLUMNNAME_M_Inventory_ID = "M_Inventory_ID";

	/** Set Phys.Inventory.
	  * Parameters for a Physical Inventory
	  */
	public void setM_Inventory_ID (int M_Inventory_ID);

	/** Get Phys.Inventory.
	  * Parameters for a Physical Inventory
	  */
	public int getM_Inventory_ID();

	public org.compiere.model.I_M_Inventory getM_Inventory() throws RuntimeException;

    /** Column name M_InventoryLine_ID */
    public static final String COLUMNNAME_M_InventoryLine_ID = "M_InventoryLine_ID";

	/** Set Phys.Inventory Line.
	  * Unique line in an Inventory document
	  */
	public void setM_InventoryLine_ID (int M_InventoryLine_ID);

	/** Get Phys.Inventory Line.
	  * Unique line in an Inventory document
	  */
	public int getM_InventoryLine_ID();

    /** Column name M_InventoryLine_UU */
    public static final String COLUMNNAME_M_InventoryLine_UU = "M_InventoryLine_UU";

	/** Set M_InventoryLine_UU	  */
	public void setM_InventoryLine_UU (String M_InventoryLine_UU);

	/** Get M_InventoryLine_UU	  */
	public String getM_InventoryLine_UU();

    /** Column name M_Locator_ID */
    public static final String COLUMNNAME_M_Locator_ID = "M_Locator_ID";

	/** Set Locator.
	  * Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID);

	/** Get Locator.
	  * Warehouse Locator
	  */
	public int getM_Locator_ID();

	public I_M_Locator getM_Locator() throws RuntimeException;

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name NewCostPrice */
    public static final String COLUMNNAME_NewCostPrice = "NewCostPrice";

	/** Set New Cost Price.
	  * New current cost price after processing of M_CostDetail
	  */
	public void setNewCostPrice (BigDecimal NewCostPrice);

	/** Get New Cost Price.
	  * New current cost price after processing of M_CostDetail
	  */
	public BigDecimal getNewCostPrice();

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name QtyBook */
    public static final String COLUMNNAME_QtyBook = "QtyBook";

	/** Set Quantity book.
	  * Book Quantity
	  */
	public void setQtyBook (BigDecimal QtyBook);

	/** Get Quantity book.
	  * Book Quantity
	  */
	public BigDecimal getQtyBook();

    /** Column name QtyCount */
    public static final String COLUMNNAME_QtyCount = "QtyCount";

	/** Set Quantity count.
	  * Counted Quantity
	  */
	public void setQtyCount (BigDecimal QtyCount);

	/** Get Quantity count.
	  * Counted Quantity
	  */
	public BigDecimal getQtyCount();

    /** Column name QtyCsv */
    public static final String COLUMNNAME_QtyCsv = "QtyCsv";

	/** Set QtyCsv	  */
	public void setQtyCsv (BigDecimal QtyCsv);

	/** Get QtyCsv	  */
	public BigDecimal getQtyCsv();

    /** Column name QtyInternalUse */
    public static final String COLUMNNAME_QtyInternalUse = "QtyInternalUse";

	/** Set Internal Use Qty.
	  * Internal Use Quantity removed from Inventory
	  */
	public void setQtyInternalUse (BigDecimal QtyInternalUse);

	/** Get Internal Use Qty.
	  * Internal Use Quantity removed from Inventory
	  */
	public BigDecimal getQtyInternalUse();

    /** Column name QtyIntransitCustomer */
    public static final String COLUMNNAME_QtyIntransitCustomer = "QtyIntransitCustomer";

	/** Set Qty Intransit Customer.
	  * The quantity still on intransit-customer (shipment).
	  */
	public void setQtyIntransitCustomer (BigDecimal QtyIntransitCustomer);

	/** Get Qty Intransit Customer.
	  * The quantity still on intransit-customer (shipment).
	  */
	public BigDecimal getQtyIntransitCustomer();

    /** Column name QtyIntransitLoc */
    public static final String COLUMNNAME_QtyIntransitLoc = "QtyIntransitLoc";

	/** Set Qty On Loc-Intransit.
	  * The quantity still on locator intransit.
	  */
	public void setQtyIntransitLoc (BigDecimal QtyIntransitLoc);

	/** Get Qty On Loc-Intransit.
	  * The quantity still on locator intransit.
	  */
	public BigDecimal getQtyIntransitLoc();

    /** Column name ReversalLine_ID */
    public static final String COLUMNNAME_ReversalLine_ID = "ReversalLine_ID";

	/** Set Reversal Line.
	  * Use to keep the reversal line ID for reversing costing purpose
	  */
	public void setReversalLine_ID (int ReversalLine_ID);

	/** Get Reversal Line.
	  * Use to keep the reversal line ID for reversing costing purpose
	  */
	public int getReversalLine_ID();

	public org.compiere.model.I_M_InventoryLine getReversalLine() throws RuntimeException;

    /** Column name TotalQtyBook */
    public static final String COLUMNNAME_TotalQtyBook = "TotalQtyBook";

	/** Set Total Quantity Book.
	  * The quantity book + quantity still on intransit customer
	  */
	public void setTotalQtyBook (BigDecimal TotalQtyBook);

	/** Get Total Quantity Book.
	  * The quantity book + quantity still on intransit customer
	  */
	public BigDecimal getTotalQtyBook();

    /** Column name UOMConversionL1_ID */
    public static final String COLUMNNAME_UOMConversionL1_ID = "UOMConversionL1_ID";

	/** Set UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID);

	/** Get UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException;

    /** Column name UOMConversionL2_ID */
    public static final String COLUMNNAME_UOMConversionL2_ID = "UOMConversionL2_ID";

	/** Set UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID);

	/** Get UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException;

    /** Column name UOMConversionL3_ID */
    public static final String COLUMNNAME_UOMConversionL3_ID = "UOMConversionL3_ID";

	/** Set UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID);

	/** Get UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException;

    /** Column name UOMConversionL4_ID */
    public static final String COLUMNNAME_UOMConversionL4_ID = "UOMConversionL4_ID";

	/** Set UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID);

	/** Get UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException;

    /** Column name UOMQtyBookL1 */
    public static final String COLUMNNAME_UOMQtyBookL1 = "UOMQtyBookL1";

	/** Set Qty Book L1	  */
	public void setUOMQtyBookL1 (BigDecimal UOMQtyBookL1);

	/** Get Qty Book L1	  */
	public BigDecimal getUOMQtyBookL1();

    /** Column name UOMQtyBookL2 */
    public static final String COLUMNNAME_UOMQtyBookL2 = "UOMQtyBookL2";

	/** Set Qty Book L2	  */
	public void setUOMQtyBookL2 (BigDecimal UOMQtyBookL2);

	/** Get Qty Book L2	  */
	public BigDecimal getUOMQtyBookL2();

    /** Column name UOMQtyBookL3 */
    public static final String COLUMNNAME_UOMQtyBookL3 = "UOMQtyBookL3";

	/** Set Qty Book L3	  */
	public void setUOMQtyBookL3 (BigDecimal UOMQtyBookL3);

	/** Get Qty Book L3	  */
	public BigDecimal getUOMQtyBookL3();

    /** Column name UOMQtyBookL4 */
    public static final String COLUMNNAME_UOMQtyBookL4 = "UOMQtyBookL4";

	/** Set Qty Book L4	  */
	public void setUOMQtyBookL4 (BigDecimal UOMQtyBookL4);

	/** Get Qty Book L4	  */
	public BigDecimal getUOMQtyBookL4();

    /** Column name UOMQtyCountL1 */
    public static final String COLUMNNAME_UOMQtyCountL1 = "UOMQtyCountL1";

	/** Set Qty Count L1	  */
	public void setUOMQtyCountL1 (BigDecimal UOMQtyCountL1);

	/** Get Qty Count L1	  */
	public BigDecimal getUOMQtyCountL1();

    /** Column name UOMQtyCountL2 */
    public static final String COLUMNNAME_UOMQtyCountL2 = "UOMQtyCountL2";

	/** Set Qty Count L2	  */
	public void setUOMQtyCountL2 (BigDecimal UOMQtyCountL2);

	/** Get Qty Count L2	  */
	public BigDecimal getUOMQtyCountL2();

    /** Column name UOMQtyCountL3 */
    public static final String COLUMNNAME_UOMQtyCountL3 = "UOMQtyCountL3";

	/** Set Qty Count L3	  */
	public void setUOMQtyCountL3 (BigDecimal UOMQtyCountL3);

	/** Get Qty Count L3	  */
	public BigDecimal getUOMQtyCountL3();

    /** Column name UOMQtyCountL4 */
    public static final String COLUMNNAME_UOMQtyCountL4 = "UOMQtyCountL4";

	/** Set Qty Count L4	  */
	public void setUOMQtyCountL4 (BigDecimal UOMQtyCountL4);

	/** Get Qty Count L4	  */
	public BigDecimal getUOMQtyCountL4();

    /** Column name UOMQtyCsvL1 */
    public static final String COLUMNNAME_UOMQtyCsvL1 = "UOMQtyCsvL1";

	/** Set Qty Csv L1	  */
	public void setUOMQtyCsvL1 (BigDecimal UOMQtyCsvL1);

	/** Get Qty Csv L1	  */
	public BigDecimal getUOMQtyCsvL1();

    /** Column name UOMQtyCsvL2 */
    public static final String COLUMNNAME_UOMQtyCsvL2 = "UOMQtyCsvL2";

	/** Set Qty Csv L2	  */
	public void setUOMQtyCsvL2 (BigDecimal UOMQtyCsvL2);

	/** Get Qty Csv L2	  */
	public BigDecimal getUOMQtyCsvL2();

    /** Column name UOMQtyCsvL3 */
    public static final String COLUMNNAME_UOMQtyCsvL3 = "UOMQtyCsvL3";

	/** Set Qty Csv L3	  */
	public void setUOMQtyCsvL3 (BigDecimal UOMQtyCsvL3);

	/** Get Qty Csv L3	  */
	public BigDecimal getUOMQtyCsvL3();

    /** Column name UOMQtyCsvL4 */
    public static final String COLUMNNAME_UOMQtyCsvL4 = "UOMQtyCsvL4";

	/** Set Qty Csv L4	  */
	public void setUOMQtyCsvL4 (BigDecimal UOMQtyCsvL4);

	/** Get Qty Csv L4	  */
	public BigDecimal getUOMQtyCsvL4();

    /** Column name UOMQtyInternalUseL1 */
    public static final String COLUMNNAME_UOMQtyInternalUseL1 = "UOMQtyInternalUseL1";

	/** Set Qty Internal Use L1	  */
	public void setUOMQtyInternalUseL1 (BigDecimal UOMQtyInternalUseL1);

	/** Get Qty Internal Use L1	  */
	public BigDecimal getUOMQtyInternalUseL1();

    /** Column name UOMQtyInternalUseL2 */
    public static final String COLUMNNAME_UOMQtyInternalUseL2 = "UOMQtyInternalUseL2";

	/** Set Qty Internal Use L2	  */
	public void setUOMQtyInternalUseL2 (BigDecimal UOMQtyInternalUseL2);

	/** Get Qty Internal Use L2	  */
	public BigDecimal getUOMQtyInternalUseL2();

    /** Column name UOMQtyInternalUseL3 */
    public static final String COLUMNNAME_UOMQtyInternalUseL3 = "UOMQtyInternalUseL3";

	/** Set Qty Internal Use L3	  */
	public void setUOMQtyInternalUseL3 (BigDecimal UOMQtyInternalUseL3);

	/** Get Qty Internal Use L3	  */
	public BigDecimal getUOMQtyInternalUseL3();

    /** Column name UOMQtyInternalUseL4 */
    public static final String COLUMNNAME_UOMQtyInternalUseL4 = "UOMQtyInternalUseL4";

	/** Set Qty Internal Use L4	  */
	public void setUOMQtyInternalUseL4 (BigDecimal UOMQtyInternalUseL4);

	/** Get Qty Internal Use L4	  */
	public BigDecimal getUOMQtyInternalUseL4();

    /** Column name UOMQtyIntransitCustomerL1 */
    public static final String COLUMNNAME_UOMQtyIntransitCustomerL1 = "UOMQtyIntransitCustomerL1";

	/** Set Qty Intransit Customer L1.
	  * The quantity still on intransit-customer (shipment) on product's uom level 1.
	  */
	public void setUOMQtyIntransitCustomerL1 (BigDecimal UOMQtyIntransitCustomerL1);

	/** Get Qty Intransit Customer L1.
	  * The quantity still on intransit-customer (shipment) on product's uom level 1.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL1();

    /** Column name UOMQtyIntransitCustomerL2 */
    public static final String COLUMNNAME_UOMQtyIntransitCustomerL2 = "UOMQtyIntransitCustomerL2";

	/** Set Qty Intransit Customer L2.
	  * The quantity still on intransit-customer (shipment) on product's uom level 2.
	  */
	public void setUOMQtyIntransitCustomerL2 (BigDecimal UOMQtyIntransitCustomerL2);

	/** Get Qty Intransit Customer L2.
	  * The quantity still on intransit-customer (shipment) on product's uom level 2.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL2();

    /** Column name UOMQtyIntransitCustomerL3 */
    public static final String COLUMNNAME_UOMQtyIntransitCustomerL3 = "UOMQtyIntransitCustomerL3";

	/** Set Qty Intransit Customer L3.
	  * The quantity still on intransit-customer (shipment) on product's uom level 3.
	  */
	public void setUOMQtyIntransitCustomerL3 (BigDecimal UOMQtyIntransitCustomerL3);

	/** Get Qty Intransit Customer L3.
	  * The quantity still on intransit-customer (shipment) on product's uom level 3.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL3();

    /** Column name UOMQtyIntransitCustomerL4 */
    public static final String COLUMNNAME_UOMQtyIntransitCustomerL4 = "UOMQtyIntransitCustomerL4";

	/** Set Qty Intransit Customer L4.
	  * The quantity still on intransit-customer (shipment) on product's uom level 4.
	  */
	public void setUOMQtyIntransitCustomerL4 (BigDecimal UOMQtyIntransitCustomerL4);

	/** Get Qty Intransit Customer L4.
	  * The quantity still on intransit-customer (shipment) on product's uom level 4.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL4();

    /** Column name UOMQtyIntransitLocL1 */
    public static final String COLUMNNAME_UOMQtyIntransitLocL1 = "UOMQtyIntransitLocL1";

	/** Set Qty On Loc-Intransit L1.
	  * The quantity still on locator intransit on product's uom level 1
	  */
	public void setUOMQtyIntransitLocL1 (BigDecimal UOMQtyIntransitLocL1);

	/** Get Qty On Loc-Intransit L1.
	  * The quantity still on locator intransit on product's uom level 1
	  */
	public BigDecimal getUOMQtyIntransitLocL1();

    /** Column name UOMQtyIntransitLocL2 */
    public static final String COLUMNNAME_UOMQtyIntransitLocL2 = "UOMQtyIntransitLocL2";

	/** Set Qty On Loc-Intransit L2.
	  * The quantity still on locator intransit on product's uom level 2
	  */
	public void setUOMQtyIntransitLocL2 (BigDecimal UOMQtyIntransitLocL2);

	/** Get Qty On Loc-Intransit L2.
	  * The quantity still on locator intransit on product's uom level 2
	  */
	public BigDecimal getUOMQtyIntransitLocL2();

    /** Column name UOMQtyIntransitLocL3 */
    public static final String COLUMNNAME_UOMQtyIntransitLocL3 = "UOMQtyIntransitLocL3";

	/** Set Qty On Loc-Intransit L3.
	  * The quantity still on locator intransit on product's uom level 3
	  */
	public void setUOMQtyIntransitLocL3 (BigDecimal UOMQtyIntransitLocL3);

	/** Get Qty On Loc-Intransit L3.
	  * The quantity still on locator intransit on product's uom level 3
	  */
	public BigDecimal getUOMQtyIntransitLocL3();

    /** Column name UOMQtyIntransitLocL4 */
    public static final String COLUMNNAME_UOMQtyIntransitLocL4 = "UOMQtyIntransitLocL4";

	/** Set Qty On Loc-Intransit L4.
	  * The quantity still on locator intransit on product's uom level 4
	  */
	public void setUOMQtyIntransitLocL4 (BigDecimal UOMQtyIntransitLocL4);

	/** Get Qty On Loc-Intransit L4.
	  * The quantity still on locator intransit on product's uom level 4
	  */
	public BigDecimal getUOMQtyIntransitLocL4();

    /** Column name UOMTotalQtyBookL1 */
    public static final String COLUMNNAME_UOMTotalQtyBookL1 = "UOMTotalQtyBookL1";

	/** Set Total Quantity Book L1.
	  * The quantity book + quantity still on intransit customer in uom level 1.
	  */
	public void setUOMTotalQtyBookL1 (BigDecimal UOMTotalQtyBookL1);

	/** Get Total Quantity Book L1.
	  * The quantity book + quantity still on intransit customer in uom level 1.
	  */
	public BigDecimal getUOMTotalQtyBookL1();

    /** Column name UOMTotalQtyBookL2 */
    public static final String COLUMNNAME_UOMTotalQtyBookL2 = "UOMTotalQtyBookL2";

	/** Set Total Quantity Book L2.
	  * The quantity book + quantity still on intransit customer in uom level 2.
	  */
	public void setUOMTotalQtyBookL2 (BigDecimal UOMTotalQtyBookL2);

	/** Get Total Quantity Book L2.
	  * The quantity book + quantity still on intransit customer in uom level 2.
	  */
	public BigDecimal getUOMTotalQtyBookL2();

    /** Column name UOMTotalQtyBookL3 */
    public static final String COLUMNNAME_UOMTotalQtyBookL3 = "UOMTotalQtyBookL3";

	/** Set Total Quantity Book L3.
	  * The quantity book + quantity still on intransit customer in uom level 3.
	  */
	public void setUOMTotalQtyBookL3 (BigDecimal UOMTotalQtyBookL3);

	/** Get Total Quantity Book L3.
	  * The quantity book + quantity still on intransit customer in uom level 3.
	  */
	public BigDecimal getUOMTotalQtyBookL3();

    /** Column name UOMTotalQtyBookL4 */
    public static final String COLUMNNAME_UOMTotalQtyBookL4 = "UOMTotalQtyBookL4";

	/** Set Total Quantity Book L4.
	  * The quantity book + quantity still on intransit customer in uom level 4.
	  */
	public void setUOMTotalQtyBookL4 (BigDecimal UOMTotalQtyBookL4);

	/** Get Total Quantity Book L4.
	  * The quantity book + quantity still on intransit customer in uom level 4.
	  */
	public BigDecimal getUOMTotalQtyBookL4();

    /** Column name UPC */
    public static final String COLUMNNAME_UPC = "UPC";

	/** Set UPC/EAN.
	  * Bar Code (Universal Product Code or its superset European Article Number)
	  */
	public void setUPC (String UPC);

	/** Get UPC/EAN.
	  * Bar Code (Universal Product Code or its superset European Article Number)
	  */
	public String getUPC();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();

    /** Column name Value */
    public static final String COLUMNNAME_Value = "Value";

	/** Set Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value);

	/** Get Search Key.
	  * Search key for the record in the format required - must be unique
	  */
	public String getValue();
}

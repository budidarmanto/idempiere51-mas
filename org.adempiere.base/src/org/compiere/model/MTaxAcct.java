/**
 * 
 */
package org.compiere.model;

import java.sql.ResultSet;
import java.util.Properties;

/**
 * @author Burhani Adam
 *
 */
public class MTaxAcct extends X_C_Tax_Acct {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5133112193896634413L;

	/**
	 * @param ctx
	 * @param C_Tax_Acct_ID
	 * @param trxName
	 */
	public MTaxAcct(Properties ctx, int ignored, String trxName) {
		super(ctx, 0, trxName);
		if (ignored != 0)
			throw new IllegalArgumentException("Multi-Key");
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param ctx
	 * @param rs
	 * @param trxName
	 */
	public MTaxAcct(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 	New Constructor
	 *	@param ctx context
	 *	@param M_PriceList_Version_ID Price List Version
	 *	@param M_Product_ID product
	 *	@param trxName transaction
	 */
	public MTaxAcct(Properties ctx, int C_Tax_ID, int C_AcctSchema_ID, String trxName)
	{
		this (ctx, 0, trxName);
		setC_Tax_ID(C_Tax_ID);
		setC_AcctSchema_ID(C_AcctSchema_ID);
	}	//	MProductPrice
}

/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for M_InventoryLine
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_M_InventoryLine extends PO implements I_M_InventoryLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171031L;

    /** Standard Constructor */
    public X_M_InventoryLine (Properties ctx, int M_InventoryLine_ID, String trxName)
    {
      super (ctx, M_InventoryLine_ID, trxName);
      /** if (M_InventoryLine_ID == 0)
        {
			setInventoryType (null);
// D
			setM_AttributeSetInstance_ID (0);
			setM_Inventory_ID (0);
			setM_InventoryLine_ID (0);
			setM_Inventory_ID (0);
			setM_Product_ID (0);
			setProcessed (false);
			setQtyBook (Env.ZERO);
			setQtyCount (Env.ZERO);
			setQtyCsv (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_M_InventoryLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_InventoryLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		throw new IllegalArgumentException ("C_UOM_ID is virtual column");	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Current Cost Price.
		@param CurrentCostPrice 
		The currently used cost price
	  */
	public void setCurrentCostPrice (BigDecimal CurrentCostPrice)
	{
		set_ValueNoCheck (COLUMNNAME_CurrentCostPrice, CurrentCostPrice);
	}

	/** Get Current Cost Price.
		@return The currently used cost price
	  */
	public BigDecimal getCurrentCostPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_CurrentCostPrice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** InventoryType AD_Reference_ID=292 */
	public static final int INVENTORYTYPE_AD_Reference_ID=292;
	/** Inventory Difference = D */
	public static final String INVENTORYTYPE_InventoryDifference = "D";
	/** Charge Account = C */
	public static final String INVENTORYTYPE_ChargeAccount = "C";
	/** Set Inventory Type.
		@param InventoryType 
		Type of inventory difference
	  */
	public void setInventoryType (String InventoryType)
	{

		set_Value (COLUMNNAME_InventoryType, InventoryType);
	}

	/** Get Inventory Type.
		@return Type of inventory difference
	  */
	public String getInventoryType () 
	{
		return (String)get_Value(COLUMNNAME_InventoryType);
	}

	/** Set UPC Based Counter.
		@param IsUPCBasedCounter 
		To indicate physical inventories are counted based on UPC existance.
	  */
	public void setIsUPCBasedCounter (boolean IsUPCBasedCounter)
	{
		set_Value (COLUMNNAME_IsUPCBasedCounter, Boolean.valueOf(IsUPCBasedCounter));
	}

	/** Get UPC Based Counter.
		@return To indicate physical inventories are counted based on UPC existance.
	  */
	public boolean isUPCBasedCounter () 
	{
		Object oo = get_Value(COLUMNNAME_IsUPCBasedCounter);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getLine()));
    }

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Inventory getM_Inventory() throws RuntimeException
    {
		return (org.compiere.model.I_M_Inventory)MTable.get(getCtx(), org.compiere.model.I_M_Inventory.Table_Name)
			.getPO(getM_Inventory_ID(), get_TrxName());	}

	/** Set Phys.Inventory.
		@param M_Inventory_ID 
		Parameters for a Physical Inventory
	  */
	public void setM_Inventory_ID (int M_Inventory_ID)
	{
		if (M_Inventory_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Inventory_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Inventory_ID, Integer.valueOf(M_Inventory_ID));
	}

	/** Get Phys.Inventory.
		@return Parameters for a Physical Inventory
	  */
	public int getM_Inventory_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Inventory_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Phys.Inventory Line.
		@param M_InventoryLine_ID 
		Unique line in an Inventory document
	  */
	public void setM_InventoryLine_ID (int M_InventoryLine_ID)
	{
		if (M_InventoryLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_InventoryLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_InventoryLine_ID, Integer.valueOf(M_InventoryLine_ID));
	}

	/** Get Phys.Inventory Line.
		@return Unique line in an Inventory document
	  */
	public int getM_InventoryLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InventoryLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set M_InventoryLine_UU.
		@param M_InventoryLine_UU M_InventoryLine_UU	  */
	public void setM_InventoryLine_UU (String M_InventoryLine_UU)
	{
		set_Value (COLUMNNAME_M_InventoryLine_UU, M_InventoryLine_UU);
	}

	/** Get M_InventoryLine_UU.
		@return M_InventoryLine_UU	  */
	public String getM_InventoryLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_M_InventoryLine_UU);
	}

	public I_M_Locator getM_Locator() throws RuntimeException
    {
		return (I_M_Locator)MTable.get(getCtx(), I_M_Locator.Table_Name)
			.getPO(getM_Locator_ID(), get_TrxName());	}

	/** Set Locator.
		@param M_Locator_ID 
		Warehouse Locator
	  */
	public void setM_Locator_ID (int M_Locator_ID)
	{
		if (M_Locator_ID < 1) 
			set_Value (COLUMNNAME_M_Locator_ID, null);
		else 
			set_Value (COLUMNNAME_M_Locator_ID, Integer.valueOf(M_Locator_ID));
	}

	/** Get Locator.
		@return Warehouse Locator
	  */
	public int getM_Locator_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Locator_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set New Cost Price.
		@param NewCostPrice 
		New current cost price after processing of M_CostDetail
	  */
	public void setNewCostPrice (BigDecimal NewCostPrice)
	{
		set_Value (COLUMNNAME_NewCostPrice, NewCostPrice);
	}

	/** Get New Cost Price.
		@return New current cost price after processing of M_CostDetail
	  */
	public BigDecimal getNewCostPrice () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_NewCostPrice);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Quantity book.
		@param QtyBook 
		Book Quantity
	  */
	public void setQtyBook (BigDecimal QtyBook)
	{
		set_Value (COLUMNNAME_QtyBook, QtyBook);
	}

	/** Get Quantity book.
		@return Book Quantity
	  */
	public BigDecimal getQtyBook () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyBook);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity count.
		@param QtyCount 
		Counted Quantity
	  */
	public void setQtyCount (BigDecimal QtyCount)
	{
		set_Value (COLUMNNAME_QtyCount, QtyCount);
	}

	/** Get Quantity count.
		@return Counted Quantity
	  */
	public BigDecimal getQtyCount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyCount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set QtyCsv.
		@param QtyCsv QtyCsv	  */
	public void setQtyCsv (BigDecimal QtyCsv)
	{
		set_Value (COLUMNNAME_QtyCsv, QtyCsv);
	}

	/** Get QtyCsv.
		@return QtyCsv	  */
	public BigDecimal getQtyCsv () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyCsv);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Internal Use Qty.
		@param QtyInternalUse 
		Internal Use Quantity removed from Inventory
	  */
	public void setQtyInternalUse (BigDecimal QtyInternalUse)
	{
		set_Value (COLUMNNAME_QtyInternalUse, QtyInternalUse);
	}

	/** Get Internal Use Qty.
		@return Internal Use Quantity removed from Inventory
	  */
	public BigDecimal getQtyInternalUse () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyInternalUse);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Intransit Customer.
		@param QtyIntransitCustomer 
		The quantity still on intransit-customer (shipment).
	  */
	public void setQtyIntransitCustomer (BigDecimal QtyIntransitCustomer)
	{
		set_ValueNoCheck (COLUMNNAME_QtyIntransitCustomer, QtyIntransitCustomer);
	}

	/** Get Qty Intransit Customer.
		@return The quantity still on intransit-customer (shipment).
	  */
	public BigDecimal getQtyIntransitCustomer () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyIntransitCustomer);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty On Loc-Intransit.
		@param QtyIntransitLoc 
		The quantity still on locator intransit.
	  */
	public void setQtyIntransitLoc (BigDecimal QtyIntransitLoc)
	{
		set_ValueNoCheck (COLUMNNAME_QtyIntransitLoc, QtyIntransitLoc);
	}

	/** Get Qty On Loc-Intransit.
		@return The quantity still on locator intransit.
	  */
	public BigDecimal getQtyIntransitLoc () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyIntransitLoc);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_InventoryLine getReversalLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_InventoryLine)MTable.get(getCtx(), org.compiere.model.I_M_InventoryLine.Table_Name)
			.getPO(getReversalLine_ID(), get_TrxName());	}

	/** Set Reversal Line.
		@param ReversalLine_ID 
		Use to keep the reversal line ID for reversing costing purpose
	  */
	public void setReversalLine_ID (int ReversalLine_ID)
	{
		if (ReversalLine_ID < 1) 
			set_Value (COLUMNNAME_ReversalLine_ID, null);
		else 
			set_Value (COLUMNNAME_ReversalLine_ID, Integer.valueOf(ReversalLine_ID));
	}

	/** Get Reversal Line.
		@return Use to keep the reversal line ID for reversing costing purpose
	  */
	public int getReversalLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ReversalLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Total Quantity Book.
		@param TotalQtyBook 
		The quantity book + quantity still on intransit customer
	  */
	public void setTotalQtyBook (BigDecimal TotalQtyBook)
	{
		set_ValueNoCheck (COLUMNNAME_TotalQtyBook, TotalQtyBook);
	}

	/** Get Total Quantity Book.
		@return The quantity book + quantity still on intransit customer
	  */
	public BigDecimal getTotalQtyBook () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TotalQtyBook);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL1_ID(), get_TrxName());	}

	/** Set UOM Conversion L1.
		@param UOMConversionL1_ID 
		The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL1_ID is virtual column");	}

	/** Get UOM Conversion L1.
		@return The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL1_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL2_ID(), get_TrxName());	}

	/** Set UOM Conversion L2.
		@param UOMConversionL2_ID 
		The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL2_ID is virtual column");	}

	/** Get UOM Conversion L2.
		@return The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL3_ID(), get_TrxName());	}

	/** Set UOM Conversion L3.
		@param UOMConversionL3_ID 
		The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL3_ID is virtual column");	}

	/** Get UOM Conversion L3.
		@return The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL3_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL4_ID(), get_TrxName());	}

	/** Set UOM Conversion L4.
		@param UOMConversionL4_ID 
		The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL4_ID is virtual column");	}

	/** Get UOM Conversion L4.
		@return The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL4_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Qty Book L1.
		@param UOMQtyBookL1 Qty Book L1	  */
	public void setUOMQtyBookL1 (BigDecimal UOMQtyBookL1)
	{
		set_Value (COLUMNNAME_UOMQtyBookL1, UOMQtyBookL1);
	}

	/** Get Qty Book L1.
		@return Qty Book L1	  */
	public BigDecimal getUOMQtyBookL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBookL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Book L2.
		@param UOMQtyBookL2 Qty Book L2	  */
	public void setUOMQtyBookL2 (BigDecimal UOMQtyBookL2)
	{
		set_Value (COLUMNNAME_UOMQtyBookL2, UOMQtyBookL2);
	}

	/** Get Qty Book L2.
		@return Qty Book L2	  */
	public BigDecimal getUOMQtyBookL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBookL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Book L3.
		@param UOMQtyBookL3 Qty Book L3	  */
	public void setUOMQtyBookL3 (BigDecimal UOMQtyBookL3)
	{
		set_Value (COLUMNNAME_UOMQtyBookL3, UOMQtyBookL3);
	}

	/** Get Qty Book L3.
		@return Qty Book L3	  */
	public BigDecimal getUOMQtyBookL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBookL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Book L4.
		@param UOMQtyBookL4 Qty Book L4	  */
	public void setUOMQtyBookL4 (BigDecimal UOMQtyBookL4)
	{
		set_Value (COLUMNNAME_UOMQtyBookL4, UOMQtyBookL4);
	}

	/** Get Qty Book L4.
		@return Qty Book L4	  */
	public BigDecimal getUOMQtyBookL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBookL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Count L1.
		@param UOMQtyCountL1 Qty Count L1	  */
	public void setUOMQtyCountL1 (BigDecimal UOMQtyCountL1)
	{
		set_Value (COLUMNNAME_UOMQtyCountL1, UOMQtyCountL1);
	}

	/** Get Qty Count L1.
		@return Qty Count L1	  */
	public BigDecimal getUOMQtyCountL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCountL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Count L2.
		@param UOMQtyCountL2 Qty Count L2	  */
	public void setUOMQtyCountL2 (BigDecimal UOMQtyCountL2)
	{
		set_Value (COLUMNNAME_UOMQtyCountL2, UOMQtyCountL2);
	}

	/** Get Qty Count L2.
		@return Qty Count L2	  */
	public BigDecimal getUOMQtyCountL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCountL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Count L3.
		@param UOMQtyCountL3 Qty Count L3	  */
	public void setUOMQtyCountL3 (BigDecimal UOMQtyCountL3)
	{
		set_Value (COLUMNNAME_UOMQtyCountL3, UOMQtyCountL3);
	}

	/** Get Qty Count L3.
		@return Qty Count L3	  */
	public BigDecimal getUOMQtyCountL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCountL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Count L4.
		@param UOMQtyCountL4 Qty Count L4	  */
	public void setUOMQtyCountL4 (BigDecimal UOMQtyCountL4)
	{
		set_Value (COLUMNNAME_UOMQtyCountL4, UOMQtyCountL4);
	}

	/** Get Qty Count L4.
		@return Qty Count L4	  */
	public BigDecimal getUOMQtyCountL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCountL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Csv L1.
		@param UOMQtyCsvL1 Qty Csv L1	  */
	public void setUOMQtyCsvL1 (BigDecimal UOMQtyCsvL1)
	{
		set_Value (COLUMNNAME_UOMQtyCsvL1, UOMQtyCsvL1);
	}

	/** Get Qty Csv L1.
		@return Qty Csv L1	  */
	public BigDecimal getUOMQtyCsvL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCsvL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Csv L2.
		@param UOMQtyCsvL2 Qty Csv L2	  */
	public void setUOMQtyCsvL2 (BigDecimal UOMQtyCsvL2)
	{
		set_Value (COLUMNNAME_UOMQtyCsvL2, UOMQtyCsvL2);
	}

	/** Get Qty Csv L2.
		@return Qty Csv L2	  */
	public BigDecimal getUOMQtyCsvL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCsvL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Csv L3.
		@param UOMQtyCsvL3 Qty Csv L3	  */
	public void setUOMQtyCsvL3 (BigDecimal UOMQtyCsvL3)
	{
		set_Value (COLUMNNAME_UOMQtyCsvL3, UOMQtyCsvL3);
	}

	/** Get Qty Csv L3.
		@return Qty Csv L3	  */
	public BigDecimal getUOMQtyCsvL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCsvL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Csv L4.
		@param UOMQtyCsvL4 Qty Csv L4	  */
	public void setUOMQtyCsvL4 (BigDecimal UOMQtyCsvL4)
	{
		set_Value (COLUMNNAME_UOMQtyCsvL4, UOMQtyCsvL4);
	}

	/** Get Qty Csv L4.
		@return Qty Csv L4	  */
	public BigDecimal getUOMQtyCsvL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyCsvL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Internal Use L1.
		@param UOMQtyInternalUseL1 Qty Internal Use L1	  */
	public void setUOMQtyInternalUseL1 (BigDecimal UOMQtyInternalUseL1)
	{
		set_Value (COLUMNNAME_UOMQtyInternalUseL1, UOMQtyInternalUseL1);
	}

	/** Get Qty Internal Use L1.
		@return Qty Internal Use L1	  */
	public BigDecimal getUOMQtyInternalUseL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInternalUseL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Internal Use L2.
		@param UOMQtyInternalUseL2 Qty Internal Use L2	  */
	public void setUOMQtyInternalUseL2 (BigDecimal UOMQtyInternalUseL2)
	{
		set_Value (COLUMNNAME_UOMQtyInternalUseL2, UOMQtyInternalUseL2);
	}

	/** Get Qty Internal Use L2.
		@return Qty Internal Use L2	  */
	public BigDecimal getUOMQtyInternalUseL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInternalUseL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Internal Use L3.
		@param UOMQtyInternalUseL3 Qty Internal Use L3	  */
	public void setUOMQtyInternalUseL3 (BigDecimal UOMQtyInternalUseL3)
	{
		set_Value (COLUMNNAME_UOMQtyInternalUseL3, UOMQtyInternalUseL3);
	}

	/** Get Qty Internal Use L3.
		@return Qty Internal Use L3	  */
	public BigDecimal getUOMQtyInternalUseL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInternalUseL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Internal Use L4.
		@param UOMQtyInternalUseL4 Qty Internal Use L4	  */
	public void setUOMQtyInternalUseL4 (BigDecimal UOMQtyInternalUseL4)
	{
		set_Value (COLUMNNAME_UOMQtyInternalUseL4, UOMQtyInternalUseL4);
	}

	/** Get Qty Internal Use L4.
		@return Qty Internal Use L4	  */
	public BigDecimal getUOMQtyInternalUseL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInternalUseL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Intransit Customer L1.
		@param UOMQtyIntransitCustomerL1 
		The quantity still on intransit-customer (shipment) on product's uom level 1.
	  */
	public void setUOMQtyIntransitCustomerL1 (BigDecimal UOMQtyIntransitCustomerL1)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitCustomerL1, UOMQtyIntransitCustomerL1);
	}

	/** Get Qty Intransit Customer L1.
		@return The quantity still on intransit-customer (shipment) on product's uom level 1.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitCustomerL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Intransit Customer L2.
		@param UOMQtyIntransitCustomerL2 
		The quantity still on intransit-customer (shipment) on product's uom level 2.
	  */
	public void setUOMQtyIntransitCustomerL2 (BigDecimal UOMQtyIntransitCustomerL2)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitCustomerL2, UOMQtyIntransitCustomerL2);
	}

	/** Get Qty Intransit Customer L2.
		@return The quantity still on intransit-customer (shipment) on product's uom level 2.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitCustomerL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Intransit Customer L3.
		@param UOMQtyIntransitCustomerL3 
		The quantity still on intransit-customer (shipment) on product's uom level 3.
	  */
	public void setUOMQtyIntransitCustomerL3 (BigDecimal UOMQtyIntransitCustomerL3)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitCustomerL3, UOMQtyIntransitCustomerL3);
	}

	/** Get Qty Intransit Customer L3.
		@return The quantity still on intransit-customer (shipment) on product's uom level 3.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitCustomerL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Intransit Customer L4.
		@param UOMQtyIntransitCustomerL4 
		The quantity still on intransit-customer (shipment) on product's uom level 4.
	  */
	public void setUOMQtyIntransitCustomerL4 (BigDecimal UOMQtyIntransitCustomerL4)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitCustomerL4, UOMQtyIntransitCustomerL4);
	}

	/** Get Qty Intransit Customer L4.
		@return The quantity still on intransit-customer (shipment) on product's uom level 4.
	  */
	public BigDecimal getUOMQtyIntransitCustomerL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitCustomerL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty On Loc-Intransit L1.
		@param UOMQtyIntransitLocL1 
		The quantity still on locator intransit on product's uom level 1
	  */
	public void setUOMQtyIntransitLocL1 (BigDecimal UOMQtyIntransitLocL1)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitLocL1, UOMQtyIntransitLocL1);
	}

	/** Get Qty On Loc-Intransit L1.
		@return The quantity still on locator intransit on product's uom level 1
	  */
	public BigDecimal getUOMQtyIntransitLocL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitLocL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty On Loc-Intransit L2.
		@param UOMQtyIntransitLocL2 
		The quantity still on locator intransit on product's uom level 2
	  */
	public void setUOMQtyIntransitLocL2 (BigDecimal UOMQtyIntransitLocL2)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitLocL2, UOMQtyIntransitLocL2);
	}

	/** Get Qty On Loc-Intransit L2.
		@return The quantity still on locator intransit on product's uom level 2
	  */
	public BigDecimal getUOMQtyIntransitLocL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitLocL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty On Loc-Intransit L3.
		@param UOMQtyIntransitLocL3 
		The quantity still on locator intransit on product's uom level 3
	  */
	public void setUOMQtyIntransitLocL3 (BigDecimal UOMQtyIntransitLocL3)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitLocL3, UOMQtyIntransitLocL3);
	}

	/** Get Qty On Loc-Intransit L3.
		@return The quantity still on locator intransit on product's uom level 3
	  */
	public BigDecimal getUOMQtyIntransitLocL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitLocL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty On Loc-Intransit L4.
		@param UOMQtyIntransitLocL4 
		The quantity still on locator intransit on product's uom level 4
	  */
	public void setUOMQtyIntransitLocL4 (BigDecimal UOMQtyIntransitLocL4)
	{
		set_ValueNoCheck (COLUMNNAME_UOMQtyIntransitLocL4, UOMQtyIntransitLocL4);
	}

	/** Get Qty On Loc-Intransit L4.
		@return The quantity still on locator intransit on product's uom level 4
	  */
	public BigDecimal getUOMQtyIntransitLocL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyIntransitLocL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Quantity Book L1.
		@param UOMTotalQtyBookL1 
		The quantity book + quantity still on intransit customer in uom level 1.
	  */
	public void setUOMTotalQtyBookL1 (BigDecimal UOMTotalQtyBookL1)
	{
		set_ValueNoCheck (COLUMNNAME_UOMTotalQtyBookL1, UOMTotalQtyBookL1);
	}

	/** Get Total Quantity Book L1.
		@return The quantity book + quantity still on intransit customer in uom level 1.
	  */
	public BigDecimal getUOMTotalQtyBookL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTotalQtyBookL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Quantity Book L2.
		@param UOMTotalQtyBookL2 
		The quantity book + quantity still on intransit customer in uom level 2.
	  */
	public void setUOMTotalQtyBookL2 (BigDecimal UOMTotalQtyBookL2)
	{
		set_ValueNoCheck (COLUMNNAME_UOMTotalQtyBookL2, UOMTotalQtyBookL2);
	}

	/** Get Total Quantity Book L2.
		@return The quantity book + quantity still on intransit customer in uom level 2.
	  */
	public BigDecimal getUOMTotalQtyBookL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTotalQtyBookL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Quantity Book L3.
		@param UOMTotalQtyBookL3 
		The quantity book + quantity still on intransit customer in uom level 3.
	  */
	public void setUOMTotalQtyBookL3 (BigDecimal UOMTotalQtyBookL3)
	{
		set_ValueNoCheck (COLUMNNAME_UOMTotalQtyBookL3, UOMTotalQtyBookL3);
	}

	/** Get Total Quantity Book L3.
		@return The quantity book + quantity still on intransit customer in uom level 3.
	  */
	public BigDecimal getUOMTotalQtyBookL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTotalQtyBookL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Total Quantity Book L4.
		@param UOMTotalQtyBookL4 
		The quantity book + quantity still on intransit customer in uom level 4.
	  */
	public void setUOMTotalQtyBookL4 (BigDecimal UOMTotalQtyBookL4)
	{
		set_ValueNoCheck (COLUMNNAME_UOMTotalQtyBookL4, UOMTotalQtyBookL4);
	}

	/** Get Total Quantity Book L4.
		@return The quantity book + quantity still on intransit customer in uom level 4.
	  */
	public BigDecimal getUOMTotalQtyBookL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTotalQtyBookL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set UPC/EAN.
		@param UPC 
		Bar Code (Universal Product Code or its superset European Article Number)
	  */
	public void setUPC (String UPC)
	{
		throw new IllegalArgumentException ("UPC is virtual column");	}

	/** Get UPC/EAN.
		@return Bar Code (Universal Product Code or its superset European Article Number)
	  */
	public String getUPC () 
	{
		return (String)get_Value(COLUMNNAME_UPC);
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		throw new IllegalArgumentException ("Value is virtual column");	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}
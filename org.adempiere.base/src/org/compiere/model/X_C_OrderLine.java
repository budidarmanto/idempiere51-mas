/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for C_OrderLine
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_C_OrderLine extends PO implements I_C_OrderLine, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171031L;

    /** Standard Constructor */
    public X_C_OrderLine (Properties ctx, int C_OrderLine_ID, String trxName)
    {
      super (ctx, C_OrderLine_ID, trxName);
      /** if (C_OrderLine_ID == 0)
        {
			setC_BPartner_Location_ID (0);
// @C_BPartner_Location_ID@
			setC_Currency_ID (0);
// @C_Currency_ID@
			setC_Order_ID (0);
			setC_OrderLine_ID (0);
			setC_Tax_ID (0);
			setC_UOM_ID (0);
// @#C_UOM_ID@
			setDateOrdered (new Timestamp( System.currentTimeMillis() ));
// @DateOrdered@
			setFreightAmt (Env.ZERO);
			setIsDescription (false);
// N
			setisProductBonuses (false);
// N
			setLine (0);
// @SQL=SELECT COALESCE(MAX(Line),0)+10 AS DefaultValue FROM C_OrderLine WHERE C_Order_ID=@C_Order_ID@
			setLineNetAmt (Env.ZERO);
			setM_AttributeSetInstance_ID (0);
			setM_Warehouse_ID (0);
// @M_Warehouse_ID@
			setPriceActual (Env.ZERO);
			setPriceEntered (Env.ZERO);
			setPriceLimit (Env.ZERO);
			setPriceList (Env.ZERO);
			setProcessed (false);
			setQtyBonuses (Env.ZERO);
// 0
			setQtyDelivered (Env.ZERO);
			setQtyEntered (Env.ZERO);
// 1
			setQtyInvoiced (Env.ZERO);
			setQtyLostSales (Env.ZERO);
			setQtyOrdered (Env.ZERO);
// 1
			setQtyReserved (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_C_OrderLine (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_C_OrderLine[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Trx Organization.
		@param AD_OrgTrx_ID 
		Performing or initiating organization
	  */
	public void setAD_OrgTrx_ID (int AD_OrgTrx_ID)
	{
		if (AD_OrgTrx_ID < 1) 
			set_Value (COLUMNNAME_AD_OrgTrx_ID, null);
		else 
			set_Value (COLUMNNAME_AD_OrgTrx_ID, Integer.valueOf(AD_OrgTrx_ID));
	}

	/** Get Trx Organization.
		@return Performing or initiating organization
	  */
	public int getAD_OrgTrx_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_OrgTrx_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Before Tax Amount.
		@param BeforeTaxAmt Before Tax Amount	  */
	public void setBeforeTaxAmt (BigDecimal BeforeTaxAmt)
	{
		throw new IllegalArgumentException ("BeforeTaxAmt is virtual column");	}

	/** Get Before Tax Amount.
		@return Before Tax Amount	  */
	public BigDecimal getBeforeTaxAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_BeforeTaxAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_Activity getC_Activity() throws RuntimeException
    {
		return (org.compiere.model.I_C_Activity)MTable.get(getCtx(), org.compiere.model.I_C_Activity.Table_Name)
			.getPO(getC_Activity_ID(), get_TrxName());	}

	/** Set Activity.
		@param C_Activity_ID 
		Business Activity
	  */
	public void setC_Activity_ID (int C_Activity_ID)
	{
		if (C_Activity_ID < 1) 
			set_Value (COLUMNNAME_C_Activity_ID, null);
		else 
			set_Value (COLUMNNAME_C_Activity_ID, Integer.valueOf(C_Activity_ID));
	}

	/** Get Activity.
		@return Business Activity
	  */
	public int getC_Activity_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Activity_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner_Location getC_BPartner_Location() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner_Location)MTable.get(getCtx(), org.compiere.model.I_C_BPartner_Location.Table_Name)
			.getPO(getC_BPartner_Location_ID(), get_TrxName());	}

	/** Set Partner Location.
		@param C_BPartner_Location_ID 
		Identifies the (ship to) address for this Business Partner
	  */
	public void setC_BPartner_Location_ID (int C_BPartner_Location_ID)
	{
		if (C_BPartner_Location_ID < 1) 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, null);
		else 
			set_Value (COLUMNNAME_C_BPartner_Location_ID, Integer.valueOf(C_BPartner_Location_ID));
	}

	/** Get Partner Location.
		@return Identifies the (ship to) address for this Business Partner
	  */
	public int getC_BPartner_Location_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_Location_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Campaign getC_Campaign() throws RuntimeException
    {
		return (org.compiere.model.I_C_Campaign)MTable.get(getCtx(), org.compiere.model.I_C_Campaign.Table_Name)
			.getPO(getC_Campaign_ID(), get_TrxName());	}

	/** Set Campaign.
		@param C_Campaign_ID 
		Marketing Campaign
	  */
	public void setC_Campaign_ID (int C_Campaign_ID)
	{
		if (C_Campaign_ID < 1) 
			set_Value (COLUMNNAME_C_Campaign_ID, null);
		else 
			set_Value (COLUMNNAME_C_Campaign_ID, Integer.valueOf(C_Campaign_ID));
	}

	/** Get Campaign.
		@return Marketing Campaign
	  */
	public int getC_Campaign_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Campaign_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Charge getC_Charge() throws RuntimeException
    {
		return (org.compiere.model.I_C_Charge)MTable.get(getCtx(), org.compiere.model.I_C_Charge.Table_Name)
			.getPO(getC_Charge_ID(), get_TrxName());	}

	/** Set Charge.
		@param C_Charge_ID 
		Additional document charges
	  */
	public void setC_Charge_ID (int C_Charge_ID)
	{
		if (C_Charge_ID < 1) 
			set_Value (COLUMNNAME_C_Charge_ID, null);
		else 
			set_Value (COLUMNNAME_C_Charge_ID, Integer.valueOf(C_Charge_ID));
	}

	/** Get Charge.
		@return Additional document charges
	  */
	public int getC_Charge_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Charge_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Currency getC_Currency() throws RuntimeException
    {
		return (org.compiere.model.I_C_Currency)MTable.get(getCtx(), org.compiere.model.I_C_Currency.Table_Name)
			.getPO(getC_Currency_ID(), get_TrxName());	}

	/** Set Currency.
		@param C_Currency_ID 
		The Currency for this record
	  */
	public void setC_Currency_ID (int C_Currency_ID)
	{
		if (C_Currency_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Currency_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Currency_ID, Integer.valueOf(C_Currency_ID));
	}

	/** Get Currency.
		@return The Currency for this record
	  */
	public int getC_Currency_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Currency_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_Order getC_Order() throws RuntimeException
    {
		return (org.compiere.model.I_C_Order)MTable.get(getCtx(), org.compiere.model.I_C_Order.Table_Name)
			.getPO(getC_Order_ID(), get_TrxName());	}

	/** Set Order.
		@param C_Order_ID 
		Order
	  */
	public void setC_Order_ID (int C_Order_ID)
	{
		if (C_Order_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_Order_ID, Integer.valueOf(C_Order_ID));
	}

	/** Get Order.
		@return Order
	  */
	public int getC_Order_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Order_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getC_Order_ID()));
    }

	/** Set Sales Order Line.
		@param C_OrderLine_ID 
		Sales Order Line
	  */
	public void setC_OrderLine_ID (int C_OrderLine_ID)
	{
		if (C_OrderLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_OrderLine_ID, Integer.valueOf(C_OrderLine_ID));
	}

	/** Get Sales Order Line.
		@return Sales Order Line
	  */
	public int getC_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set C_OrderLine_UU.
		@param C_OrderLine_UU C_OrderLine_UU	  */
	public void setC_OrderLine_UU (String C_OrderLine_UU)
	{
		set_Value (COLUMNNAME_C_OrderLine_UU, C_OrderLine_UU);
	}

	/** Get C_OrderLine_UU.
		@return C_OrderLine_UU	  */
	public String getC_OrderLine_UU () 
	{
		return (String)get_Value(COLUMNNAME_C_OrderLine_UU);
	}

	public org.compiere.model.I_C_Project getC_Project() throws RuntimeException
    {
		return (org.compiere.model.I_C_Project)MTable.get(getCtx(), org.compiere.model.I_C_Project.Table_Name)
			.getPO(getC_Project_ID(), get_TrxName());	}

	/** Set Project.
		@param C_Project_ID 
		Financial Project
	  */
	public void setC_Project_ID (int C_Project_ID)
	{
		if (C_Project_ID < 1) 
			set_Value (COLUMNNAME_C_Project_ID, null);
		else 
			set_Value (COLUMNNAME_C_Project_ID, Integer.valueOf(C_Project_ID));
	}

	/** Get Project.
		@return Financial Project
	  */
	public int getC_Project_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Project_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_ProjectPhase getC_ProjectPhase() throws RuntimeException
    {
		return (org.compiere.model.I_C_ProjectPhase)MTable.get(getCtx(), org.compiere.model.I_C_ProjectPhase.Table_Name)
			.getPO(getC_ProjectPhase_ID(), get_TrxName());	}

	/** Set Project Phase.
		@param C_ProjectPhase_ID 
		Phase of a Project
	  */
	public void setC_ProjectPhase_ID (int C_ProjectPhase_ID)
	{
		if (C_ProjectPhase_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_ProjectPhase_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_ProjectPhase_ID, Integer.valueOf(C_ProjectPhase_ID));
	}

	/** Get Project Phase.
		@return Phase of a Project
	  */
	public int getC_ProjectPhase_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ProjectPhase_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_ProjectTask getC_ProjectTask() throws RuntimeException
    {
		return (org.compiere.model.I_C_ProjectTask)MTable.get(getCtx(), org.compiere.model.I_C_ProjectTask.Table_Name)
			.getPO(getC_ProjectTask_ID(), get_TrxName());	}

	/** Set Project Task.
		@param C_ProjectTask_ID 
		Actual Project Task in a Phase
	  */
	public void setC_ProjectTask_ID (int C_ProjectTask_ID)
	{
		if (C_ProjectTask_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_ProjectTask_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_ProjectTask_ID, Integer.valueOf(C_ProjectTask_ID));
	}

	/** Get Project Task.
		@return Actual Project Task in a Phase
	  */
	public int getC_ProjectTask_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_ProjectTask_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Create Production.
		@param CreateProduction Create Production	  */
	public void setCreateProduction (String CreateProduction)
	{
		set_Value (COLUMNNAME_CreateProduction, CreateProduction);
	}

	/** Get Create Production.
		@return Create Production	  */
	public String getCreateProduction () 
	{
		return (String)get_Value(COLUMNNAME_CreateProduction);
	}

	/** Set Create Shipment.
		@param CreateShipment Create Shipment	  */
	public void setCreateShipment (String CreateShipment)
	{
		set_Value (COLUMNNAME_CreateShipment, CreateShipment);
	}

	/** Get Create Shipment.
		@return Create Shipment	  */
	public String getCreateShipment () 
	{
		return (String)get_Value(COLUMNNAME_CreateShipment);
	}

	public org.compiere.model.I_C_Tax getC_Tax() throws RuntimeException
    {
		return (org.compiere.model.I_C_Tax)MTable.get(getCtx(), org.compiere.model.I_C_Tax.Table_Name)
			.getPO(getC_Tax_ID(), get_TrxName());	}

	/** Set Tax.
		@param C_Tax_ID 
		Tax identifier
	  */
	public void setC_Tax_ID (int C_Tax_ID)
	{
		if (C_Tax_ID < 1) 
			set_Value (COLUMNNAME_C_Tax_ID, null);
		else 
			set_Value (COLUMNNAME_C_Tax_ID, Integer.valueOf(C_Tax_ID));
	}

	/** Get Tax.
		@return Tax identifier
	  */
	public int getC_Tax_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_Tax_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		if (C_UOM_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_UOM_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_UOM_ID, Integer.valueOf(C_UOM_ID));
	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Date Delivered.
		@param DateDelivered 
		Date when the product was delivered
	  */
	public void setDateDelivered (Timestamp DateDelivered)
	{
		set_ValueNoCheck (COLUMNNAME_DateDelivered, DateDelivered);
	}

	/** Get Date Delivered.
		@return Date when the product was delivered
	  */
	public Timestamp getDateDelivered () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateDelivered);
	}

	/** Set Date Invoiced.
		@param DateInvoiced 
		Date printed on Invoice
	  */
	public void setDateInvoiced (Timestamp DateInvoiced)
	{
		set_ValueNoCheck (COLUMNNAME_DateInvoiced, DateInvoiced);
	}

	/** Get Date Invoiced.
		@return Date printed on Invoice
	  */
	public Timestamp getDateInvoiced () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateInvoiced);
	}

	/** Set Date Ordered.
		@param DateOrdered 
		Date of Order
	  */
	public void setDateOrdered (Timestamp DateOrdered)
	{
		set_Value (COLUMNNAME_DateOrdered, DateOrdered);
	}

	/** Get Date Ordered.
		@return Date of Order
	  */
	public Timestamp getDateOrdered () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DateOrdered);
	}

	/** Set Date Promised.
		@param DatePromised 
		Date Order was promised
	  */
	public void setDatePromised (Timestamp DatePromised)
	{
		set_Value (COLUMNNAME_DatePromised, DatePromised);
	}

	/** Get Date Promised.
		@return Date Order was promised
	  */
	public Timestamp getDatePromised () 
	{
		return (Timestamp)get_Value(COLUMNNAME_DatePromised);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Discount %.
		@param Discount 
		Discount in percent
	  */
	public void setDiscount (BigDecimal Discount)
	{
		set_Value (COLUMNNAME_Discount, Discount);
	}

	/** Get Discount %.
		@return Discount in percent
	  */
	public BigDecimal getDiscount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Discount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Discount Amount.
		@param DiscountAmt 
		Calculated amount of discount
	  */
	public void setDiscountAmt (BigDecimal DiscountAmt)
	{
		set_Value (COLUMNNAME_DiscountAmt, DiscountAmt);
	}

	/** Get Discount Amount.
		@return Calculated amount of discount
	  */
	public BigDecimal getDiscountAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DiscountAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Freight Amount.
		@param FreightAmt 
		Freight Amount 
	  */
	public void setFreightAmt (BigDecimal FreightAmt)
	{
		set_Value (COLUMNNAME_FreightAmt, FreightAmt);
	}

	/** Get Freight Amount.
		@return Freight Amount 
	  */
	public BigDecimal getFreightAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_FreightAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Gross Amount.
		@param GrossAmt Gross Amount	  */
	public void setGrossAmt (BigDecimal GrossAmt)
	{
		throw new IllegalArgumentException ("GrossAmt is virtual column");	}

	/** Get Gross Amount.
		@return Gross Amount	  */
	public BigDecimal getGrossAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_GrossAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description Only.
		@param IsDescription 
		if true, the line is just description and no transaction
	  */
	public void setIsDescription (boolean IsDescription)
	{
		set_Value (COLUMNNAME_IsDescription, Boolean.valueOf(IsDescription));
	}

	/** Get Description Only.
		@return if true, the line is just description and no transaction
	  */
	public boolean isDescription () 
	{
		Object oo = get_Value(COLUMNNAME_IsDescription);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Product Bonuses.
		@param isProductBonuses 
		Product Bonuses
	  */
	public void setisProductBonuses (boolean isProductBonuses)
	{
		set_Value (COLUMNNAME_isProductBonuses, Boolean.valueOf(isProductBonuses));
	}

	/** Get Product Bonuses.
		@return Product Bonuses
	  */
	public boolean isProductBonuses () 
	{
		Object oo = get_Value(COLUMNNAME_isProductBonuses);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Line No.
		@param Line 
		Unique line for this document
	  */
	public void setLine (int Line)
	{
		set_Value (COLUMNNAME_Line, Integer.valueOf(Line));
	}

	/** Get Line No.
		@return Unique line for this document
	  */
	public int getLine () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Line);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Line Amount.
		@param LineNetAmt 
		Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public void setLineNetAmt (BigDecimal LineNetAmt)
	{
		set_ValueNoCheck (COLUMNNAME_LineNetAmt, LineNetAmt);
	}

	/** Get Line Amount.
		@return Line Extended Amount (Quantity * Actual Price) without Freight and Charges
	  */
	public BigDecimal getLineNetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_LineNetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_OrderLine getLink_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getLink_OrderLine_ID(), get_TrxName());	}

	/** Set Linked Order Line.
		@param Link_OrderLine_ID 
		This field links a sales order line to the purchase order line that is generated from it.
	  */
	public void setLink_OrderLine_ID (int Link_OrderLine_ID)
	{
		if (Link_OrderLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_Link_OrderLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_Link_OrderLine_ID, Integer.valueOf(Link_OrderLine_ID));
	}

	/** Get Linked Order Line.
		@return This field links a sales order line to the purchase order line that is generated from it.
	  */
	public int getLink_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Link_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public I_M_AttributeSetInstance getM_AttributeSetInstance() throws RuntimeException
    {
		return (I_M_AttributeSetInstance)MTable.get(getCtx(), I_M_AttributeSetInstance.Table_Name)
			.getPO(getM_AttributeSetInstance_ID(), get_TrxName());	}

	/** Set Attribute Set Instance.
		@param M_AttributeSetInstance_ID 
		Product Attribute Set Instance
	  */
	public void setM_AttributeSetInstance_ID (int M_AttributeSetInstance_ID)
	{
		if (M_AttributeSetInstance_ID < 0) 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, null);
		else 
			set_Value (COLUMNNAME_M_AttributeSetInstance_ID, Integer.valueOf(M_AttributeSetInstance_ID));
	}

	/** Get Attribute Set Instance.
		@return Product Attribute Set Instance
	  */
	public int getM_AttributeSetInstance_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_AttributeSetInstance_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Discount Schema Break.
		@param M_DiscountSchemaBreak_ID 
		Trade Discount Break
	  */
	public void setM_DiscountSchemaBreak_ID (int M_DiscountSchemaBreak_ID)
	{
		if (M_DiscountSchemaBreak_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_DiscountSchemaBreak_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_DiscountSchemaBreak_ID, Integer.valueOf(M_DiscountSchemaBreak_ID));
	}

	/** Get Discount Schema Break.
		@return Trade Discount Break
	  */
	public int getM_DiscountSchemaBreak_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_DiscountSchemaBreak_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		if (M_Product_ID < 1) 
			set_Value (COLUMNNAME_M_Product_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_ID, Integer.valueOf(M_Product_ID));
	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Promotion getM_Promotion() throws RuntimeException
    {
		return (org.compiere.model.I_M_Promotion)MTable.get(getCtx(), org.compiere.model.I_M_Promotion.Table_Name)
			.getPO(getM_Promotion_ID(), get_TrxName());	}

	/** Set Promotion.
		@param M_Promotion_ID Promotion	  */
	public void setM_Promotion_ID (int M_Promotion_ID)
	{
		if (M_Promotion_ID < 1) 
			set_Value (COLUMNNAME_M_Promotion_ID, null);
		else 
			set_Value (COLUMNNAME_M_Promotion_ID, Integer.valueOf(M_Promotion_ID));
	}

	/** Get Promotion.
		@return Promotion	  */
	public int getM_Promotion_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Promotion_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_RequisitionLine getM_RequisitionLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_RequisitionLine)MTable.get(getCtx(), org.compiere.model.I_M_RequisitionLine.Table_Name)
			.getPO(getM_RequisitionLine_ID(), get_TrxName());	}

	/** Set Requisition Line.
		@param M_RequisitionLine_ID 
		Material Requisition Line
	  */
	public void setM_RequisitionLine_ID (int M_RequisitionLine_ID)
	{
		if (M_RequisitionLine_ID < 1) 
			set_Value (COLUMNNAME_M_RequisitionLine_ID, null);
		else 
			set_Value (COLUMNNAME_M_RequisitionLine_ID, Integer.valueOf(M_RequisitionLine_ID));
	}

	/** Get Requisition Line.
		@return Material Requisition Line
	  */
	public int getM_RequisitionLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_RequisitionLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (org.compiere.model.I_M_Shipper)MTable.get(getCtx(), org.compiere.model.I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_Value (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_Value (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_Value (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_Value (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.eevolution.model.I_PP_Cost_Collector getPP_Cost_Collector() throws RuntimeException
    {
		return (org.eevolution.model.I_PP_Cost_Collector)MTable.get(getCtx(), org.eevolution.model.I_PP_Cost_Collector.Table_Name)
			.getPO(getPP_Cost_Collector_ID(), get_TrxName());	}

	/** Set Manufacturing Cost Collector.
		@param PP_Cost_Collector_ID Manufacturing Cost Collector	  */
	public void setPP_Cost_Collector_ID (int PP_Cost_Collector_ID)
	{
		if (PP_Cost_Collector_ID < 1) 
			set_Value (COLUMNNAME_PP_Cost_Collector_ID, null);
		else 
			set_Value (COLUMNNAME_PP_Cost_Collector_ID, Integer.valueOf(PP_Cost_Collector_ID));
	}

	/** Get Manufacturing Cost Collector.
		@return Manufacturing Cost Collector	  */
	public int getPP_Cost_Collector_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_PP_Cost_Collector_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Unit Price.
		@param PriceActual 
		Actual Price
	  */
	public void setPriceActual (BigDecimal PriceActual)
	{
		set_ValueNoCheck (COLUMNNAME_PriceActual, PriceActual);
	}

	/** Get Unit Price.
		@return Actual Price 
	  */
	public BigDecimal getPriceActual () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceActual);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Cost Price.
		@param PriceCost 
		Price per Unit of Measure including all indirect costs (Freight, etc.)
	  */
	public void setPriceCost (BigDecimal PriceCost)
	{
		set_Value (COLUMNNAME_PriceCost, PriceCost);
	}

	/** Get Cost Price.
		@return Price per Unit of Measure including all indirect costs (Freight, etc.)
	  */
	public BigDecimal getPriceCost () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceCost);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Price.
		@param PriceEntered 
		Price Entered - the price based on the selected/base UoM
	  */
	public void setPriceEntered (BigDecimal PriceEntered)
	{
		set_Value (COLUMNNAME_PriceEntered, PriceEntered);
	}

	/** Get Price.
		@return Price Entered - the price based on the selected/base UoM
	  */
	public BigDecimal getPriceEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Limit Price.
		@param PriceLimit 
		Lowest price for a product
	  */
	public void setPriceLimit (BigDecimal PriceLimit)
	{
		set_Value (COLUMNNAME_PriceLimit, PriceLimit);
	}

	/** Get Limit Price.
		@return Lowest price for a product
	  */
	public BigDecimal getPriceLimit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceLimit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set List Price.
		@param PriceList 
		List Price
	  */
	public void setPriceList (BigDecimal PriceList)
	{
		set_Value (COLUMNNAME_PriceList, PriceList);
	}

	/** Get List Price.
		@return List Price
	  */
	public BigDecimal getPriceList () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PriceList);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Product Key.
		@param ProductValue 
		Key of the Product
	  */
	public void setProductValue (String ProductValue)
	{
		set_Value (COLUMNNAME_ProductValue, ProductValue);
	}

	/** Get Product Key.
		@return Key of the Product
	  */
	public String getProductValue () 
	{
		return (String)get_Value(COLUMNNAME_ProductValue);
	}

	/** Set Bonuses Qty.
		@param QtyBonuses 
		Bonuses Qty
	  */
	public void setQtyBonuses (BigDecimal QtyBonuses)
	{
		set_Value (COLUMNNAME_QtyBonuses, QtyBonuses);
	}

	/** Get Bonuses Qty.
		@return Bonuses Qty
	  */
	public BigDecimal getQtyBonuses () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyBonuses);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Delivered Quantity.
		@param QtyDelivered 
		Delivered Quantity
	  */
	public void setQtyDelivered (BigDecimal QtyDelivered)
	{
		set_Value (COLUMNNAME_QtyDelivered, QtyDelivered);
	}

	/** Get Delivered Quantity.
		@return Delivered Quantity
	  */
	public BigDecimal getQtyDelivered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyDelivered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity.
		@param QtyEntered 
		The Quantity Entered is based on the selected UoM
	  */
	public void setQtyEntered (BigDecimal QtyEntered)
	{
		set_Value (COLUMNNAME_QtyEntered, QtyEntered);
	}

	/** Get Quantity.
		@return The Quantity Entered is based on the selected UoM
	  */
	public BigDecimal getQtyEntered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyEntered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity Invoiced.
		@param QtyInvoiced 
		Invoiced Quantity
	  */
	public void setQtyInvoiced (BigDecimal QtyInvoiced)
	{
		set_Value (COLUMNNAME_QtyInvoiced, QtyInvoiced);
	}

	/** Get Quantity Invoiced.
		@return Invoiced Quantity
	  */
	public BigDecimal getQtyInvoiced () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyInvoiced);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Lost Sales Qty.
		@param QtyLostSales 
		Quantity of potential sales
	  */
	public void setQtyLostSales (BigDecimal QtyLostSales)
	{
		set_Value (COLUMNNAME_QtyLostSales, QtyLostSales);
	}

	/** Get Lost Sales Qty.
		@return Quantity of potential sales
	  */
	public BigDecimal getQtyLostSales () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyLostSales);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Ordered Quantity.
		@param QtyOrdered 
		Ordered Quantity
	  */
	public void setQtyOrdered (BigDecimal QtyOrdered)
	{
		set_Value (COLUMNNAME_QtyOrdered, QtyOrdered);
	}

	/** Get Ordered Quantity.
		@return Ordered Quantity
	  */
	public BigDecimal getQtyOrdered () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyOrdered);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Quantity Packed.
		@param QtyPacked Quantity Packed	  */
	public void setQtyPacked (BigDecimal QtyPacked)
	{
		set_Value (COLUMNNAME_QtyPacked, QtyPacked);
	}

	/** Get Quantity Packed.
		@return Quantity Packed	  */
	public BigDecimal getQtyPacked () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyPacked);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Reserved Quantity.
		@param QtyReserved 
		Reserved Quantity
	  */
	public void setQtyReserved (BigDecimal QtyReserved)
	{
		set_ValueNoCheck (COLUMNNAME_QtyReserved, QtyReserved);
	}

	/** Get Reserved Quantity.
		@return Reserved Quantity
	  */
	public BigDecimal getQtyReserved () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_QtyReserved);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_OrderLine getRef_OrderLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getRef_OrderLine_ID(), get_TrxName());	}

	/** Set Referenced Order Line.
		@param Ref_OrderLine_ID 
		Reference to corresponding Sales/Purchase Order
	  */
	public void setRef_OrderLine_ID (int Ref_OrderLine_ID)
	{
		if (Ref_OrderLine_ID < 1) 
			set_Value (COLUMNNAME_Ref_OrderLine_ID, null);
		else 
			set_Value (COLUMNNAME_Ref_OrderLine_ID, Integer.valueOf(Ref_OrderLine_ID));
	}

	/** Get Referenced Order Line.
		@return Reference to corresponding Sales/Purchase Order
	  */
	public int getRef_OrderLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Ref_OrderLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_OrderLine getReference() throws RuntimeException
    {
		return (org.compiere.model.I_C_OrderLine)MTable.get(getCtx(), org.compiere.model.I_C_OrderLine.Table_Name)
			.getPO(getReference_ID(), get_TrxName());	}

	/** Set Refrerence Record.
		@param Reference_ID Refrerence Record	  */
	public void setReference_ID (int Reference_ID)
	{
		if (Reference_ID < 1) 
			set_Value (COLUMNNAME_Reference_ID, null);
		else 
			set_Value (COLUMNNAME_Reference_ID, Integer.valueOf(Reference_ID));
	}

	/** Get Refrerence Record.
		@return Refrerence Record	  */
	public int getReference_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_Reference_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Revenue Recognition Amt.
		@param RRAmt 
		Revenue Recognition Amount
	  */
	public void setRRAmt (BigDecimal RRAmt)
	{
		set_Value (COLUMNNAME_RRAmt, RRAmt);
	}

	/** Get Revenue Recognition Amt.
		@return Revenue Recognition Amount
	  */
	public BigDecimal getRRAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_RRAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Revenue Recognition Start.
		@param RRStartDate 
		Revenue Recognition Start Date
	  */
	public void setRRStartDate (Timestamp RRStartDate)
	{
		set_Value (COLUMNNAME_RRStartDate, RRStartDate);
	}

	/** Get Revenue Recognition Start.
		@return Revenue Recognition Start Date
	  */
	public Timestamp getRRStartDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_RRStartDate);
	}

	/** Set Resource Assignment.
		@param S_ResourceAssignment_ID 
		Resource Assignment
	  */
	public void setS_ResourceAssignment_ID (int S_ResourceAssignment_ID)
	{
		if (S_ResourceAssignment_ID < 1) 
			set_Value (COLUMNNAME_S_ResourceAssignment_ID, null);
		else 
			set_Value (COLUMNNAME_S_ResourceAssignment_ID, Integer.valueOf(S_ResourceAssignment_ID));
	}

	/** Get Resource Assignment.
		@return Resource Assignment
	  */
	public int getS_ResourceAssignment_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_S_ResourceAssignment_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Standart Discount %.
		@param StandartDiscount Standart Discount %	  */
	public void setStandartDiscount (BigDecimal StandartDiscount)
	{
		set_Value (COLUMNNAME_StandartDiscount, StandartDiscount);
	}

	/** Get Standart Discount %.
		@return Standart Discount %	  */
	public BigDecimal getStandartDiscount () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StandartDiscount);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Standart Discount.
		@param StandartDiscountAmt Standart Discount	  */
	public void setStandartDiscountAmt (BigDecimal StandartDiscountAmt)
	{
		set_Value (COLUMNNAME_StandartDiscountAmt, StandartDiscountAmt);
	}

	/** Get Standart Discount.
		@return Standart Discount	  */
	public BigDecimal getStandartDiscountAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_StandartDiscountAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Target Line Amount.
		@param TargetLineNetAmt 
		Target Line Amount
	  */
	public void setTargetLineNetAmt (BigDecimal TargetLineNetAmt)
	{
		set_Value (COLUMNNAME_TargetLineNetAmt, TargetLineNetAmt);
	}

	/** Get Target Line Amount.
		@return Target Line Amount
	  */
	public BigDecimal getTargetLineNetAmt () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TargetLineNetAmt);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL1_ID(), get_TrxName());	}

	/** Set UOM Conversion L1.
		@param UOMConversionL1_ID 
		The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID)
	{
		if (UOMConversionL1_ID < 1) 
			set_Value (COLUMNNAME_UOMConversionL1_ID, null);
		else 
			set_Value (COLUMNNAME_UOMConversionL1_ID, Integer.valueOf(UOMConversionL1_ID));
	}

	/** Get UOM Conversion L1.
		@return The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL1_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL2_ID(), get_TrxName());	}

	/** Set UOM Conversion L2.
		@param UOMConversionL2_ID 
		The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID)
	{
		if (UOMConversionL2_ID < 1) 
			set_Value (COLUMNNAME_UOMConversionL2_ID, null);
		else 
			set_Value (COLUMNNAME_UOMConversionL2_ID, Integer.valueOf(UOMConversionL2_ID));
	}

	/** Get UOM Conversion L2.
		@return The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL3_ID(), get_TrxName());	}

	/** Set UOM Conversion L3.
		@param UOMConversionL3_ID 
		The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID)
	{
		if (UOMConversionL3_ID < 1) 
			set_Value (COLUMNNAME_UOMConversionL3_ID, null);
		else 
			set_Value (COLUMNNAME_UOMConversionL3_ID, Integer.valueOf(UOMConversionL3_ID));
	}

	/** Get UOM Conversion L3.
		@return The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL3_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL4_ID(), get_TrxName());	}

	/** Set UOM Conversion L4.
		@param UOMConversionL4_ID 
		The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID)
	{
		if (UOMConversionL4_ID < 1) 
			set_Value (COLUMNNAME_UOMConversionL4_ID, null);
		else 
			set_Value (COLUMNNAME_UOMConversionL4_ID, Integer.valueOf(UOMConversionL4_ID));
	}

	/** Get UOM Conversion L4.
		@return The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL4_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Qty Bonus L1.
		@param UOMQtyBonusesL1 Qty Bonus L1	  */
	public void setUOMQtyBonusesL1 (BigDecimal UOMQtyBonusesL1)
	{
		set_Value (COLUMNNAME_UOMQtyBonusesL1, UOMQtyBonusesL1);
	}

	/** Get Qty Bonus L1.
		@return Qty Bonus L1	  */
	public BigDecimal getUOMQtyBonusesL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBonusesL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Bonus L2.
		@param UOMQtyBonusesL2 Qty Bonus L2	  */
	public void setUOMQtyBonusesL2 (BigDecimal UOMQtyBonusesL2)
	{
		set_Value (COLUMNNAME_UOMQtyBonusesL2, UOMQtyBonusesL2);
	}

	/** Get Qty Bonus L2.
		@return Qty Bonus L2	  */
	public BigDecimal getUOMQtyBonusesL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBonusesL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Bonus L3.
		@param UOMQtyBonusesL3 Qty Bonus L3	  */
	public void setUOMQtyBonusesL3 (BigDecimal UOMQtyBonusesL3)
	{
		set_Value (COLUMNNAME_UOMQtyBonusesL3, UOMQtyBonusesL3);
	}

	/** Get Qty Bonus L3.
		@return Qty Bonus L3	  */
	public BigDecimal getUOMQtyBonusesL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBonusesL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Bonus L4.
		@param UOMQtyBonusesL4 Qty Bonus L4	  */
	public void setUOMQtyBonusesL4 (BigDecimal UOMQtyBonusesL4)
	{
		set_Value (COLUMNNAME_UOMQtyBonusesL4, UOMQtyBonusesL4);
	}

	/** Get Qty Bonus L4.
		@return Qty Bonus L4	  */
	public BigDecimal getUOMQtyBonusesL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyBonusesL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Delivered L1.
		@param UOMQtyDeliveredL1 Qty Delivered L1	  */
	public void setUOMQtyDeliveredL1 (BigDecimal UOMQtyDeliveredL1)
	{
		set_Value (COLUMNNAME_UOMQtyDeliveredL1, UOMQtyDeliveredL1);
	}

	/** Get Qty Delivered L1.
		@return Qty Delivered L1	  */
	public BigDecimal getUOMQtyDeliveredL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyDeliveredL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Delivered L2.
		@param UOMQtyDeliveredL2 Qty Delivered L2	  */
	public void setUOMQtyDeliveredL2 (BigDecimal UOMQtyDeliveredL2)
	{
		set_Value (COLUMNNAME_UOMQtyDeliveredL2, UOMQtyDeliveredL2);
	}

	/** Get Qty Delivered L2.
		@return Qty Delivered L2	  */
	public BigDecimal getUOMQtyDeliveredL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyDeliveredL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Delivered L3.
		@param UOMQtyDeliveredL3 Qty Delivered L3	  */
	public void setUOMQtyDeliveredL3 (BigDecimal UOMQtyDeliveredL3)
	{
		set_Value (COLUMNNAME_UOMQtyDeliveredL3, UOMQtyDeliveredL3);
	}

	/** Get Qty Delivered L3.
		@return Qty Delivered L3	  */
	public BigDecimal getUOMQtyDeliveredL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyDeliveredL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Delivered L4.
		@param UOMQtyDeliveredL4 Qty Delivered L4	  */
	public void setUOMQtyDeliveredL4 (BigDecimal UOMQtyDeliveredL4)
	{
		set_Value (COLUMNNAME_UOMQtyDeliveredL4, UOMQtyDeliveredL4);
	}

	/** Get Qty Delivered L4.
		@return Qty Delivered L4	  */
	public BigDecimal getUOMQtyDeliveredL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyDeliveredL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Entered L1.
		@param UOMQtyEnteredL1 Qty Entered L1	  */
	public void setUOMQtyEnteredL1 (BigDecimal UOMQtyEnteredL1)
	{
		set_Value (COLUMNNAME_UOMQtyEnteredL1, UOMQtyEnteredL1);
	}

	/** Get Qty Entered L1.
		@return Qty Entered L1	  */
	public BigDecimal getUOMQtyEnteredL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyEnteredL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Entered L2.
		@param UOMQtyEnteredL2 Qty Entered L2	  */
	public void setUOMQtyEnteredL2 (BigDecimal UOMQtyEnteredL2)
	{
		set_Value (COLUMNNAME_UOMQtyEnteredL2, UOMQtyEnteredL2);
	}

	/** Get Qty Entered L2.
		@return Qty Entered L2	  */
	public BigDecimal getUOMQtyEnteredL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyEnteredL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Entered L3.
		@param UOMQtyEnteredL3 Qty Entered L3	  */
	public void setUOMQtyEnteredL3 (BigDecimal UOMQtyEnteredL3)
	{
		set_Value (COLUMNNAME_UOMQtyEnteredL3, UOMQtyEnteredL3);
	}

	/** Get Qty Entered L3.
		@return Qty Entered L3	  */
	public BigDecimal getUOMQtyEnteredL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyEnteredL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Entered L4.
		@param UOMQtyEnteredL4 Qty Entered L4	  */
	public void setUOMQtyEnteredL4 (BigDecimal UOMQtyEnteredL4)
	{
		set_Value (COLUMNNAME_UOMQtyEnteredL4, UOMQtyEnteredL4);
	}

	/** Get Qty Entered L4.
		@return Qty Entered L4	  */
	public BigDecimal getUOMQtyEnteredL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyEnteredL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Invoiced L1.
		@param UOMQtyInvoicedL1 Qty Invoiced L1	  */
	public void setUOMQtyInvoicedL1 (BigDecimal UOMQtyInvoicedL1)
	{
		set_Value (COLUMNNAME_UOMQtyInvoicedL1, UOMQtyInvoicedL1);
	}

	/** Get Qty Invoiced L1.
		@return Qty Invoiced L1	  */
	public BigDecimal getUOMQtyInvoicedL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInvoicedL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Invoiced L2.
		@param UOMQtyInvoicedL2 Qty Invoiced L2	  */
	public void setUOMQtyInvoicedL2 (BigDecimal UOMQtyInvoicedL2)
	{
		set_Value (COLUMNNAME_UOMQtyInvoicedL2, UOMQtyInvoicedL2);
	}

	/** Get Qty Invoiced L2.
		@return Qty Invoiced L2	  */
	public BigDecimal getUOMQtyInvoicedL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInvoicedL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Invoiced L3.
		@param UOMQtyInvoicedL3 Qty Invoiced L3	  */
	public void setUOMQtyInvoicedL3 (BigDecimal UOMQtyInvoicedL3)
	{
		set_Value (COLUMNNAME_UOMQtyInvoicedL3, UOMQtyInvoicedL3);
	}

	/** Get Qty Invoiced L3.
		@return Qty Invoiced L3	  */
	public BigDecimal getUOMQtyInvoicedL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInvoicedL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Invoiced L4.
		@param UOMQtyInvoicedL4 Qty Invoiced L4	  */
	public void setUOMQtyInvoicedL4 (BigDecimal UOMQtyInvoicedL4)
	{
		set_Value (COLUMNNAME_UOMQtyInvoicedL4, UOMQtyInvoicedL4);
	}

	/** Get Qty Invoiced L4.
		@return Qty Invoiced L4	  */
	public BigDecimal getUOMQtyInvoicedL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyInvoicedL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Lost Sales L1.
		@param UOMQtyLostSalesL1 Qty Lost Sales L1	  */
	public void setUOMQtyLostSalesL1 (BigDecimal UOMQtyLostSalesL1)
	{
		set_Value (COLUMNNAME_UOMQtyLostSalesL1, UOMQtyLostSalesL1);
	}

	/** Get Qty Lost Sales L1.
		@return Qty Lost Sales L1	  */
	public BigDecimal getUOMQtyLostSalesL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyLostSalesL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Lost Sales L2.
		@param UOMQtyLostSalesL2 Qty Lost Sales L2	  */
	public void setUOMQtyLostSalesL2 (BigDecimal UOMQtyLostSalesL2)
	{
		set_Value (COLUMNNAME_UOMQtyLostSalesL2, UOMQtyLostSalesL2);
	}

	/** Get Qty Lost Sales L2.
		@return Qty Lost Sales L2	  */
	public BigDecimal getUOMQtyLostSalesL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyLostSalesL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Lost Sales L3.
		@param UOMQtyLostSalesL3 Qty Lost Sales L3	  */
	public void setUOMQtyLostSalesL3 (BigDecimal UOMQtyLostSalesL3)
	{
		set_Value (COLUMNNAME_UOMQtyLostSalesL3, UOMQtyLostSalesL3);
	}

	/** Get Qty Lost Sales L3.
		@return Qty Lost Sales L3	  */
	public BigDecimal getUOMQtyLostSalesL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyLostSalesL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Lost Sales L4.
		@param UOMQtyLostSalesL4 Qty Lost Sales L4	  */
	public void setUOMQtyLostSalesL4 (BigDecimal UOMQtyLostSalesL4)
	{
		set_Value (COLUMNNAME_UOMQtyLostSalesL4, UOMQtyLostSalesL4);
	}

	/** Get Qty Lost Sales L4.
		@return Qty Lost Sales L4	  */
	public BigDecimal getUOMQtyLostSalesL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyLostSalesL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Ordered L1.
		@param UOMQtyOrderedL1 Qty Ordered L1	  */
	public void setUOMQtyOrderedL1 (BigDecimal UOMQtyOrderedL1)
	{
		set_Value (COLUMNNAME_UOMQtyOrderedL1, UOMQtyOrderedL1);
	}

	/** Get Qty Ordered L1.
		@return Qty Ordered L1	  */
	public BigDecimal getUOMQtyOrderedL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyOrderedL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Ordered L2.
		@param UOMQtyOrderedL2 Qty Ordered L2	  */
	public void setUOMQtyOrderedL2 (BigDecimal UOMQtyOrderedL2)
	{
		set_Value (COLUMNNAME_UOMQtyOrderedL2, UOMQtyOrderedL2);
	}

	/** Get Qty Ordered L2.
		@return Qty Ordered L2	  */
	public BigDecimal getUOMQtyOrderedL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyOrderedL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Ordered L3.
		@param UOMQtyOrderedL3 Qty Ordered L3	  */
	public void setUOMQtyOrderedL3 (BigDecimal UOMQtyOrderedL3)
	{
		set_Value (COLUMNNAME_UOMQtyOrderedL3, UOMQtyOrderedL3);
	}

	/** Get Qty Ordered L3.
		@return Qty Ordered L3	  */
	public BigDecimal getUOMQtyOrderedL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyOrderedL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Ordered L4.
		@param UOMQtyOrderedL4 Qty Ordered L4	  */
	public void setUOMQtyOrderedL4 (BigDecimal UOMQtyOrderedL4)
	{
		set_Value (COLUMNNAME_UOMQtyOrderedL4, UOMQtyOrderedL4);
	}

	/** Get Qty Ordered L4.
		@return Qty Ordered L4	  */
	public BigDecimal getUOMQtyOrderedL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyOrderedL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Packed L1.
		@param UOMQtyPackedL1 Qty Packed L1	  */
	public void setUOMQtyPackedL1 (BigDecimal UOMQtyPackedL1)
	{
		set_Value (COLUMNNAME_UOMQtyPackedL1, UOMQtyPackedL1);
	}

	/** Get Qty Packed L1.
		@return Qty Packed L1	  */
	public BigDecimal getUOMQtyPackedL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyPackedL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Packed L2.
		@param UOMQtyPackedL2 Qty Packed L2	  */
	public void setUOMQtyPackedL2 (BigDecimal UOMQtyPackedL2)
	{
		set_Value (COLUMNNAME_UOMQtyPackedL2, UOMQtyPackedL2);
	}

	/** Get Qty Packed L2.
		@return Qty Packed L2	  */
	public BigDecimal getUOMQtyPackedL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyPackedL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Packed L3.
		@param UOMQtyPackedL3 Qty Packed L3	  */
	public void setUOMQtyPackedL3 (BigDecimal UOMQtyPackedL3)
	{
		set_Value (COLUMNNAME_UOMQtyPackedL3, UOMQtyPackedL3);
	}

	/** Get Qty Packed L3.
		@return Qty Packed L3	  */
	public BigDecimal getUOMQtyPackedL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyPackedL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Packed L4.
		@param UOMQtyPackedL4 Qty Packed L4	  */
	public void setUOMQtyPackedL4 (BigDecimal UOMQtyPackedL4)
	{
		set_Value (COLUMNNAME_UOMQtyPackedL4, UOMQtyPackedL4);
	}

	/** Get Qty Packed L4.
		@return Qty Packed L4	  */
	public BigDecimal getUOMQtyPackedL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyPackedL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Reserved L1.
		@param UOMQtyReservedL1 Qty Reserved L1	  */
	public void setUOMQtyReservedL1 (BigDecimal UOMQtyReservedL1)
	{
		set_Value (COLUMNNAME_UOMQtyReservedL1, UOMQtyReservedL1);
	}

	/** Get Qty Reserved L1.
		@return Qty Reserved L1	  */
	public BigDecimal getUOMQtyReservedL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyReservedL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Reserved L2.
		@param UOMQtyReservedL2 Qty Reserved L2	  */
	public void setUOMQtyReservedL2 (BigDecimal UOMQtyReservedL2)
	{
		set_Value (COLUMNNAME_UOMQtyReservedL2, UOMQtyReservedL2);
	}

	/** Get Qty Reserved L2.
		@return Qty Reserved L2	  */
	public BigDecimal getUOMQtyReservedL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyReservedL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Reserved L3.
		@param UOMQtyReservedL3 Qty Reserved L3	  */
	public void setUOMQtyReservedL3 (BigDecimal UOMQtyReservedL3)
	{
		set_Value (COLUMNNAME_UOMQtyReservedL3, UOMQtyReservedL3);
	}

	/** Get Qty Reserved L3.
		@return Qty Reserved L3	  */
	public BigDecimal getUOMQtyReservedL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyReservedL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Qty Reserved L4.
		@param UOMQtyReservedL4 Qty Reserved L4	  */
	public void setUOMQtyReservedL4 (BigDecimal UOMQtyReservedL4)
	{
		set_Value (COLUMNNAME_UOMQtyReservedL4, UOMQtyReservedL4);
	}

	/** Get Qty Reserved L4.
		@return Qty Reserved L4	  */
	public BigDecimal getUOMQtyReservedL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMQtyReservedL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_ElementValue getUser1() throws RuntimeException
    {
		return (org.compiere.model.I_C_ElementValue)MTable.get(getCtx(), org.compiere.model.I_C_ElementValue.Table_Name)
			.getPO(getUser1_ID(), get_TrxName());	}

	/** Set User Element List 1.
		@param User1_ID 
		User defined list element #1
	  */
	public void setUser1_ID (int User1_ID)
	{
		if (User1_ID < 1) 
			set_Value (COLUMNNAME_User1_ID, null);
		else 
			set_Value (COLUMNNAME_User1_ID, Integer.valueOf(User1_ID));
	}

	/** Get User Element List 1.
		@return User defined list element #1
	  */
	public int getUser1_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_User1_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_ElementValue getUser2() throws RuntimeException
    {
		return (org.compiere.model.I_C_ElementValue)MTable.get(getCtx(), org.compiere.model.I_C_ElementValue.Table_Name)
			.getPO(getUser2_ID(), get_TrxName());	}

	/** Set User Element List 2.
		@param User2_ID 
		User defined list element #2
	  */
	public void setUser2_ID (int User2_ID)
	{
		if (User2_ID < 1) 
			set_Value (COLUMNNAME_User2_ID, null);
		else 
			set_Value (COLUMNNAME_User2_ID, Integer.valueOf(User2_ID));
	}

	/** Get User Element List 2.
		@return User defined list element #2
	  */
	public int getUser2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_User2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}
}
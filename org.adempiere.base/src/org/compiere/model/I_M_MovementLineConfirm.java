/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.Timestamp;
import org.compiere.util.KeyNamePair;

/** Generated Interface for M_MovementLineConfirm
 *  @author iDempiere (generated) 
 *  @version Release 5.1
 */
public interface I_M_MovementLineConfirm 
{

    /** TableName=M_MovementLineConfirm */
    public static final String Table_Name = "M_MovementLineConfirm";

    /** AD_Table_ID=737 */
    public static final int Table_ID = 737;

    KeyNamePair Model = new KeyNamePair(Table_ID, Table_Name);

    /** AccessLevel = 1 - Org 
     */
    BigDecimal accessLevel = BigDecimal.valueOf(1);

    /** Load Meta Data */

    /** Column name AD_Client_ID */
    public static final String COLUMNNAME_AD_Client_ID = "AD_Client_ID";

	/** Get Client.
	  * Client/Tenant for this installation.
	  */
	public int getAD_Client_ID();

    /** Column name AD_Org_ID */
    public static final String COLUMNNAME_AD_Org_ID = "AD_Org_ID";

	/** Set Organization.
	  * Organizational entity within client
	  */
	public void setAD_Org_ID (int AD_Org_ID);

	/** Get Organization.
	  * Organizational entity within client
	  */
	public int getAD_Org_ID();

    /** Column name C_InvoiceLine_ID */
    public static final String COLUMNNAME_C_InvoiceLine_ID = "C_InvoiceLine_ID";

	/** Set Invoice Line.
	  * Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID);

	/** Get Invoice Line.
	  * Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID();

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException;

    /** Column name C_UOM_ID */
    public static final String COLUMNNAME_C_UOM_ID = "C_UOM_ID";

	/** Set UOM.
	  * Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID);

	/** Get UOM.
	  * Unit of Measure
	  */
	public int getC_UOM_ID();

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException;

    /** Column name ConfirmedQty */
    public static final String COLUMNNAME_ConfirmedQty = "ConfirmedQty";

	/** Set Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public void setConfirmedQty (BigDecimal ConfirmedQty);

	/** Get Confirmed Quantity.
	  * Confirmation of a received quantity
	  */
	public BigDecimal getConfirmedQty();

    /** Column name Created */
    public static final String COLUMNNAME_Created = "Created";

	/** Get Created.
	  * Date this record was created
	  */
	public Timestamp getCreated();

    /** Column name CreatedBy */
    public static final String COLUMNNAME_CreatedBy = "CreatedBy";

	/** Get Created By.
	  * User who created this records
	  */
	public int getCreatedBy();

    /** Column name Description */
    public static final String COLUMNNAME_Description = "Description";

	/** Set Description.
	  * Optional short description of the record
	  */
	public void setDescription (String Description);

	/** Get Description.
	  * Optional short description of the record
	  */
	public String getDescription();

    /** Column name DifferenceQty */
    public static final String COLUMNNAME_DifferenceQty = "DifferenceQty";

	/** Set Difference.
	  * Difference Quantity
	  */
	public void setDifferenceQty (BigDecimal DifferenceQty);

	/** Get Difference.
	  * Difference Quantity
	  */
	public BigDecimal getDifferenceQty();

    /** Column name IsActive */
    public static final String COLUMNNAME_IsActive = "IsActive";

	/** Set Active.
	  * The record is active in the system
	  */
	public void setIsActive (boolean IsActive);

	/** Get Active.
	  * The record is active in the system
	  */
	public boolean isActive();

    /** Column name M_InventoryLine_ID */
    public static final String COLUMNNAME_M_InventoryLine_ID = "M_InventoryLine_ID";

	/** Set Phys.Inventory Line.
	  * Unique line in an Inventory document
	  */
	public void setM_InventoryLine_ID (int M_InventoryLine_ID);

	/** Get Phys.Inventory Line.
	  * Unique line in an Inventory document
	  */
	public int getM_InventoryLine_ID();

	public org.compiere.model.I_M_InventoryLine getM_InventoryLine() throws RuntimeException;

    /** Column name M_LocatorTo_ID */
    public static final String COLUMNNAME_M_LocatorTo_ID = "M_LocatorTo_ID";

	/** Set Locator To.
	  * Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID);

	/** Get Locator To.
	  * Location inventory is moved to
	  */
	public int getM_LocatorTo_ID();

	public org.compiere.model.I_M_Locator getM_LocatorTo() throws RuntimeException;

    /** Column name M_MovementConfirm_ID */
    public static final String COLUMNNAME_M_MovementConfirm_ID = "M_MovementConfirm_ID";

	/** Set Move Confirm.
	  * Inventory Move Confirmation
	  */
	public void setM_MovementConfirm_ID (int M_MovementConfirm_ID);

	/** Get Move Confirm.
	  * Inventory Move Confirmation
	  */
	public int getM_MovementConfirm_ID();

	public org.compiere.model.I_M_MovementConfirm getM_MovementConfirm() throws RuntimeException;

    /** Column name M_MovementLine_ID */
    public static final String COLUMNNAME_M_MovementLine_ID = "M_MovementLine_ID";

	/** Set Move Line.
	  * Inventory Move document Line
	  */
	public void setM_MovementLine_ID (int M_MovementLine_ID);

	/** Get Move Line.
	  * Inventory Move document Line
	  */
	public int getM_MovementLine_ID();

	public org.compiere.model.I_M_MovementLine getM_MovementLine() throws RuntimeException;

    /** Column name M_MovementLineConfirm_ID */
    public static final String COLUMNNAME_M_MovementLineConfirm_ID = "M_MovementLineConfirm_ID";

	/** Set Move Line Confirm.
	  * Inventory Move Line Confirmation
	  */
	public void setM_MovementLineConfirm_ID (int M_MovementLineConfirm_ID);

	/** Get Move Line Confirm.
	  * Inventory Move Line Confirmation
	  */
	public int getM_MovementLineConfirm_ID();

    /** Column name M_MovementLineConfirm_UU */
    public static final String COLUMNNAME_M_MovementLineConfirm_UU = "M_MovementLineConfirm_UU";

	/** Set M_MovementLineConfirm_UU	  */
	public void setM_MovementLineConfirm_UU (String M_MovementLineConfirm_UU);

	/** Get M_MovementLineConfirm_UU	  */
	public String getM_MovementLineConfirm_UU();

    /** Column name M_Product_ID */
    public static final String COLUMNNAME_M_Product_ID = "M_Product_ID";

	/** Set Product.
	  * Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID);

	/** Get Product.
	  * Product, Service, Item
	  */
	public int getM_Product_ID();

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException;

    /** Column name Processed */
    public static final String COLUMNNAME_Processed = "Processed";

	/** Set Processed.
	  * The document has been processed
	  */
	public void setProcessed (boolean Processed);

	/** Get Processed.
	  * The document has been processed
	  */
	public boolean isProcessed();

    /** Column name ReversalLine_ID */
    public static final String COLUMNNAME_ReversalLine_ID = "ReversalLine_ID";

	/** Set Reversal Line.
	  * Use to keep the reversal line ID for reversing costing purpose
	  */
	public void setReversalLine_ID (int ReversalLine_ID);

	/** Get Reversal Line.
	  * Use to keep the reversal line ID for reversing costing purpose
	  */
	public int getReversalLine_ID();

	public org.compiere.model.I_M_MovementLineConfirm getReversalLine() throws RuntimeException;

    /** Column name ScrappedQty */
    public static final String COLUMNNAME_ScrappedQty = "ScrappedQty";

	/** Set Scrapped Quantity.
	  * The Quantity scrapped due to QA issues
	  */
	public void setScrappedQty (BigDecimal ScrappedQty);

	/** Get Scrapped Quantity.
	  * The Quantity scrapped due to QA issues
	  */
	public BigDecimal getScrappedQty();

    /** Column name TargetQty */
    public static final String COLUMNNAME_TargetQty = "TargetQty";

	/** Set Target Quantity.
	  * Target Movement Quantity
	  */
	public void setTargetQty (BigDecimal TargetQty);

	/** Get Target Quantity.
	  * Target Movement Quantity
	  */
	public BigDecimal getTargetQty();

    /** Column name UOMConfirmedQtyL1 */
    public static final String COLUMNNAME_UOMConfirmedQtyL1 = "UOMConfirmedQtyL1";

	/** Set Confirmed Qty L1	  */
	public void setUOMConfirmedQtyL1 (BigDecimal UOMConfirmedQtyL1);

	/** Get Confirmed Qty L1	  */
	public BigDecimal getUOMConfirmedQtyL1();

    /** Column name UOMConfirmedQtyL2 */
    public static final String COLUMNNAME_UOMConfirmedQtyL2 = "UOMConfirmedQtyL2";

	/** Set Confirmed Qty L2	  */
	public void setUOMConfirmedQtyL2 (BigDecimal UOMConfirmedQtyL2);

	/** Get Confirmed Qty L2	  */
	public BigDecimal getUOMConfirmedQtyL2();

    /** Column name UOMConfirmedQtyL3 */
    public static final String COLUMNNAME_UOMConfirmedQtyL3 = "UOMConfirmedQtyL3";

	/** Set Confirmed Qty L3	  */
	public void setUOMConfirmedQtyL3 (BigDecimal UOMConfirmedQtyL3);

	/** Get Confirmed Qty L3	  */
	public BigDecimal getUOMConfirmedQtyL3();

    /** Column name UOMConfirmedQtyL4 */
    public static final String COLUMNNAME_UOMConfirmedQtyL4 = "UOMConfirmedQtyL4";

	/** Set Confirmed Qty L4	  */
	public void setUOMConfirmedQtyL4 (BigDecimal UOMConfirmedQtyL4);

	/** Get Confirmed Qty L4	  */
	public BigDecimal getUOMConfirmedQtyL4();

    /** Column name UOMConversionL1_ID */
    public static final String COLUMNNAME_UOMConversionL1_ID = "UOMConversionL1_ID";

	/** Set UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID);

	/** Get UOM Conversion L1.
	  * The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException;

    /** Column name UOMConversionL2_ID */
    public static final String COLUMNNAME_UOMConversionL2_ID = "UOMConversionL2_ID";

	/** Set UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID);

	/** Get UOM Conversion L2.
	  * The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException;

    /** Column name UOMConversionL3_ID */
    public static final String COLUMNNAME_UOMConversionL3_ID = "UOMConversionL3_ID";

	/** Set UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID);

	/** Get UOM Conversion L3.
	  * The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException;

    /** Column name UOMConversionL4_ID */
    public static final String COLUMNNAME_UOMConversionL4_ID = "UOMConversionL4_ID";

	/** Set UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID);

	/** Get UOM Conversion L4.
	  * The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID();

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException;

    /** Column name UOMDifferenceQtyL1 */
    public static final String COLUMNNAME_UOMDifferenceQtyL1 = "UOMDifferenceQtyL1";

	/** Set Difference Qty L1	  */
	public void setUOMDifferenceQtyL1 (BigDecimal UOMDifferenceQtyL1);

	/** Get Difference Qty L1	  */
	public BigDecimal getUOMDifferenceQtyL1();

    /** Column name UOMDifferenceQtyL2 */
    public static final String COLUMNNAME_UOMDifferenceQtyL2 = "UOMDifferenceQtyL2";

	/** Set Difference Qty L2	  */
	public void setUOMDifferenceQtyL2 (BigDecimal UOMDifferenceQtyL2);

	/** Get Difference Qty L2	  */
	public BigDecimal getUOMDifferenceQtyL2();

    /** Column name UOMDifferenceQtyL3 */
    public static final String COLUMNNAME_UOMDifferenceQtyL3 = "UOMDifferenceQtyL3";

	/** Set Difference Qty L3	  */
	public void setUOMDifferenceQtyL3 (BigDecimal UOMDifferenceQtyL3);

	/** Get Difference Qty L3	  */
	public BigDecimal getUOMDifferenceQtyL3();

    /** Column name UOMDifferenceQtyL4 */
    public static final String COLUMNNAME_UOMDifferenceQtyL4 = "UOMDifferenceQtyL4";

	/** Set Difference Qty L4	  */
	public void setUOMDifferenceQtyL4 (BigDecimal UOMDifferenceQtyL4);

	/** Get Difference Qty L4	  */
	public BigDecimal getUOMDifferenceQtyL4();

    /** Column name UOMScrappedQtyL1 */
    public static final String COLUMNNAME_UOMScrappedQtyL1 = "UOMScrappedQtyL1";

	/** Set Scrapped Qty L1	  */
	public void setUOMScrappedQtyL1 (BigDecimal UOMScrappedQtyL1);

	/** Get Scrapped Qty L1	  */
	public BigDecimal getUOMScrappedQtyL1();

    /** Column name UOMScrappedQtyL2 */
    public static final String COLUMNNAME_UOMScrappedQtyL2 = "UOMScrappedQtyL2";

	/** Set Scrapped Qty L2	  */
	public void setUOMScrappedQtyL2 (BigDecimal UOMScrappedQtyL2);

	/** Get Scrapped Qty L2	  */
	public BigDecimal getUOMScrappedQtyL2();

    /** Column name UOMScrappedQtyL3 */
    public static final String COLUMNNAME_UOMScrappedQtyL3 = "UOMScrappedQtyL3";

	/** Set Scrapped Qty L3	  */
	public void setUOMScrappedQtyL3 (BigDecimal UOMScrappedQtyL3);

	/** Get Scrapped Qty L3	  */
	public BigDecimal getUOMScrappedQtyL3();

    /** Column name UOMScrappedQtyL4 */
    public static final String COLUMNNAME_UOMScrappedQtyL4 = "UOMScrappedQtyL4";

	/** Set Scrapped Qty L4	  */
	public void setUOMScrappedQtyL4 (BigDecimal UOMScrappedQtyL4);

	/** Get Scrapped Qty L4	  */
	public BigDecimal getUOMScrappedQtyL4();

    /** Column name UOMTargetQtyL1 */
    public static final String COLUMNNAME_UOMTargetQtyL1 = "UOMTargetQtyL1";

	/** Set Target Qty L1	  */
	public void setUOMTargetQtyL1 (BigDecimal UOMTargetQtyL1);

	/** Get Target Qty L1	  */
	public BigDecimal getUOMTargetQtyL1();

    /** Column name UOMTargetQtyL2 */
    public static final String COLUMNNAME_UOMTargetQtyL2 = "UOMTargetQtyL2";

	/** Set Target Qty L2	  */
	public void setUOMTargetQtyL2 (BigDecimal UOMTargetQtyL2);

	/** Get Target Qty L2	  */
	public BigDecimal getUOMTargetQtyL2();

    /** Column name UOMTargetQtyL3 */
    public static final String COLUMNNAME_UOMTargetQtyL3 = "UOMTargetQtyL3";

	/** Set Target Qty L3	  */
	public void setUOMTargetQtyL3 (BigDecimal UOMTargetQtyL3);

	/** Get Target Qty L3	  */
	public BigDecimal getUOMTargetQtyL3();

    /** Column name UOMTargetQtyL4 */
    public static final String COLUMNNAME_UOMTargetQtyL4 = "UOMTargetQtyL4";

	/** Set Target Qty L4	  */
	public void setUOMTargetQtyL4 (BigDecimal UOMTargetQtyL4);

	/** Get Target Qty L4	  */
	public BigDecimal getUOMTargetQtyL4();

    /** Column name Updated */
    public static final String COLUMNNAME_Updated = "Updated";

	/** Get Updated.
	  * Date this record was updated
	  */
	public Timestamp getUpdated();

    /** Column name UpdatedBy */
    public static final String COLUMNNAME_UpdatedBy = "UpdatedBy";

	/** Get Updated By.
	  * User who updated this records
	  */
	public int getUpdatedBy();
}

/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for M_Product_Category
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_M_Product_Category extends PO implements I_M_Product_Category, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171031L;

    /** Standard Constructor */
    public X_M_Product_Category (Properties ctx, int M_Product_Category_ID, String trxName)
    {
      super (ctx, M_Product_Category_ID, trxName);
      /** if (M_Product_Category_ID == 0)
        {
<<<<<<< .mine
			setCommodityType (null);
// FM
			setInitialQAStatus (null);
// NQ
=======
			setInitialQAStatus (null);
// NQ
>>>>>>> .r174
			setIsDefault (false);
			setIsSelfService (true);
// Y
			setM_Product_Category_ID (0);
			setMMPolicy (null);
// F
			setM_Product_Category_ID (0);
			setName (null);
			setPlannedMargin (Env.ZERO);
			setQAMonitoring (false);
// N
			setValue (null);
        } */
    }

    /** Load Constructor */
    public X_M_Product_Category (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_Product_Category[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_PrintColor getAD_PrintColor() throws RuntimeException
    {
		return (org.compiere.model.I_AD_PrintColor)MTable.get(getCtx(), org.compiere.model.I_AD_PrintColor.Table_Name)
			.getPO(getAD_PrintColor_ID(), get_TrxName());	}

	/** Set Print Color.
		@param AD_PrintColor_ID 
		Color used for printing and display
	  */
	public void setAD_PrintColor_ID (int AD_PrintColor_ID)
	{
		if (AD_PrintColor_ID < 1) 
			set_Value (COLUMNNAME_AD_PrintColor_ID, null);
		else 
			set_Value (COLUMNNAME_AD_PrintColor_ID, Integer.valueOf(AD_PrintColor_ID));
	}

	/** Get Print Color.
		@return Color used for printing and display
	  */
	public int getAD_PrintColor_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_PrintColor_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_A_Asset_Group getA_Asset_Group() throws RuntimeException
    {
		return (org.compiere.model.I_A_Asset_Group)MTable.get(getCtx(), org.compiere.model.I_A_Asset_Group.Table_Name)
			.getPO(getA_Asset_Group_ID(), get_TrxName());	}

	/** Set Asset Group.
		@param A_Asset_Group_ID 
		Group of Assets
	  */
	public void setA_Asset_Group_ID (int A_Asset_Group_ID)
	{
		if (A_Asset_Group_ID < 1) 
			set_Value (COLUMNNAME_A_Asset_Group_ID, null);
		else 
			set_Value (COLUMNNAME_A_Asset_Group_ID, Integer.valueOf(A_Asset_Group_ID));
	}

	/** Get Asset Group.
		@return Group of Assets
	  */
	public int getA_Asset_Group_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_A_Asset_Group_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Slow Moving = SLO */
	public static final String COMMODITYTYPE_SlowMoving = "SLO";
	/** Fast Moving = FAS */
	public static final String COMMODITYTYPE_FastMoving = "FAS";
	/** Set Commodity Type.
		@param CommodityType 
		Commodity type to categorize a product/category.
	  */
	public void setCommodityType (String CommodityType)
	{

		set_Value (COLUMNNAME_CommodityType, CommodityType);
	}

	/** Get Commodity Type.
		@return Commodity type to categorize a product/category.
	  */
	public String getCommodityType () 
	{
		return (String)get_Value(COLUMNNAME_CommodityType);
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** On Hold = OH */
	public static final String INITIALQASTATUS_OnHold = "OH";
	/** Reject = RJ */
	public static final String INITIALQASTATUS_Reject = "RJ";
	/** Reprocess = RP */
	public static final String INITIALQASTATUS_Reprocess = "RP";
	/** Blending = BL */
	public static final String INITIALQASTATUS_Blending = "BL";
	/** Resorting = RS */
	public static final String INITIALQASTATUS_Resorting = "RS";
	/** Regrade = RG */
	public static final String INITIALQASTATUS_Regrade = "RG";
	/** Repacking = RA */
	public static final String INITIALQASTATUS_Repacking = "RA";
	/** Non Conformance = NC */
	public static final String INITIALQASTATUS_NonConformance = "NC";
	/** Premature Released = PR */
	public static final String INITIALQASTATUS_PrematureReleased = "PR";
	/** Release = RE */
	public static final String INITIALQASTATUS_Release = "RE";
	/** Pending Inspection = PI */
	public static final String INITIALQASTATUS_PendingInspection = "PI";
	/** Incubation = IC */
	public static final String INITIALQASTATUS_Incubation = "IC";
	/** Non QA = NQ */
	public static final String INITIALQASTATUS_NonQA = "NQ";
	/** QA Tested = QT */
	public static final String INITIALQASTATUS_QATested = "QT";
	/** MIX = MX */
	public static final String INITIALQASTATUS_MIX = "MX";
	/** Pending Lab Test = PL */
	public static final String INITIALQASTATUS_PendingLabTest = "PL";
	/** Set Initial QA Status.
		@param InitialQAStatus 
		Initial QA Status
	  */
	public void setInitialQAStatus (String InitialQAStatus)
	{

		set_Value (COLUMNNAME_InitialQAStatus, InitialQAStatus);
	}

	/** Get Initial QA Status.
		@return Initial QA Status
	  */
	public String getInitialQAStatus () 
	{
		return (String)get_Value(COLUMNNAME_InitialQAStatus);
	}

	/** Set Default.
		@param IsDefault 
		Default value
	  */
	public void setIsDefault (boolean IsDefault)
	{
		set_Value (COLUMNNAME_IsDefault, Boolean.valueOf(IsDefault));
	}

	/** Get Default.
		@return Default value
	  */
	public boolean isDefault () 
	{
		Object oo = get_Value(COLUMNNAME_IsDefault);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Self-Service.
		@param IsSelfService 
		This is a Self-Service entry or this entry can be changed via Self-Service
	  */
	public void setIsSelfService (boolean IsSelfService)
	{
		set_Value (COLUMNNAME_IsSelfService, Boolean.valueOf(IsSelfService));
	}

	/** Get Self-Service.
		@return This is a Self-Service entry or this entry can be changed via Self-Service
	  */
	public boolean isSelfService () 
	{
		Object oo = get_Value(COLUMNNAME_IsSelfService);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** MMPolicy AD_Reference_ID=335 */
	public static final int MMPOLICY_AD_Reference_ID=335;
	/** LiFo = L */
	public static final String MMPOLICY_LiFo = "L";
	/** FiFo = F */
	public static final String MMPOLICY_FiFo = "F";
	/** Set Material Policy.
		@param MMPolicy 
		Material Movement Policy
	  */
	public void setMMPolicy (String MMPolicy)
	{

		set_Value (COLUMNNAME_MMPolicy, MMPolicy);
	}

	/** Get Material Policy.
		@return Material Movement Policy
	  */
	public String getMMPolicy () 
	{
		return (String)get_Value(COLUMNNAME_MMPolicy);
	}

	/** Set Product Category.
		@param M_Product_Category_ID 
		Category of a Product
	  */
	public void setM_Product_Category_ID (int M_Product_Category_ID)
	{
		if (M_Product_Category_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Product_Category_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Product_Category_ID, Integer.valueOf(M_Product_Category_ID));
	}

	/** Get Product Category.
		@return Category of a Product
	  */
	public int getM_Product_Category_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Product_Category getM_Product_Category_Parent() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product_Category)MTable.get(getCtx(), org.compiere.model.I_M_Product_Category.Table_Name)
			.getPO(getM_Product_Category_Parent_ID(), get_TrxName());	}

	/** Set Parent Product Category.
		@param M_Product_Category_Parent_ID Parent Product Category	  */
	public void setM_Product_Category_Parent_ID (int M_Product_Category_Parent_ID)
	{
		if (M_Product_Category_Parent_ID < 1) 
			set_Value (COLUMNNAME_M_Product_Category_Parent_ID, null);
		else 
			set_Value (COLUMNNAME_M_Product_Category_Parent_ID, Integer.valueOf(M_Product_Category_Parent_ID));
	}

	/** Get Parent Product Category.
		@return Parent Product Category	  */
	public int getM_Product_Category_Parent_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_Category_Parent_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set M_Product_Category_UU.
		@param M_Product_Category_UU M_Product_Category_UU	  */
	public void setM_Product_Category_UU (String M_Product_Category_UU)
	{
		set_Value (COLUMNNAME_M_Product_Category_UU, M_Product_Category_UU);
	}

	/** Get M_Product_Category_UU.
		@return M_Product_Category_UU	  */
	public String getM_Product_Category_UU () 
	{
		return (String)get_Value(COLUMNNAME_M_Product_Category_UU);
	}

	/** Set Name.
		@param Name 
		Alphanumeric identifier of the entity
	  */
	public void setName (String Name)
	{
		set_Value (COLUMNNAME_Name, Name);
	}

	/** Get Name.
		@return Alphanumeric identifier of the entity
	  */
	public String getName () 
	{
		return (String)get_Value(COLUMNNAME_Name);
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), getName());
    }

	/** Set Planned Margin %.
		@param PlannedMargin 
		Project's planned margin as a percentage
	  */
	public void setPlannedMargin (BigDecimal PlannedMargin)
	{
		set_Value (COLUMNNAME_PlannedMargin, PlannedMargin);
	}

	/** Get Planned Margin %.
		@return Project's planned margin as a percentage
	  */
	public BigDecimal getPlannedMargin () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_PlannedMargin);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set QA Monitoring.
		@param QAMonitoring 
		This product must be monitoring by QA before next process.
	  */
	public void setQAMonitoring (boolean QAMonitoring)
	{
		set_Value (COLUMNNAME_QAMonitoring, Boolean.valueOf(QAMonitoring));
	}

	/** Get QA Monitoring.
		@return This product must be monitoring by QA before next process.
	  */
	public boolean isQAMonitoring () 
	{
		Object oo = get_Value(COLUMNNAME_QAMonitoring);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}
/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;

import org.adempiere.exceptions.NegativeInventoryDisallowedException;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Util;

/**
 * 	Inventory Storage Model
 *
 *	@author Jorg Janke
 *	@version $Id: MStorageOnHand.java,v 1.3 2006/07/30 00:51:05 jjanke Exp $
 */
public class MStorageOnHand extends X_M_StorageOnHand
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3820729340100521329L;

	/**
	 * 
	 * @param ctx
	 * @param M_Locator_ID
	 * @param M_Product_ID
	 * @param M_AttributeSetInstance_ID
	 * @param trxName
	 * @deprecated
	 * @return MStorageOnHand
	 */
	public static MStorageOnHand get (Properties ctx, int M_Locator_ID, 
			int M_Product_ID, int M_AttributeSetInstance_ID, String trxName) {
		return get (ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, null, trxName);
	}

	/**
	 * 	Get Storage Info
	 *	@param ctx context
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance
	 *  @param dateMPolicy
	 *	@param trxName transaction
	 *	@return existing or null
	 */
	public static MStorageOnHand get (Properties ctx, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID,Timestamp dateMPolicy, String trxName)
	{
		String sqlWhere = "M_Locator_ID=? AND M_Product_ID=? AND ";
		if (M_AttributeSetInstance_ID == 0)
			sqlWhere += "(M_AttributeSetInstance_ID=? OR M_AttributeSetInstance_ID IS NULL)";
		else
			sqlWhere += "M_AttributeSetInstance_ID=?";
	
		if (dateMPolicy == null)
		{
			if (M_AttributeSetInstance_ID > 0)
			{
				MAttributeSetInstance asi = new MAttributeSetInstance(ctx, M_AttributeSetInstance_ID, trxName);
				dateMPolicy = asi.getCreated();
			}
		}

		if (dateMPolicy != null)
			sqlWhere += " AND DateMaterialPolicy=trunc(cast(? as date))";
		
		Query query = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName);
		if (dateMPolicy != null)
			query.setParameters(M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, dateMPolicy);
		else
			query.setParameters(M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID);									 
		MStorageOnHand retValue = query.first();
		
		if (retValue == null) {
			if (s_log.isLoggable(Level.FINE)) s_log.fine("Not Found - M_Locator_ID=" + M_Locator_ID 
					+ ", M_Product_ID=" + M_Product_ID + ", M_AttributeSetInstance_ID=" + M_AttributeSetInstance_ID);
		} else {
			if (s_log.isLoggable(Level.FINE)) s_log.fine("M_Locator_ID=" + M_Locator_ID 
					+ ", M_Product_ID=" + M_Product_ID + ", M_AttributeSetInstance_ID=" + M_AttributeSetInstance_ID);
		}
		return retValue;
	}	//	get

	/**
	 * 	Get all Storages for Product with ASI and QtyOnHand <> 0
	 *	@param ctx context
	 *	@param M_Product_ID product
	 *	@param M_Locator_ID locator
	 *	@param FiFo first in-first-out
	 *	@param trxName transaction
	 *	@return existing or null
	 */
	public static MStorageOnHand[] getAllWithASI (Properties ctx, int M_Product_ID, int M_Locator_ID, 
		boolean FiFo, String trxName)
	{
		ArrayList<MStorageOnHand> list = new ArrayList<MStorageOnHand>();
		String sql = "SELECT * FROM M_StorageOnHand "
			+ "WHERE M_Product_ID=? AND M_Locator_ID=?"
			+ " AND M_AttributeSetInstance_ID > 0 "
			+ " AND QtyOnHand <> 0 "			
			+ "ORDER BY M_AttributeSetInstance_ID";
		if (!FiFo)
			sql += " DESC";
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql, trxName);
			pstmt.setInt (1, M_Product_ID);
			pstmt.setInt (2, M_Locator_ID);
			rs = pstmt.executeQuery ();
			while (rs.next ())
				list.add(new MStorageOnHand (ctx, rs, trxName));
		}
		catch (SQLException ex)
		{
			s_log.log(Level.SEVERE, sql, ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getAllWithASI

	/**
	 * 	Get all Storages for Product where QtyOnHand <> 0
	 *	@param ctx context
	 *	@param M_Product_ID product
	 *	@param M_Locator_ID locator
	 *	@param trxName transaction
	 *	@return existing or null
	 */
	public static MStorageOnHand[] getAll (Properties ctx, 
		int M_Product_ID, int M_Locator_ID, String trxName)
	{
		return getAll(ctx, M_Product_ID, M_Locator_ID, trxName, false, 0);
	}
	
	/**
	 * 	Get all Storages for Product where QtyOnHand <> 0
	 *	@param ctx context
	 *	@param M_Product_ID product
	 *	@param M_Locator_ID locator
	 *	@param trxName transaction
	 *	@return existing or null
	 */
	public static MStorageOnHand[] getAll (Properties ctx, 
		int M_Product_ID, int M_Locator_ID, String trxName, boolean forUpdate, int timeout)
	{
		String sqlWhere = "M_Product_ID=? AND M_Locator_ID=? AND QtyOnHand <> 0";
		Query query = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
								.setParameters(M_Product_ID, M_Locator_ID);
		MProduct product = new MProduct(ctx, M_Product_ID, trxName);
		if (product.isUseGuaranteeDateForMPolicy()) 
		{
			query.addJoinClause(" LEFT OUTER JOIN M_AttributeSetInstance asi ON (M_StorageOnHand.M_AttributeSetInstance_ID=asi.M_AttributeSetInstance_ID) ")
				 .setOrderBy("asi."+I_M_AttributeSetInstance.COLUMNNAME_GuaranteeDate);
		}
		else
		{
			query.setOrderBy(MStorageOnHand.COLUMNNAME_DateMaterialPolicy);
		}
		if (forUpdate)
		{
			query.setForUpdate(forUpdate);
			if (timeout > 0)
			{
				query.setQueryTimeout(timeout);
			}
		}
		List<MStorageOnHand> list = query.list(); 
		
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getAll

	
	/**
	 * 	Get Storage Info for Product across warehouses
	 *	@param ctx context
	 *	@param M_Product_ID product
	 *	@param trxName transaction
	 *	@return existing or null
	 */
	public static MStorageOnHand[] getOfProduct (Properties ctx, int M_Product_ID, String trxName)
	{
		return getOfProduct(ctx, 0, M_Product_ID, trxName);
	}	//	getOfProduct

	/**
	 * Get Storage Info for Product across warehouses of an organization if not zero value.
	 * @param ctx
	 * @param AD_Org_ID
	 * @param M_Product_ID
	 * @param trxName
	 *	@return existing or null
	 */
	public static MStorageOnHand[] getOfProduct (Properties ctx, int AD_Org_ID, int M_Product_ID, String trxName)
	{
		String sqlWhere = "M_Product_ID=?";
		if (AD_Org_ID > 0)
			sqlWhere += " AND AD_Org_ID=" + AD_Org_ID;
		
		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
								.setParameters(M_Product_ID)
								.list(); 
		
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
		
	}	//	getOfProduct
	
	/**
	 * 	Get Storage Info for Warehouse
	 *	@param ctx context
	 *	@param M_Warehouse_ID 
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance
	 *	@param M_AttributeSet_ID attribute set (NOT USED)
	 *	@param allAttributeInstances if true, all attribute set instances (NOT USED)
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *	@param trxName transaction
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 *
	 *  @deprecated
	 */
	public static MStorageOnHand[] getWarehouse (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, int M_AttributeSet_ID,
		boolean allAttributeInstances, Timestamp minGuaranteeDate,
		boolean FiFo, String trxName)
	{
		return getWarehouse(ctx, M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID, 
				minGuaranteeDate, FiFo, false, 0, trxName);
	}
	
	/**
	 * 	Get Storage Info for Warehouse or locator
	 *	@param ctx context
	 *	@param M_Warehouse_ID ignore if M_Locator_ID > 0
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance id, 0 to retrieve all instance
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *  @param positiveOnly if true, only return storage records with qtyOnHand > 0
	 *  @param M_Locator_ID optional locator id
	 *	@param trxName transaction
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 */
	public static MStorageOnHand[] getWarehouse (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, Timestamp minGuaranteeDate,
		boolean FiFo, boolean positiveOnly, int M_Locator_ID, String trxName)
	{
		return getWarehouse(ctx, M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID, minGuaranteeDate, FiFo, 
				positiveOnly, M_Locator_ID, trxName, false);
	}
	
	/**
	 * 	Get Storage Info for Warehouse or locator
	 *	@param ctx context
	 *	@param M_Warehouse_ID ignore if M_Locator_ID > 0
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance id, 0 to retrieve all instance
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *  @param positiveOnly if true, only return storage records with qtyOnHand > 0
	 *  @param M_Locator_ID optional locator id
	 *	@param trxName transaction
	 *  @param forUpdate
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 */
	public static MStorageOnHand[] getWarehouse (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, Timestamp minGuaranteeDate,
		boolean FiFo, boolean positiveOnly, int M_Locator_ID, String trxName, boolean forUpdate)
	{
		return getWarehouse(ctx, M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID, minGuaranteeDate, FiFo, positiveOnly, M_Locator_ID, trxName, forUpdate, 0);
	}
	
	/**
	 * 	Get Storage Info for Warehouse or locator
	 *	@param ctx context
	 *	@param M_Warehouse_ID ignore if M_Locator_ID > 0
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance id, 0 to retrieve all instance
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *  @param positiveOnly if true, only return storage records with qtyOnHand > 0
	 *  @param M_Locator_ID optional locator id
	 *	@param trxName transaction
	 *  @param forUpdate
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 */
	public static MStorageOnHand[] getWarehouse (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, Timestamp minGuaranteeDate,
		boolean FiFo, boolean positiveOnly, int M_Locator_ID, String trxName, boolean forUpdate, int timeout)
	{
		if ((M_Warehouse_ID == 0 && M_Locator_ID == 0) || M_Product_ID == 0)
			return new MStorageOnHand[0];
		
		boolean allAttributeInstances = false;
		if (M_AttributeSetInstance_ID == 0)
			allAttributeInstances = true;		
		
		ArrayList<MStorageOnHand> list = new ArrayList<MStorageOnHand>();
		//	Specific Attribute Set Instance
//		String sql = "SELECT s.M_Product_ID,s.M_Locator_ID,s.M_AttributeSetInstance_ID,"
//			+ "s.AD_Client_ID,s.AD_Org_ID,s.IsActive,s.Created,s.CreatedBy,s.Updated,s.UpdatedBy,"
//			+ "s.QtyOnHand,s.DateLastInventory,s.DateMaterialPolicy "
//			+ "s.M_StorageOnHand_UU, s.C_UOM_ID, s.UOMConvertionL1_ID FROM M_StorageOnHand s"
//			+ " INNER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID) ";
		String sql = "SELECT s.* FROM M_StorageOnHand s"
				+ " INNER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID) ";
		if (M_Locator_ID > 0)
			sql += "WHERE l.M_Locator_ID = ?";
		else
			sql += "WHERE l.M_Warehouse_ID=?";
		sql += " AND s.M_Product_ID=?"
			 + " AND COALESCE(s.M_AttributeSetInstance_ID,0)=? ";
		if (positiveOnly)
		{
			sql += " AND s.QtyOnHand > 0 ";
		}
		else
		{
			sql += " AND s.QtyOnHand <> 0 ";
		}
		sql += "ORDER BY l.PriorityNo DESC, DateMaterialPolicy ";
		if (!FiFo)
			sql += " DESC";
		//	All Attribute Set Instances
		if (allAttributeInstances)
		{
//			sql = "SELECT s.M_Product_ID,s.M_Locator_ID,s.M_AttributeSetInstance_ID,"
//				+ " s.AD_Client_ID,s.AD_Org_ID,s.IsActive,s.Created,s.CreatedBy,s.Updated,s.UpdatedBy,"
//				+ " s.QtyOnHand,s.DateLastInventory,s.M_StorageOnHand_UU,s.DateMaterialPolicy "
//				+ " FROM M_StorageOnHand s"
//				+ " INNER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID)"
//				+ " LEFT OUTER JOIN M_AttributeSetInstance asi ON (s.M_AttributeSetInstance_ID=asi.M_AttributeSetInstance_ID) ";
			sql = "SELECT s.* "
					+ " FROM M_StorageOnHand s"
					+ " INNER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID)"
					+ " LEFT OUTER JOIN M_AttributeSetInstance asi ON (s.M_AttributeSetInstance_ID=asi.M_AttributeSetInstance_ID) ";
			if (M_Locator_ID > 0)
				sql += "WHERE l.M_Locator_ID = ?";
			else
				sql += "WHERE l.M_Warehouse_ID=?";
			sql += " AND s.M_Product_ID=? ";
			if (positiveOnly)
			{
				sql += " AND s.QtyOnHand > 0 ";
			}
			else
			{
				sql += " AND s.QtyOnHand <> 0 ";
			}
			
			if (minGuaranteeDate != null)
			{
				sql += "AND (asi.GuaranteeDate IS NULL OR asi.GuaranteeDate>?) ";
			}
			
			MProduct product = MProduct.get(Env.getCtx(), M_Product_ID);
			
			if(product.isUseGuaranteeDateForMPolicy()){
				sql += "ORDER BY l.PriorityNo DESC, asi.GuaranteeDate";
				if (!FiFo)
					sql += " DESC";
			}
			else
			{
				sql += "ORDER BY l.PriorityNo DESC, l.M_Locator_ID, s.DateMaterialPolicy";
				if (!FiFo)
					sql += " DESC";
			}
			
			sql += ", s.QtyOnHand DESC";
		} 
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, M_Locator_ID > 0 ? M_Locator_ID : M_Warehouse_ID);
			pstmt.setInt(2, M_Product_ID);
			if (!allAttributeInstances)
			{
				pstmt.setInt(3, M_AttributeSetInstance_ID);
			}
			else if (minGuaranteeDate != null)
			{
				pstmt.setTimestamp(3, minGuaranteeDate);
			}
			rs = pstmt.executeQuery();
			while (rs.next())
			{	
				if(rs.getBigDecimal("QtyOnHand").signum() != 0)
				{
					MStorageOnHand storage = new MStorageOnHand (ctx, rs, trxName);
					if (!Util.isEmpty(trxName) && forUpdate)
					{
						DB.getDatabase().forUpdate(storage, timeout);
					}
					list.add (storage);
				}
			}	
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getWarehouse

	/**
	 * 	Get Storage Info for Warehouse or locator
	 *	@param ctx context
	 *	@param M_Warehouse_ID ignore if M_Locator_ID > 0
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance id, 0 to retrieve all instance
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *  @param M_Locator_ID optional locator id
	 *	@param trxName transaction
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 */
	public static MStorageOnHand[] getWarehouseNegative (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, Timestamp minGuaranteeDate,
		boolean FiFo, int M_Locator_ID, String trxName)
	{
		return getWarehouseNegative(ctx, M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID, minGuaranteeDate, FiFo, M_Locator_ID, trxName, false);
	}
	
	/**
	 * 	Get Storage Info for Warehouse or locator
	 *	@param ctx context
	 *	@param M_Warehouse_ID ignore if M_Locator_ID > 0
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance id, 0 to retrieve storages that don't have asi, -1 to retrieve all instance
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *  @param M_Locator_ID optional locator id
	 *	@param trxName transaction
	 *  @param forUpdate
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 */
	public static MStorageOnHand[] getWarehouseNegative (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, Timestamp minGuaranteeDate,
		boolean FiFo, int M_Locator_ID, String trxName, boolean forUpdate)
	{
		return getWarehouseNegative(ctx, M_Warehouse_ID, M_Product_ID, M_AttributeSetInstance_ID, minGuaranteeDate, FiFo, M_Locator_ID, trxName, forUpdate, 0);
	}
	
	/**
	 * 	Get Storage Info for Warehouse or locator
	 *	@param ctx context
	 *	@param M_Warehouse_ID ignore if M_Locator_ID > 0
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance id, 0 to retrieve storages that don't have asi, -1 to retrieve all instance
	 *	@param minGuaranteeDate optional minimum guarantee date if all attribute instances
	 *	@param FiFo first in-first-out
	 *  @param M_Locator_ID optional locator id
	 *	@param trxName transaction
	 *  @param forUpdate
	 *  @param timeout
	 *	@return existing - ordered by location priority (desc) and/or guarantee date
	 */
	public static MStorageOnHand[] getWarehouseNegative (Properties ctx, int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, Timestamp minGuaranteeDate,
		boolean FiFo, int M_Locator_ID, String trxName, boolean forUpdate, int timeout)
	{
		if ((M_Warehouse_ID == 0 && M_Locator_ID == 0) || M_Product_ID == 0)
			return new MStorageOnHand[0];
		
		boolean allAttributeInstances = false;
		if (M_AttributeSetInstance_ID == 0)
			allAttributeInstances = true;		
		
		ArrayList<MStorageOnHand> list = new ArrayList<MStorageOnHand>();
		//	Specific Attribute Set Instance
		String sql = "SELECT s.M_Product_ID,s.M_Locator_ID,s.M_AttributeSetInstance_ID,"
			+ "s.AD_Client_ID,s.AD_Org_ID,s.IsActive,s.Created,s.CreatedBy,s.Updated,s.UpdatedBy,"
			+ "s.QtyOnHand,s.DateLastInventory,s.DateMaterialPolicy "
			+ "FROM M_StorageOnHand s"
			+ " INNER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID) ";
		if (M_Locator_ID > 0)
			sql += "WHERE l.M_Locator_ID = ?";
		else
			sql += "WHERE l.M_Warehouse_ID=?";
		sql += " AND s.M_Product_ID=?"
			+ " AND COALESCE(s.M_AttributeSetInstance_ID,0)=? "
			+ " AND s.QtyOnHand < 0 ";
		sql += "ORDER BY l.PriorityNo DESC, DateMaterialPolicy ";
		if (!FiFo)
			sql += " DESC";
		//	All Attribute Set Instances
		if (allAttributeInstances)
		{
			sql = "SELECT s.M_Product_ID,s.M_Locator_ID,s.M_AttributeSetInstance_ID,"
				+ "s.AD_Client_ID,s.AD_Org_ID,s.IsActive,s.Created,s.CreatedBy,s.Updated,s.UpdatedBy,"
				+ "s.QtyOnHand,s.DateLastInventory,s.M_StorageOnHand_UU,s.DateMaterialPolicy "
				+ "FROM M_StorageOnHand s"
				+ " INNER JOIN M_Locator l ON (l.M_Locator_ID=s.M_Locator_ID)"
				+ " LEFT OUTER JOIN M_AttributeSetInstance asi ON (s.M_AttributeSetInstance_ID=asi.M_AttributeSetInstance_ID) ";
			if (M_Locator_ID > 0)
				sql += "WHERE l.M_Locator_ID = ?";
			else
				sql += "WHERE l.M_Warehouse_ID=?";
			sql += " AND s.M_Product_ID=? "
				+ " AND s.QtyOnHand < 0 ";
			
			if (minGuaranteeDate != null)
			{
				sql += "AND (asi.GuaranteeDate IS NULL OR asi.GuaranteeDate>?) ";
			}
			
			MProduct product = MProduct.get(Env.getCtx(), M_Product_ID);
			
			if(product.isUseGuaranteeDateForMPolicy()){
				sql += "ORDER BY l.PriorityNo DESC, " +
					   "asi.GuaranteeDate";
				if (!FiFo)
					sql += " DESC";
			}
			else
			{
				sql += "ORDER BY l.PriorityNo DESC, l.M_Locator_ID, s.DateMaterialPolicy";
				if (!FiFo)
					sql += " DESC";
			}
			
			sql += ", s.QtyOnHand DESC";
		} 
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, M_Locator_ID > 0 ? M_Locator_ID : M_Warehouse_ID);
			pstmt.setInt(2, M_Product_ID);
			if (!allAttributeInstances)
			{
				pstmt.setInt(3, M_AttributeSetInstance_ID);
			}
			else if (minGuaranteeDate != null)
			{
				pstmt.setTimestamp(3, minGuaranteeDate);
			}
			rs = pstmt.executeQuery();
			while (rs.next())
			{	
				if(rs.getBigDecimal(11).signum() != 0)
				{
					MStorageOnHand storage = new MStorageOnHand (ctx, rs, trxName);
					if (!Util.isEmpty(trxName) && forUpdate)
					{
						DB.getDatabase().forUpdate(storage, timeout);
					}
					list.add(storage);
				}
			}	
		}
		catch (Exception e)
		{
			s_log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getWarehouse
		
	/**
	 * 	Create or Get Storage Info
	 *	@param ctx context
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance
	 *	@param trxName transaction
	 *	@return existing/new or null
	 */
	public static MStorageOnHand getCreate (Properties ctx, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID,Timestamp dateMPolicy, String trxName)
	{
		return getCreate(ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, dateMPolicy, trxName, false);
	}
	
	/**
	 * 	Create or Get Storage Info
	 *	@param ctx context
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance
	 *	@param trxName transaction
	 *  @param forUpdate
	 *	@return existing/new or null
	 */
	public static MStorageOnHand getCreate (Properties ctx, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID,Timestamp dateMPolicy, String trxName, boolean forUpdate)
	{
		return getCreate(ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, dateMPolicy, trxName, forUpdate, 0);
	}
	
	/**
	 * 	Create or Get Storage Info
	 *	@param ctx context
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance
	 *	@param trxName transaction
	 *  @param forUpdate
	 *  @param timeout
	 *	@return existing/new or null
	 */
	public static MStorageOnHand getCreate (Properties ctx, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID,Timestamp dateMPolicy, String trxName, boolean forUpdate, int timeout)
	{
		if (M_Locator_ID == 0)
			throw new IllegalArgumentException("M_Locator_ID=0");
		if (M_Product_ID == 0)
			throw new IllegalArgumentException("M_Product_ID=0");
		if (dateMPolicy == null)
			dateMPolicy = new Timestamp(new Date().getTime());
		
		dateMPolicy = Util.removeTime(dateMPolicy);
		
		MStorageOnHand retValue = get(ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID,dateMPolicy, trxName);
		if (retValue != null)
		{
			if (forUpdate)
				DB.getDatabase().forUpdate(retValue, timeout);
			return retValue;
		}
		
		//	Insert row based on locator
		MLocator locator = new MLocator (ctx, M_Locator_ID, trxName);
		if (locator.get_ID() != M_Locator_ID)
			throw new IllegalArgumentException("Not found M_Locator_ID=" + M_Locator_ID);
		//
		if (dateMPolicy == null)
		{
			dateMPolicy = new Timestamp(new Date().getTime());		
			dateMPolicy = Util.removeTime(dateMPolicy);
		}
		retValue = new MStorageOnHand (locator, M_Product_ID, M_AttributeSetInstance_ID,dateMPolicy);
		retValue.saveEx(trxName);
		if (s_log.isLoggable(Level.FINE)) s_log.fine("New " + retValue);
		return retValue;
	}	//	getCreate

	/**
	 * 	Update Storage Info add.
	 * 	Called from MProjectIssue
	 *	@param ctx context
	 *	@param M_Warehouse_ID warehouse
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID AS Instance
	 *	@param reservationAttributeSetInstance_ID reservation AS Instance
	 *	@param diffQtyOnHand add on hand
	 *	@param trxName transaction
	 *  @deprecated
	 *	@return true if updated
	 */
	public static boolean add (Properties ctx, int M_Warehouse_ID, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID,
		BigDecimal diffQtyOnHand, String trxName)
	{
		return add(ctx, M_Warehouse_ID, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, diffQtyOnHand, null, trxName);
	}
	
	/**
	 * 	Update Storage Info add.
	 * 	Called from MProjectIssue
	 *	@param ctx context
	 *	@param M_Warehouse_ID warehouse
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID AS Instance
	 *	@param reservationAttributeSetInstance_ID reservation AS Instance
	 *	@param diffQtyOnHand add on hand
	 *  @param dateMPolicy
	 *	@param trxName transaction
	 *	@return true if updated
	 */
	public static boolean add (Properties ctx, int M_Warehouse_ID, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID,
		BigDecimal diffQtyOnHand,Timestamp dateMPolicy, String trxName)
	{
		if (diffQtyOnHand == null || diffQtyOnHand.signum() == 0)
			return true;

		if (dateMPolicy == null)
		{
			if (M_AttributeSetInstance_ID > 0)
			{
				MAttributeSetInstance asi = new MAttributeSetInstance(ctx, M_AttributeSetInstance_ID, trxName);
				dateMPolicy = asi.getCreated();				
			}
			else
			{
				dateMPolicy = new Timestamp(System.currentTimeMillis());
			}
		}
		
		dateMPolicy = Util.removeTime(dateMPolicy);
		
		//	Get Storage
		MStorageOnHand storage = getCreate (ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, dateMPolicy, trxName, true, 120);
		//	Verify
		if (storage.getM_Locator_ID() != M_Locator_ID 
			&& storage.getM_Product_ID() != M_Product_ID
			&& storage.getM_AttributeSetInstance_ID() != M_AttributeSetInstance_ID)
		{
			s_log.severe ("No Storage found - M_Locator_ID=" + M_Locator_ID 
				+ ",M_Product_ID=" + M_Product_ID + ",ASI=" + M_AttributeSetInstance_ID);
			return false;
		}
		
		BigDecimal diff = storage.getQtyOnHand().add(diffQtyOnHand);
		BigDecimal sepuluhribu = new BigDecimal(10000);
		BigDecimal nolkomanolnolnolsatu = Env.ONE.divide(sepuluhribu);
		if(diff.compareTo(Env.ZERO) == -1 && diff.compareTo(nolkomanolnolnolsatu.negate()) == 1)
		{
			diffQtyOnHand = storage.getQtyOnHand().negate();
		}

		storage.setQtyOnHand (storage.getQtyOnHand().add (diffQtyOnHand));
		if (s_log.isLoggable(Level.FINE)) {
			StringBuilder diffText = new StringBuilder("(OnHand=").append(diffQtyOnHand).append(") -> ").append(storage.toString());
			s_log.fine(diffText.toString());
		}
		return storage.save (trxName);
	}	//	add

	/**
	 * Add quantity on hand directly - not using cached value - solving IDEMPIERE-2629
	 * @param addition
	 */
	public void addQtyOnHand(BigDecimal addition) {
		final String sql = "UPDATE M_StorageOnHand SET QtyOnHand=QtyOnHand+?, Updated=SYSDATE, UpdatedBy=? " +
				"WHERE M_Product_ID=? AND M_Locator_ID=? AND M_AttributeSetInstance_ID=? AND DateMaterialPolicy=?";
		DB.executeUpdateEx(sql, 
			new Object[] {addition, Env.getAD_User_ID(Env.getCtx()), getM_Product_ID(), getM_Locator_ID(), getM_AttributeSetInstance_ID(), getDateMaterialPolicy()}, 
			get_TrxName());
		load(get_TrxName());
		if (getQtyOnHand().signum() == -1) {
			MWarehouse wh = MWarehouse.get(Env.getCtx(), getM_Warehouse_ID());
			if (wh.isDisallowNegativeInv()) {
				throw new NegativeInventoryDisallowedException(getCtx(), getM_Warehouse_ID(), getM_Product_ID(), getM_AttributeSetInstance_ID(), getM_Locator_ID(),
						getQtyOnHand().subtract(addition), addition.negate());
			}
		}
	}

	/**************************************************************************
	 * 	Get Location with highest Locator Priority and a sufficient OnHand Qty
	 * 	@param M_Warehouse_ID warehouse
	 * 	@param M_Product_ID product
	 * 	@param M_AttributeSetInstance_ID asi
	 * 	@param Qty qty
	 *	@param trxName transaction
	 * 	@return id
	 */
	public static int getM_Locator_ID (int M_Warehouse_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, BigDecimal Qty,
		String trxName)
	{
		int M_Locator_ID = 0;
		int firstM_Locator_ID = 0;
		String sql = "SELECT s.M_Locator_ID, s.QtyOnHand "
			+ "FROM M_StorageOnHand s"
			+ " INNER JOIN M_Locator l ON (s.M_Locator_ID=l.M_Locator_ID)"
			+ " INNER JOIN M_Product p ON (s.M_Product_ID=p.M_Product_ID)"
			+ " LEFT OUTER JOIN M_AttributeSet mas ON (p.M_AttributeSet_ID=mas.M_AttributeSet_ID) "
			+ "WHERE l.M_Warehouse_ID=?"
			+ " AND s.M_Product_ID=?"
			+ " AND (mas.IsInstanceAttribute IS NULL OR mas.IsInstanceAttribute='N' OR s.M_AttributeSetInstance_ID=?)"
			+ " AND l.IsActive='Y' "
			+ "ORDER BY l.PriorityNo DESC, s.QtyOnHand DESC";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, M_Warehouse_ID);
			pstmt.setInt(2, M_Product_ID);
			pstmt.setInt(3, M_AttributeSetInstance_ID);
			rs = pstmt.executeQuery();
			while (rs.next())
			{
				BigDecimal QtyOnHand = rs.getBigDecimal(2);
				if (QtyOnHand != null && Qty.compareTo(QtyOnHand) <= 0)
				{
					M_Locator_ID = rs.getInt(1);
					break;
				}
				if (firstM_Locator_ID == 0)
					firstM_Locator_ID = rs.getInt(1);
			}
		}
		catch (SQLException ex)
		{
			s_log.log(Level.SEVERE, sql, ex);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		if (M_Locator_ID != 0)
			return M_Locator_ID;
		return firstM_Locator_ID;
	}	//	getM_Locator_ID
	
	/**************************************************************************
	 * 	Persistency Constructor
	 *	@param ctx context
	 *	@param ignored ignored
	 *	@param trxName transaction
	 */
	public MStorageOnHand (Properties ctx, int ignored, String trxName)
	{
		super(ctx, 0, trxName);
		if (ignored != 0)
			throw new IllegalArgumentException("Multi-Key");
		//
		setQtyOnHand (Env.ZERO);
		
	}	//	MStorageOnHand

	/**
	 * 	Load Constructor
	 *	@param ctx context
	 *	@param rs result set
	 *	@param trxName transaction
	 */
	public MStorageOnHand (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MStorageOnHand

	/**
	 * 	Full NEW Constructor
	 *	@param locator (parent) locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID attribute
	 */
	private MStorageOnHand (MLocator locator, int M_Product_ID, int M_AttributeSetInstance_ID)
	{
		this (locator.getCtx(), 0, locator.get_TrxName());
		setClientOrg(locator);
		setM_Locator_ID (locator.getM_Locator_ID());
		setM_Product_ID (M_Product_ID);
		setM_AttributeSetInstance_ID (M_AttributeSetInstance_ID);
	}	//	MStorageOnHand

	/**
	 * 	Full NEW Constructor
	 *	@param locator (parent) locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID attribute
	 */
	private MStorageOnHand (MLocator locator, int M_Product_ID, int M_AttributeSetInstance_ID,Timestamp dateMPolicy)
	{
		this (locator.getCtx(), 0, locator.get_TrxName());
		setClientOrg(locator);
		setM_Locator_ID (locator.getM_Locator_ID());
		setM_Product_ID (M_Product_ID);
		setM_AttributeSetInstance_ID (M_AttributeSetInstance_ID);
		dateMPolicy = Util.removeTime(dateMPolicy);
		setDateMaterialPolicy(dateMPolicy);
	}	//	MStorageOnHand

	/** Log								*/
	private static CLogger		s_log = CLogger.getCLogger (MStorageOnHand.class);

	/** Warehouse						*/
	private int		m_M_Warehouse_ID = 0;
	
	/**
	 * 	Change Qty OnHand
	 *	@param qty quantity
	 *	@param add add if true 
	 */
	public void changeQtyOnHand (BigDecimal qty, boolean add)
	{
		if (qty == null || qty.signum() == 0)
			return;
		if (add)
			setQtyOnHand(getQtyOnHand().add(qty));
		else
			setQtyOnHand(getQtyOnHand().subtract(qty));
	}	//	changeQtyOnHand

	/**
	 * 	Get M_Warehouse_ID of Locator
	 *	@return warehouse
	 */
	public int getM_Warehouse_ID()
	{
		if (m_M_Warehouse_ID == 0)
		{
			MLocator loc = new MLocator(getCtx(), getM_Locator_ID(), get_TrxName());
			m_M_Warehouse_ID = loc.getM_Warehouse_ID();
		}
		return m_M_Warehouse_ID;
	}	//	getM_Warehouse_ID
	

	/**
	 * 
	 * 
	 * Before Save
	 * @param newRecord new
	 * @param success success
	 * @return success
	 */
	@Override
	protected boolean beforeSave(boolean newRecord) 
	{
		//	Negative Inventory check
		if (newRecord || is_ValueChanged("QtyOnHand"))
		{
			MWarehouse wh = new MWarehouse(getCtx(), getM_Warehouse_ID(), get_TrxName());
			if (wh.isDisallowNegativeInv())
			{
				String sql = "SELECT SUM(QtyOnHand) "
					+ "FROM M_StorageOnHand s"
					+ " INNER JOIN M_Locator l ON (s.M_Locator_ID=l.M_Locator_ID) "
					+ "WHERE s.M_Product_ID=?"		//	#1
					+ " AND l.M_Warehouse_ID=?"
					+ " AND l.M_Locator_ID=?"
					+ " AND s.M_AttributeSetInstance_ID<>?";
				BigDecimal QtyOnHand = DB.getSQLValueBDEx(get_TrxName(), sql, new Object[] {getM_Product_ID(), getM_Warehouse_ID(), getM_Locator_ID(), getM_AttributeSetInstance_ID()});
				if (QtyOnHand == null)
					QtyOnHand = Env.ZERO;
				
				//TODO QA STATUS
				/**
				if((getReleaseQty().signum() > 0 && (getNCQty().signum() > 0 
						|| getOnHoldQty().signum() > 0)) || (getNCQty().signum() > 0
								&& (getReleaseQty().signum() > 0 || getOnHoldQty().signum() > 0)))
					setQAStatus(QASTATUS_QATested);
				*/
				// Add qty onhand for current record
				QtyOnHand = QtyOnHand.add(getQtyOnHand());
				
				if (getQtyOnHand().compareTo(BigDecimal.ZERO) < 0 ||
						QtyOnHand.compareTo(Env.ZERO) < 0)
				{
					log.saveError("Error", Msg.getMsg(getCtx(), "NegativeInventoryDisallowed"));
					return false;
				}
			}
		}

		return true;
	}
	
	/**
	 * Get Quantity On Hand of Warehouse
	 * @param M_Product_ID
	 * @param M_Warehouse_ID
	 * @param M_AttributeSetInstance_ID
	 * @param trxName
	 * @return QtyOnHand
	 */
	public static BigDecimal getQtyOnHand(int M_Product_ID, int M_Warehouse_ID, int M_AttributeSetInstance_ID, String trxName) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT SUM(QtyOnHand) FROM M_StorageOnHand oh JOIN M_Locator loc ON (oh.M_Locator_ID=loc.M_Locator_ID)")
			.append(" WHERE oh.M_Product_ID=?")
			.append(" AND loc.M_Warehouse_ID=?");

		ArrayList<Object> params = new ArrayList<Object>();
		params.add(M_Product_ID);
		params.add(M_Warehouse_ID);

		// With ASI
		if (M_AttributeSetInstance_ID != 0) {
			sql.append(" AND oh.M_AttributeSetInstance_ID=?");
			params.add(M_AttributeSetInstance_ID);
		}

		BigDecimal qty = DB.getSQLValueBD(trxName, sql.toString(), params);
		if (qty == null)
			qty = Env.ZERO;

		return qty;
	}

	/**
	 * Get Quantity On Hand of Warehouse Available for Reservation
	 * @param M_Product_ID
	 * @param M_Warehouse_ID
	 * @param M_AttributeSetInstance_ID
	 * @param trxName
	 * @return QtyOnHand
	 */
	public static BigDecimal getQtyOnHandForReservation(int M_Product_ID, int M_Warehouse_ID, int M_AttributeSetInstance_ID, String trxName) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT SUM(QtyOnHand) FROM M_StorageOnHand oh"
				+ " JOIN M_Locator loc ON (oh.M_Locator_ID=loc.M_Locator_ID)"
				+ " LEFT JOIN M_LocatorType lt ON (loc.M_LocatorType_ID=lt.M_LocatorType_ID)")
			.append(" WHERE oh.M_Product_ID=?")
			.append(" AND loc.M_Warehouse_ID=? AND COALESCE(lt.IsAvailableForReservation,'Y')='Y'");

		ArrayList<Object> params = new ArrayList<Object>();
		params.add(M_Product_ID);
		params.add(M_Warehouse_ID);

		// With ASI
		if (M_AttributeSetInstance_ID != 0) {
			sql.append(" AND oh.M_AttributeSetInstance_ID=?");
			params.add(M_AttributeSetInstance_ID);
		}

		BigDecimal qty = DB.getSQLValueBD(trxName, sql.toString(), params);
		if (qty == null)
			qty = Env.ZERO;

		return qty;
	}

	/**
	 * Get Quantity On Hand of Locator
	 * @param M_Product_ID
	 * @param M_Locator_ID
	 * @param M_AttributeSetInstance_ID
	 * @param trxName
	 * @return QtyOnHand
	 */
	public static BigDecimal getQtyOnHandForLocator(int M_Product_ID, int M_Locator_ID, int M_AttributeSetInstance_ID, String trxName) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT SUM(oh.QtyOnHand) FROM M_StorageOnHand oh")
			.append(" WHERE oh.M_Product_ID=?")
			.append(" AND oh.M_Locator_ID=?");

		ArrayList<Object> params = new ArrayList<Object>();
		params.add(M_Product_ID);
		params.add(M_Locator_ID);

		// With ASI
		if (M_AttributeSetInstance_ID != 0) {
			sql.append(" AND oh.M_AttributeSetInstance_ID=?");
			params.add(M_AttributeSetInstance_ID);
		}

		BigDecimal qty = DB.getSQLValueBD(trxName, sql.toString(), params);
		if (qty == null)
			qty = Env.ZERO;

		return qty;
	}
	
	/**
	 *	String Representation
	 * 	@return info
	 */
	public String toString()
	{
		StringBuffer sb = new StringBuffer("MStorageOnHand[")
			.append("M_Locator_ID=").append(getM_Locator_ID())
				.append(",M_Product_ID=").append(getM_Product_ID())
				.append(",M_AttributeSetInstance_ID=").append(getM_AttributeSetInstance_ID())
			.append(": OnHand=").append(getQtyOnHand())
			/* @win commented out
			.append(",Reserved=").append(getQtyReserved())
			.append(",Ordered=").append(getQtyOrdered())
			*/
			.append("]");
		return sb.toString();
	}	//	toString

	/**
	 * 
	 * @param M_Product_ID
	 * @param M_AttributeSetInstance_ID
	 * @param trxName
	 * @return datempolicy timestamp
	 */
	public static Timestamp getDateMaterialPolicy(int M_Product_ID, int M_AttributeSetInstance_ID,String trxName){
		
		if (M_Product_ID <= 0  || M_AttributeSetInstance_ID <= 0)
			return null;
		
		String sql = "SELECT dateMaterialPolicy FROM M_StorageOnHand WHERE M_Product_ID=? and M_AttributeSetInstance_ID=?";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, M_Product_ID);
			pstmt.setInt(2, M_AttributeSetInstance_ID);
			
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				return rs.getTimestamp(1);
			}
		}catch (SQLException ex)
		{
			s_log.log(Level.SEVERE, sql, ex);
			
		}finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		
		return null;
	}  //getDateMaterialPolicy
	
	/**
	 * 	Get all Storages for Product where QtyOnHand <> 0
	 *	@param ctx context
	 *	@param M_Product_ID product
	 *	@param M_Locator_ID locator
	 *	@param trxName transaction
	 *	@return existing or null
	 */
	public static MStorageOnHand[] getOfLocator (Properties ctx, 
		int M_Product_ID, int M_Locator_ID, String trxName)
	{
		String sqlWhere = "M_Product_ID=? AND M_Locator_ID=? AND QtyOnHand <> 0";
		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
								.setParameters(M_Product_ID, M_Locator_ID)
								.setOrderBy(MStorageOnHand.COLUMNNAME_M_AttributeSetInstance_ID)
								.list(); 
		
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getAll

	/**
	 * Get storage of a product with ASI from all locator of warehouses.
	 * @param ctx
	 * @param M_Product_ID
	 * @param ASI_ID
	 * @param trxName
	 * @return
	 */
	public static MStorageOnHand[] getOfASI (Properties ctx, 
			int M_Product_ID, int ASI_ID, String trxName)
	{
		String sqlWhere = "M_Product_ID=? AND M_AttributeSetInstance_ID=? AND QtyOnHand <> 0";
		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
								.setParameters(M_Product_ID, ASI_ID)
								.setOrderBy(MStorageOnHand.COLUMNNAME_M_Locator_ID)
								.list(); 
		
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getAll
    
	/**
	 * 	Create or Get Storage Info
	 *	@param ctx context
	 *	@param M_Locator_ID locator
	 *	@param M_Product_ID product
	 *	@param M_AttributeSetInstance_ID instance
	 *	@param trxName transaction
	 *	@return existing/new or null
	 */
	public static MStorageOnHand getCreate (Properties ctx, int M_Locator_ID, 
		int M_Product_ID, int M_AttributeSetInstance_ID, String trxName)
	{
		if (M_Locator_ID == 0)
			throw new IllegalArgumentException("M_Locator_ID=0");
		if (M_Product_ID == 0)
			throw new IllegalArgumentException("M_Product_ID=0");
		
		MStorageOnHand retValue = get(ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, trxName);
		
		if (retValue != null)
			return retValue;
		//	Insert row based on locator
		MLocator locator = new MLocator (ctx, M_Locator_ID, trxName);
		if (locator.get_ID() != M_Locator_ID)
			throw new IllegalArgumentException("Not found M_Locator_ID=" + M_Locator_ID);
		//
		retValue = new MStorageOnHand (locator, M_Product_ID, M_AttributeSetInstance_ID);
		retValue.saveEx(trxName);
		if (s_log.isLoggable(Level.FINE)) s_log.fine("New " + retValue);
		return retValue;
	}	//	getCreate

	//TODO FIXME FOR QA STATUS
//	/**
//	 * Update storage quantity (add it with diffQtyOnHand).
//	 * Regarding with QA Status, only call this method if you are quite sure that the diffQtyOnHand not exceeding
//	 * the ReleaseQty when trying to decrease the quantity of this storage record.
//	 * @param ctx context
//	 * @param M_Warehouse_ID warehouse
//	 * @param M_Locator_ID locator
//	 * @param M_Product_ID product
//	 * @param M_AttributeSetInstance_ID AS Instance
//	 * @param diffQtyOnHand add on hand
//	 * @param trxName transaction
//	 * @param reservationAttributeSetInstance_ID reservation AS Instance
//	 *	@return true if updated
//	 */
//	public static boolean add_old (Properties ctx, int M_Warehouse_ID, int M_Locator_ID, 
//		int M_Product_ID, int M_AttributeSetInstance_ID,
//		BigDecimal diffQtyOnHand, String trxName)
//	{
//		if (diffQtyOnHand == null || diffQtyOnHand.signum() == 0)
//			return true;
//
//		//	Get Storage
//		MStorageOnHand storage = getCreate (ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, trxName);
//		DB.getDatabase().forUpdate(storage, 120);
//		//	Verify
//		if (storage.getM_Locator_ID() != M_Locator_ID 
//			&& storage.getM_Product_ID() != M_Product_ID
//			&& storage.getM_AttributeSetInstance_ID() != M_AttributeSetInstance_ID)
//		{
//			s_log.severe ("No Storage found - M_Locator_ID=" + M_Locator_ID 
//				+ ",M_Product_ID=" + M_Product_ID + ",ASI=" + M_AttributeSetInstance_ID);
//			return false;
//		}
//		
//		storage.setQtyOnHand (storage.getQtyOnHand().add (diffQtyOnHand));
//		
//		
//		if (s_log.isLoggable(Level.FINE)) {
//			StringBuilder diffText = new StringBuilder("(OnHand=").append(diffQtyOnHand).append(") -> ").append(storage.toString());
//			if (s_log.isLoggable(Level.FINE)) s_log.fine(diffText.toString());
//		}
//		
//		// Default QA Status is Release.
//		if (storage.getReleaseQty().add(diffQtyOnHand).compareTo(BigDecimal.ZERO) < 0)
//		{
//			s_log.severe ("Can not decreasing ReleaseQty into minus value for - M_Locator_ID=" + M_Locator_ID 
//					+ ",M_Product_ID=" + M_Product_ID + ",ASI=" + M_AttributeSetInstance_ID);
//			throw new AdempiereException("Can not decreasing ReleaseQty into minus value for - M_Locator_ID=" + M_Locator_ID 
//					+ ",M_Product_ID=" + M_Product_ID + ",ASI=" + M_AttributeSetInstance_ID);
//		}
//
//		storage.setReleaseQty(storage.getReleaseQty().add(diffQtyOnHand));
//		storage.setQAStatus(QASTATUS_Release);
//		
//		/** @author YAKA, 07/06/2013 */
//		//storage.setQAStatus(QAStatus, storage.getQtyOnHand());
//		
//		if (s_log.isLoggable(Level.FINE)) {
//			StringBuilder diffText = new StringBuilder("(OnHand=").append(diffQtyOnHand).append(") -> ").append(storage.toString());
//			if (s_log.isLoggable(Level.FINE)) s_log.fine(diffText.toString());
//		}
//		
//		// Added by Khamim on 27/05/2013 related to Pallet Modul
//		String checkQuery = "Select Count(*) From UNS_Pallet Where M_Product_ID = "+M_Product_ID+
//				" And M_AttributeSetInstance_ID = "+M_AttributeSetInstance_ID;
//		int count = DB.getSQLValue(null, checkQuery);
//		// Condition where the pallet from Combination of ASI and Product are inserted 
//		// for the first time
//		MProduct product = new MProduct(ctx, M_Product_ID, null);
//		int unitPerPallet = product.getUnitsPerPallet().intValue();
//		if(count == 0 && unitPerPallet > 0) {
//			// Make sure that it is adding process to storage
//			if(diffQtyOnHand.signum() > 0) {
//				int movementQty = diffQtyOnHand.intValue();
//				int palletTotal = movementQty/unitPerPallet;
//				if(movementQty%unitPerPallet > 0)
//					++palletTotal;
//				int start = 1;
//				int end = 0;
//				for(int i = 0; i < palletTotal; ++i) {
//					if(i >= 1)
//						start = end + 1;
//					if(i == palletTotal - 1)
//						end = start + (movementQty%unitPerPallet) - 1;
//					else
//						end = start + unitPerPallet - 1;
//					Pallet p = new Pallet((i+1), start, end, null);
//					X_UNS_Pallet pallet = new MUNSPallet(ctx, 0, null);
//					pallet.setM_Product_ID(M_Product_ID);
//					pallet.setM_AttributeSetInstance_ID(M_AttributeSetInstance_ID);
//					pallet.setM_Locator_ID(M_Locator_ID);
//					pallet.setPalletNo(p.getPalletNo());
//					pallet.setPackageIn(p.getPackageInInfo());
//					pallet.setPackageOut(p.getPackageOutInfo());
//					pallet.setReleasedPackage(p.getReleasedPackageInfo());
//					pallet.setAvailableQty(new BigDecimal(p.getAvailablePackage()));
//					pallet.setUnreleasedPackage(p.getInfo(p.getUnreleasedPackage()));
//					pallet.setReleaseQty(new BigDecimal(p.getReleasePackageQty()));
//					pallet.setOnHoldQty(BigDecimal.ZERO);
//					pallet.setNCQty(BigDecimal.ZERO);
//					pallet.save();
//				}
//			}
//		}
//		
//		
//		
//		return storage.save (trxName);
//	}	//	add
//	
//	/**
//	 * Create or Update storage quantity (add it with diffQtyOnHand).
//	 * Regarding with QA Status, only call this method if you are quite sure that the diffQtyOnHand not exceeding
//	 * the ReleaseQty when trying to decrease the quantity of this storage record.
//	 * 
//	 * @param ctx
//	 * @param M_Locator_ID
//	 * @param M_Product_ID
//	 * @param M_AttributeSetInstance_ID
//	 * @param diffQtyOnHand
//	 * @param QAStatus
//	 * @param trxName
//	 * @return true if succeed, false otherwise.
//	 */
//	public static boolean add (Properties ctx, int M_Locator_ID, 
//			int M_Product_ID, int M_AttributeSetInstance_ID,
//			BigDecimal diffQtyOnHand, String QAStatus, boolean createPallet, String trxName)
//	{
//		if (diffQtyOnHand == null || diffQtyOnHand.signum() == 0)
//			return true;
//		
//		if (null == QAStatus || "".equals(QAStatus))
//			QAStatus = QASTATUS_DEFAULT;
//
//		//	Get Storage
//		MStorageOnHand storage = getCreate (ctx, M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID, trxName);
//		DB.getDatabase().forUpdate(storage, 120);
//		//	Verify
//		if (storage.getM_Locator_ID() != M_Locator_ID 
//			&& storage.getM_Product_ID() != M_Product_ID
//			&& storage.getM_AttributeSetInstance_ID() != M_AttributeSetInstance_ID)
//		{
//			s_log.severe ("No Storage found - M_Locator_ID=" + M_Locator_ID 
//				+ ",M_Product_ID=" + M_Product_ID + ",ASI=" + M_AttributeSetInstance_ID);
//			return false;
//		}
//		
//		if (storage.getQtyOnHand().signum() == 0 
//			&& diffQtyOnHand.signum() > 0
//			&& org.compiere.model.MProduct.isStockedOnPallet(ctx, M_Product_ID)
//			&& createPallet)
//		{   // @First initial storage and must be stocked on pallet mechanism.
//			// Added by Khamim on 27/05/2013 related to Pallet Modul. Updated and moved by AzHaidar @2013.06.18.
//			PalletHelper.createPalletInfoOf(ctx, MProduct.get(ctx, M_Product_ID), M_Locator_ID, M_AttributeSetInstance_ID, 
//					diffQtyOnHand.intValue(), QAStatus, trxName);
//		}
//		
//		storage.setQtyOnHand (storage.getQtyOnHand().add (diffQtyOnHand));
//		
//		if (s_log.isLoggable(Level.FINE)) {
//			StringBuilder diffText = new StringBuilder("(OnHand=").append(diffQtyOnHand).append(") -> ").append(storage.toString());
//			if (s_log.isLoggable(Level.FINE)) s_log.fine(diffText.toString());
//		}
//		// The field QAStatu in storage will only be set to other than "release" by QA Monitoring module.
//		// In here we only set it to "release" if it's not been set yet.
//		if (null == storage.getQAStatus() || "".equals(storage.getQAStatus()))
//			storage.setQAStatus(QAStatus);
//		/** ADD Incubation in if Condition*/
//		if (null != QAStatus && !QAStatus.equals(QASTATUS_PendingInspectionLabTest) 
//				&& !QAStatus.equals(QASTATUS_Incubation)
//				&& !QASTATUS_QATested.equals(QAStatus)) 
//		{
//			BigDecimal QAStatusQty = null;
//			if (MStorageOnHand.checkQAStatus(QAStatus).equals(QASTATUS_Release)) {
//				QAStatusQty = storage.getReleaseQty();
//				storage.setReleaseQty(QAStatusQty.add(diffQtyOnHand));
//			}
//			else if (MStorageOnHand.checkQAStatus(QAStatus).equals(QASTATUS_OnHold)) {
//				QAStatusQty = storage.getOnHoldQty();
//				storage.setOnHoldQty(QAStatusQty.add(diffQtyOnHand));
//			}
//			else if (MStorageOnHand.checkQAStatus(QAStatus).equals(QASTATUS_NonConformance)) {
//				QAStatusQty = storage.getNCQty();
//				storage.setNCQty(QAStatusQty.add(diffQtyOnHand));
//			}
//			else
//				throw new AdempiereException ("Unknown QA Status of " + QAStatus);
//			
//			if (QAStatusQty.add(diffQtyOnHand).compareTo(BigDecimal.ZERO) < 0)
//			{
//				s_log.severe ("Can not decreasing quantity of QA Status-" + QAStatus 
//						+ " into minus value for - M_Locator_ID-" + M_Locator_ID 
//						+ ", M_Product_ID-" + M_Product_ID + ", ASI-" + M_AttributeSetInstance_ID);
//				return false;
//			}
//		}		
//
//		if (s_log.isLoggable(Level.FINE)) {
//			StringBuilder diffText = new StringBuilder("(OnHand=").append(diffQtyOnHand).append(") -> ").append(storage.toString());
//			if (s_log.isLoggable(Level.FINE)) s_log.fine(diffText.toString());
//		}
//		
//		return storage.save (trxName);
//	}	//	add
//
//	/**
//	 * 	Get Storage Info (with QAStatus)
//	 *	@param ctx context
//	 *	@param M_Locator_ID locator
//	 *	@param M_Product_ID product
//	 *	@param M_AttributeSetInstance_ID instance
//	 *  @param list of QAStatus 
//	 *  @param boolean for QAStatus
//	 *	@param trxName transaction
//	 *	@return existing or null
//	 *  overloading method get
//	 *  @author YAKA 10/05/2013
//	 */
//	public static MStorageOnHand get (Properties ctx, int M_Locator_ID, 
//		int M_Product_ID, int M_AttributeSetInstance_ID, String trxName, /*String[] listQAStatus,*/ boolean nextprocess)
//	{
//		String sqlWhere = "M_Locator_ID=? AND M_Product_ID=? AND QtyOnHand<>0 AND ";
//			if (M_AttributeSetInstance_ID == 0)
//				sqlWhere += "(M_AttributeSetInstance_ID=? OR M_AttributeSetInstance_ID IS NULL)";
//			else
//				sqlWhere += "M_AttributeSetInstance_ID=?";
//
//		if (nextprocess)
//			sqlWhere += " AND (QAStatus IS NOT NULL OR QAStatus <> '-')";
//		else
//			sqlWhere += " AND (QAStatus IS NULL OR QAStatus = '-')";
//		
////		if (nextprocess)
////			sqlWhere += " AND QAStatus IN ('";
////		else
////			sqlWhere += " AND QAStatus NOT IN ('";
////		
////		for(int i=0; i<listQAStatus.length; i++){
////			if (i==listQAStatus.length-1)
////				sqlWhere += listQAStatus[i] + "') ";
////			else
////				sqlWhere += listQAStatus[i] + "', '";
////		}
//		
//		
//		MStorageOnHand retValue = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
//									.setParameters(M_Locator_ID, M_Product_ID, M_AttributeSetInstance_ID/*, listQAStatus*/)
//									.first(); 
//		
//		if (retValue == null)
//			if (s_log.isLoggable(Level.FINE)) s_log.fine("Not Found - M_Locator_ID=" + M_Locator_ID 
//				+ ", M_Product_ID=" + M_Product_ID + ", M_AttributeSetInstance_ID=" + M_AttributeSetInstance_ID);
//		else
//			if (s_log.isLoggable(Level.FINE)) s_log.fine("M_Locator_ID=" + M_Locator_ID 
//				+ ", M_Product_ID=" + M_Product_ID + ", M_AttributeSetInstance_ID=" + M_AttributeSetInstance_ID);
//		return retValue;
//	}	//	get
//	
//	/**
//	 * Get storage for the specified QAStatus with QAStatus non-null or non-zero quantity.
//	 * @param ctx
//	 * @param M_Locator_ID
//	 * @param M_Product_ID
//	 * @param QAStatus
//	 * @param trxName
//	 * @return An empty array of MStorageOnHand if no storage is found, or found but with null 
//	 * 		   or zero quantity for the specified QAStatus.
//	 */
//	public static MStorageOnHand[] getOfQAStatus (Properties ctx, int M_Locator_ID, 
//			int M_Product_ID, String QAStatus, String trxName)
//	{
//		String sqlWhere = "M_Locator_ID=? AND M_Product_ID=? ";
//		if(QASTATUS_PendingInspectionLabTest.equals(QAStatus) 
//				|| QASTATUS_Incubation.equals(QAStatus))
//			sqlWhere += " AND QAStatus = '"+ QASTATUS_PendingInspectionLabTest + 
//						"' OR QAStatus= '" + QASTATUS_Incubation;
//		else if (QASTATUS_NonConformance.equals(QAStatus))
//			sqlWhere += " AND (NCQty IS NOT NULL OR NCQty<>0)";
//		else if (QASTATUS_OnHold.equals(QAStatus))
//			sqlWhere += " AND (OnHoldQty IS NOT NULL OR OnHoldQty<>0)";
//		else //(QASTATUS_Release.equals(QAStatus))
//			sqlWhere += " AND (ReleaseQty IS NOT NULL OR ReleaseQty<>0)";
//		
//		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
//									.setParameters(M_Locator_ID, M_Product_ID)
//									.list(); 
//		
//		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
//		list.toArray(retValue);
//
//		return retValue;
//	}	//	get
//
//	/**
//	 * List of QAStatus which can't to next process
//	 * @author YAKA 10/05/2013
//	 * added QA Status IC (20140221)
//	 */
//	public static String[] PoorQAStatus = {"RJ", "PI", "PL", "OH", "NC","IC"};
//	
//	/**
//	 * List of release QAStatus
//	 * @author YAKA 10/05/2013
//	 */
//	public static String[] ReleaseQAStatus = {"RE", "PR"};
//
//	/**
//	 * 	Get all Storages for Product where QtyOnHand <> 0
//	 *	@param ctx context
//	 *	@param M_Product_ID product
//	 *	@param M_Locator_ID locator
//	 *	@param trxName transaction
//	 *  @param list of QAStatus 
//	 *  @param boolean for QAStatus
//	 *	@return existing or null
//	 *  overloading method getAll
//	 *  @author YAKA 10/05/2013
//	 */
//	public static MStorageOnHand[] getAll (Properties ctx, 
//		int M_Product_ID, int M_Locator_ID, String trxName, /*String[] listQAStatus,*/ boolean nextprocess)
//	{
//		return getAll(ctx, M_Product_ID, M_Locator_ID, trxName, /*listQAStatus, */nextprocess, null);
//		/*
//		String sqlWhere = "M_Product_ID=? AND M_Locator_ID=? AND QtyOnHand <> 0";
//		
//		if (nextprocess)
//			sqlWhere += " AND QAStatus IN (";
//		else
//			sqlWhere += " AND QAStatus NOT IN ('";
//		
//		for(int i=0; i<listQAStatus.length; i++){
//			if (i==listQAStatus.length-1)
//				sqlWhere += listQAStatus[i] + "') ";
//			else
//				sqlWhere += listQAStatus[i] + "', '";
//		}
//		
//		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
//								.setParameters(M_Product_ID, M_Locator_ID)
//								.setOrderBy(MStorageOnHand.COLUMNNAME_M_AttributeSetInstance_ID)
//								.list(); 
//		
//		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
//		list.toArray(retValue);
//		return retValue;
//		*/
//	}	//	getAll
//	
//	/**
//	 * 
//	 * @param ctx
//	 * @param M_Product_ID
//	 * @param M_Locator_ID
//	 * @param trxName
//	 * @param listQAStatus
//	 * @param nextprocess
//	 * @param ofQAStatusQty
//	 * @return
//	 */
//	public static MStorageOnHand[] getAll (Properties ctx, 
//			int M_Product_ID, int M_Locator_ID, String trxName, 
//			/*String[] listQAStatus,*/ boolean nextprocess, String ofQAStatusQty)
//	{
//		String sqlWhere = "M_Product_ID=? AND M_Locator_ID=? AND QtyOnHand <> 0";
//		
//		if (nextprocess)
//			sqlWhere += " AND ((QAStatus IS NOT NULL OR QAStatus <> '-')";
//		else
//			sqlWhere += " AND ((QAStatus IS NULL OR QAStatus = '-')";
//		
////		if (nextprocess)
////			sqlWhere += " AND (QAStatus IN (";
////		else
////			sqlWhere += " AND (QAStatus NOT IN ('";
////		
////		for(int i=0; i<listQAStatus.length; i++){
////			if (i==listQAStatus.length-1)
////				sqlWhere += listQAStatus[i] + "') ";
////			else
////				sqlWhere += listQAStatus[i] + "', '";
////		}
//		if (ofQAStatusQty != null && !"-".equals(ofQAStatusQty) && !ofQAStatusQty.equals("")) {
//			if (QASTATUS_Release.equals(ofQAStatusQty))
//				sqlWhere += " OR ReleaseQty IS NOT NULL OR ReleaseQty<>0";
//			else if (QASTATUS_NonConformance.equals(ofQAStatusQty))
//				sqlWhere += " OR NCQty IS NOT NULL OR NCQty<>0";
//			else if (QASTATUS_OnHold.equals(ofQAStatusQty))
//				sqlWhere += " OR OnHoldQty IS NOT NULL OR OnHoldQty<>0";
//		}
//		sqlWhere += ")";
//		
//		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
//								.setParameters(M_Product_ID, M_Locator_ID)
//								.setOrderBy(MStorageOnHand.COLUMNNAME_M_AttributeSetInstance_ID)
//								.list(); 
//		
//		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
//		list.toArray(retValue);
//		return retValue;
//	}	//	getAll
//		
//	/**
//	 * Add (positive QAQty) or subtract (negate QAQty) of storage.
//	 * Throw AdempiereException if no storage found. 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param QAQty
//	 * @param QAStatus
//	 * @param trxName
//	 */
//	public static void addQAStatusQtyOf (int productID, int locatorID, int asiID, 
//			BigDecimal QAQty, String QAStatus, String trxName) 
//	{
//		String columnQAStatus = null;
//		if (QASTATUS_Release.equals(QAStatus))
//			columnQAStatus = COLUMNNAME_ReleaseQty;
//		else if (QASTATUS_OnHold.equals(QAStatus))
//			columnQAStatus = COLUMNNAME_OnHoldQty;
//		else if (QASTATUS_NonConformance.equals(QAStatus))
//			columnQAStatus = COLUMNNAME_NCQty;
//		else
//			throw new AdempiereException("Unknown QA Status for " + QAStatus);
//		
//		String sql = "SELECT " + columnQAStatus + " FROM M_StorageOnHand " +
//				" WHERE M_Product_ID=? And M_Locator_ID? And M_AttributeSetInstance_ID=?";
//		
//		BigDecimal QAStatusQty = DB.getSQLValueBD(trxName, sql, productID, locatorID, asiID);
//		
//		if (null == QAStatusQty)
//			throw new AdempiereException("Not found storage for Product[" + productID + 
//					"]-Locator[" + locatorID + "]-AttributeSetInstance[" + asiID + "]");
//		QAStatusQty = QAStatusQty.add(QAQty);
//		
//		if (QASTATUS_Release.equals(QAStatus))
//			setReleaseQty(productID, locatorID, asiID, QAStatusQty, trxName);
//		else if (QASTATUS_OnHold.equals(QAStatus))
//			setOnholdQty(productID, locatorID, asiID, QAStatusQty, trxName);
//		else if (QASTATUS_NonConformance.equals(QAStatus))
//			setNCQty(productID, locatorID, asiID, QAStatusQty, trxName);
//	}
//	
//	/******************** Added by khamim on 04/06/2013 *****************************/
//	/**
//	 * Set new OnHoldQty.
//	 * 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param newOnHoldQty
//	 * @param trxName
//	 */
//	public static void setOnholdQty(int productID, int locatorID, int asiID, 
//			BigDecimal newOnHoldQty, String trxName) {
//		String query = "Update "+I_M_StorageOnHand.Table_Name+" Set "+
//				I_M_StorageOnHand.COLUMNNAME_OnHoldQty+" = "+newOnHoldQty+" Where "+
//				"M_Product_ID = "+productID+" And M_Locator_ID = "+locatorID+" And "+
//				"M_AttributeSetInstance_ID = "+asiID;
//		setQtyNeeded(query, trxName);
//	}
//	
//	/**
//	 * Get OnHold quantity for the given product of an ASI_ID in the given locator.
//	 * 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param trxName
//	 * @return The quantity of the product detail, or BigDecimal.ZERO if the product detail can not be found.
//	 */
//	public static BigDecimal getOnHoldQty(int productID, int locatorID, int asiID, String trxName) {
//		String query = "Select "+I_M_StorageOnHand.COLUMNNAME_OnHoldQty+" From "+
//				I_M_StorageOnHand.Table_Name+" Where M_Product_ID = "+productID+" And "+
//				"M_Locator_ID = "+locatorID+" And M_AttributeSetInstance_ID = "+asiID;
//		BigDecimal onHoldQty = getQtyNeeded(query, trxName);
//		
//		return onHoldQty;
//	}
//	
//	/**
//	 * Set new Release Qty.
//	 * 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param newReleaseQty
//	 * @param trxName
//	 */
//	public static void setReleaseQty(int productID, int locatorID, int asiID, BigDecimal newReleaseQty, String trxName) {
//		String query = "Update "+I_M_StorageOnHand.Table_Name+" Set "+
//				I_M_StorageOnHand.COLUMNNAME_ReleaseQty+" = "+newReleaseQty+" Where "+
//				"M_Product_ID = "+productID+" And M_Locator_ID = "+locatorID+" And "+
//				"M_AttributeSetInstance_ID = "+asiID;
//		setQtyNeeded(query, trxName);
//	}
//	
//	/**
//	 * Get Released quantity for the given product of an ASI_ID in the given locator.
//	 * 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param trxName
//	 * @return The quantity of the product detail, or BigDecimal.ZERO if the product detail can not be found.
//	 */
//	public static BigDecimal getReleaseQty(int productID, int locatorID, int asiID, String trxName) {
//		String query = "Select "+I_M_StorageOnHand.COLUMNNAME_ReleaseQty+" From "+
//				I_M_StorageOnHand.Table_Name+" Where M_Product_ID = "+productID+" And "+
//				"M_Locator_ID = "+locatorID+" And M_AttributeSetInstance_ID = "+asiID;
//		BigDecimal releaseQty = getQtyNeeded(query, trxName);
//		
//		return releaseQty;
//	}
//	
//	/**
//	 * Set new NC Qty.
//	 * 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param newNCQty
//	 * @param trxName
//	 */
//	public static void setNCQty(int productID, int locatorID, int asiID, BigDecimal newNCQty, String trxName) {
//		String query = "Update "+I_M_StorageOnHand.Table_Name+" Set "+
//				I_M_StorageOnHand.COLUMNNAME_NCQty+" = "+newNCQty+" Where "+
//				"M_Product_ID = "+productID+" And M_Locator_ID = "+locatorID+" And "+
//				"M_AttributeSetInstance_ID = "+asiID;
//		setQtyNeeded(query, trxName);
//	}
//	
//	/**
//	 * Get Non-Conformance (NC) quantity for the given product of an ASI_ID in the given locator.
//	 * 
//	 * @param productID
//	 * @param locatorID
//	 * @param asiID
//	 * @param trxName
//	 * @return The quantity of the product detail, or BigDecimal.ZERO if the product detail can not be found.
//	 */
//	public static BigDecimal getNCQty(int productID, int locatorID, int asiID, String trxName) {
//		String query = "Select "+I_M_StorageOnHand.COLUMNNAME_NCQty+" From "+
//				I_M_StorageOnHand.Table_Name+" Where M_Product_ID = "+productID+" And "+
//				"M_Locator_ID = "+locatorID+" And M_AttributeSetInstance_ID = "+asiID;
//		BigDecimal ncQty = getQtyNeeded(query, trxName);
//		
//		return ncQty;
//	}
//	
//	/**
//	 * Get the needed Quantity for the StorageOnHand of a specified QAStatus inherited in the query string.
//	 *  
//	 * @param query
//	 * @param trxName
//	 * @return The quantity of the product detail, or BigDecimal.ZERO if the product detail can not be found.
//	 */
//	protected static BigDecimal getQtyNeeded(String query, String trxName) {
//		BigDecimal qty = BigDecimal.ZERO;
//		try {
//			PreparedStatement pst = DB.prepareStatement(query, trxName);
//			ResultSet rs = pst.executeQuery();
//			if(rs.next())
//				qty = rs.getBigDecimal(1);
//			pst.close();
//			rs.close();
//		} catch(SQLException ex) {
//			new AdempiereException("Error while trying to execute query");
//		}
//		
//		return qty;
//	}
//	
//	/**
//	 * Set the needed quantity for the given query String.
//	 * 
//	 * @param query
//	 * @param trxName
//	 */
//	protected static void setQtyNeeded(String query, String trxName) {
//		try {
//			PreparedStatement pst = DB.prepareStatement(query, trxName);
//			pst.executeUpdate();
//			pst.close();
//		} catch(SQLException e) {
//			throw new AdempiereException("Error while trying to update Storage. " + e.getMessage());
//		}
//	}
//	
//	/**
//	 * 
//	 * @param ctx
//	 * @param product
//	 * @param M_Locator_ID
//	 * @param M_ASI_ID
//	 * @return
//	 */
//	public boolean isInsideIncubationPeriod()
//	{
//		if (isPrematureReleased())
//			return false;
//		
//		return ((MProduct) getM_Product()).isInsideIncubationPeriod(getM_AttributeSetInstance_ID());
//	}
//	
//	public static String checkQAStatus(String QAStatus){
//		if(null == QAStatus)
//			QAStatus = QASTATUS_DEFAULT;
//		if (QAStatus.equals(QASTATUS_Release) || 
//				QAStatus.equals(QASTATUS_PrematureReleased)
//				|| QASTATUS_NonQA.equals(QAStatus)){
//			return QASTATUS_Release;
//		} else if(QAStatus.equals(QASTATUS_OnHold))
//		{
//			return QASTATUS_OnHold;
//		} else if(QAStatus.equals(QASTATUS_NonConformance))
//			return QASTATUS_NonConformance;
//		else
//			return QAStatus;
//		
//	}
//	
//	private static void setZeroQty(MStorageOnHand soh, String trxName) {
//		MStorageOnHand.setReleaseQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//				soh.getM_AttributeSetInstance_ID(), BigDecimal.ZERO, trxName);
//		MStorageOnHand.setOnholdQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//				soh.getM_AttributeSetInstance_ID(), BigDecimal.ZERO, trxName);
//		MStorageOnHand.setNCQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//				soh.getM_AttributeSetInstance_ID(), BigDecimal.ZERO, trxName);
//	}
//	
//	public static void setQAStatus(String QAStatus, MStorageOnHand soh, BigDecimal qty,
//			String trxName)
//	{
//
//		MStorageOnHand.setZeroQty(soh, trxName);
//		
//		if (checkQAStatus(QAStatus).equals(QASTATUS_Release)){
//			MStorageOnHand.setReleaseQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//					soh.getM_AttributeSetInstance_ID(), qty, trxName);
//		} else if (checkQAStatus(QAStatus).equals(QASTATUS_OnHold)){
//			MStorageOnHand.setOnholdQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//					soh.getM_AttributeSetInstance_ID(), qty, trxName);
//		} else {
//			MStorageOnHand.setNCQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//					soh.getM_AttributeSetInstance_ID(), qty, trxName);
//		}
//		
//	}
//
//	/**
//	 * before set QA Status please set Quantity On Hand first.
//	 */
//	public void setQAStatus(String QAStatus, BigDecimal diffQty) {
//		setQAStatus(QAStatus);
//		
//		MStorageOnHand.setQAStatus(MStorageOnHand.checkQAStatus(QAStatus), this, 
//				diffQty, get_TrxName());
//	}
//	
//	/**
//	 * 
//	 * @param QAStatus
//	 * @return
//	 */
//	public String initQAStatus(String QAStatus)
//	{
//		String status = QASTATUS_DEFAULT;
//		if(QASTATUS_PendingInspectionLabTest.equals(QAStatus))
//			status = QAStatus;
//		else if(QASTATUS_Incubation.equals(QAStatus))
//			status = QAStatus;
//		else if(QASTATUS_QATested.equals(QAStatus))
//			status = QAStatus;
//		
//		if(QASTATUS_NonQA.equals(status))
//		{
//			I_M_Product_Category prodCat = getM_Product().getM_Product_Category();
//			if(!QASTATUS_NonQA.equals(prodCat.getInitialQAStatus()))
//				status = QASTATUS_QATested;
//		}
//			
//		return status;
//	}
//	
//	/**
//	 * 
//	 */
//	@Override
//	public void setQAStatus(String QAStatus)
//	{
//		super.setQAStatus(initQAStatus(QAStatus));
//	}
//	
//	public void setQtyQAStatus(BigDecimal releaseQty, BigDecimal onHoldQty, BigDecimal NCQty) {
//		MStorageOnHand.setReleaseQty(getM_Product_ID(), getM_Locator_ID(), 
//			getM_AttributeSetInstance_ID(), releaseQty, get_TrxName());
//		MStorageOnHand.setOnholdQty(getM_Product_ID(), getM_Locator_ID(), 
//			getM_AttributeSetInstance_ID(), onHoldQty, get_TrxName());
//		MStorageOnHand.setNCQty(getM_Product_ID(), getM_Locator_ID(), 
//			getM_AttributeSetInstance_ID(), NCQty, get_TrxName());
//	}
//	
//	/**
//	 * 
//	 * @param QAStatus
//	 * @param soh
//	 * @param trxName
//	 * @return
//	 */
//	public static BigDecimal getQtyOnHand(String QAStatus, MStorageOnHand soh, String trxName) {
//		if (checkQAStatus(QAStatus).equals(QASTATUS_Release)){
//			return MStorageOnHand.getReleaseQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//					soh.getM_AttributeSetInstance_ID(), trxName);
//		} else if (checkQAStatus(QAStatus).equals(QASTATUS_OnHold)){
//			return MStorageOnHand.getOnHoldQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//					soh.getM_AttributeSetInstance_ID(), trxName);
//		} else if(checkQAStatus(QAStatus).equals(QASTATUS_NonConformance)){
//			return MStorageOnHand.getNCQty(soh.getM_Product_ID(), soh.getM_Locator_ID(), 
//					soh.getM_AttributeSetInstance_ID(), trxName);
//		}
//		else
//			return soh.getQtyOnHand();
//	}
//	
//	/**
//	 * 
//	 * @param QAStatus
//	 * @param soh
//	 * @param qtyCompare
//	 * @param trxName
//	 * @return
//	 */
//	public static boolean checkQtyBaseQAStatus(String QAStatus, MStorageOnHand soh,
//			 BigDecimal qtyCompare, String trxName) 
//	{
//		if (QASTATUS_PendingInspectionLabTest.equals(QAStatus)
//				|| QASTATUS_Incubation.equals(QAStatus))
//		{
//			if (MStorageOnHand.getQtyOnHand(QASTATUS_Release, soh, trxName).compareTo(qtyCompare) > 0
//				|| MStorageOnHand.getQtyOnHand(QASTATUS_OnHold, soh, trxName).compareTo(qtyCompare) > 0
//				|| MStorageOnHand.getQtyOnHand(QASTATUS_NonConformance, soh, trxName).compareTo(qtyCompare) > 0)
//				return false;
//			else if (soh.getQtyOnHand().compareTo(qtyCompare) < 0)
//				return false;
//			else
//				return true;
//		}
//		if (MStorageOnHand.getQtyOnHand(QAStatus, soh, trxName).compareTo(qtyCompare)<0)
//			return false;
//		else
//			return true;
//	}
//	
//	/**
//	 * Get on hand quantity for the expected product at a locator.
//	 * 
//	 * @param ctx
//	 * @param M_Locator_ID
//	 * @param M_Product_ID
//	 * @param trxName
//	 * @return The available on hand quantity or null.
//	 */
//	public static BigDecimal getOnHandOfProductLocator(int M_Locator_ID, 
//			int M_Product_ID, String trxName)
//	{
//		String sql = "SELECT SUM(QtyOnHand) FROM M_StorageOnHand " +
//				" WHERE M_Locator_ID=? AND M_Product_ID=? ";
//		
//		BigDecimal qtyOnHand = DB.getSQLValueBD(trxName, sql, M_Locator_ID, M_Product_ID);
//		
//		return qtyOnHand;
//	}
//	
//	/**
//	 * Get on hand quantity for the expected product of an Organization.
//	 * @param ctx
//	 * @param AD_Org_ID
//	 * @param M_Product_ID
//	 * @param trxName
//	 * @return The available on hand quantity or null.
//	 */
//	public static BigDecimal getOnHandOfProductOrg (Properties ctx, int AD_Org_ID, int M_Product_ID, String trxName)
//	{
//		String sql = "SELECT SUM(QtyOnHand) FROM M_StorageOnHand WHERE M_Product_ID=? ";
//				//" WHERE AD_Client_ID=? AND M_Product_ID=? ";
//		if (AD_Org_ID > 0)
//			sql += "AND AD_Org_ID=" + AD_Org_ID;
//		
//		BigDecimal qtyOnHand = DB.getSQLValueBD(trxName, sql, 
//												//Integer.valueOf(ctx.getProperty("#" +MStorageOnHand.COLUMNNAME_AD_Client_ID)),
//												M_Product_ID);
//		
//		return qtyOnHand;
//	}
//	
//	@Override
//	public String getQAStatus()
//	{
//		return super.getQAStatus();
////		return super.getQAStatus();
//	}
//	
//	public boolean isFinalQAStatus()
//	{
//		
//		return (!QASTATUS_PendingInspectionLabTest.equals(getQAStatus()) 
//				|| !QASTATUS_Incubation.equals(getQAStatus()));
//	}
	
    /**
	 * Get all storage of locator where QtyOnHand <> 0
	 * @param ctx
	 * @param M_Locator_ID
	 * @param trxName
	 * @return
	 */
	public static MStorageOnHand[] getOfLocator (Properties ctx, int M_Locator_ID, String trxName)
	{
		String sqlWhere = "M_Locator_ID=? AND QtyOnHand <> 0";
		List<MStorageOnHand> list = new Query(ctx, MStorageOnHand.Table_Name, sqlWhere, trxName)
								.setParameters(M_Locator_ID)
								.setOrderBy(MStorageOnHand.COLUMNNAME_M_AttributeSetInstance_ID)
								.list(); 
		
		MStorageOnHand[] retValue = new MStorageOnHand[list.size()];
		list.toArray(retValue);
		return retValue;
	}	//	getAll of locator
    
	/**
	 * Get all storage of locator where QtyOnHand <> 0
	 * @param ctx
	 * @param M_Product_ID
	 * @param M_AttributeSetInstance_ID
	 * @param M_Locator_ID
	 * @param trxName
	 * @return datempolicy timestamp
	 */
	public static Timestamp getDateMaterialPolicy(int M_Product_ID, int M_AttributeSetInstance_ID, int M_Locator_ID, String trxName){
		
		if (M_Product_ID <= 0  || M_AttributeSetInstance_ID <= 0)
			return null;
		
		String sql = "SELECT dateMaterialPolicy FROM M_StorageOnHand WHERE M_Product_ID=? and M_AttributeSetInstance_ID=? AND M_Locator_ID=?";
		
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement(sql, trxName);
			pstmt.setInt(1, M_Product_ID);
			pstmt.setInt(2, M_AttributeSetInstance_ID);
			pstmt.setInt(3, M_Locator_ID);
			
			rs = pstmt.executeQuery();
			if (rs.next())
			{
				return rs.getTimestamp(1);
			}
		} catch (SQLException ex)
		{
			s_log.log(Level.SEVERE, sql, ex);
			
		}finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		
		
		return null;
	}  //getDateMaterialPolicy
}	//	MStorageOnHand

/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package org.compiere.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

/** Generated Model for M_MovementLineConfirm
 *  @author iDempiere (generated) 
 *  @version Release 5.1 - $Id$ */
public class X_M_MovementLineConfirm extends PO implements I_M_MovementLineConfirm, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171031L;

    /** Standard Constructor */
    public X_M_MovementLineConfirm (Properties ctx, int M_MovementLineConfirm_ID, String trxName)
    {
      super (ctx, M_MovementLineConfirm_ID, trxName);
      /** if (M_MovementLineConfirm_ID == 0)
        {
			setConfirmedQty (Env.ZERO);
			setDifferenceQty (Env.ZERO);
			setM_MovementConfirm_ID (0);
			setM_MovementLineConfirm_ID (0);
			setM_MovementLine_ID (0);
			setM_MovementLineConfirm_ID (0);
			setProcessed (false);
			setScrappedQty (Env.ZERO);
			setTargetQty (Env.ZERO);
        } */
    }

    /** Load Constructor */
    public X_M_MovementLineConfirm (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 1 - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_M_MovementLineConfirm[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_C_InvoiceLine getC_InvoiceLine() throws RuntimeException
    {
		return (org.compiere.model.I_C_InvoiceLine)MTable.get(getCtx(), org.compiere.model.I_C_InvoiceLine.Table_Name)
			.getPO(getC_InvoiceLine_ID(), get_TrxName());	}

	/** Set Invoice Line.
		@param C_InvoiceLine_ID 
		Invoice Detail Line
	  */
	public void setC_InvoiceLine_ID (int C_InvoiceLine_ID)
	{
		if (C_InvoiceLine_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_InvoiceLine_ID, Integer.valueOf(C_InvoiceLine_ID));
	}

	/** Get Invoice Line.
		@return Invoice Detail Line
	  */
	public int getC_InvoiceLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_InvoiceLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getC_UOM() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getC_UOM_ID(), get_TrxName());	}

	/** Set UOM.
		@param C_UOM_ID 
		Unit of Measure
	  */
	public void setC_UOM_ID (int C_UOM_ID)
	{
		throw new IllegalArgumentException ("C_UOM_ID is virtual column");	}

	/** Get UOM.
		@return Unit of Measure
	  */
	public int getC_UOM_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_UOM_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Confirmed Quantity.
		@param ConfirmedQty 
		Confirmation of a received quantity
	  */
	public void setConfirmedQty (BigDecimal ConfirmedQty)
	{
		set_Value (COLUMNNAME_ConfirmedQty, ConfirmedQty);
	}

	/** Get Confirmed Quantity.
		@return Confirmation of a received quantity
	  */
	public BigDecimal getConfirmedQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ConfirmedQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Difference.
		@param DifferenceQty 
		Difference Quantity
	  */
	public void setDifferenceQty (BigDecimal DifferenceQty)
	{
		set_Value (COLUMNNAME_DifferenceQty, DifferenceQty);
	}

	/** Get Difference.
		@return Difference Quantity
	  */
	public BigDecimal getDifferenceQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_DifferenceQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_InventoryLine getM_InventoryLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_InventoryLine)MTable.get(getCtx(), org.compiere.model.I_M_InventoryLine.Table_Name)
			.getPO(getM_InventoryLine_ID(), get_TrxName());	}

	/** Set Phys.Inventory Line.
		@param M_InventoryLine_ID 
		Unique line in an Inventory document
	  */
	public void setM_InventoryLine_ID (int M_InventoryLine_ID)
	{
		if (M_InventoryLine_ID < 1) 
			set_Value (COLUMNNAME_M_InventoryLine_ID, null);
		else 
			set_Value (COLUMNNAME_M_InventoryLine_ID, Integer.valueOf(M_InventoryLine_ID));
	}

	/** Get Phys.Inventory Line.
		@return Unique line in an Inventory document
	  */
	public int getM_InventoryLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_InventoryLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_Locator getM_LocatorTo() throws RuntimeException
    {
		return (org.compiere.model.I_M_Locator)MTable.get(getCtx(), org.compiere.model.I_M_Locator.Table_Name)
			.getPO(getM_LocatorTo_ID(), get_TrxName());	}

	/** Set Locator To.
		@param M_LocatorTo_ID 
		Location inventory is moved to
	  */
	public void setM_LocatorTo_ID (int M_LocatorTo_ID)
	{
		if (M_LocatorTo_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_LocatorTo_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_LocatorTo_ID, Integer.valueOf(M_LocatorTo_ID));
	}

	/** Get Locator To.
		@return Location inventory is moved to
	  */
	public int getM_LocatorTo_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_LocatorTo_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_M_MovementConfirm getM_MovementConfirm() throws RuntimeException
    {
		return (org.compiere.model.I_M_MovementConfirm)MTable.get(getCtx(), org.compiere.model.I_M_MovementConfirm.Table_Name)
			.getPO(getM_MovementConfirm_ID(), get_TrxName());	}

	/** Set Move Confirm.
		@param M_MovementConfirm_ID 
		Inventory Move Confirmation
	  */
	public void setM_MovementConfirm_ID (int M_MovementConfirm_ID)
	{
		if (M_MovementConfirm_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_MovementConfirm_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_MovementConfirm_ID, Integer.valueOf(M_MovementConfirm_ID));
	}

	/** Get Move Confirm.
		@return Inventory Move Confirmation
	  */
	public int getM_MovementConfirm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_MovementConfirm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

    /** Get Record ID/ColumnName
        @return ID/ColumnName pair
      */
    public KeyNamePair getKeyNamePair() 
    {
        return new KeyNamePair(get_ID(), String.valueOf(getM_MovementConfirm_ID()));
    }

	public org.compiere.model.I_M_MovementLine getM_MovementLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_MovementLine)MTable.get(getCtx(), org.compiere.model.I_M_MovementLine.Table_Name)
			.getPO(getM_MovementLine_ID(), get_TrxName());	}

	/** Set Move Line.
		@param M_MovementLine_ID 
		Inventory Move document Line
	  */
	public void setM_MovementLine_ID (int M_MovementLine_ID)
	{
		if (M_MovementLine_ID < 1) 
			set_Value (COLUMNNAME_M_MovementLine_ID, null);
		else 
			set_Value (COLUMNNAME_M_MovementLine_ID, Integer.valueOf(M_MovementLine_ID));
	}

	/** Get Move Line.
		@return Inventory Move document Line
	  */
	public int getM_MovementLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_MovementLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Move Line Confirm.
		@param M_MovementLineConfirm_ID 
		Inventory Move Line Confirmation
	  */
	public void setM_MovementLineConfirm_ID (int M_MovementLineConfirm_ID)
	{
		if (M_MovementLineConfirm_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_MovementLineConfirm_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_MovementLineConfirm_ID, Integer.valueOf(M_MovementLineConfirm_ID));
	}

	/** Get Move Line Confirm.
		@return Inventory Move Line Confirmation
	  */
	public int getM_MovementLineConfirm_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_MovementLineConfirm_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set M_MovementLineConfirm_UU.
		@param M_MovementLineConfirm_UU M_MovementLineConfirm_UU	  */
	public void setM_MovementLineConfirm_UU (String M_MovementLineConfirm_UU)
	{
		set_Value (COLUMNNAME_M_MovementLineConfirm_UU, M_MovementLineConfirm_UU);
	}

	/** Get M_MovementLineConfirm_UU.
		@return M_MovementLineConfirm_UU	  */
	public String getM_MovementLineConfirm_UU () 
	{
		return (String)get_Value(COLUMNNAME_M_MovementLineConfirm_UU);
	}

	public org.compiere.model.I_M_Product getM_Product() throws RuntimeException
    {
		return (org.compiere.model.I_M_Product)MTable.get(getCtx(), org.compiere.model.I_M_Product.Table_Name)
			.getPO(getM_Product_ID(), get_TrxName());	}

	/** Set Product.
		@param M_Product_ID 
		Product, Service, Item
	  */
	public void setM_Product_ID (int M_Product_ID)
	{
		throw new IllegalArgumentException ("M_Product_ID is virtual column");	}

	/** Get Product.
		@return Product, Service, Item
	  */
	public int getM_Product_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Product_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	public org.compiere.model.I_M_MovementLineConfirm getReversalLine() throws RuntimeException
    {
		return (org.compiere.model.I_M_MovementLineConfirm)MTable.get(getCtx(), org.compiere.model.I_M_MovementLineConfirm.Table_Name)
			.getPO(getReversalLine_ID(), get_TrxName());	}

	/** Set Reversal Line.
		@param ReversalLine_ID 
		Use to keep the reversal line ID for reversing costing purpose
	  */
	public void setReversalLine_ID (int ReversalLine_ID)
	{
		if (ReversalLine_ID < 1) 
			set_Value (COLUMNNAME_ReversalLine_ID, null);
		else 
			set_Value (COLUMNNAME_ReversalLine_ID, Integer.valueOf(ReversalLine_ID));
	}

	/** Get Reversal Line.
		@return Use to keep the reversal line ID for reversing costing purpose
	  */
	public int getReversalLine_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_ReversalLine_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Scrapped Quantity.
		@param ScrappedQty 
		The Quantity scrapped due to QA issues
	  */
	public void setScrappedQty (BigDecimal ScrappedQty)
	{
		set_Value (COLUMNNAME_ScrappedQty, ScrappedQty);
	}

	/** Get Scrapped Quantity.
		@return The Quantity scrapped due to QA issues
	  */
	public BigDecimal getScrappedQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ScrappedQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Target Quantity.
		@param TargetQty 
		Target Movement Quantity
	  */
	public void setTargetQty (BigDecimal TargetQty)
	{
		set_Value (COLUMNNAME_TargetQty, TargetQty);
	}

	/** Get Target Quantity.
		@return Target Movement Quantity
	  */
	public BigDecimal getTargetQty () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_TargetQty);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Confirmed Qty L1.
		@param UOMConfirmedQtyL1 Confirmed Qty L1	  */
	public void setUOMConfirmedQtyL1 (BigDecimal UOMConfirmedQtyL1)
	{
		set_Value (COLUMNNAME_UOMConfirmedQtyL1, UOMConfirmedQtyL1);
	}

	/** Get Confirmed Qty L1.
		@return Confirmed Qty L1	  */
	public BigDecimal getUOMConfirmedQtyL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMConfirmedQtyL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Confirmed Qty L2.
		@param UOMConfirmedQtyL2 Confirmed Qty L2	  */
	public void setUOMConfirmedQtyL2 (BigDecimal UOMConfirmedQtyL2)
	{
		set_Value (COLUMNNAME_UOMConfirmedQtyL2, UOMConfirmedQtyL2);
	}

	/** Get Confirmed Qty L2.
		@return Confirmed Qty L2	  */
	public BigDecimal getUOMConfirmedQtyL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMConfirmedQtyL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Confirmed Qty L3.
		@param UOMConfirmedQtyL3 Confirmed Qty L3	  */
	public void setUOMConfirmedQtyL3 (BigDecimal UOMConfirmedQtyL3)
	{
		set_Value (COLUMNNAME_UOMConfirmedQtyL3, UOMConfirmedQtyL3);
	}

	/** Get Confirmed Qty L3.
		@return Confirmed Qty L3	  */
	public BigDecimal getUOMConfirmedQtyL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMConfirmedQtyL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Confirmed Qty L4.
		@param UOMConfirmedQtyL4 Confirmed Qty L4	  */
	public void setUOMConfirmedQtyL4 (BigDecimal UOMConfirmedQtyL4)
	{
		set_Value (COLUMNNAME_UOMConfirmedQtyL4, UOMConfirmedQtyL4);
	}

	/** Get Confirmed Qty L4.
		@return Confirmed Qty L4	  */
	public BigDecimal getUOMConfirmedQtyL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMConfirmedQtyL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_C_UOM getUOMConversionL1() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL1_ID(), get_TrxName());	}

	/** Set UOM Conversion L1.
		@param UOMConversionL1_ID 
		The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public void setUOMConversionL1_ID (int UOMConversionL1_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL1_ID is virtual column");	}

	/** Get UOM Conversion L1.
		@return The conversion of the product's base UOM to the biggest package (Level 1)
	  */
	public int getUOMConversionL1_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL1_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL2() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL2_ID(), get_TrxName());	}

	/** Set UOM Conversion L2.
		@param UOMConversionL2_ID 
		The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public void setUOMConversionL2_ID (int UOMConversionL2_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL2_ID is virtual column");	}

	/** Get UOM Conversion L2.
		@return The conversion of the product's base UOM to the number 2 level package (if defined)
	  */
	public int getUOMConversionL2_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL2_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL3() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL3_ID(), get_TrxName());	}

	/** Set UOM Conversion L3.
		@param UOMConversionL3_ID 
		The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public void setUOMConversionL3_ID (int UOMConversionL3_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL3_ID is virtual column");	}

	/** Get UOM Conversion L3.
		@return The conversion of the product's base UOM to the number 3 level package (if defined)
	  */
	public int getUOMConversionL3_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL3_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_UOM getUOMConversionL4() throws RuntimeException
    {
		return (org.compiere.model.I_C_UOM)MTable.get(getCtx(), org.compiere.model.I_C_UOM.Table_Name)
			.getPO(getUOMConversionL4_ID(), get_TrxName());	}

	/** Set UOM Conversion L4.
		@param UOMConversionL4_ID 
		The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public void setUOMConversionL4_ID (int UOMConversionL4_ID)
	{
		throw new IllegalArgumentException ("UOMConversionL4_ID is virtual column");	}

	/** Get UOM Conversion L4.
		@return The conversion of the product's base UOM to the number 4 level package (if defined)
	  */
	public int getUOMConversionL4_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_UOMConversionL4_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Difference Qty L1.
		@param UOMDifferenceQtyL1 Difference Qty L1	  */
	public void setUOMDifferenceQtyL1 (BigDecimal UOMDifferenceQtyL1)
	{
		set_Value (COLUMNNAME_UOMDifferenceQtyL1, UOMDifferenceQtyL1);
	}

	/** Get Difference Qty L1.
		@return Difference Qty L1	  */
	public BigDecimal getUOMDifferenceQtyL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMDifferenceQtyL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Difference Qty L2.
		@param UOMDifferenceQtyL2 Difference Qty L2	  */
	public void setUOMDifferenceQtyL2 (BigDecimal UOMDifferenceQtyL2)
	{
		set_Value (COLUMNNAME_UOMDifferenceQtyL2, UOMDifferenceQtyL2);
	}

	/** Get Difference Qty L2.
		@return Difference Qty L2	  */
	public BigDecimal getUOMDifferenceQtyL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMDifferenceQtyL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Difference Qty L3.
		@param UOMDifferenceQtyL3 Difference Qty L3	  */
	public void setUOMDifferenceQtyL3 (BigDecimal UOMDifferenceQtyL3)
	{
		set_Value (COLUMNNAME_UOMDifferenceQtyL3, UOMDifferenceQtyL3);
	}

	/** Get Difference Qty L3.
		@return Difference Qty L3	  */
	public BigDecimal getUOMDifferenceQtyL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMDifferenceQtyL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Difference Qty L4.
		@param UOMDifferenceQtyL4 Difference Qty L4	  */
	public void setUOMDifferenceQtyL4 (BigDecimal UOMDifferenceQtyL4)
	{
		set_Value (COLUMNNAME_UOMDifferenceQtyL4, UOMDifferenceQtyL4);
	}

	/** Get Difference Qty L4.
		@return Difference Qty L4	  */
	public BigDecimal getUOMDifferenceQtyL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMDifferenceQtyL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Scrapped Qty L1.
		@param UOMScrappedQtyL1 Scrapped Qty L1	  */
	public void setUOMScrappedQtyL1 (BigDecimal UOMScrappedQtyL1)
	{
		set_Value (COLUMNNAME_UOMScrappedQtyL1, UOMScrappedQtyL1);
	}

	/** Get Scrapped Qty L1.
		@return Scrapped Qty L1	  */
	public BigDecimal getUOMScrappedQtyL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMScrappedQtyL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Scrapped Qty L2.
		@param UOMScrappedQtyL2 Scrapped Qty L2	  */
	public void setUOMScrappedQtyL2 (BigDecimal UOMScrappedQtyL2)
	{
		set_Value (COLUMNNAME_UOMScrappedQtyL2, UOMScrappedQtyL2);
	}

	/** Get Scrapped Qty L2.
		@return Scrapped Qty L2	  */
	public BigDecimal getUOMScrappedQtyL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMScrappedQtyL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Scrapped Qty L3.
		@param UOMScrappedQtyL3 Scrapped Qty L3	  */
	public void setUOMScrappedQtyL3 (BigDecimal UOMScrappedQtyL3)
	{
		set_Value (COLUMNNAME_UOMScrappedQtyL3, UOMScrappedQtyL3);
	}

	/** Get Scrapped Qty L3.
		@return Scrapped Qty L3	  */
	public BigDecimal getUOMScrappedQtyL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMScrappedQtyL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Scrapped Qty L4.
		@param UOMScrappedQtyL4 Scrapped Qty L4	  */
	public void setUOMScrappedQtyL4 (BigDecimal UOMScrappedQtyL4)
	{
		set_Value (COLUMNNAME_UOMScrappedQtyL4, UOMScrappedQtyL4);
	}

	/** Get Scrapped Qty L4.
		@return Scrapped Qty L4	  */
	public BigDecimal getUOMScrappedQtyL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMScrappedQtyL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Target Qty L1.
		@param UOMTargetQtyL1 Target Qty L1	  */
	public void setUOMTargetQtyL1 (BigDecimal UOMTargetQtyL1)
	{
		set_Value (COLUMNNAME_UOMTargetQtyL1, UOMTargetQtyL1);
	}

	/** Get Target Qty L1.
		@return Target Qty L1	  */
	public BigDecimal getUOMTargetQtyL1 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTargetQtyL1);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Target Qty L2.
		@param UOMTargetQtyL2 Target Qty L2	  */
	public void setUOMTargetQtyL2 (BigDecimal UOMTargetQtyL2)
	{
		set_Value (COLUMNNAME_UOMTargetQtyL2, UOMTargetQtyL2);
	}

	/** Get Target Qty L2.
		@return Target Qty L2	  */
	public BigDecimal getUOMTargetQtyL2 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTargetQtyL2);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Target Qty L3.
		@param UOMTargetQtyL3 Target Qty L3	  */
	public void setUOMTargetQtyL3 (BigDecimal UOMTargetQtyL3)
	{
		set_Value (COLUMNNAME_UOMTargetQtyL3, UOMTargetQtyL3);
	}

	/** Get Target Qty L3.
		@return Target Qty L3	  */
	public BigDecimal getUOMTargetQtyL3 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTargetQtyL3);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Target Qty L4.
		@param UOMTargetQtyL4 Target Qty L4	  */
	public void setUOMTargetQtyL4 (BigDecimal UOMTargetQtyL4)
	{
		set_Value (COLUMNNAME_UOMTargetQtyL4, UOMTargetQtyL4);
	}

	/** Get Target Qty L4.
		@return Target Qty L4	  */
	public BigDecimal getUOMTargetQtyL4 () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_UOMTargetQtyL4);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}
}
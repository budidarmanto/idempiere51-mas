/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package org.compiere.report;

import java.sql.ResultSet;
import java.util.Properties;
import java.util.WeakHashMap;

import org.compiere.model.MElementValue;
import org.compiere.model.MTable;
import org.compiere.model.X_PA_ReportSource;
import org.compiere.util.Util;

import com.unicore.ui.ISortTabRecord;
import com.uns.model.X_PA_ReportLineColumns;


/**
 *	Report Line Source Model
 *
 *  @author Jorg Janke
 *  @version $Id: MReportSource.java,v 1.3 2006/08/03 22:16:52 jjanke Exp $
 */
public class MReportSource extends X_PA_ReportSource implements ISortTabRecord
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6085437491271873555L;

	/** UNS Sepecific customization 		*/
	private String m_periodWhereClause = null;
	
	private MReportLine m_reportLine = null;
			
	/**
	 * 	Constructor
	 * 	@param ctx context
	 * 	@param PA_ReportSource_ID id
	 * 	@param trxName transaction
	 */
	public MReportSource (Properties ctx, int PA_ReportSource_ID, String trxName)
	{
		super (ctx, PA_ReportSource_ID, trxName);
		if (PA_ReportSource_ID == 0)
		{
		}
	}	//	MReportSource

	/**
	 * 	Constructor
	 * 	@param ctx context
	 * 	@param rs ResultSet to load from
	 * 	@param trxName transaction
	 */
	public MReportSource (Properties ctx, ResultSet rs, String trxName)
	{
		super(ctx, rs, trxName);
	}	//	MReportSource

	/**
	 * Set the report line instance to be used frequently to minimize DB connections.
	 * @param rLine
	 */
	public void setReportLine(MReportLine rLine)
	{
		m_reportLine = rLine;
	}
	
	/**
	 * Get current instance line.
	 * @return
	 */
	public MReportLine getReportLine()
	{
		if (m_reportLine == null)
			m_reportLine = (MReportLine) super.getPA_ReportLine();
		
		return m_reportLine;
	}
	
	/**
	 * Get the specific column definition of this line's source.
	 * @param theColumn
	 * @return
	 */
	public X_PA_ReportLineColumns getSpecificColumnOf(MReportColumn theColumn)
	{
		return getReportLine().getSpecificColumnOf(theColumn);
	}
	
	/**
	 * 	Get SQL where clause
	 * 	@param PA_Hierarchy_ID hierarchy 
	 * 	@return where clause
	 */
	public String getWhereClause(int PA_Hierarchy_ID)
	{
		String et = getElementType();
		//	ID for Tree Leaf Value
		int ID = 0;
		//
		if (MReportSource.ELEMENTTYPE_Account.equals(et))
			ID = getC_ElementValue_ID();
		else if (MReportSource.ELEMENTTYPE_Activity.equals(et))
			ID = getC_Activity_ID();
		else if (MReportSource.ELEMENTTYPE_BPartner.equals(et))
			ID = getC_BPartner_ID();
		else if (MReportSource.ELEMENTTYPE_Campaign.equals(et))
			ID = getC_Campaign_ID();
		else if (MReportSource.ELEMENTTYPE_LocationFrom.equals(et))
			ID = getC_Location_ID();
		else if (MReportSource.ELEMENTTYPE_LocationTo.equals(et))
			ID = getC_Location_ID();
		else if (MReportSource.ELEMENTTYPE_Organization.equals(et))
			ID = getOrg_ID();
		else if (MReportSource.ELEMENTTYPE_Product.equals(et))
			ID = getM_Product_ID();
		else if (MReportSource.ELEMENTTYPE_Project.equals(et))
			ID = getC_Project_ID();
		else if (MReportSource.ELEMENTTYPE_SalesRegion.equals(et))
			ID = getC_SalesRegion_ID();
		else if (MReportSource.ELEMENTTYPE_OrgTrx.equals(et))
			ID = getOrg_ID();	//	(re)uses Org_ID
		else if (MReportSource.ELEMENTTYPE_UserElementList1.equals(et))
			ID = getC_ElementValue_ID();
		else if (MReportSource.ELEMENTTYPE_UserElementList2.equals(et))
			ID = getC_ElementValue_ID();
        else if (MReportSource.ELEMENTTYPE_UserElement1.equals(et))
			ID = getC_ElementValue_ID();
		else if (MReportSource.ELEMENTTYPE_UserElement2.equals(et))
			ID = getC_ElementValue_ID();
		else if (MReportSource.ELEMENTTYPE_UserColumn1.equals(et))
			return "UserElement1_ID="+getUserElement1_ID(); // Not Tree
		else if (MReportSource.ELEMENTTYPE_UserColumn2.equals(et))
			return "UserElement2_ID="+getUserElement2_ID(); // Not Tree
		// Financial Report Source with Type Combination
		else if (MReportSource.ELEMENTTYPE_Combination.equals(et))
			return getWhereCombination(PA_Hierarchy_ID);

		//
		return MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, et, ID);
	}	//	getWhereClause

	/**
	 * Obtain where clause for the combination type
	 * @param PA_Hierarchy_ID
	 * @return
	 */
	private String getWhereCombination(int PA_Hierarchy_ID) {
		StringBuilder whcomb = new StringBuilder();
		if (getC_ElementValue_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_Account, getC_ElementValue_ID());
			if (isIncludeNullsElementValue())
				whcomb.append(" AND (Account_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else
			if (isIncludeNullsElementValue())
				whcomb.append(" AND Account_ID IS NULL");

		if (getC_Activity_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_Activity, getC_Activity_ID());
			if (isIncludeNullsActivity())
				whcomb.append(" AND (C_Activity_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByActivity()) {
			if (!isIncludeNullsActivity())
				whcomb.append(" AND C_Activity_ID IS NOT NULL");
		} else 
			if (isIncludeNullsActivity())
				whcomb.append(" AND C_Activity_ID IS NULL");


		if (getC_BPartner_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_BPartner, getC_BPartner_ID());
			if (isIncludeNullsBPartner())
				whcomb.append(" AND (C_BPartner_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByBPartner()) {
			if (!isIncludeNullsBPartner())
				whcomb.append(" AND C_BPartner_ID IS NOT NULL");
		} else 
			if (isIncludeNullsBPartner())
				whcomb.append(" AND C_BPartner_ID IS NULL");

		if (getC_Campaign_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_Campaign, getC_Campaign_ID());
			if (isIncludeNullsCampaign())
				whcomb.append(" AND (C_Campaign_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByCampaign()) {
			if (!isIncludeNullsCampaign())
				whcomb.append(" AND C_Campaign_ID IS NOT NULL");
		} else 
			if (isIncludeNullsCampaign())
				whcomb.append(" AND C_Campaign_ID IS NULL");

		if (getC_Location_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_LocationFrom, getC_Location_ID());
			if (isIncludeNullsLocation())
				whcomb.append(" AND (C_LocFrom_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByAddress()) {
			if (!isIncludeNullsLocation())
				whcomb.append(" AND C_LocFrom_ID IS NOT NULL");
		} else 
			if (isIncludeNullsLocation())
				whcomb.append(" AND C_LocFrom_ID IS NULL");

		if (getOrg_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_Organization, getOrg_ID());
			if (isIncludeNullsOrg())
				whcomb.append(" AND (AD_Org_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByOrg()) {
			if (!isIncludeNullsOrg())
				whcomb.append(" AND AD_Org_ID IS NOT NULL");
		} else 
			if (isIncludeNullsOrg())
				whcomb.append(" AND AD_Org_ID IS NULL");
		
		if (getAD_OrgTrx_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_OrgTrx, getAD_OrgTrx_ID());
			if (isIncludeNullsOrgTrx())
				whcomb.append(" AND (AD_OrgTrx_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByOrgTrx()) {
			if (!isIncludeNullsOrgTrx())
				whcomb.append(" AND AD_OrgTrx_ID IS NOT NULL");
		} else 
			if (isIncludeNullsOrgTrx())
				whcomb.append(" AND AD_OrgTrx_ID IS NULL");

		if (getM_Product_Category_ID() > 0) {
			whcomb.append(" AND M_Product_ID IN (SELECT p.M_Product_ID FROM M_Product p WHERE p.M_Product_Category_ID IN (")
				  .append("SELECT * FROM GetProductCategoryRecursively(" + getM_Product_Category_ID() + ")))");
		}
		else if (getM_Product_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_Product, getM_Product_ID());
			if (isIncludeNullsProduct())
				whcomb.append(" AND (M_Product_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByProduct()) {
			if (!isIncludeNullsProduct())
				whcomb.append(" AND M_Product_ID IS NOT NULL");
		} else 
			if (isIncludeNullsProduct())
				whcomb.append(" AND M_Product_ID IS NULL");

		if (getC_Project_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_Project, getC_Project_ID());
			if (isIncludeNullsProject())
				whcomb.append(" AND (C_Project_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByProject()) {
			if (!isIncludeNullsProject())
				whcomb.append(" AND C_Project_ID IS NOT NULL");
		} else 
			if (isIncludeNullsProject())
				whcomb.append(" AND C_Project_ID IS NULL");

		if (getC_SalesRegion_ID() > 0) {
			String whtree = MReportTree.getWhereClause (getCtx(), PA_Hierarchy_ID, MReportSource.ELEMENTTYPE_SalesRegion, getC_SalesRegion_ID());
			if (isIncludeNullsSalesRegion())
				whcomb.append(" AND (C_SalesRegion_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupBySalesRegion()) {
			if (!isIncludeNullsSalesRegion())
				whcomb.append(" AND C_SalesRegion_ID IS NOT NULL");
		} else 
			if (isIncludeNullsSalesRegion())
				whcomb.append(" AND C_SalesRegion_ID IS NULL");

		if (getUserElement1_ID() > 0) {
			String whtree = "UserElement1_ID=" + getUserElement1_ID(); // No Tree
			if (isIncludeNullsUserElement1())
				whcomb.append(" AND (UserElement1_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByUserElement1()) {
			if (!isIncludeNullsUserElement1())
				whcomb.append(" AND UserElement1_ID IS NOT NULL");
		} else 
			if (isIncludeNullsUserElement1())
				whcomb.append(" AND UserElement1_ID IS NULL");

		if (getUserElement2_ID() > 0) {
			String whtree = "UserElement2_ID=" + getUserElement2_ID(); // No Tree
			if (isIncludeNullsUserElement2())
				whcomb.append(" AND (UserElement2_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
		} else if (isGroupByUserElement2()) {
			if (!isIncludeNullsUserElement2())
				whcomb.append(" AND UserElement2_ID IS NOT NULL");
		} else 
			if (isIncludeNullsUserElement2())
				whcomb.append(" AND UserElement2_ID IS NULL");
		
		if (getAD_Table_ID() > 0) {
			String whtree = "AD_Table_ID=" + getAD_Table_ID(); // No Tree
			if (isIncludeNullsTable())
				whcomb.append(" AND (AD_Table_ID IS NULL OR ").append(whtree).append(")");
			else
				whcomb.append(" AND ").append(whtree);
			
			if (getC_DocType_ID() > 0) {
				String tableName = MTable.getTableName(getCtx(), getAD_Table_ID());
				whcomb.append(" AND EXISTS (SELECT 1").append(" FROM ").append(tableName)//.append(tableName).append("_ID FROM ").append(tableName);
				.append(" WHERE C_DocType_ID=").append(getC_DocType_ID())
				.append(" AND " + tableName).append("_ID=Record_ID)");//.append(") ");
			}
			
			if (getUNS_Resource_ID() > 0) 
			{
				String rscToSelect = "";
				if (RESOURCETYPE_2ProductionLine.equals(getResourceType())) {
					rscToSelect = " IN (SELECT ws.UNS_Resource_ID FROM UNS_Resource ws WHERE ws.ResourceParent_ID IN " +
							"(SELECT wc.UNS_Resource_ID FROM UNS_Resource wc WHERE wc.ResourceParent_ID=" + 
							getUNS_Resource_ID() + "))";
				}
				else if (RESOURCETYPE_3WorkCenter.equals(getResourceType())) {
					rscToSelect = " IN (SELECT ws.UNS_Resource_ID FROM UNS_Resource ws WHERE ws.ResourceParent_ID=" + 
							getUNS_Resource_ID() + ")";
				}
				else if (RESOURCETYPE_4WorkStation.equals(getResourceType())) {
					rscToSelect = "=" + getUNS_Resource_ID();
				}
				
				if (m_periodWhereClause == null)
					whcomb.append(" AND Record_ID IN (SELECT prod.UNS_Production_ID " +
						" FROM UNS_Production prod WHERE prod.UNS_Resource_ID" + rscToSelect + ")");
				else 
					whcomb.append(" AND Record_ID IN (SELECT prod.UNS_Production_ID " +
							" FROM UNS_Production prod WHERE prod.UNS_Resource_ID" + rscToSelect + 
							" AND prod.MovementDate " + m_periodWhereClause + ")");
			}
		} else
			if (!isIncludeNullsTable())
				whcomb.append(" AND AD_Table_ID IS NOT NULL");
		
		if (!Util.isEmpty(getSQLWhere(), true)) {
			whcomb.append(" AND " + getSQLWhere());
		}
		
		// drop the first " AND "
		if (whcomb.length() > 5 && whcomb.toString().startsWith(" AND "))
			whcomb.delete(0, 5);

		return whcomb.toString();
	}

	/**
	 * Get where clause to link between two table alias based on the source's element type combination.
	 * @param aliasT1
	 * @param aliasT2
	 * @return
	 */
	public String getWhereLinkBetweenAlias(String aliasT1, String aliasT2, boolean isGroupSrcDetail)
	{
		StringBuilder whlink = new StringBuilder();
		if (getC_ElementValue_ID() > 0) {
			whlink.append(aliasT1 + ".").append("Account_ID=")
			.append(aliasT2 + ".").append("Account_ID");
		}

		if (getC_Activity_ID() > 0 || (isGroupSrcDetail && isGroupByActivity())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("C_Activity_ID=")
			.append(aliasT2 + ".").append("C_Activity_ID");
		}

		if (getC_BPartner_ID() > 0 || (isGroupSrcDetail && isGroupByBPartner())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("C_BPartner_ID=")
			.append(aliasT2 + ".").append("C_BPartner_ID");
		}

		if (getC_Campaign_ID() > 0 || (isGroupSrcDetail && isGroupByCampaign())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("C_Campaign_ID=")
			.append(aliasT2 + ".").append("C_Campaign_ID");
		}

		if (getC_Location_ID() > 0 || (isGroupSrcDetail && isGroupByAddress())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("C_Location_ID=")
			.append(aliasT2 + ".").append("C_Location_ID");
		}

		if (getOrg_ID() > 0 || (isGroupSrcDetail && isGroupByOrg())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("AD_Org_ID=")
			.append(aliasT2 + ".").append("AD_Org_ID");
		}
		
		if (getAD_OrgTrx_ID() > 0 || (isGroupSrcDetail && isGroupByOrgTrx())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("AD_OrgTrx_ID=")
			.append(aliasT2 + ".").append("AD_OrgTrx_ID");
		}

		if (getM_Product_ID() > 0 || getM_Product_Category_ID() > 0 || 
				(isGroupSrcDetail && (isGroupByProduct() || isGroupByProductCategory()))) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("M_Product_ID=")
			.append(aliasT2 + ".").append("M_Product_ID");
		}
		
		if (getC_Project_ID() > 0 || (isGroupSrcDetail && isGroupByProject())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("C_Project_ID=")
			.append(aliasT2 + ".").append("C_Project_ID");
		}

		if (getC_SalesRegion_ID() > 0 || (isGroupSrcDetail && isGroupBySalesRegion())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("C_SalesRegion_ID=")
			.append(aliasT2 + ".").append("C_SalesRegion_ID");
		}

		if (getUserElement1_ID() > 0 || (isGroupSrcDetail && isGroupByUserElement1())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("UserElement1_ID=")
			.append(aliasT2 + ".").append("UserElement1_ID");
		}

		if (getUserElement2_ID() > 0 || (isGroupSrcDetail && isGroupByUserElement2())) {
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("UserElement2_ID=")
			.append(aliasT2 + ".").append("UserElement2_ID");
		}
		
		if (getAD_Table_ID() > 0) 
		{
			if(whlink.length() > 0)
				whlink.append(" AND ");
			whlink.append(aliasT1 + ".").append("AD_Table_ID=")
			.append(aliasT2 + ".").append("AD_Table_ID");
			
			if (getC_DocType_ID() > 0) {
				String tableName = MTable.getTableName(getCtx(), getAD_Table_ID());
				whlink.append(" AND EXISTS (SELECT 1").append(" FROM ").append(tableName)
				.append(" WHERE C_DocType_ID=").append(getC_DocType_ID())
				.append(" AND " + tableName).append("_ID=Record_ID)");//.append(") ");
			}
			
			if (getUNS_Resource_ID() > 0) 
			{
				String rscToSelect = "";
				if (RESOURCETYPE_2ProductionLine.equals(getResourceType())) {
					rscToSelect = " IN (SELECT ws.UNS_Resource_ID FROM UNS_Resource ws WHERE ws.ResourceParent_ID IN " +
							"(SELECT wc.UNS_Resource_ID FROM UNS_Resource wc WHERE wc.ResourceParent_ID=" + 
							getUNS_Resource_ID() + "))";
				}
				else if (RESOURCETYPE_3WorkCenter.equals(getResourceType())) {
					rscToSelect = " IN (SELECT ws.UNS_Resource_ID FROM UNS_Resource ws WHERE ws.ResourceParent_ID=" + 
							getUNS_Resource_ID() + ")";
				}
				else if (RESOURCETYPE_4WorkStation.equals(getResourceType())) {
					rscToSelect = "=" + getUNS_Resource_ID();
				}
				
				if (m_periodWhereClause == null)
					whlink.append(" AND Record_ID IN (SELECT prod.UNS_Production_ID " +
						" FROM UNS_Production prod WHERE prod.UNS_Resource_ID" + rscToSelect + ")");
				else 
					whlink.append(" AND Record_ID IN (SELECT prod.UNS_Production_ID " +
							" FROM UNS_Production prod WHERE prod.UNS_Resource_ID" + rscToSelect + 
							" AND prod.MovementDate " + m_periodWhereClause + ")");
			}
		} else
			if (!isIncludeNullsTable())
				whlink.append(" AND AD_Table_ID IS NOT NULL");
		
		if (!Util.isEmpty(getSQLWhere(), true)) {
			whlink.append(" AND " + getSQLWhere().replaceAll(aliasT2, aliasT1));
		}

		return whlink.toString();
	}

	/**
	 * Get sequence of the source's element type combination to be used to normal order or group by clause.
	 * 
	 * @return
	 */
	public String getCSVOfElements(boolean isGroupSrcDetail)
	{
		StringBuilder commaSprtElmts = new StringBuilder();
		if (getC_ElementValue_ID() > 0) {
			commaSprtElmts.append("Account_ID");
		}

		if (getC_Activity_ID() > 0 || (isGroupSrcDetail && isGroupByActivity())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("C_Activity_ID");
		}

		if (getC_BPartner_ID() > 0 || (isGroupSrcDetail && isGroupByBPartner())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("C_BPartner_ID");
		}

		if (getC_Campaign_ID() > 0 || (isGroupSrcDetail && isGroupByCampaign())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("C_Campaign_ID");
		}

		if (getC_Location_ID() > 0 || (isGroupSrcDetail && isGroupByAddress())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("C_Location_ID");
		}

		if (getOrg_ID() > 0 || (isGroupSrcDetail && isGroupByOrg())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("AD_Org_ID");
		}
		
		if (getAD_OrgTrx_ID() > 0 || (isGroupSrcDetail && isGroupByOrgTrx())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("AD_OrgTrx_ID");
		}

		if (getM_Product_ID() > 0 || getM_Product_Category_ID() > 0 || (isGroupSrcDetail && isGroupByProduct())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("M_Product_ID");
		}
		if (isGroupSrcDetail && isGroupByProductCategory()) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("pc.M_Product_Category_ID");
		}
		
		if (getC_Project_ID() > 0 || (isGroupSrcDetail && isGroupByProject())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("C_Project_ID");
		}

		if (getC_SalesRegion_ID() > 0 || (isGroupSrcDetail && isGroupBySalesRegion())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("C_SalesRegion_ID");
		}

		if (getUserElement1_ID() > 0 || (isGroupSrcDetail && isGroupByUserElement1())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("UserElement1_ID");
		}

		if (getUserElement2_ID() > 0 || (isGroupSrcDetail && isGroupByUserElement2())) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("UserElement2_ID");
		}
		
		if (getAD_Table_ID() > 0) {
			if(commaSprtElmts.length() > 0)
				commaSprtElmts.append(", ");
			commaSprtElmts.append("AD_Table_ID");
		}
		
		return commaSprtElmts.toString();
	}

	/**
	 * 	String Representation
	 * 	@return info
	 */
	public String toString ()
	{
		StringBuffer sb = new StringBuffer ("MReportSource[")
			.append(get_ID()).append(" - ").append(super.getName())
			.append(" - ").append(getDescription())
			.append(" - ").append(getElementType());
		sb.append ("]");
		return sb.toString ();
	}	//	toString


	/**************************************************************************
	 * 	Copy Constructor
	 * 	@param ctx context
	 * 	@param AD_Client_ID parent
	 * 	@param AD_Org_ID parent
	 * 	@param PA_ReportLine_ID parent
	 * 	@param source copy source
	 * 	@param trxName transaction
	 * 	@return Report Source
	 */
	public static MReportSource copy (Properties ctx, int AD_Client_ID, int AD_Org_ID, 
		int PA_ReportLine_ID, MReportSource source, String trxName)
	{
		MReportSource retValue = new MReportSource (ctx, 0, trxName);
		MReportSource.copyValues(source, retValue, AD_Client_ID, AD_Org_ID);
		retValue.setPA_ReportLine_ID(PA_ReportLine_ID);
		return retValue;
	}	//	copy

	/**
	 * Set the period where clause of this report line.
	 * 
	 * @param periodWhere
	 */
	public void setPeriodWhereClause(String periodWhere)
	{
		m_periodWhereClause = periodWhere;
	}
	
	/**
	 * 
	 */
	public String getGroupName()
	{
		String name = "'" + super.getName() + "'";
		String et = getElementType();
		
		if (et.equals(ELEMENTTYPE_Combination))
		{
			name = "";
			WeakHashMap<String, String> combinations = new WeakHashMap<String, String>();
			
			String select = "";
			
			if (isGroupByOrg()) {
				select = "(SELECT t.Value || '_' || t.Name FROM AD_Org t WHERE t.AD_Org_ID=fx.AD_Org_ID)";
				combinations.put(ELEMENTTYPE_Organization, select);
			}
			if (isGroupByOrgTrx()) {
				select = "(SELECT t.Value || '_' || t.Name FROM AD_Org t WHERE t.AD_Org_ID=fx.AD_OrgTrx_ID)";
				combinations.put(ELEMENTTYPE_OrgTrx, select);
			}
			if (isGroupByBPartner()) {
				select = "(SELECT t.Value || '_' || t.Name FROM C_BPartner t WHERE t.C_BPartner_ID=fx.C_BPartner_ID)";
				combinations.put(ELEMENTTYPE_BPartner, select);
			}
			if (isGroupByProductCategory()) {
				select = "(SELECT t.Value || '_' || t.Name FROM M_Product_Category t"
						+ "INNER JOIN M_Product p ON p.M_Product_Category_ID=t.M_Product_Category_ID "
						+ "WHERE p.M_Product_ID=fx.M_Product_ID)";
				combinations.put(ELEMENTTYPE_ProductCategory, select);
			}
			if (isGroupByProduct()) {
				select = "(SELECT t.Value || '_' || t.Name FROM M_Product t WHERE t.M_Product_ID=fx.M_Product_ID)";
				combinations.put(ELEMENTTYPE_Product, select);
			}
			if (isGroupByProject()) {
				select = "(SELECT t.Value || '_' || t.Name FROM C_Project t WHERE t.C_Project_ID=fx.C_Project_ID)";
				combinations.put(ELEMENTTYPE_Project, select);
			}
			if (isGroupBySalesRegion()) {
				select = "(SELECT t.Value || '_' || t.Name FROM C_SalesRegion t WHERE t.C_SalesRegion_ID=fx.C_SalesRegion_ID)";
				combinations.put(ELEMENTTYPE_SalesRegion, select);
			}
			if (isGroupByActivity()) {
				select = "(SELECT t.Value || '_' || t.Name FROM C_Activity t WHERE  t.C_Activity_ID=fx.C_Activity_ID)";
				combinations.put(ELEMENTTYPE_Activity, select);
			}
			if (isGroupByCampaign()) {
				select = "(SELECT t.Value || '_' || t.Name FROM C_Campaign t WHERE  t.C_Campaign_ID=fx.C_Campaign_ID)";
				combinations.put(ELEMENTTYPE_Campaign, select);
			}
			if (isGroupByUserElement1()) {
				select = "(SELECT t.Value || '_' || t.Name FROM AD_Org t WHERE  t.AD_Org_ID=fx.AD_OrgTrx_ID)";
				combinations.put(ELEMENTTYPE_UserElement1, select);
			}
			if (isGroupByUserElement2()) {
				select = "(SELECT t.Value || '_' || t.Name FROM AD_Org t WHERE  t.AD_Org_ID=fx.AD_OrgTrx_ID)";
				combinations.put(ELEMENTTYPE_UserElement1, select);
			}
			String dn1 = getDisplayName1();
			if (!Util.isEmpty(dn1, true) && combinations.get(dn1) != null) {
				name += combinations.get(dn1);
				combinations.remove(dn1);
			}
			
			String dn2 = getDisplayName2();
			if (!Util.isEmpty(dn2, true) && combinations.get(dn2) != null) {
				if (!Util.isEmpty(name, true))
					name += " || '::' || ";
				name += combinations.get(dn2);
				combinations.remove(dn2);
			}

			String dn3 = getDisplayName3();
			if (!Util.isEmpty(dn3, true) && combinations.get(dn3) != null) {
				if (!Util.isEmpty(name, true))
					name += " || '::' || ";
				name += combinations.get(dn3);
				combinations.remove(dn3);
			}
			
			for (String dn : combinations.values())
			{
				if (!Util.isEmpty(name, true))
					name += " || '::' || ";
				name += dn;
			}
			
			name += " AS DisplayName ";
		}
		
		return name;
	} // getName
	
	
	/**
	 * 
	 * @return true if it is a combination element type and there is a group by clause.
	 */
	public boolean hasGroupByClause()
	{
		return getElementType().equals(ELEMENTTYPE_Combination) && 
				(isGroupByActivity() | isGroupByAddress() || isGroupByBPartner() || isGroupByCampaign()
				 || isGroupByManfResource() || isGroupByOrg() || isGroupByOrgTrx() || isGroupByProduct()
				 || isGroupByProductCategory() || isGroupByProject() || isGroupBySalesRegion()
				 || isGroupByUserElement1() || isGroupByUserElement2());
	} // hasGroupByClause

	@Override
	public String beforeSaveTabRecord(int parentRecord_ID) 
	{
		if(get_ID() == 0)
		{
			setElementType(ELEMENTTYPE_Account);
			MElementValue ev = new MElementValue(getCtx(), getC_ElementValue_ID(), get_TrxName());
			setName(ev.getName());
			setDescription(ev.getDescription());
		}
		return null;
	}

	@Override
	public String beforeRemoveSelection() {
		// TODO Auto-generated method stub
		return null;
	}
	
}	//	MReportSource

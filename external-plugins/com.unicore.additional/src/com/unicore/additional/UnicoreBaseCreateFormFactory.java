package com.unicore.additional;

import org.compiere.grid.ICreateFrom;
import org.compiere.grid.ICreateFromFactory;
import org.compiere.model.GridTab;
import org.compiere.model.I_C_Invoice;
import org.compiere.model.I_C_Order;

import com.unicore.additional.form.WCreateFromAudit;
import com.unicore.additional.form.WCreateFromCanvas;
import com.unicore.additional.form.WCreateFromOrder;
import com.unicore.additional.form.WCreateFromPackingListOrder;
import com.unicore.model.I_UNS_Cvs_RptCustomer;
import com.unicore.model.I_UNS_PackingList;
import com.unicore.model.I_UNS_PackingList_Order;
import com.uns.model.I_UNS_Audit;

public class UnicoreBaseCreateFormFactory implements ICreateFromFactory {

    @Override
    public ICreateFrom create(GridTab mTab) {
        String tableName = mTab.getTableName();

        if (tableName.equals(I_C_Invoice.Table_Name))
			return new com.unicore.additional.form.WCreateFromInvoice(mTab);
		else if (tableName.equals(I_UNS_PackingList_Order.Table_Name))
			return new WCreateFromPackingListOrder(mTab);
		else if (tableName.equals(I_UNS_Cvs_RptCustomer.Table_Name))
			return new WCreateFromCanvas(mTab);
		else if (tableName.equals(I_UNS_PackingList.Table_Name))
			return new WCreateFromPackingListOrder(mTab);
		else if (tableName.equals(I_UNS_Audit.Table_Name))
			return new WCreateFromAudit(mTab);
		else if (tableName.equals(I_C_Order.Table_Name))
			return new WCreateFromOrder(mTab);

        return null;
    }
}


/**
 * 
 */
package com.unicore.additional.form;

import static org.compiere.model.SystemIDs.COLUMN_C_ORDER_M_WAREHOUSE_ID;

import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.webui.ClientInfo;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.apps.form.WCreateFromWindow;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.GridFactory;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.ListboxFactory;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.compiere.model.GridTab;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.util.CLogger;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.Msg;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Vlayout;

import com.unicore.base.model.MOrder;

/**
 * @author Haryadi
 * 
 */
public class WCreateFromOrder extends CreateFromOrder implements EventListener<Event>, ValueChangeListener {

	private WCreateFromWindow window;
	private Grid parameterStdLayout;
	private int noOfParameterColumn;
	private boolean 	m_actionActive = false;
	
	public WCreateFromOrder(GridTab gridTab) {
		super(gridTab);
		log.info(getGridTab().toString());

		window = new WCreateFromWindow(this, getGridTab().getWindowNo());

		p_WindowNo = getGridTab().getWindowNo();

		try {
			if (!dynInit())
				return;
			zkInit();
			setInitOK(true);
		} catch (Exception e) {
			log.log(Level.SEVERE, "", e);
			setInitOK(false);
			throw new AdempiereException(e.getMessage());
		}
		AEnv.showWindow(window);
	}
	
	/** Window No               */
	private int p_WindowNo;

	/**	Logger			*/
	private CLogger log = CLogger.getCLogger(getClass());

	private Label warehouseLabel = new Label();
	private WEditor warehouseField;
	
	private Label requisitionLabel = new Label();
	protected Listbox requisitionField = ListboxFactory.newDropdownListbox();

	
	public boolean dynInit() throws Exception {
		log.config("");
		
		super.dynInit();
		
		window.setTitle(getTitle());
		
		initRequisition(false);	

		return true;
	}
	
	@Override
	public void valueChange(ValueChangeEvent e) {
		if (log.isLoggable(Level.CONFIG)) log.config(e.getPropertyName() + "=" + e.getNewValue());

		//  BPartner - load Order/Invoice/Shipment
//		if (e.getPropertyName().equals("C_BPartner_ID"))
//		{
//			int C_BPartner_ID = 0; 
//			if (e.getNewValue() != null){
//				C_BPartner_ID = ((Integer)e.getNewValue()).intValue();
//			}
//			
//			initRequisitionOrderDetails (C_BPartner_ID, true);
//		}
		window.tableChanged(null);
	}

	@Override
	public void onEvent(Event e) throws Exception {
//		if (m_actionActive)
//			return;
//		m_actionActive = true;
		
		if (e.getTarget().equals(requisitionField))
		{
			ListItem li = requisitionField.getSelectedItem();
			int M_Requisition_ID = 0;
			if (li != null && li.getValue() != null)
				M_Requisition_ID = ((Integer) li.getValue()).intValue();
			loadRequistion(M_Requisition_ID, (Integer) warehouseField.getValue());
		}

	}
	
	protected void loadRequistion (int M_Requisition_ID, int DestinationWarehouse_ID)
	{
		loadTableOIS(getRequisitionData(M_Requisition_ID, DestinationWarehouse_ID));
	}
	
	protected void initRequisition (boolean forInvoice) throws Exception
	{
		//  load BPartner
		int AD_Column_ID = COLUMN_C_ORDER_M_WAREHOUSE_ID;          //  C_Invoice.C_BPartner_ID
		MLookup lookup = MLookupFactory.get (Env.getCtx(), p_WindowNo, 0, AD_Column_ID, DisplayType.Search);
		warehouseField = new WSearchEditor("M_Warehouse_ID", true, false, true, lookup);
		//
		int DestinationWarehouse_ID = Env.getContextAsInt(Env.getCtx(), p_WindowNo, MOrder.COLUMNNAME_M_Warehouse_ID);
		warehouseField.setValue(new Integer(DestinationWarehouse_ID));

		//  initial loading
		initRequisitionOrderDetails(DestinationWarehouse_ID, forInvoice);
	}   //  initRequisition
	
	protected void initRequisitionOrderDetails (int C_BPartner_ID, boolean forInvoice)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID=" + C_BPartner_ID);
		KeyNamePair pp = new KeyNamePair(0,"");
		requisitionField.removeActionListener(this);
		requisitionField.removeAllItems();
		requisitionField.addItem(pp);
		requisitionField.setSelectedIndex(0);
		requisitionField.addActionListener(this);

		initRequisitionDetails(C_BPartner_ID);
	}   //  initRequisitionOIS
	
	private void initRequisitionDetails(int C_BPartner_ID)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("C_BPartner_ID" + C_BPartner_ID);
		
		ArrayList<KeyNamePair> list = loadRequisitionData(C_BPartner_ID);
		for(KeyNamePair knp : list)
			requisitionField.addItem(knp);
	}
	
	protected void loadInvoice (int C_Invoice_ID, int M_Locator_ID)
	{
		loadTableOIS(getRequisitionData(C_Invoice_ID, (Integer) warehouseField.getValue()));
	}
	
	protected void loadTableOIS (Vector<?> data)
	{
		window.getWListbox().clear();
		
		//  Remove previous listeners
		window.getWListbox().getModel().removeTableModelListener(window);
		//  Set Model
		ListModelTable model = new ListModelTable(data);
		model.addTableModelListener(window);
		window.getWListbox().setData(model, getOISColumnNames());
		//
		
		configureMiniTable(window.getWListbox());
	}
	
	public void showWindow()
	{
		window.setVisible(true);
	}
	
	public void closeWindow()
	{
		window.dispose();
	}

	@Override
	public Object getWindow() {
		return window;
	}
	
	
	
	protected void zkInit() throws Exception
	{
		warehouseLabel.setText(Msg.getElement(Env.getCtx(), "M_Warehouse_ID"));
		requisitionLabel.setText(Msg.getElement(Env.getCtx(), "M_Requisition_ID"));

    	Vlayout vlayout = new Vlayout();
		ZKUpdateUtil.setVflex(vlayout, "min");
		ZKUpdateUtil.setWidth(vlayout, "100%");
    	Panel parameterPanel = window.getParameterPanel();
		parameterPanel.appendChild(vlayout);
		
		parameterStdLayout = GridFactory.newGridLayout();
    	vlayout.appendChild(parameterStdLayout);
    	ZKUpdateUtil.setVflex(vlayout, "parameterStdLayout");
    	
    	setupColumns(parameterStdLayout);
		
    	Rows rows = (Rows) parameterStdLayout.newRows();
		Row row = rows.newRow();
		row.appendChild(warehouseLabel.rightAlign());
		if (warehouseField != null) {
			row.appendChild(warehouseField.getComponent());
			warehouseField.fillHorizontal();
		}
		
//		row = rows.newRow();
		row.appendChild(requisitionLabel.rightAlign());
		if (requisitionField != null) {
			row.appendChild(requisitionField);
			ZKUpdateUtil.setHflex(requisitionField, "1");
		}
    	
		if (ClientInfo.isMobile()) {    		
    		if (noOfParameterColumn == 2)
				LayoutUtils.compactTo(parameterStdLayout, 2);		
			ClientInfo.onClientInfo(window, this::onClientInfo);
		}
    }
	
	protected void setupColumns(Grid parameterGrid) {
		noOfParameterColumn = ClientInfo.maxWidth((ClientInfo.EXTRA_SMALL_WIDTH+ClientInfo.SMALL_WIDTH)/2) ? 2 : 4;
		Columns columns = new Columns();
		parameterGrid.appendChild(columns);
		if (ClientInfo.maxWidth((ClientInfo.EXTRA_SMALL_WIDTH+ClientInfo.SMALL_WIDTH)/2))
		{
			Column column = new Column();
			ZKUpdateUtil.setWidth(column, "35%");
			columns.appendChild(column);
			column = new Column();
			ZKUpdateUtil.setWidth(column, "65%");
			columns.appendChild(column);
		}
		else
		{
			Column column = new Column();
			columns.appendChild(column);		
			column = new Column();
			ZKUpdateUtil.setWidth(column, "15%");
			columns.appendChild(column);
			ZKUpdateUtil.setWidth(column, "35%");
			column = new Column();
			ZKUpdateUtil.setWidth(column, "15%");
			columns.appendChild(column);
			column = new Column();
			ZKUpdateUtil.setWidth(column, "35%");
			columns.appendChild(column);
		}
	}
	
	protected void onClientInfo()
	{
		if (ClientInfo.isMobile() && parameterStdLayout != null && parameterStdLayout.getRows() != null)
		{
			int nc = ClientInfo.maxWidth((ClientInfo.EXTRA_SMALL_WIDTH+ClientInfo.SMALL_WIDTH)/2) ? 2 : 4;
			int cc = noOfParameterColumn;
			if (nc == cc)
				return;
			
			parameterStdLayout.getColumns().detach();
			setupColumns(parameterStdLayout);
			if (cc > nc)
			{
				LayoutUtils.compactTo(parameterStdLayout, nc);
			}
			else
			{
				LayoutUtils.expandTo(parameterStdLayout, nc, false);
			}
			
			ZKUpdateUtil.setCSSHeight(window);
			ZKUpdateUtil.setCSSWidth(window);
			window.invalidate();			
		}
	}
}

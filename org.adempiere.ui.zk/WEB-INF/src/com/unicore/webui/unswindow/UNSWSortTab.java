/******************************************************************************
 * ----------- Product: UniCore ERP - Created By UntaSoft-Dev ----------------*
 *****************************************************************************/
package com.unicore.webui.unswindow;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.adempiere.exceptions.AdempiereException;
import org.adempiere.util.Callback;
import org.adempiere.webui.AdempiereIdGenerator;
import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.adwindow.ADTreePanel;
import org.adempiere.webui.adwindow.AbstractADWindowContent;
import org.adempiere.webui.adwindow.DetailPane;
import org.adempiere.webui.adwindow.GridView;
import org.adempiere.webui.adwindow.IADTabpanel;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHead;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.SimpleListModel;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.factory.ButtonFactory;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.GridTab;
import org.compiere.model.MColumn;
import org.compiere.model.MRole;
import org.compiere.model.MTab;
import org.compiere.model.MTable;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import org.compiere.util.NamePair;
import org.compiere.util.Util;
import org.zkoss.zk.au.out.AuFocus;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.event.ListDataEvent;

import com.unicore.ui.ISortTabRecord;

/**
 *	Tab to maintain Order/Sequence
 *
 * 	@author 	Jorg Janke
 * 	@version 	$Id: VSortTab.java,v 1.2 2006/07/30 00:51:28 jjanke Exp $
 *
 * @author Teo Sarca, SC ARHIPAC SERVICE SRL
 * 				FR [ 1779410 ] VSortTab: display ID for not visible columns
 *
 * @author victor.perez@e-evolution.com, e-Evolution
 * 				FR [ 2826406 ] The Tab Sort without parent column
 *				<li> https://sourceforge.net/tracker/?func=detail&atid=879335&aid=2826406&group_id=176962
 * Zk Port
 * @author Low Heng Sin
 * @author Juan David Arboleda : Refactoring Yes and No List to work with multiple choice.
 * @author Menjangan
 */
public class UNSWSortTab extends Panel implements IADTabpanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4161399343247477912L;

	/**
	 *	Sort Tab Constructor
	 *  @param WindowNo Window No
	 *  @param GridTab
	 */
	public UNSWSortTab(int WindowNo, GridTab gridTab)
	{
		if (log.isLoggable(Level.CONFIG)) log.config("SortOrder=" + gridTab.getAD_ColumnSortOrder_ID() + ", SortYesNo=" + gridTab.getAD_ColumnSortYesNo_ID());
		m_WindowNo = WindowNo;
		this.gridTab = gridTab;
		m_trxName = this.gridTab.getTableModel().get_TrxName();
		m_AD_Table_ID = gridTab.getAD_Table_ID();
		this.setVflex("true");
	}	//	ADSortTab

	/**	Logger			*/
	static CLogger log = CLogger.getCLogger(UNSWSortTab.class);
	private int			m_WindowNo;
	private int			m_AD_Table_ID;
	private AbstractADWindowContent adWindowPanel = null;
	private StringBuilder m_columnIdentifier = new StringBuilder();

	//	UI variables
	private Label noLabel = new Label();
	private Label yesLabel = new Label();
	private Button bAdd = ButtonFactory.createButton(null, ThemeManager.getThemeResource("images/MoveRight16.png"), null);
	private Button bRemove = ButtonFactory.createButton(null, ThemeManager.getThemeResource("images/MoveLeft16.png"), null);
	private Button bUp = ButtonFactory.createButton(null, ThemeManager.getThemeResource("images/MoveUp16.png"), null);
	private Button bDown = ButtonFactory.createButton(null, ThemeManager.getThemeResource("images/MoveDown16.png"), null);
	//
	SimpleListModel noModel = new SimpleListModel() {
		/**
		 *
		 */
		private static final long serialVersionUID = -8261235952902938774L;

		@Override
		public void addElement(Object obj) {
			Object[] elements = list.toArray();
			Arrays.sort(elements);
			int index = Arrays.binarySearch(elements, obj);
			if (index < 0)
				index = -1 * index - 1;
			if (index >= elements.length)
				list.add(obj);
			else
				list.add(index, obj);
			fireEvent(ListDataEvent.INTERVAL_ADDED, index, index);
		}
	};
	SimpleListModel yesModel = new SimpleListModel();
	Listbox noList = new Listbox();
	Listbox yesList = new Listbox();

	private GridTab gridTab;
	private boolean uiCreated;
	private boolean active = false;
	private boolean isChanged;
	private boolean detailPaneMode;
	private int tabNo;
	
	private String m_yesModelTableName = null;
	private String m_yesModelKeyColumn = null;
	private String m_noModelTableName = null;
	private String m_noModelKeyColumn = null;
	private String m_parentColumn = null;
	private String m_RefColName = null;
	private String m_linkColName = null;
	private boolean m_RefColIsTableID = false;
	private MTab m_Tab = null;
	private StringBuilder m_RefColumnIdentifier = new StringBuilder();
	private String m_noModelWC = null;
	private MTable m_Table = null;
	private String m_trxName = null;
	private List<Integer> m_LastSelectedRecord = new ArrayList<>();
	private String m_columnToSort = null;
	private Textbox m_fYesModelFilter;
	private Textbox m_fNoModelFilter;
	private SimpleListModel m_noModelNoFilterTmp;
	private SimpleListModel m_yesModelNoFilterTmp;
	

	/**
	 * 	Dynamic Init
	 *  @param AD_Table_ID Table No
	 *  @param AD_ColumnSortOrder_ID Sort Column
	 *  @param AD_ColumnSortYesNo_ID YesNo Column
	 */
	private void dynInit ()
	{
		m_Table = MTable.get(Env.getCtx(), m_AD_Table_ID);
		m_Table.setGridTab(gridTab);
		m_Tab = new MTab(Env.getCtx(), gridTab.getAD_Tab_ID(), null);
		m_Table.setExtensionHandler(m_Tab.getAD_Window().getExtensionHandler());
		m_yesModelTableName = m_Table.getTableName();
		m_AD_Table_ID = m_Table.get_ID();
		int columnToSort_ID = m_Tab.getAD_ColumnSortOrder_ID();
		
		int identifierCount = 0;
		
		List<MColumn> identifiers = new ArrayList<>();
		for(MColumn col : m_Table.getColumns(false)) {
			//if (col.isIdentifier())
				identifiers.add(col);
		}
		
		Collections.sort(identifiers, new Comparator<MColumn>() {
			@Override
			public int compare(MColumn o1, MColumn o2) {
				return o1.getSeqNo() - o2.getSeqNo();
			}
			
		});
				
		for(MColumn col : m_Table.getColumns(false))
		{
			if(col.isKey())
				m_yesModelKeyColumn = col.getColumnName();
			
			if(columnToSort_ID == col.get_ID())
				m_columnToSort = col.getColumnName();
			
			if(!col.isIdentifier())
				continue;
			
			if(identifierCount <=0)
				m_columnIdentifier.append("CONCAT(");
			else
				m_columnIdentifier.append(",'-',");
			
			identifierCount++;
			
			String iRefTableName = col.getReferenceTableName();
			if(col.isVirtualColumn())
			{
				m_columnIdentifier.append(col.getColumnSQL());
				continue;
			}
			else if(null != iRefTableName && !iRefTableName.equals("AD_Ref_list"))
			{
				m_columnIdentifier.append(getIdentifierValue(
						iRefTableName, m_yesModelTableName, col.getColumnName()));
				continue;
			}
 
			m_columnIdentifier.append(m_yesModelTableName).append(".").append(col.getColumnName());
		}
		
		if(identifierCount > 0)
			m_columnIdentifier.append(")");
		
		if(m_Tab.getParent_Column_ID() > 0)
			m_parentColumn = m_Tab.getParent_Column().getColumnName();
		
		if (m_Tab.getAD_Column_ID() > 0)
			m_linkColName = m_Tab.getAD_Column().getColumnName();
		
		MColumn refColumn = MColumn.get(Env.getCtx(), m_Tab.get_ValueAsInt(MTab.COLUMNNAME_ColumnReferer_ID));
		m_RefColName = refColumn.getColumnName();
		m_RefColIsTableID = refColumn.getAD_Reference_ID() == DisplayType.ID? true : false;
		m_noModelTableName = refColumn.getReferenceTableName();

		m_noModelWC = m_Tab.get_ValueAsString(MTab.COLUMNNAME_RefWhereClause);
		
		MTable table = MTable.get(Env.getCtx(), m_noModelTableName);
		
		identifierCount = 0;

		identifiers = new ArrayList<>();
		for(MColumn col : table.getColumns(false)) {
			//if (col.isIdentifier())
				identifiers.add(col);
		}
		
		Collections.sort(identifiers, new Comparator<MColumn>() {
			@Override
			public int compare(MColumn o1, MColumn o2) {
				return o1.getSeqNo() - o2.getSeqNo();
			}
			
		});
				
		for(MColumn col : identifiers)
		{
			if(col.isKey())
				m_noModelKeyColumn = col.getColumnName();
			
			if(!col.isIdentifier())
				continue;
		
			if(identifierCount <=0)
				m_RefColumnIdentifier.append("CONCAT(");
			else
				m_RefColumnIdentifier.append(",'-',");
			
			identifierCount++;
			
			
			String iRefTableName = col.getReferenceTableName();
			if(col.isVirtualColumn())
			{
				m_RefColumnIdentifier.append(col.getColumnSQL());
				continue;
			}
			if(null != iRefTableName)
			{
				m_RefColumnIdentifier.append(
						getIdentifierValue(iRefTableName, table.getTableName(), 
								col.getColumnName()));
				continue;
			}
			m_RefColumnIdentifier.append(table.getTableName()).append(".").append(col.getColumnName());
		}
		
		if(identifierCount > 0)
			m_RefColumnIdentifier.append(")");
		
		noLabel.setText("Unselected");
		yesLabel.setText("Selected");
	}	//	dynInit

	/**
	 * 	Static Layout
	 * 	@throws Exception
	 */
	private void init() throws Exception
	{
		m_noModelNoFilterTmp = new SimpleListModel();
		m_yesModelNoFilterTmp = new SimpleListModel();
		//
		noLabel.setValue("Unselected");
		yesLabel.setValue("Selected");

		yesList.setVflex(true);
		noList.setVflex(true);

        setId(AdempiereIdGenerator.escapeId(gridTab.getName()));

        EventListener<Event> mouseListener = new EventListener<Event>()
		{

			public void onEvent(Event event) throws Exception
			{
				if (Events.ON_DOUBLE_CLICK.equals(event.getName()))
				{
					migrateValueAcrossLists(event);
				}
			}
		};
		yesList.addDoubleClickListener(mouseListener);
		noList.addDoubleClickListener(mouseListener);
		//
		EventListener<Event> actionListener = new EventListener<Event>()
		{
			public void onEvent(Event event) throws Exception {
				migrateValueAcrossLists(event);
			}
		};
		yesModel.setMultiple(true);
		noModel.setMultiple(true);

		LayoutUtils.addSclass("btn-small", bAdd);
		LayoutUtils.addSclass("btn-sorttab small-img-btn", bAdd);
		bAdd.addEventListener(Events.ON_CLICK, actionListener);

		LayoutUtils.addSclass("btn-small", bRemove);
		LayoutUtils.addSclass("btn-sorttab small-img-btn", bRemove);
		bRemove.addEventListener(Events.ON_CLICK, actionListener);

		EventListener<Event> crossListMouseListener = new DragListener();
		yesList.addOnDropListener(crossListMouseListener);
		noList.addOnDropListener(crossListMouseListener);
		yesList.setItemDraggable(true);
		noList.setItemDraggable(true);

		EventListener<Event> actionListener2 = new EventListener<Event>()
		{
			public void onEvent(Event event) throws Exception {
				migrateValueWithinYesList(event);
			}
		};

		LayoutUtils.addSclass("btn-small", bUp);
		LayoutUtils.addSclass("btn-sorttab small-img-btn", bUp);
		bUp.addEventListener(Events.ON_CLICK, actionListener2);

		LayoutUtils.addSclass("btn-small", bDown);
		LayoutUtils.addSclass("btn-sorttab small-img-btn", bDown);
		bDown.addEventListener(Events.ON_CLICK, actionListener2);
		
		m_fNoModelFilter = new Textbox();
		m_fYesModelFilter = new Textbox();
		m_fNoModelFilter.addEventListener(Events.ON_CHANGE, 
				new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				doNoModelFilter();
			}
		});
		
		
		m_fYesModelFilter.addEventListener(Events.ON_CHANGE, new EventListener<Event>() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				doYesModelFilter();
			}
		});

		ListHead listHead = new ListHead();
		listHead.setParent(yesList);
		ListHeader listHeader = new ListHeader();
		listHeader.appendChild(yesLabel);
		listHeader.appendChild(m_fYesModelFilter);
		Hlayout yesButtonLayout = new Hlayout();
		yesButtonLayout.appendChild(bUp);
		yesButtonLayout.appendChild(bDown);
		listHeader.appendChild(yesButtonLayout);
		yesButtonLayout.setStyle("display: inline-block; float: right;");
		listHeader.setParent(listHead);

		listHead = new ListHead();
		listHead.setParent(noList);
		listHeader = new ListHeader();
		listHeader.appendChild(noLabel);
		listHeader.appendChild(m_fNoModelFilter);
		Hlayout noButtonLayout = new Hlayout();
		noButtonLayout.appendChild(bRemove);
		noButtonLayout.appendChild(bAdd);
		listHeader.appendChild(noButtonLayout);
		noButtonLayout.setStyle("display: inline-block; float: right;");
		listHeader.setParent(listHead);

		Hlayout hlayout = new Hlayout();
		hlayout.setVflex("true");
		hlayout.setHflex("true");
		hlayout.setStyle("margin: auto;");
		appendChild(hlayout);
		noList.setHflex("1");
		noList.setVflex(true);
		hlayout.appendChild(noList);

		yesList.setVflex(true);
		yesList.setHflex("1");
		hlayout.appendChild(yesList);
		
		addEventListener(ON_ACTIVATE_EVENT, new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				removeAttribute(ATTR_ON_ACTIVATE_POSTED);
			}
		});
	}	//	Init

	/* (non-Javadoc)
	 * @see org.compiere.grid.APanelTab#loadData()
	 */
	public void loadData()
	{
		yesModel.removeAllElements();
		noModel.removeAllElements();
		m_yesModelNoFilterTmp.removeAllElements();
		m_noModelNoFilterTmp.removeAllElements();
		m_fYesModelFilter.setText(null);
		m_fNoModelFilter.setText(null);
		clearLastSelectedRecord();
		
		boolean isReadWrite = true;
		StringBuilder sql = new StringBuilder("SELECT ").append(m_yesModelTableName).append(".")
				.append(m_yesModelKeyColumn).append(",").append(m_yesModelTableName)
				.append(".").append(m_RefColName).append(",").append(m_yesModelTableName).append(".")
				.append("AD_Client_ID").append(",").append(m_yesModelTableName).append(".")
				.append("AD_Org_ID");
		
		if(!Util.isEmpty(m_columnIdentifier.toString(), true))
			sql.append(",").append(m_columnIdentifier);
		else
			sql.append(",'No Identifier'").append("AS NoIdentifier");
		
		if(!Util.isEmpty(m_columnToSort, true))
			sql.append(", ").append(m_yesModelTableName).append(".").append(m_columnToSort);

		sql.append(" FROM ").append(m_yesModelTableName);
		
		if(!Util.isEmpty(m_parentColumn, true))
			sql.append(" WHERE ").append(m_parentColumn).append("=").append(
					Env.getContextAsInt(Env.getCtx(), gridTab.getWindowNo(), m_parentColumn));
		
		if(!Util.isEmpty(m_Tab.getWhereClause(), true))
		{
			if(!Util.isEmpty(m_parentColumn, true))
				sql.append(" AND ");
			else
				sql.append(" WHERE ");
			
			sql.append(Env.parseContext(Env.getCtx(), gridTab.getWindowNo(), gridTab.getTabNo()
					, m_Tab.getWhereClause(), false));
		}
		
		if(!Util.isEmpty(m_columnToSort, true))
			sql.append(" Order By ").append(m_yesModelTableName).append(".").append(m_columnToSort);

		
		PreparedStatement stm = null;
		ResultSet rs = null;
		
		try {
			String query = sql.toString();
			stm = DB.prepareStatement(query, null);
			rs = stm.executeQuery();
			while (rs.next()) {
				addLastSelectedRecord(rs.getInt(1));
				ListElement pp = new ListElement(rs.getInt(2), rs.getString(5), rs.getInt(1), true, rs.getInt(3), rs.getInt(4));
				yesModel.addElement(pp);
				isReadWrite = pp.isUpdateable();
				log.log(Level.INFO, "Loading selected record ".concat(rs.getString(5)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, stm);
		}

		if (!gridTab.getParentTab().needSave(true, true))
			setIsChanged(false);

		bAdd.setEnabled(isReadWrite);
		bRemove.setEnabled(isReadWrite);
		bUp.setEnabled(isReadWrite);
		bDown.setEnabled(isReadWrite);
		yesList.setEnabled(isReadWrite);
		yesList.setItemRenderer(yesModel);
		yesList.setModel(yesModel);

		noList.setEnabled(isReadWrite);
		
		loadNoModelData();
		
	}	//	loadData

	
	public void loadNoModelData()
	{
		StringBuilder sql = new StringBuilder("SELECT ").append(m_noModelTableName).append(".")
				.append(m_noModelKeyColumn).append(",").append(m_RefColumnIdentifier == null 
						? "'No Identifier'" : m_RefColumnIdentifier)
				.append(",").append(m_noModelTableName).append(".").append("AD_Client_ID")
				.append(", ").append(m_noModelTableName).append(".").append("AD_Org_ID")
				.append(" FROM ").append(m_noModelTableName);
		
		int count = 0;
		if(yesModel != null && yesModel.getSize() > 0)
		{
			sql.append(" WHERE ").append(" ").append(m_noModelKeyColumn).append(" NOT IN (");
			for(int i=0; i<yesModel.getSize(); i++)
			{
				if(i > 0)
					sql.append(",");
				
				ListElement li = (ListElement) yesModel.getElementAt(i);
				sql.append(li.getKey());
				count++;
			}
			
			sql.append(")");
		}
		
		if(!Util.isEmpty(m_noModelWC, true))
		{
			if(count > 0)
				sql.append(" AND ");
			else
				sql.append(" WHERE ");
			
			sql.append(Env.parseContext(
					Env.getCtx(), gridTab.getWindowNo(), gridTab.getTabNo(), m_noModelWC, false));
		}
		
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = DB.prepareStatement(sql.toString(), m_trxName);
			rs = st.executeQuery();
			while (rs.next()) {
				int refID = m_RefColIsTableID? rs.getInt(1) : 0;
				ListElement pp = new ListElement(rs.getInt(1), rs.getString(2), refID, false, rs.getInt(3), rs.getInt(4));
				noModel.addElement(pp);
				log.log(Level.INFO, "Loading unselected record ".concat(rs.getString(2)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, st);
		}

		this.gridTab.getTableModel().open(noModel.getSize());
		noList.setItemRenderer(noModel);
		noList.setModel(noModel);
		
	}
	
	
	/**
	 * Set tab change status.
	 * @param value
	 */
	public void setIsChanged(boolean value) {
		isChanged = value;
		if (adWindowPanel != null) {
			adWindowPanel.getToolbar().enableSave(value);
			adWindowPanel.getToolbar().enableIgnore(value);
		}
	}

	public boolean isChanged() {
		return isChanged;
	}
	
	/**
	 * @param event
	 */
	void migrateValueAcrossLists (Event event)
	{
		Object source = event.getTarget();
		if (source instanceof ListItem) {
			source = ((ListItem)source).getListbox();
		}
		Listbox listFrom = (source == bAdd || source == noList) ? noList : yesList;
		Listbox listTo =  (source == bAdd || source == noList) ? yesList : noList;

		int endIndex = yesList.getIndexOfItem(listTo.getSelectedItem());	
		//Listto is empty. 
		if (endIndex<0 )
			endIndex=0;

		migrateLists (listFrom,listTo,endIndex);
	}	//	migrateValueAcrossLists

	void migrateLists (Listbox listFrom , Listbox listTo , int endIndex)
	{
		int index = 0; 
		SimpleListModel lmFrom = (listFrom == yesList) ? yesModel:noModel;
		SimpleListModel lmTo = (lmFrom == yesModel) ? noModel:yesModel;
		Set<?> selectedItems = listFrom.getSelectedItems();
		List<ListElement> selObjects = new ArrayList<ListElement>();
		for (Object obj : selectedItems) {
			ListItem listItem = (ListItem) obj;
			index = listFrom.getIndexOfItem(listItem);
			ListElement selObject = (ListElement)lmFrom.getElementAt(index);
			selObjects.add(selObject);
		}
		index = 0;
		Arrays.sort(selObjects.toArray());	
		for (ListElement selObject : selObjects)
		{
			if (selObject == null || !selObject.isUpdateable())
				continue;

			lmFrom.removeElement(selObject);
			lmTo.add(endIndex, selObject);
		}
		//  Enable explicit Save
		setIsChanged(true);
		if ( listTo.getSelectedItem() != null)
		{
			AuFocus focus = new AuFocus(listTo.getSelectedItem());
			Clients.response(focus);
		}
	}

	/**
	 * 	Move within Yes List
	 *	@param event event
	 */
	void migrateValueWithinYesList (Event event)
	{
		Object[] selObjects = yesList.getSelectedItems().toArray();
		if (selObjects == null)
			return;
		int length = selObjects.length;
		if (length == 0)
			return;
		//
		int[] indices = yesList.getSelectedIndices();
		//
		boolean change = false;
		//
		Object source = event.getTarget();
		if (source == bUp)
		{
			for (int i = 0; i < length; i++) {
				int index = indices[i];
				if (index == 0)
					break;
				ListElement selObject = (ListElement) yesModel.getElementAt(index);
				ListElement newObject = (ListElement)yesModel.getElementAt(index - 1);
				if (!selObject.isUpdateable() || !newObject.isUpdateable())
					break;
				yesModel.setElementAt(newObject, index);
				yesModel.setElementAt(selObject, index - 1);
				indices[i] = index - 1;
				change = true;
			}
		}	//	up

		else if (source == bDown)
		{
			for (int i = length - 1; i >= 0; i--) {
				int index = indices[i];
				if (index  >= yesModel.getSize() - 1)
					break;
				ListElement selObject = (ListElement) yesModel.getElementAt(index);
				ListElement newObject = (ListElement)yesModel.getElementAt(index + 1);
				if (!selObject.isUpdateable() || !newObject.isUpdateable())
					break;
				yesModel.setElementAt(newObject, index);
				yesModel.setElementAt(selObject, index + 1);
				yesList.setSelectedIndex(index + 1);
				indices[i] = index + 1;
				change = true;
			}
		}	//	down

		//
		if (change) {
			yesList.setSelectedIndices(indices);
			setIsChanged(true);
			if ( yesList.getSelectedItem() != null)
			{
				AuFocus focus = new AuFocus(yesList.getSelectedItem());
				Clients.response(focus);
			}
		}
	}	//	migrateValueWithinYesList


	/**
	 * 	Move within Yes List with Drag Event and Multiple Choice
	 *	@param event event
	 */
	void migrateValueWithinYesList (int endIndex, List<ListElement> selObjects)
	{
		int iniIndex =0;
		Arrays.sort(selObjects.toArray());	
		ListElement selObject= null;
		ListElement endObject = (ListElement)yesModel.getElementAt(endIndex);
		for (ListElement selected : selObjects) {
   		    iniIndex = yesModel.indexOf(selected);
			selObject = (ListElement)yesModel.getElementAt(iniIndex);
			yesModel.removeElement(selObject);
			endIndex = yesModel.indexOf(endObject);
			yesModel.add(endIndex, selObject);			
		}	
		yesList.removeAllItems();
	    for(int i=0 ; i<yesModel.getSize(); i++) { 	
			ListElement pp = (ListElement)yesModel.getElementAt(i);
			yesList.addItem(new KeyNamePair(pp.m_key, pp.getName()));
		}
		setIsChanged(true);
	}

	/* (non-Javadoc)
	 * @see org.compiere.grid.APanelTab#registerAPanel(APanel)
	 */
	public void registerAPanel (AbstractADWindowContent panel)
	{
		adWindowPanel = panel;
	}	//	registerAPanel
	
	/**
	 * 
	 * @param currentList
	 */
	private void saveData(ListElement list, int idx)
	{		
		m_Table.setExtensionHandler(m_Tab.getAD_Window().getExtensionHandler());
		PO po = m_Table.getPO(list.getRef_ID(), m_trxName);
		
		int parentRecord_ID = Env.getContextAsInt(Env.getCtx(), m_WindowNo, m_parentColumn);
		
		if(m_RefColIsTableID) {
			po.set_ValueOfColumn(m_linkColName, parentRecord_ID);
		}
		else if(po.is_new())
		{
			po.set_ValueOfColumn(m_RefColName, list.getKey());
			po.set_ValueOfColumn("AD_Org_ID", list.getAD_Org_ID());
			po.set_ValueOfColumn(m_linkColName, parentRecord_ID);
		}
		
		if(!Util.isEmpty(m_columnToSort, true))
			po.set_ValueOfColumn(m_columnToSort, idx);
		
		try {
			if (po instanceof ISortTabRecord) {
				String errorMsg = ((ISortTabRecord) po).beforeSaveTabRecord(parentRecord_ID);
				if (errorMsg != null)
					throw new AdempiereException(errorMsg);
			}
			
			po.saveEx();
			list.set_Ref_ID(po.get_ID());
		} catch (Exception e) {
			FDialog.error(m_WindowNo, null, "SaveError", e.getMessage());
		}
	}

	/** (non-Javadoc)
	 * @see org.compiere.grid.APanelTab#saveData()
	 */
	public void saveData()
	{
		if (!adWindowPanel.getToolbar().isSaveEnable())
			return;
		log.fine("");
		if(!isChanged)
			return;
		
		resetYesModelFilter();
		int idx = 0;
		for(int i=0; i< yesModel.getSize(); i++)
		{
			ListElement currentList = (ListElement) yesModel.getElementAt(i);
			if(!isLastSelectedRecord(currentList.getRef_ID()) && !m_RefColIsTableID)
				currentList.set_Ref_ID(0);
			idx += 10;
			saveData(currentList, idx);
			if(!isLastSelectedRecord(currentList.getRef_ID()))
				addLastSelectedRecord(currentList.getRef_ID());
		}
		removeData();
		setIsChanged(false);
	}	//	saveData
	
	private void removeData()
	{
		for(int x=0; x<getLastSelectedRecords().size(); x++)
		{
			boolean ok = false;
			for(int i=0; i< yesModel.getSize(); i++)
			{
				ListElement currentList = (ListElement) yesModel.getElementAt(i);
				if(getLastSelectedRecords().get(x) != currentList.getRef_ID())
					continue;
				
				ok = true;
				break;
			}
			
			if(ok)
				continue;
			
			try
			{
				PO po = m_Table.getPO(getLastSelectedRecords().get(x), m_trxName);
				
				if (!m_RefColIsTableID && m_linkColName == null) {
					po.deleteEx(true);
				}
				else {
					po.set_ValueOfColumn(m_linkColName, null);
					po.saveEx();
				}
			}
			catch (Exception e)
			{
				throw new AdempiereException(e.getMessage());
			}
			removeLastSelectedRecord(getLastSelectedRecords().get(x));
			x--;
		}
	}

	/**
	 * List Item
	 * @author Teo Sarca
	 */
	class ListElement extends NamePair {
		/**
		 *
		 */
		private static final long serialVersionUID = -5645910649588308798L;
		private int		m_key;
		private int		m_AD_Client_ID;
		private int		m_AD_Org_ID;
		/** Initial seq number */
		private int		m_ref_ID;
		/** Initial selection flag */
		private boolean m_isYes;
		private boolean	m_updateable;

		public ListElement(int key, String name, int sortNo, boolean isYes, int AD_Client_ID, int AD_Org_ID) {
			super(name);
			this.m_key = key;
			this.m_AD_Client_ID = AD_Client_ID;
			this.m_AD_Org_ID = AD_Org_ID;
			this.m_ref_ID = sortNo;
			this.m_isYes = isYes;
			this.m_updateable = AD_Org_ID == 0 ? true : 
				MRole.getDefault().canUpdate(m_AD_Client_ID, m_AD_Org_ID, m_AD_Table_ID, m_key, false);
		}
		public int getKey() {
			return m_key;
		}
		public void set_Ref_ID(int sortNo) {
			m_ref_ID = sortNo;
		}
		public int getRef_ID() {
			return m_ref_ID;
		}
		public void setIsYes(boolean value) {
			m_isYes = value;
		}
		public boolean isYes() {
			return m_isYes;
		}
		public int getAD_Client_ID() {
			return m_AD_Client_ID;
		}
		public int getAD_Org_ID() {
			return m_AD_Org_ID;
		}
		public boolean isUpdateable() {
			return m_updateable;
		}
		@Override
		public String getID() {
			return m_key != -1 ? String.valueOf(m_key) : null;
		}
		@Override
		public int hashCode() {
			return m_key;
		}
		@Override
		public boolean equals(Object obj)
		{
			if (obj instanceof ListElement)
			{
				ListElement li = (ListElement)obj;
				return
					li.getKey() == m_key
					&& li.getName() != null
					&& li.getName().equals(getName())
					&& li.getAD_Client_ID() == m_AD_Client_ID
					&& li.getAD_Org_ID() == m_AD_Org_ID;
			}
			return false;
		}	//	equals

		@Override
		public String toString() {
			String s = super.toString();
			if (s == null || s.trim().length() == 0)
				s = "<" + getKey() + ">";
			return s;
		}
	}

	/**
	 * @author eslatis
	 *
	 */
	private class DragListener implements EventListener<Event>
	{

		/**
		 * Creates a ADSortTab.DragListener.
		 */
		public DragListener()
		{
		}

		public void onEvent(Event event) throws Exception {
			if (event instanceof DropEvent)
			{
				int endIndex = 0;
				DropEvent me = (DropEvent) event;
				ListItem endItem = (ListItem) me.getTarget();
				ListItem startItem = (ListItem) me.getDragged();

				if (!startItem.isSelected())
					startItem.setSelected(true);
				
				if (!(startItem.getListbox() == endItem.getListbox()))
				{
					Listbox listFrom = (Listbox)startItem.getListbox();
					Listbox listTo =  (Listbox)endItem.getListbox();
					endIndex = yesList.getIndexOfItem(endItem);
					migrateLists (listFrom,listTo,endIndex);
				} else if (startItem.getListbox() == endItem.getListbox() && startItem.getListbox() == yesList)
				{
					List<ListElement> selObjects = new ArrayList<ListElement>();
					endIndex = yesList.getIndexOfItem(endItem);	
					for (Object obj : yesList.getSelectedItems()) {
						ListItem listItem = (ListItem) obj;
						int index = yesList.getIndexOfItem(listItem);
						ListElement selObject = (ListElement)yesModel.getElementAt(index);				
						selObjects.add(selObject);						
					}
					migrateValueWithinYesList (endIndex, selObjects);
			   }
		   }
		}
	}

	public void activate(boolean b) {
		if (b) {
	    	if (getAttribute(ATTR_ON_ACTIVATE_POSTED) != null) {
	    		return;
	    	}
	    	
	    	setAttribute(ATTR_ON_ACTIVATE_POSTED, Boolean.TRUE);
    	}

    	active = b;

    	Event event = new Event(ON_ACTIVATE_EVENT, this, b);
        Events.postEvent(event);
	}

	public void createUI() {
		if (uiCreated) return;
		try
		{
			init();
			dynInit ();
		}
		catch(Exception e)
		{
			log.log(Level.SEVERE, "", e);
		}
		uiCreated = true;
	}

	public void dynamicDisplay(int i) {
		loadData();
	}

	public void editRecord(boolean b) {
	}

	public String getDisplayLogic() {
		return gridTab.getDisplayLogic();
	}

	public GridTab getGridTab() {
		return gridTab;
	}

	public int getTabLevel() {
		return gridTab.getTabLevel();
	}

    public String getTableName()
    {
        return gridTab.getTableName();
    }

	public int getRecord_ID() {
		return gridTab.getRecord_ID();
	}

	public String getTitle() {
		return gridTab.getName();
	}

	public boolean isCurrent() {
		return gridTab != null ? gridTab.isCurrent() : false;
	}

	public void query() {
		loadData();
	}

	public void query(boolean currentRows, int currentDays, int i) {
		loadData();
	}

	public void refresh() {
		loadData();
	}

	public void switchRowPresentation() {
	String test = "";
	test.toString();
	
	}

	public String get_ValueAsString(String variableName) {
		return Env.getContext(Env.getCtx(), m_WindowNo, variableName);
	}

	public void afterSave(boolean onSaveEvent) {
	}

	public boolean onEnterKey() {
		return false;
	}

	@Override
	public boolean isGridView() {
		return false;
	}

	@Override
	public boolean isActivated() {
		return active;
	}

	@Override
	public void setDetailPaneMode(boolean detailMode) {
		this.detailPaneMode = detailMode;
		this.setVflex("true");
	}
	
	public boolean isDetailPaneMode() {
		return this.detailPaneMode;
	}

	@Override
	public GridView getGridView() {
		return null;
	}

	@Override
	public boolean needSave(boolean rowChange, boolean onlyRealChange) {
		return isChanged();
	}

	@Override
	public boolean dataSave(boolean onSaveEvent) {
		if (isChanged()) {
			saveData();
			return isChanged() == false;
		} else {
			return true;
		}
	}

	@Override
	public void setTabNo(int tabNo) {
		this.tabNo = tabNo;
	}

	@Override
	public int getTabNo() {
		return tabNo;
	}

	@Override
	public void setDetailPane(DetailPane detailPane) {
	}

	@Override
	public DetailPane getDetailPane() {
		return null;
	}

	@Override
	public void resetDetailForNewParentRecord() {
		yesModel.removeAllElements();
		noModel.removeAllElements();
		clearLastSelectedRecord();

		//setIsChanged(false);
		bAdd.setEnabled(false);
		bRemove.setEnabled(false);
		bUp.setEnabled(false);
		bDown.setEnabled(false);
		yesList.setEnabled(false);
		noList.setEnabled(false);

		yesList.setItemRenderer(yesModel);
		yesList.setModel(yesModel);
		noList.setItemRenderer(noModel);
		noList.setModel(noModel);
	}

	public ADTreePanel getTreePanel() {
		return null;
	}
	
	/**
	 * 
	 * @param record_ID
	 * @return
	 */
	private boolean isLastSelectedRecord(int record_ID)
	{
		return getLastSelectedRecord(record_ID) > 0;
	}
	
	private List<Integer> getLastSelectedRecords()
	{
		return m_LastSelectedRecord;
	}
	/**
	 * 
	 * @param record_id
	 * @return
	 */
	private int getLastSelectedRecord(int record_id)
	{
		int removedRecord_ID = -1;
		for(int i=0; i<m_LastSelectedRecord.size(); i++)
		{
			if(m_LastSelectedRecord.get(i) != record_id)
				continue;
			removedRecord_ID = m_LastSelectedRecord.get(i);
			break;
		}
		return removedRecord_ID;
	}
	
	private void addLastSelectedRecord(int record_iD)
	{
		m_LastSelectedRecord.add(record_iD);
	}
	
	private void removeLastSelectedRecord(int record_ID)
	{
		for(int i=0; i<m_LastSelectedRecord.size(); i++)
		{
			if(m_LastSelectedRecord.get(i) != record_ID)
				continue;
			m_LastSelectedRecord.remove(i);
			break;
		}
	}
	
	private void clearLastSelectedRecord()
	{
		m_LastSelectedRecord.clear();
	}
	
	/**
	 * 
	 * @param tableName
	 * @param sourceTableName
	 * @param sourceColumnName
	 * @return
	 */
	private String getIdentifierValue(String tableName, String sourceTableName, String sourceColumnName)
	{
		StringBuilder sb = new StringBuilder("(SELECT CONCAT(");
		int identifierCount = 0;
		MTable table = MTable.get(Env.getCtx(), tableName);
		MColumn[] columns = table.getColumns(false);
		for(int i=0; i<columns.length; i++){
			MColumn column = columns[i];
			if(!column.isIdentifier())
				continue;
			
			String tableRef = column.getReferenceTableName();
			if(identifierCount > 0)
				sb.append(",'_', ");
			if(column.isVirtualColumn())
			{
				m_columnIdentifier.append(column.getColumnSQL());
				identifierCount++;
				continue;
			}
			if(null != tableRef)
			{
				sb.append(getIdentifierValue(tableRef, tableName, column.getColumnName()));
				identifierCount++;
				continue;
			}
			sb.append(table.getTableName()).append(".").append(column.getColumnName());
			identifierCount++;
		}
		
		if(identifierCount == 0)
			sb.append("'1'");
		sb.append(") FROM ").append(tableName).append(" WHERE ").append(tableName).append(".");			
		sb.append(tableName.equals("AD_Ref_List") ? "Value = " : tableName.concat("_ID = "));
		sb.append(sourceTableName).append(".").append(sourceColumnName);
		
		if(tableName.equals("AD_Ref_List"))
		{
			sb.append(" AND ").append("AD_Reference_ID = ").append("(SELECT AD_Reference_Value_ID FROM ")
			.append("AD_Column WHERE AD_Column.ColumnName = '").append(sourceColumnName)
			.append("' AND AD_Table_ID = (SELECT AD_Table_ID FROM AD_Table WHERE TableName ='")
			.append(sourceTableName).append("'))");
		}
		sb.append(")");
		return sb.toString();
	}

	@Override
	public boolean dataSave(boolean onSaveEvent,
			Callback<Boolean> afterSaveCallback) {
		return dataSave(onSaveEvent);
	}

	@Override
	public void fireEventAfterDataSave(boolean onSaveEvent) {
		// TODO Auto-generated method stub
		
	}
	
	private void doYesModelFilter()
	{
		if(Util.isEmpty(m_fYesModelFilter.getValue(), true))
		{
			resetYesModelFilter();
			return;
		}
		for (int i=0; i<m_yesModelNoFilterTmp.getSize(); i++)
		{
			ListElement items = (ListElement) m_yesModelNoFilterTmp.getElementAt(i);
			Pattern pattern = Pattern.compile(m_fYesModelFilter.getValue().toUpperCase());
			Matcher matcher = pattern.matcher(items.getName().toUpperCase());
			
			if(!matcher.find())
			{
				continue;
			}
			
			yesModel.addElement(items);
			m_yesModelNoFilterTmp.removeElement(items);
			i--;
		}
		for (int i=0; i<yesModel.getSize(); i++)
		{
			ListElement items = (ListElement) yesModel.getElementAt(i);
			Pattern pattern = Pattern.compile(
					m_fYesModelFilter.getValue().toUpperCase());
			Matcher matcher = pattern.matcher(items.getName().toUpperCase());
			
			if(matcher.find())
			{
				continue;
			}
			
			m_yesModelNoFilterTmp.addElement(items);
			yesModel.removeElement(items);
			i--;
		}
	}

	private void doNoModelFilter()
	{
		if(Util.isEmpty(m_fNoModelFilter.getValue(), true))
		{
			resetNoModelFilter();
			return;
		}
		for (int i=0; i<m_noModelNoFilterTmp.getSize(); i++)
		{
			ListElement items = (ListElement) m_noModelNoFilterTmp.getElementAt(i);
			Pattern pattern = Pattern.compile(m_fNoModelFilter.getValue().toUpperCase());
			Matcher matcher = pattern.matcher(items.getName().toUpperCase());
			
			if(!matcher.find())
			{
				continue;
			}
			
			noModel.addElement(items);
			m_noModelNoFilterTmp.removeElement(items);
			i--;
		}
		for (int i=0; i<noModel.getSize(); i++)
		{
			ListElement items = (ListElement) noModel.getElementAt(i);
			Pattern pattern = Pattern.compile(m_fNoModelFilter.getValue().toUpperCase());
			Matcher matcher = pattern.matcher(items.getName().toUpperCase());
			
			if(matcher.find())
			{
				continue;
			}
			
			m_noModelNoFilterTmp.addElement(items);
			noModel.removeElement(items);
			i--;
		}
	}
	
	private void resetYesModelFilter()
	{
		for(int i=0; i<m_yesModelNoFilterTmp.getSize(); i++)
		{
			ListElement items = (ListElement) m_yesModelNoFilterTmp.getElementAt(i);
			yesModel.addElement(items);
			m_yesModelNoFilterTmp.removeElement(items);
			i--;
		}
	}
	
	private void resetNoModelFilter()
	{
		for(int i=0; i<m_noModelNoFilterTmp.getSize(); i++)
		{
			ListElement items = (ListElement) m_noModelNoFilterTmp.getElementAt(i);
			noModel.addElement(items);
			m_noModelNoFilterTmp.removeElement(items);
			i--;
		}
	}
	
}	//UNSWSortTab


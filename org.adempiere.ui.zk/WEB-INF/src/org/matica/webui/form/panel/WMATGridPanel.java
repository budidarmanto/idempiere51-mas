/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Matteo Carminati	                                    * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Matteo Carminati (mcarminati@ma-tica.it)                        *
 *                                                                    *
 * Sponsors:                                                          *
 *  - MA-TICA (http://www.ma-tica.it/)                                *
 **********************************************************************/

package org.matica.webui.form.panel;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;

import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.compiere.model.MMAT3FormContainer;

import org.matica.webui.form.WMATListElementItem;
import org.matica.webui.form.MATADForm.WMATFormfunction;
import org.matica.webui.form.listener.WMATActionListener;
import org.zkoss.zul.Caption;
import org.zkoss.zul.Groupbox;
import org.zkoss.zul.impl.XulElement;

public class WMATGridPanel extends WMATBasePanel {

	private Grid m_grd = null;
	
	public WMATGridPanel(MMAT3FormContainer container,int matformid,WMATActionListener listener) {
		super(container,matformid,listener);
	}

	@Override
	public XulElement create() {
		m_grd = new Grid();
		
		Columns cols = new Columns();
		
		
//		m_grd.setOddRowSclass(null);
        m_grd.setWidth("100%");
        m_grd.setHeight("100%");
        
//        m_grd.setVflex(true);
//        m_grd.setStyle("margin:5px; padding:0; position: absolute");
        m_grd.makeNoStrip();
		
		double fldWidth = 100 / mat_formContainer.getMAT3NCols() * 0.75;
		double lblWidth = 100 / mat_formContainer.getMAT3NCols() * 0.25;;
		for (int i = 0 ; i < mat_formContainer.getMAT3NCols(); i++){			
			Column colLabel = new Column();
			colLabel.setWidth(Double.toString(lblWidth) + "%");
			colLabel.setAlign("right");
			cols.appendChild(colLabel);
			
			Column colFld = new Column();
			colFld.setWidth(Double.toString(fldWidth) + "%");
			colFld.setAlign("left");
			cols.appendChild(colFld);
			
			
		}
		
		m_grd.appendChild(cols);
		
		createChilds();
		
		if (mat_formContainer.getDescription() != null){
			
			Groupbox gb =new org.zkoss.zul.Groupbox();
			Caption caption = new Caption(mat_formContainer.getDescription());
			caption.setParent(gb);
			
			gb.setId(mat_formContainer.getName());
			gb.setClosable(false);
			gb.setMold("3d");
			gb.appendChild(m_grd);
			return (XulElement)gb;
			
		}else{
			m_grd.setId(mat_formContainer.getName());	
			return m_grd;
		}
		
		
	}

	
	
	@SuppressWarnings("deprecation")
	@Override
	protected void createChilds()
	{
		SortedMap<String,WMATListElementItem> lsted = WMATFormfunction.getFieldEditors(mat_form_id, mat_formContainer.get_ID(),mat_listener);
		SortedMap<String,WMATListElementItem> lstcn = WMATFormfunction.getContainers(mat_form_id,  mat_formContainer.get_ID(),mat_listener);

		
		
		SortedMap<String,WMATListElementItem> lst = this.unifyListComponents(lsted, lstcn);
		
		
		org.zkoss.zul.Row row = null;
		Rows rows = m_grd.newRows();

		
		int curlineno = -1;	
		String spanline = "";
		String valign = "";

		
		Set<String> keyset = lst.keySet();
		Iterator<String> iKeyset = keyset.iterator();
		
		while(iKeyset.hasNext())
		{
			String k = iKeyset.next();			
			WMATListElementItem item = lst.get(k);		
			
			int lineno = item.getRowpos();	
//			int colno = item.getColpos();	
			if (curlineno < 0){
				curlineno = lineno;
				row = new Row();
				spanline = "";
				valign = "";
			}else if (curlineno != lineno){
				row.setSpans(spanline.substring(0, spanline.length()-1));
				row.setValign(valign);
				rows.appendChild(row);
				curlineno =  lineno;
				row = new Row();	
				spanline = "";
				valign = "";
			}			
//			row.setValign(item.getVAlign());
			row.appendChild(item.getComponent());
			spanline += item.getRowspan() + ",";
			valign = item.getVAlign();
		}
		if (row != null){
			row.setValign(valign);
			row.setSpans(spanline.substring(0, spanline.length()-1));
			rows.appendChild(row);
		}

	}
	
	
}

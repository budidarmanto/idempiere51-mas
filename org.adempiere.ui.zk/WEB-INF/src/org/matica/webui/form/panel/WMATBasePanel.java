/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Matteo Carminati	                                    * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Matteo Carminati (mcarminati@ma-tica.it)                        *
 *                                                                    *
 * Sponsors:                                                          *
 *  - MA-TICA (http://www.ma-tica.it/)                                *
 **********************************************************************/

package org.matica.webui.form.panel;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;

import org.compiere.model.GridField;
import org.compiere.model.MMAT3FormContainer;
//import org.matica.form.listener.MATActionListener;
import org.matica.webui.form.WMATListElementItem;
import org.matica.webui.form.listener.WMATActionListener;
import org.zkoss.zk.ui.Component;
//import org.zkoss.zul.impl.XulElement;

public abstract class WMATBasePanel 
{
	protected MMAT3FormContainer mat_formContainer = null;
	protected int mat_form_id = 0;
	protected Hashtable<String, GridField> mat_fields = null;
	protected WMATActionListener  mat_listener = null;
	
	public WMATBasePanel(MMAT3FormContainer container,  int matformid, WMATActionListener listener)
	{
		mat_formContainer = container;
		mat_form_id = matformid;
		mat_listener = listener;
	}
	

	public abstract Component create();
	
	protected abstract void createChilds();
	
	protected SortedMap<String,WMATListElementItem> unifyListComponents(SortedMap<String,WMATListElementItem> lste,
			  SortedMap<String,WMATListElementItem> lstc)
	{
		SortedMap<String,WMATListElementItem> xret = lste;
		
		Set<String> keyset = lstc.keySet();
		Iterator<String> iKeyset = keyset.iterator();
		
		while(iKeyset.hasNext())
		{
			String k = iKeyset.next();			
			WMATListElementItem item = lstc.get(k);		
			item.setRowspan(item.getRowspan()+1);
			
			xret.put(k, item);
		
		}
		return xret;
	}
	
	
}

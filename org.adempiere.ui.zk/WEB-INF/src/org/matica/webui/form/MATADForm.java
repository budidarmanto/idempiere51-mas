/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Matteo Carminati	                                  * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Matteo Carminati (mcarminati@ma-tica.it)                        *
 *                                                                    *
 * Sponsors:                                                          *
 *  - MA-TICA (http://www.ma-tica.it/)                                *
 **********************************************************************/

package org.matica.webui.form;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Level;

import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.exception.ApplicationException;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.IFormController;
import org.adempiere.webui.util.ADClassNameMap;
import org.compiere.model.GridField;
import org.compiere.model.GridFieldVO;
import org.compiere.model.MMAT3Form;
import org.compiere.model.MMAT3FormContainer;
import org.compiere.model.X_AD_Val_Rule;
import org.compiere.model.X_MAT3_FormContainer;
import org.compiere.model.X_MAT3_FormField;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.matica.webui.form.listener.WMATActionListener;
import org.matica.webui.form.panel.WMATGridPanel;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Space;
import org.zkoss.zul.impl.XulElement;

public class MATADForm extends ADForm {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3848897932501220132L;

	private WMATActionListener m_alistner = null;
	
	@Override
	public void initForm() {
		this.setBorder("normal");
		this.setClosable(true);
		this.setSizable(true);
		
		this.setMaximizable(true);
		
		
		this.setHeight(null);
		this.setWidth(null);

		
//		this.setStyle("height: 100%; width: 100%");
	}

	@Override
	public void init(int adFormId, String name) {
		// TODO Auto-generated method stub
		super.init(adFormId, name);
	}

	public void setActionListner(WMATActionListener listener)
	{
		m_alistner = listener;
		m_alistner.setFrame(this);
	}

	public WMATActionListener getActionListner()
	{
		return m_alistner;
	}

//	public static MATADForm openForm (int adFormID)
	public boolean matOpenForm (int MAT3_Form_ID, WMATActionListener listener)
	{
		this.setActionListner(listener);
		
		
		Object obj;
		WMATForm form;
		String webClassName = "";
		MMAT3Form mform = new MMAT3Form(Env.getCtx(), MAT3_Form_ID, null);
    	String richClassName = mform.getClassname();
//    	String name = mform.get_Translation(MForm.COLUMNNAME_Name);

    	if (mform.get_ID() == 0 || richClassName == null)
    	{
			throw new ApplicationException("There is no form associated with the specified selection");
    	}
    	else
    	{

    		logger.info("MAT3_Form_ID=" + MAT3_Form_ID + " - Class=" + richClassName);

    		//static lookup
    		webClassName = ADClassNameMap.get(richClassName);
    		//fallback to dynamic translation
    		if (webClassName == null || webClassName.trim().length() == 0)
    		{
    			webClassName = WMATFormfunction.translateFormClassName(richClassName);
    		}
    		
    		if (webClassName == null)
    		{
    			throw new ApplicationException("Web UI form not implemented for the swing form " +
 					   richClassName);
    		}

    		try
    		{
    			//	Create instance w/o parameters
        		obj = ADForm.class.getClassLoader().loadClass(webClassName).newInstance();
    		}
    		catch (Exception e)
    		{
    			throw new ApplicationException("The selected web user interface custom form '" +
    					   webClassName +
    					   "' is not accessible.", e);
    		}

        	try
        	{
        		if (obj instanceof WMATForm)
        		{
        			init(mform.get_ID(), mform.getName());
        			form = (WMATForm)obj;
    				form.init(MAT3_Form_ID, this );    		
    				return true;
        		}
/*        		else if (obj instanceof IFormController)
        		{
        			IFormController customForm = (IFormController)obj;
        			Object o = customForm.getForm();
        			if(o instanceof MATADForm)
        			{
        				form = (MATADForm)o;
        				form.setICustomForm(customForm);
        				form.init(adFormID, name);
        				return true;
        			}
        			else
	        			throw new ApplicationException("The web user interface custom form '" +
	    						webClassName +
	    						"' cannot be displayed in the web user interface.");
	    						
        		}
*/        		
        		else
        		{
    				throw new ApplicationException("The web user interface custom form '" +
    						webClassName +
    						"' cannot be displayed in the web user interface.");
        		}
        	}
        	catch (Exception ex)
        	{
    			logger.log(Level.SEVERE, "Class=" + webClassName + ", MAT3_Form_ID=" + MAT3_Form_ID, ex);
    			throw new ApplicationException("The web user interface custom form '" +
    					webClassName +
    					"' failed to initialise:" + ex);
        	}
    	}
	}	//	openForm
	
	
	public static class WMATFormfunction
	{
		public static SortedMap<String,WMATListElementItem>  getContainers(int mat_form_id, Integer paretcontainer_id, WMATActionListener listener)
		{
			SortedMap<String,WMATListElementItem> lstc = new TreeMap<String, WMATListElementItem>();
			
			
			String sql = "SELECT  " + X_MAT3_FormContainer.COLUMNNAME_MAT3_FormContainer_ID + " " + 
			"FROM " + X_MAT3_FormContainer.Table_Name + " where " + X_MAT3_FormContainer.COLUMNNAME_MAT3_Form_ID + " = ? " +
			" and " + X_MAT3_FormContainer.COLUMNNAME_IsActive + " = ? " ;
			if (paretcontainer_id == null){
				sql += "and " + X_MAT3_FormContainer.COLUMNNAME_MAT3_ContainerParent_ID + " is null  " ;
			}else{
				sql += "and " + X_MAT3_FormContainer.COLUMNNAME_MAT3_ContainerParent_ID + " = " + Integer.toString(paretcontainer_id)  + " ";
			}
					
			
			sql += "ORDER BY " + X_MAT3_FormContainer.COLUMNNAME_MAT3RowPos + ", " + X_MAT3_FormContainer.COLUMNNAME_MAT3ColPos + " ";

			XulElement cntr = null;
			
			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			try {
				pstmt.setInt(1, mat_form_id	);
				pstmt.setString(2, "Y");
				
				ResultSet rs = pstmt.executeQuery();
				while (rs.next())
				{
					//create the container
					MMAT3FormContainer mfc = new MMAT3FormContainer(Env.getCtx(), rs.getInt(X_MAT3_FormContainer.COLUMNNAME_MAT3_FormContainer_ID), null);
					if (mfc.getMAT3ContainerType().equals(MMAT3FormContainer.MAT3CONTAINERTYPE_Panel)){
						WMATGridPanel gpanel = new WMATGridPanel(mfc,  mat_form_id, listener);
						cntr = gpanel.create();												
					}
					
					WMATListElementItem item = new  WMATListElementItem(cntr,mfc.getMAT3RowPos(),mfc.getMAT3ColPos());
	                int rowspan = mfc.getMAT3RowSpan();
	                if (rowspan > 1 & paretcontainer_id != null){
	                	rowspan = rowspan * 2;
	                }
					item.setRowspan(rowspan);
					item.setVAlign("top");
					lstc.put(item.getKey(), item);
									
				}
				rs.close();
				pstmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return lstc;

		}
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		public static SortedMap<String,WMATListElementItem>  getFieldEditors(int mat_form_id, int mat_formcontainer_id, WMATActionListener listener)
		{
			SortedMap<String,WMATListElementItem> lstf = new TreeMap<String, WMATListElementItem>();
			WMATListElementItem item = null;
			
			String sql = "SELECT " +
					 X_MAT3_FormField.COLUMNNAME_MAT3_FormField_ID + " as AD_Field_ID, " +
				     "vr." + X_AD_Val_Rule.COLUMNNAME_Code + " as Validationcode, " + 
					 "f." + X_MAT3_FormField.COLUMNNAME_DisplayLength + " as fieldlength, f.* " + 
					 "FROM " + X_MAT3_FormField.Table_Name +" f " + 
					 "LEFT JOIN " + X_AD_Val_Rule.Table_Name + " vr ON vr." + X_AD_Val_Rule.COLUMNNAME_AD_Val_Rule_ID + " = f." + X_MAT3_FormField.COLUMNNAME_AD_Val_Rule_ID + " " +
					 "WHERE f." + X_MAT3_FormField.COLUMNNAME_MAT3_FormContainer_ID + " = ? " +
					 "and f." + X_MAT3_FormField.COLUMNNAME_IsActive + " = ? " +
					 "ORDER BY " + X_MAT3_FormField.COLUMNNAME_MAT3RowPos + ", "  + X_MAT3_FormField.COLUMNNAME_MAT3ColPos + "  ";

			PreparedStatement pstmt = DB.prepareStatement(sql, null);
			try {
				pstmt.setInt(1, mat_formcontainer_id	);
				pstmt.setString(2, "Y");
				
				ResultSet rs = pstmt.executeQuery();
				while (rs.next())
				{	
					int rowpos = rs.getInt(X_MAT3_FormField.COLUMNNAME_MAT3RowPos);
					int colpos = rs.getInt(X_MAT3_FormField.COLUMNNAME_MAT3ColPos) * 2;
					
					String fieldName = rs.getString(X_MAT3_FormField.COLUMNNAME_ColumnName);
					//String customEditorClass =  rs.getString(X_MAT3_FormField.COLUMNNAME_MAT3EditorClass);
					GridFieldVO voF = GridFieldVO.create(Env.getCtx(), mat_form_id, mat_formcontainer_id, mat_form_id, mat_formcontainer_id, false, rs);
					GridField mField = new GridField (voF);
					
					

					//create the elment
		     		WEditor editor = WMATEditorFactory.getEditor(mField);
		            if (editor != null) // Not heading
		            {
		                editor.setGridTab(null);
		                mField.addPropertyChangeListener(editor);
		                if (mField.isFieldOnly())
		                {
							item = new  WMATListElementItem(new Space(),rowpos,colpos-1);
							lstf.put(item.getKey(), item);
		                }
		                else
		                {
		                	Div div = new Div();
		                    div.setAlign("right");
		                    org.adempiere.webui.component.Label label = editor.getLabel();
		                    div.appendChild(label);
		                    if (label.getDecorator() != null)
		                    {
		                     	div.appendChild(label.getDecorator());
		                    }
		                    item = new  WMATListElementItem(div,rowpos,colpos-1);
		                    item.setVAlign("center");
		                    lstf.put(item.getKey(), item);
		                	
		                }
		                
		                editor.removeValuechangeListener(null);
		                editor.addValueChangeListener(listener);
		                
		                //init listeners
		                for (String event : editor.getEvents())
		                {
		                	editor.getComponent().addEventListener(event, listener);
		                }

		                
		                
		    			//  Set Default	    			
		    			Object defaultObject = mField.getDefault();
		    			mField.setValue (defaultObject, true);

		 			                  					
	                    item = new  WMATListElementItem(editor.getComponent(),rowpos,colpos);
		                int rowspan = rs.getInt(X_MAT3_FormField.COLUMNNAME_MAT3RowSpan);
		                if (rowspan > 1 ){
		                	rowspan = rowspan * 2;
		                }
	                    item.setRowspan(rowspan);
	                    item.setVAlign("center");
	                    lstf.put(item.getKey(), item);
	                    
	                    listener.addField(fieldName, mField, editor);
		                
		            }
					
				}
				rs.close();
				pstmt.close();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			return lstf;
		}
		
	
	
	
		private static String translateFormClassName(String originalName)
		{
			
			if (originalName.equals("org.matica.form.VMATForm")){
				return "org.matica.webui.form.WMATForm";
			}
			
			String zkName = null;
			/*
			 * replacement string to translate class paths to the form
			 * "org.adempiere.webui.apps.form.<classname>"
			 */
			final String zkPackage = "org.adempiere.webui.";
			/*
			 * replacement string to translate custom form class name from
			 * "V<name>" to "W<name>"
			 */
			final String zkPrefix = "W";
			final String swingPrefix = "V";
	
	        String tail = null;
	        //first, try replace package
			if (originalName.startsWith("org.compiere."))
			{
				tail = originalName.substring("org.compiere.".length());
			}
			else if(originalName.startsWith("org.adempiere."))
			{
				tail = originalName.substring("org.adempiere.".length());
			}
			if (tail != null)
			{
				zkName = zkPackage + tail;
				
	    		try {
					Class<?> clazz = ADForm.class.getClassLoader().loadClass(zkName);
					if (!isZkFormClass(clazz))
					{
						zkName = null;
					}
				} catch (ClassNotFoundException e) {
					zkName = null;
				}
				
				//try replace package and add W prefix to class name
				if (zkName == null)
				{
					String packageName = zkPackage;
					int lastdot = tail.lastIndexOf(".");
					String className = null;
					if (lastdot >= 0)
					{
						if (lastdot > 0)
							packageName = packageName + tail.substring(0, lastdot+1);
						className = tail.substring(lastdot+1);
					}
					else
					{
						className = tail;
					}
					
					//try convert V* to W*
					if (className.startsWith(swingPrefix))
					{
						zkName = packageName + zkPrefix + className.substring(1);
						try {
							Class<?> clazz = ADForm.class.getClassLoader().loadClass(zkName);
							if (!isZkFormClass(clazz))
							{
								zkName = null;
							}
						} catch (ClassNotFoundException e) {
							zkName = null;
						}
					}
					
					//try append W prefix to original class name
					if (zkName == null)
					{
						zkName = packageName + zkPrefix + className;
						try {
							Class<?> clazz = ADForm.class.getClassLoader().loadClass(zkName);
							if (!isZkFormClass(clazz))
							{
								zkName = null;
							}
						} catch (ClassNotFoundException e) {
							zkName = null;
						}
					}
				}
	        }
	        
			/*
			 *  not found, try changing only the class name 
			 */
			if (zkName == null)
			{
				int lastdot = originalName.lastIndexOf(".");
				String packageName = originalName.substring(0, lastdot);
				String className = originalName.substring(lastdot+1);
				//try convert V* to W*
				if (className.startsWith(swingPrefix))
				{
					String zkClassName = zkPrefix + className.substring(1);
					zkName = packageName + "." + zkClassName;
					try {
						Class<?> clazz = ADForm.class.getClassLoader().loadClass(zkName);
						if (!isZkFormClass(clazz))
						{
							zkName = null;
						}
					} catch (ClassNotFoundException e) {
						zkName = null;
					}
				}
				
				//try just append W to the original class name
				if (zkName == null)
				{				
					String zkClassName = zkPrefix + className;
					zkName = packageName + "." + zkClassName;
					try {
						Class<?> clazz = ADForm.class.getClassLoader().loadClass(zkName);
						if (!isZkFormClass(clazz))
						{
							zkName = null;
						}
					} catch (ClassNotFoundException e) {
						zkName = null;
					}
				}
				
				if (zkName == null)
				{
					//finally try whether same name is used for zk
					zkName = originalName;
					try {
						Class<?> clazz = ADForm.class.getClassLoader().loadClass(zkName);
						if (!isZkFormClass(clazz))
						{
							zkName = null;
						}
					} catch (ClassNotFoundException e) {
						zkName = null;
					}
				}
			}
	
			return zkName;
		}
	
		private static boolean isZkFormClass(Class<?> clazz) {
			return IFormController.class.isAssignableFrom(clazz) || Component.class.isAssignableFrom(clazz);
		}

	}


	
	
	
}

/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Matteo Carminati	                                    * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Matteo Carminati (mcarminati@ma-tica.it)                        *
 *                                                                    *
 * Sponsors:                                                          *
 *  - MA-TICA (http://www.ma-tica.it/)                                *
 **********************************************************************/

package org.matica.webui.form;



import java.util.Properties;


import org.adempiere.webui.component.Window;
//import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.session.SessionManager;
import org.compiere.model.MMAT3Form;
import org.compiere.process.ProcessInfo;
//import org.matica.form.listener.MATActionListener;
import org.matica.form.listener.MAT_I_BusinessLogic;
import org.uns.matica.loader.LoaderFactory;


public class WMATOpenForm  {
	
	
	

	public static boolean open(Properties ctx, String trxName, int AD_Form_ID, ProcessInfo pi) throws Exception 
	{
		
		MMAT3Form form = null;
		try {
			form = MMAT3Form.get(ctx,trxName,AD_Form_ID);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
		
		
		if (form == null){
			return false;
		}
		
		//Integer m_form_id = null;
		String webuiListnerClass = form.getMAT3WebuiListenerClass();
		String businessLogicClass = form.getMAT3BusinessLogicClass();
		int mat3_form_id = form.get_ID();
		//Integer record_id = 0;
		boolean ismodal = form.isMAT3IsModal();
		
		form = null;
		
		
		String errmsg ="";
		if (webuiListnerClass == null) {
			errmsg = "Il processo chiamante deve avere il parametro 'WebuiListnerClass'";
			throw new Exception(errmsg);
		}
		if (businessLogicClass == null) {
			errmsg = "Il processo chiamante deve avere il parametro 'BusinessClass'";
			throw new Exception(errmsg);
		}
		
		

		org.matica.webui.form.listener.WMATActionListener lst = (org.matica.webui.form.listener.WMATActionListener)Class.forName(webuiListnerClass).newInstance();
		
		MATADForm frame = null;
		frame = new MATADForm();
		lst.setEnvironment(pi, ctx, trxName);
		//MAT_I_BusinessLogic bLogic = (MAT_I_BusinessLogic)Class.forName(businessLogicClass).newInstance();
		/*
		 * Begin: AzHaidar modification to make it compatible with iDempiere architecture.
		 */
		MAT_I_BusinessLogic bLogic = LoaderFactory.getBusinessLogic(businessLogicClass);
		/*
		 * End: AzHaidar.
		 */
		
		lst.setBusinessLogicHandler(bLogic);

		
		frame.matOpenForm(mat3_form_id,lst);
		
		
		if (ismodal == false){
			frame.setAttribute(Window.MODE_KEY, Window.MODE_EMBEDDED);
			frame.setAttribute(Window.INSERT_POSITION_KEY, Window.INSERT_NEXT);
			
			SessionManager.getAppDesktop().showWindow(frame);
		}else{
			org.adempiere.webui.apps.AEnv.showCenterScreen(frame);
		}
		
		return true;
		
/**
		if (Env.getCtx().containsKey("servlet.sessionId")){
			org.adempiere.webui.session.SessionManager.getAppDesktop().openForm(formId)

			org.adempiere.webui.window.FDialog.info(0, null, â€œHello World WEB Clientâ€�);
			//	Create instance w/o parameters
    		
			Object obj = org.adempiere.webui.panel.ADForm.class.getClassLoader().loadClass("org.adempiere.webui.apps.form.WBOMDrop").newInstance();
			org.adempiere.webui.panel.ADForm form = (org.adempiere.webui.panel.ADForm)obj;
		
			org.adempiere.webui.panel.ADForm form = (org.adempiere.webui.panel.ADForm)Class.forName("org.adempiere.webui.apps.form.WBOMDrop").newInstance();
			
			org.matica.webui.form.listener.WMATActionListener lst = (org.matica.webui.form.listener.WMATActionListener)Class.forName(this.m_webuiListnerClass).newInstance();
			
			MATADForm frame = null;
			frame = new MATADForm();
			lst.setRecord_ID(m_record_id);
			MAT_I_BusinessLogic bLogic = (MAT_I_BusinessLogic)Class.forName(this.m_businessLogicClass).newInstance();
			
			lst.setBusinessLogicHandler(bLogic);

			frame.matOpenForm(m_form_id,lst);
			
			org.adempiere.webui.apps.AEnv.showCenterScreen(frame);
		
		} else {		
*
*/			
	}
}
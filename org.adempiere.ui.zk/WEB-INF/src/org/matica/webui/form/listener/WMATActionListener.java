/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Matteo Carminati	                                    * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Matteo Carminati (mcarminati@ma-tica.it)                        *
 *                                                                    *
 * Sponsors:                                                          *
 *  - MA-TICA (http://www.ma-tica.it/)                                *
 **********************************************************************/

package org.matica.webui.form.listener;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.adempiere.exceptions.FillMandatoryException;
import org.adempiere.webui.editor.WEditor;
import org.adempiere.webui.event.ValueChangeEvent;
import org.adempiere.webui.event.ValueChangeListener;
import org.compiere.model.GridField;
import org.compiere.model.PO;
import org.compiere.process.ProcessInfo;
import org.matica.form.listener.MAT_ButtonEvent;
import org.matica.form.listener.MAT_FieldChangeEvent;
import org.matica.form.listener.MAT_I_BusinessLogic;
import org.matica.form.listener.MAT_I_Listener;
import org.matica.webui.form.MATADForm;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;

@SuppressWarnings("rawtypes")
public  class WMATActionListener implements EventListener, 
											ValueChangeListener,
											PropertyChangeListener,
											MAT_I_Listener
{

	protected ProcessInfo m_pi;
	protected MATADForm m_frame = null;	

	private boolean wopen = false;

	protected static Hashtable<String, GridField> m_mFields;
	private static Hashtable<String, WEditor> m_vEditors;
	
	private MAT_I_BusinessLogic m_bLogicHandler  = null; 
	
	private Properties m_ctx;
	private String m_trxName;

	public WMATActionListener()
	{
		m_mFields = new Hashtable<String, GridField>();
		m_vEditors = new Hashtable<String, WEditor>();
		
	}

	public void setFrame(MATADForm frame)
	{
		m_frame = frame;
	}

	public void setEnvironment(ProcessInfo pi, Properties ctx, String trxName)
	{
		m_pi = pi;
		m_ctx = ctx;
		m_trxName = trxName;
		
	}

	public void setBusinessLogicHandler(MAT_I_BusinessLogic blogichnd)
	{
		m_bLogicHandler = blogichnd;
		blogichnd.setListenerOwner(this);
	}

	
	
	
	
	public void addField(String fieldname, GridField field,WEditor editor){
		m_mFields.put(fieldname, field);
		m_vEditors.put(fieldname, editor);
	}

	public GridField getField(int idx)
	{
		
		return (GridField)m_mFields.values().toArray()[idx];
		
	}

		
	public GridField getField(String fieldName)
	{		
		return m_mFields.get(fieldName);
	}
	
	public GridField getFieldByZkID(String id)
    {
		
		
		
		Set<String> keyset = m_vEditors.keySet();
		Iterator<String> iKeyset = keyset.iterator();
		
		while(iKeyset.hasNext())
		{	
			String k = iKeyset.next();					
			WEditor ed = m_vEditors.get(k);
			if (ed.getComponent().getId().equals(id)){
				return ed.getGridField();
			}
			
		}
		
		return null;
	}
	
	public WEditor getEditor(String fieldName)
	{
		return m_vEditors.get(fieldName);
	}

	@Override
	public final void onEvent(Event event) throws Exception {
		if (wopen == false && event.getName().equals(Events.ON_MOVE) )
		{
			//Fire onWindowOpened Event
			this.m_bLogicHandler.onWindowOpened();
			wopen = true;
		}else if (event.getName().equals(Events.ON_CLOSE)){
			this.m_bLogicHandler.onWindowClosed();
		}else{	
			GridField fld = getFieldByZkID(event.getTarget().getId());	
			if (fld != null){
				if (event.getName().equals(Events.ON_CLICK)){
					this.m_bLogicHandler.onButton(new MAT_ButtonEvent(event.getTarget(), 
														fld.getColumnName()));
				}
			}else if (event.getTarget().getDefinition().getName().equals("button")){
				if (event.getTarget().getId().equals("Ok")){
					
					this.m_bLogicHandler.onConfirmOK();
				}else if (event.getTarget().getId().equals("Cancel")){
					
					this.m_bLogicHandler.onConfirmCancel();
				}

			}
		}
		
	}

	@Override
	public final void valueChange(ValueChangeEvent evt) {
		System.out.println("onEvent --> eventName:" + evt.getPropertyName() +  " " +evt);
		//Refresh other field by display logic expression
		GridField fld = getField(evt.getPropertyName());
		fld.setValue(evt.getNewValue(),true);

		this.m_bLogicHandler.onFieldChange(new MAT_FieldChangeEvent(evt.getSource(), 
								evt.getPropertyName(), evt.getOldValue(), evt.getNewValue()));
	}


	@Override
	public final void propertyChange(PropertyChangeEvent evt) {
		System.out.println("propertyChange --> " + evt.getPropertyName() + " " + evt);		
	}
	
	@Override
	public Object getFieldValue(String fieldName) {
		return m_mFields.get(fieldName).getValue();		
	}

	@Override
	public void setFieldValue(String fieldName, Object newValue) {
		//GridField fld = m_mFields.get(fieldName);
		//fld.setValue(newValue, false);		
		GridField fld = m_mFields.get(fieldName);
		
		if (fld.isMandatory(false) && newValue == null)
		{
			Object currValue = fld.getValue();
			if (currValue == null)
				currValue = fld.getOldValue();
			if (currValue == null) // if still null then throw 
				throw new FillMandatoryException(fieldName);
		}
		fld.setValue(newValue, false);
	}

	@Override
	public ProcessInfo getProcessInfo() {
		return m_pi;
	}

	@Override
	public String getTrxName() {
		return m_trxName;
	}

	@Override
	public Properties getCtx() {
		return m_ctx;
	}

	@Override
	public void disposeFrame() {
		m_frame.dispose();
		
	}

	@Override
	public void setFrameTitle(String title) {
		m_frame.setTitle(title);		
	}

	@Override
	public void setFrameWidth(int width) {
		m_frame.setWidth(width+"px");
	}

	@Override
	public void setFrameHeight(int height) {
		m_frame.setHeight(height+"px");		
	}

	@Override
	public boolean isWebui() {
		return true;
	}

	@Override
	public void setPanelVisible(String panelName, boolean visible) {

		Component cmp =  (Component)m_frame.getFellow(panelName);
		if (cmp != null){
			cmp.setVisible(visible);
		}
	
	}

	@Override
	public void setFieldValue(String fieldName, PO po) 
	{
		GridField fld = m_mFields.get(fieldName);
		
		Object newValue = po.get_Value(fieldName);
		
		if (fld.isMandatory(false) && newValue == null)
		{
			Object currValue = fld.getValue();
			if (currValue == null)
				currValue = fld.getOldValue();
			if (currValue == null) // if still null then throw 
				throw new FillMandatoryException(fieldName);
			newValue = currValue;
		}
		fld.setValue(newValue, false);
	}
}

/**********************************************************************
 * This file is part of Adempiere ERP Bazaar                          * 
 * http://www.adempiere.org                                           * 
 *                                                                    * 
 * Copyright (C) Matteo Carminati	                                    * 
 * Copyright (C) Contributors                                         * 
 *                                                                    * 
 * This program is free software; you can redistribute it and/or      * 
 * modify it under the terms of the GNU General Public License        * 
 * as published by the Free Software Foundation; either version 2     * 
 * of the License, or (at your option) any later version.             * 
 *                                                                    * 
 * This program is distributed in the hope that it will be useful,    * 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of     * 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the       * 
 * GNU General Public License for more details.                       * 
 *                                                                    * 
 * You should have received a copy of the GNU General Public License  * 
 * along with this program; if not, write to the Free Software        * 
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,         * 
 * MA 02110-1301, USA.                                                * 
 *                                                                    * 
 * Contributors:                                                      * 
 *  - Matteo Carminati (mcarminati@ma-tica.it)                        *
 *                                                                    *
 * Sponsors:                                                          *
 *  - MA-TICA (http://www.ma-tica.it/)                                *
 **********************************************************************/
package org.matica.webui.form;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;

import org.zkoss.zk.ui.event.Events;
import org.adempiere.webui.component.Column;
import org.adempiere.webui.component.Columns;
import org.adempiere.webui.component.ConfirmPanel;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Panel;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.panel.ADForm;
import org.compiere.model.MMAT3Form;

import org.compiere.util.Env;

//import org.matica.form.MATFormFrame;
import org.matica.webui.form.MATADForm.WMATFormfunction;


//public class WMATForm extends MATADForm {
public class WMATForm extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3496532499253883758L;


	//private Hashtable<String, GridField> m_mFields = new Hashtable<String, GridField>();

	
	/**	MAT3_Form_ID				*/
	private int m_mat3formid = 0;
	
	/** MAT3_form					*/
	private MMAT3Form m_mat3form = null;

	/**	FormFrame					*/
	protected MATADForm m_frame;

	
	public void init(int MAT3_Form_ID, ADForm formFrame) {
		
		m_mat3formid = MAT3_Form_ID;
		m_mat3form = new MMAT3Form(Env.getCtx(), m_mat3formid, null);
		
		m_frame = (MATADForm)formFrame;
		
		m_frame.addEventListener(Events.ON_MOVE, m_frame.getActionListner());
		m_frame.addEventListener(Events.ON_CLOSE, m_frame.getActionListner());
		
		m_frame.getChildren().clear();
		Grid child = this.createMainContainer();
		child.setInnerWidth(null);
		m_frame.appendChild(child);

		
		
/*		this.setBorder("normal");
		this.setClosable(true);
		this.setSizable(true);
		
		this.setMaximizable(true);
		*/
		
//		this.setHeight("100%");
//		this.setWidth("100%");
	}


	

	
	
	
	
	private Grid createMainContainer()
	{
		Grid grd = new Grid();
		
		Columns cols = new Columns();
		
		
//		grd.setOddRowSclass(null);
        grd.setWidth("100%");
        grd.setHeight("100%");
        grd.setVflex(true);
//        grd.setStyle("margin:0; padding:0; position: absolute");
        grd.makeNoStrip();
		
        MMAT3Form mform = new MMAT3Form(Env.getCtx(), m_mat3formid, null); 
        
        
        double colWidth = 100.0 / mform.getMAT3NCols();
        
		for (int i = 0 ; i < mform.getMAT3NCols(); i++){			
			Column col = new Column();
			col.setWidth(Double.toString(colWidth) + "%");
			cols.appendChild(col);
			
		}
		
		grd.appendChild(cols);
		
		
		org.zkoss.zul.Row row = null;
		Rows rows = grd.newRows();

		SortedMap<String,WMATListElementItem> lstc = WMATFormfunction.getContainers(mform.get_ID(),null,m_frame.getActionListner());
		
		int curlineno = -1;		
		
		Set<String> keyset = lstc.keySet();
		Iterator<String> iKeyset = keyset.iterator();
		
		while(iKeyset.hasNext())
		{
			String k = iKeyset.next();			
			WMATListElementItem item = lstc.get(k);		
			
			int lineno = item.getRowpos();	
			if (curlineno < 0){
				curlineno = lineno;
				row = new Row();
				
			}else if (curlineno != lineno){
				rows.appendChild(row);
				curlineno =  lineno;
				row = new Row();				
			}
			row.setValign("top");
			row.appendChild(item.getComponent());
		}
		if (row != null){
			row.setValign("top");
			rows.appendChild(row);
		}
		
		//Confirmpanel
		ConfirmPanel cpanel = createConfirmPanel();
		if (cpanel != null){
			row = new Row();
			row.setValign("bottom");
			row.setSpans("" + mform.getMAT3NCols() );					
			row.appendChild(cpanel);	
			rows.appendChild(row);
		}
		
		
		return grd;

	}

	/**
	 * Build Adempier Confirm panel
	 * @return
	 */
	private ConfirmPanel createConfirmPanel()
	{
	
		if (m_mat3form.isMAT3ConfirmPanel() == true){
			ConfirmPanel confirmPanel = new ConfirmPanel (m_mat3form.isMAT3ConfirmPanel_withCancel(),
														  m_mat3form.isMAT3ConfirmPanel_withRefresh(),
														  false,
														  false,
														  false,
														  false,
														  false);
			confirmPanel.addActionListener(m_frame.getActionListner());
			
			return confirmPanel;
		}else{
			return null;
		}
	}
	
	
	
	
}



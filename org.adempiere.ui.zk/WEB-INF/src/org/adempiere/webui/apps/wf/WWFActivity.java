/******************************************************************************
 * Copyright (C) 2008 Low Heng Sin                                            *
 * Copyright (C) 2008 Idalica Corporation                                     *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 *****************************************************************************/
package org.adempiere.webui.apps.wf;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.adempiere.webui.LayoutUtils;
import org.adempiere.webui.apps.AEnv;
import org.adempiere.webui.component.Button;
import org.adempiere.webui.component.Grid;
import org.adempiere.webui.component.Label;
import org.adempiere.webui.component.ListHeader;
import org.adempiere.webui.component.ListItem;
import org.adempiere.webui.component.ListModelTable;
import org.adempiere.webui.component.Listbox;
import org.adempiere.webui.component.Row;
import org.adempiere.webui.component.Rows;
import org.adempiere.webui.component.Textbox;
import org.adempiere.webui.component.WListItemRenderer;
import org.adempiere.webui.component.WListbox;
import org.adempiere.webui.component.Window;
import org.adempiere.webui.editor.WSearchEditor;
import org.adempiere.webui.panel.ADForm;
import org.adempiere.webui.panel.StatusBarPanel;
import org.adempiere.webui.session.SessionManager;
import org.adempiere.webui.panel.WAttachment;
import org.adempiere.webui.theme.ThemeManager;
import org.adempiere.webui.util.ZKUpdateUtil;
import org.adempiere.webui.window.FDialog;
import org.compiere.model.MAttachment;
import org.compiere.model.MColumn;
import org.compiere.model.MLookup;
import org.compiere.model.MLookupFactory;
import org.compiere.model.MPayment;
import org.compiere.model.MQuery;
import org.compiere.model.MRefList;
import org.compiere.model.MRole;
import org.compiere.model.MSysConfig;
import org.compiere.model.PO;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.util.ValueNamePair;
import org.compiere.wf.MWFActivity;
import org.compiere.wf.MWFNode;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Borderlayout;
import org.zkoss.zul.Center;
import org.zkoss.zul.North;
import org.zkoss.zul.South;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Html;
import org.zkoss.zul.West;

import com.uns.model.IUNSApprovalInfo;
import com.uns.model.MUNSJobCareTaker;

/**
 * Direct port from WFActivity
 * @author hengsin
 *
 */
public class WWFActivity extends ADForm implements EventListener<Event>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8405802852868437716L;
	/**	Window No					*/
	private int         		m_WindowNo = 0;
	/**	Open Activities				*/
	private List<MWFActivity> 		m_activities = null;
	/**	Current Activity			*/
	private MWFActivity 		m_activity = null;
	/**	Current Activity			*/
	private int	 				m_index = 0;
	/**	Set Column					*/
	private	MColumn 			m_column = null;
	/**	Logger			*/
	private static CLogger log = CLogger.getCLogger(WWFActivity.class);

	//
	private Label lSearch = new Label ("Search");
	private Textbox fSearch = new Textbox();	
	private Label lNode = new Label(Msg.translate(
			Env.getCtx(), "AD_WF_Node_ID"));
	private Textbox fNode = new Textbox();
	private Label lDesctiption = new Label(Msg.translate(Env.getCtx(), "Description"));
	private Textbox fDescription = new Textbox();
	private Label lHelp = new Label(Msg.translate(Env.getCtx(), "Help"));
	private Textbox fHelp = new Textbox();
	private Label lHistory = new Label(Msg.translate(Env.getCtx(), "History"));
	private Html fHistory = new Html();
	private Label lAnswer = new Label(Msg.getMsg(Env.getCtx(), "Answer"));
	private Textbox fAnswerText = new Textbox();
	private Listbox fAnswerList = new Listbox();
	private Button fAnswerButton = new Button();
	private Button bZoom = new Button();
	private Button bAtt = new Button();
	private Label lTextMsg = new Label(Msg.getMsg(Env.getCtx(), "Messages"));
	private Textbox fTextMsg = new Textbox();
	private Button bOK = new Button();
	private WSearchEditor fForward = null;	//	dynInit
	private Label lForward = new Label(Msg.getMsg(Env.getCtx(), "Forward"));
	private Label lOptional = new Label("(" + Msg.translate(Env.getCtx(), "Optional") + ")");
	private StatusBarPanel statusBar = new StatusBarPanel();

	private ListModelTable model = null;
	private WListbox listbox = new WListbox();
	private ListModelTable detailTable = null;
	private WListbox detailListBox = new WListbox();
	private Vector<ActivityMapping> m_tmp = new Vector<>();
	private final static String HISTORY_DIV_START_TAG = 
			"<div style='height: auto; border: 1px solid #7F9DB9;'>";
	private boolean m_isMultiselection = false;
	
	public WWFActivity()
	{
		super();
		LayoutUtils.addSclass("workflow-activity-form", this);
	}

    protected void initForm()
    {
        fAnswerList.setMold("select");   
        String multiSelection = DB.getSQLValueString(null, 
				"SELECT IsApprovalMultiSelection FROM AD_User WHERE AD_User_ID = ?",
				Env.getAD_User_ID(Env.getCtx()));
		m_isMultiselection = multiSelection == null ? false 
				: multiSelection.equals("Y");

		loadActivities();
		detailListBox.addActionListener(new EventListener<Event>() {
   			public void onEvent(Event event) throws Exception {
   				PO po = m_activity.getPO();
				IUNSApprovalInfo appInfo = (IUNSApprovalInfo) po;
				boolean isShowAttachmentDetail = appInfo.isShowAttachmentDetail();
				int tableIDDetail = appInfo.getTableIDDetail();
				int row = detailListBox.getSelectedRow();
	        	int column = detailListBox.getColumnCount() - 1;
				if(event.getName().equals(Events.ON_SELECT) && isShowAttachmentDetail)
		        {
		        	int recordID = new Integer(detailListBox.getValueAt(row, column).toString());
		        	int Attach_ID = MAttachment.getID(tableIDDetail, recordID);
					new WAttachment(m_WindowNo, Attach_ID, tableIDDetail, recordID, null);
		        }
   			}
   		}); 

    	bZoom.setImage(ThemeManager.getThemeResource("images/Zoom16.png"));
    	bOK.setImage(ThemeManager.getThemeResource("images/Ok24.png"));
    	bAtt.setImage(ThemeManager.getThemeResource("images/Attachment16.png"));

        MLookup lookup = MLookupFactory.get(Env.getCtx(), m_WindowNo,
                0, 10443, DisplayType.Search);
        fForward = new WSearchEditor(lookup, Msg.translate(
                Env.getCtx(), "AD_User_ID"), "", true, false, true);

        init();
        display(-1);
    }

	private void init()
	{
		Grid grid = new Grid();
		grid.setWidth("100%");
        grid.setHeight("100%");
        grid.setStyle("margin:0; padding:0; position: absolute; align: center; valign: center;");
        grid.makeNoStrip();
        grid.setOddRowSclass("even");

		Rows rows = new Rows();
		grid.appendChild(rows);

		Row row = new Row();
		rows.appendChild(row);
		Div div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lSearch);
		row.appendChild(div);
		row.appendChild(fSearch);
		fSearch.setWidth("100%");
//		fSearch.setHflex("true");
		fSearch.setReadonly(false);
		fSearch.addEventListener(Events.ON_CHANGE, this);
		row.appendChild(bAtt);
		bAtt.addEventListener(Events.ON_CLICK, this);
		
		row = new Row();
		rows.appendChild(row);
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lNode);
		row.appendChild(div);
		row.appendChild(fNode);
		fNode.setWidth("100%");
//		fNode.setHflex("true");
		fNode.setReadonly(true);

		row = new Row();
		rows.appendChild(row);
		row.setValign("top");
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lDesctiption);
		row.appendChild(div);
		row.appendChild(fDescription);
		fDescription.setMultiline(true);
		fDescription.setWidth("100%");
//		fDescription.setHflex("true");
		fDescription.setReadonly(true);

		row = new Row();
		rows.appendChild(row);
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lHelp);
		row.appendChild(div);
		row.appendChild(fHelp);
		fHelp.setMultiline(true);
		fHelp.setRows(3);
		fHelp.setWidth("100%");
		fHelp.setHeight("100%");
//		fHelp.setHflex("true");
		fHelp.setReadonly(true);
		row.appendChild(new Label());

		row = new Row();
		rows.appendChild(row);
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lHistory);
		row.appendChild(div);
		row.appendChild(fHistory);
		fHistory.setHflex("true");
		row.appendChild(new Label());

		row = new Row();
		rows.appendChild(row);
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lAnswer);
		row.appendChild(div);
		Hbox hbox = new Hbox();
		hbox.appendChild(fAnswerText);
		fAnswerText.setHflex("true");
		hbox.appendChild(fAnswerList);
		hbox.appendChild(fAnswerButton);
		fAnswerButton.addEventListener(Events.ON_CLICK, this);
		row.appendChild(hbox);
		row.appendChild(bZoom);
		bZoom.addEventListener(Events.ON_CLICK, this);

		row = new Row();
		rows.appendChild(row);
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lTextMsg);
		row.appendChild(div);
		row.appendChild(fTextMsg);
//		fTextMsg.setHflex("true");
		fTextMsg.setMultiline(true);
		fTextMsg.setWidth("100%");
		row.appendChild(new Label());

		row = new Row();
		rows.appendChild(row);
		div = new Div();
		div.setStyle("text-align: right;");
		div.appendChild(lForward);
		row.appendChild(div);
		hbox = new Hbox();
		hbox.appendChild(fForward.getComponent());
		hbox.appendChild(lOptional);
		row.appendChild(hbox);
		row.appendChild(bOK);
		bOK.addEventListener(Events.ON_CLICK, this);

		Borderlayout layout = new Borderlayout();
		layout.setWidth("100%");
		layout.setHeight("100%");
		layout.setStyle("background-color: transparent; position: absolute;");

		North north = new North();
		north.appendChild(listbox);
		listbox.setVflex("1");
		listbox.setHflex("1");
		north.setHeight("50%");
		north.setSplittable(false);
		layout.appendChild(north);
		north.setStyle("background-color: transparent");
		listbox.addEventListener(Events.ON_SELECT, this);
		
		West west = new West();
		west.appendChild(detailListBox);
		west.setStyle("background-color: transparent");
		layout.appendChild(west);
		west.setWidth("50%");
		detailListBox.setVflex("1");
		detailListBox.setHflex("1");
		west.setSplittable(false);

		Center center = new Center();
		center.appendChild(grid);
		layout.appendChild(center);
		center.setStyle("background-color: transparent");
		center.focus();
//		grid.setVflex("1");
//		grid.setHflex("1");

		South south = new South();
		south.appendChild(statusBar);
		layout.appendChild(south);
		south.setStyle("background-color: transparent");

		this.appendChild(layout);
		this.setStyle("height: 100%; width: 100%; position: absolute;");
	}

	public void onEvent(Event event) throws Exception
	{
		listbox.setMultiple(m_isMultiselection);
		Component comp = event.getTarget();
        String eventName = event.getName();

        if(eventName.equals(Events.ON_CLICK))
        {
    		if (comp == bZoom)
    			cmd_zoom();
    		else if (comp == bOK)
    		{
    			Clients.showBusy(Msg.getMsg(Env.getCtx(), "Processing"));
    			Events.echoEvent("onOK", this, null);
    		}
    		else if (comp == fAnswerButton)
    			cmd_button();
    		else if (comp == bAtt)
    		{
    			int Attach_ID = MAttachment.getID(m_activity.getAD_Table_ID(), m_activity.getRecord_ID());
    			new WAttachment(m_WindowNo, Attach_ID, m_activity.getAD_Table_ID(), m_activity.getRecord_ID(), null);
    		}
        } 
        else if (Events.ON_SELECT.equals(eventName) && comp == listbox)
        {
        	m_index = listbox.getSelectedIndex();
        	if (m_index >= 0)
    			display(m_index);
        }
        else if (Events.ON_CHANGE.equals(eventName))
        {
        	doSearchActivity();
        	display(-1);
        	listbox.repaint();
        	model.clearSelection();
        }
        else
        {
    		super.onEvent(event);
        }
	}

	/**
	 * Get active activities count
	 * @return int
	 */
	public int getActivitiesCount()
	{
		int count = 0;

		String sql = "SELECT COUNT(*) FROM AD_WF_Activity a "
			+ "WHERE " + getWhereActivities();
		int AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		MRole role = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		sql = role.addAccessSQL(sql, "a", true, false);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql, null);
			pstmt.setInt (1, AD_User_ID);
			pstmt.setInt (2, AD_User_ID);
			pstmt.setInt (3, AD_User_ID);
			pstmt.setInt (4, AD_User_ID);
			pstmt.setInt (5, AD_User_ID);
			pstmt.setInt (6, AD_User_ID);
			pstmt.setInt (7, AD_Client_ID);
			rs = pstmt.executeQuery ();
			if (rs.next ()) {
				count = rs.getInt(1);
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}

		return count;

	}

	/**
	 * 	Load Activities
	 * 	@return int
	 */
	public int loadActivities()
	{
		long start = System.currentTimeMillis();

		int MAX_ACTIVITIES_IN_LIST = MSysConfig.getIntValue(MSysConfig.MAX_ACTIVITIES_IN_LIST, 200, Env.getAD_Client_ID(Env.getCtx()));

		m_tmp.removeAllElements();
		model = new ListModelTable();
		m_activities = new ArrayList<MWFActivity>();
		
		String sql = "SELECT * FROM AD_WF_Activity a "
			+ "WHERE " + getWhereActivities()
			+ " ORDER BY a.Priority DESC, Created";
		int AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		MRole role = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		sql = role.addAccessSQL(sql, "a", true, false);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try
		{
			pstmt = DB.prepareStatement (sql, null);
			pstmt.setInt (1, AD_User_ID);
			pstmt.setInt (2, AD_User_ID);
			pstmt.setInt (3, AD_User_ID);
			pstmt.setInt (4, AD_User_ID);
			pstmt.setInt (5, AD_User_ID);
			pstmt.setInt (6, AD_User_ID);
			pstmt.setInt (7, AD_Client_ID);
			rs = pstmt.executeQuery ();
			
			while (rs.next ())
			{
				MWFActivity activity = new MWFActivity(Env.getCtx(), rs, null);
				PO po = activity.getPO();
				if (po.get_TableName().equals("C_Payment"))
				{
					boolean isReceipt = po.get_ValueAsBoolean("IsReceipt");
					Object oo = po.get_Value("Reference_ID");
					boolean skip = isReceipt && oo != null;
					if (skip)
					{
						continue;
					}
				}
				
				List<Object> rowData = new ArrayList<Object>();
				rowData.add(activity.getPriority());
				rowData.add(activity.getNodeName());
				rowData.add(activity.getApprovalAmt());
				rowData.add(activity.getSummary());
				model.add(rowData);
				m_activities.add (activity);
							
				if (m_activities.size() > MAX_ACTIVITIES_IN_LIST 
						&& MAX_ACTIVITIES_IN_LIST > 0)
				{
					log.warning("More then 200 Activities - ignored");
					break;
				}
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
		//
		if (log.isLoggable(Level.FINE)) log.fine("#" + m_activities.size()
			+ "(" + (System.currentTimeMillis()-start) + "ms)");
		m_index = 0;

		String[] columns = new String[]{Msg.translate(Env.getCtx(), "Priority"),
				Msg.translate(Env.getCtx(), "AD_WF_Node_ID"),
				Msg.translate(Env.getCtx(), "Amount"),
				Msg.translate(Env.getCtx(), "Summary")};

		WListItemRenderer renderer =
				new WListItemRenderer(Arrays.asList(columns));
		renderer.addTableValueChangeListener(listbox);
		model.setNoColumns(columns.length);
		listbox.setModel(model);
		listbox.setItemRenderer(renderer);
		listbox.repaint();
		listbox.setSizedByContent(true);
        
		return m_activities.size();
	}	//	loadActivities

	private String getWhereActivities() {
		List<Integer> caretakers = MUNSJobCareTaker.getReplacedCareTaker(
				null, new Timestamp(System.currentTimeMillis()), 
				Env.getAD_User_ID(Env.getCtx()));
		String whereClauseCareTaker = null;
		if (caretakers.size() > 0)
		{
			whereClauseCareTaker = " AD_User_ID IN (";
			for (int i=0; i<caretakers.size(); i++)
			{
				if (i > 0)
					whereClauseCareTaker += ",";
				
				whereClauseCareTaker += caretakers.get(i);
			}
			whereClauseCareTaker += ") ";
		}
		
		StringBuilder sb = new StringBuilder(" a.Processed='N' AND ")
		.append(" a.WFState='OS' AND (( a.AD_User_ID=? ")
		.append(whereClauseCareTaker != null ? " OR a.".concat(whereClauseCareTaker) : " ")
		.append(" ) OR EXISTS (SELECT * FROM AD_WF_Responsible r ")
		.append(" WHERE a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID ")
		.append(" AND r.ResponsibleType='H' AND COALESCE(r.AD_User_ID,0)=0 ")
		.append(" AND COALESCE(r.AD_Role_ID,0)=0 AND (a.AD_User_ID=? ")
		.append(whereClauseCareTaker != null ? " OR a.".concat(whereClauseCareTaker) : " ")
		.append(" OR a.AD_User_ID IS NULL)) OR EXISTS (SELECT * FROM ")
		.append(" AD_WF_Responsible r WHERE a.AD_WF_Responsible_ID= ")
		.append(" r.AD_WF_Responsible_ID AND r.ResponsibleType='H' AND (r.AD_User_ID=? ")
		.append(whereClauseCareTaker != null ? " OR r.".concat(whereClauseCareTaker) : " " )
		.append(" )) OR EXISTS (SELECT * FROM AD_WF_Responsible r INNER JOIN ")
		.append(" AD_User_Roles ur ON (r.AD_Role_ID=ur.AD_Role_ID) ")
		.append("WHERE a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID AND ")
		.append(" r.ResponsibleType='R' AND (ur.AD_User_ID=? ")
		.append(whereClauseCareTaker != null ? " OR ur.".concat(whereClauseCareTaker) : " ")
		.append(" )) OR EXISTS (SELECT * FROM AD_WF_Responsible r INNER JOIN ")
		.append(" UNS_Org_Approver oa ON (oa.AD_Org_ID = a.AD_Org_ID) ")
		.append(" WHERE a.AD_WF_Responsible_ID = r.AD_WF_Responsible_ID AND ")
		.append(" r.ResponsibleType = 'O' AND (oa.Supervisor_ID = ? ")
		.append(whereClauseCareTaker != null ? " OR oa.".concat(whereClauseCareTaker) : " ")
		.append(")) OR EXISTS (SELECT * FROM AD_WF_Responsible r INNER JOIN ")
		.append(" AD_OrgInfo oi ON (oi.AD_Org_ID = a.AD_Org_ID) ")
		.append(" WHERE a.AD_WF_Responsible_ID = r.AD_WF_Responsible_ID AND ")
		.append(" r.ResponsibleType = 'O' AND (oi.Supervisor_ID = ? ")
		.append(whereClauseCareTaker != null ? " OR oi.".concat(whereClauseCareTaker) : " ")
		.append("))) AND a.AD_Client_ID=?");
		
		String where = sb.toString();
		return where;
	}

	/**
	 * 	Reset Display
	 *	@param selIndex select index
	 *	@return selected activity
	 */
	private MWFActivity resetDisplay(int selIndex)
	{
		fAnswerText.setVisible(false);
		fAnswerList.setVisible(false);
		fAnswerButton.setVisible(false);
		fAnswerButton.setImage(ThemeManager.
				getThemeResource("images/mWindow.png"));
		fTextMsg.setReadonly(!(selIndex >= 0));
		fTextMsg.setValue(null);
		bZoom.setEnabled(selIndex >= 0);
		bOK.setEnabled(selIndex >= 0);
		bAtt.setDisabled(true);
		fForward.setValue(null);
		fForward.setReadWrite(selIndex >= 0);
		//
		statusBar.setStatusDB(String.valueOf(selIndex+1) 
				+ "/" + m_activities.size());
		m_activity = null;
		m_column = null;
		if (m_activities.size() > 0)
		{
			if (selIndex >= 0 && selIndex < m_activities.size())
				m_activity = m_activities.get(selIndex);
			int[] rows = listbox.getSelectedIndices();
			bAtt.setEnabled(selIndex >= 0 && enabledAttachment(m_activity.get_ID()) && rows.length == 1);
		}
		//	Nothing to show
		if (m_activity == null)
		{
			fNode.setText ("");
			fDescription.setText ("");
			fHelp.setText ("");
			fHistory.setContent(HISTORY_DIV_START_TAG + "&nbsp;</div>");
			statusBar.setStatusDB("0/0");
			statusBar.setStatusLine(Msg.getMsg(Env.getCtx(), "WFNoActivities"));
		}
		
		detailListBox.removeAllItems();
		return m_activity;
	}	//	resetDisplay

	/**
	 * 	Display.
	 * 	Fill Editors
	 */
	public void display (int index)
	{
		if (log.isLoggable(Level.FINE)) log.fine("Index=" + index);
		//
		m_activity = resetDisplay(index);
		//	Nothing to show
		if (m_activity == null)
		{
			return;
		}
		//	Display Activity
		fNode.setText (m_activity.getNodeName());
		fDescription.setValue (m_activity.getNodeDescription());
		fHelp.setValue (m_activity.getNodeHelp());
		//
		fHistory.setContent (HISTORY_DIV_START_TAG+m_activity.getHistoryHTML()+"</div>");
		PO po = m_activity.getPO();
		if (po instanceof IUNSApprovalInfo)
		{
			loadDetailPane(po);
		}
		//	User Actions
		MWFNode node = m_activity.getNode();
		if (MWFNode.ACTION_UserChoice.equals(node.getAction()))
		{
			if (m_column == null)
				m_column = node.getColumn();
			if (m_column != null && m_column.get_ID() != 0)
			{
				fAnswerList.removeAllItems();
				int dt = m_column.getAD_Reference_ID();
				if (dt == DisplayType.YesNo)
				{
					ValueNamePair[] values = MRefList.getList(Env.getCtx(), 319, false);		//	_YesNo
					for(int i = 0; i < values.length; i++)
					{
						fAnswerList.appendItem(values[i].getName(), values[i].getValue());
					}
					fAnswerList.setVisible(true);
				}
				else if (dt == DisplayType.List)
				{
					ValueNamePair[] values = MRefList.getList(Env.getCtx(), m_column.getAD_Reference_Value_ID(), false);
					for(int i = 0; i < values.length; i++)
					{
						fAnswerList.appendItem(values[i].getName(), values[i].getValue());
					}
					fAnswerList.setVisible(true);
				}
				else	//	other display types come here
				{
					fAnswerText.setText ("");
					fAnswerText.setVisible(true);
				}
			}
		}
		//	--
		else if (MWFNode.ACTION_UserWindow.equals(node.getAction())
			|| MWFNode.ACTION_UserForm.equals(node.getAction())
			|| MWFNode.ACTION_UserInfo.equals(node.getAction()))
		{
			fAnswerButton.setLabel(node.getName());
			fAnswerButton.setTooltiptext(node.getDescription());
			fAnswerButton.setVisible(true);
		}
		else
			log.log(Level.SEVERE, "Unknown Node Action: " + node.getAction());

		statusBar.setStatusDB((m_index+1) + "/" + m_activities.size());
		statusBar.setStatusLine(Msg.getMsg(Env.getCtx(), "WFActivities"));
	}	//	display


	/**
	 * 	Zoom
	 */
	private void cmd_zoom()
	{
		if (log.isLoggable(Level.CONFIG)) log.config("Activity=" + m_activity);
		if (m_activity == null)
			return;
		
		int[] rows = listbox.getSelectedIndices();
		if (m_isMultiselection && rows.length > 1)
		{
			for (int i=0; i<rows.length; i++)
			{
				MWFActivity activity = m_activities.get(rows[i]);
				AEnv.zoom(activity.getAD_Table_ID(), activity.getRecord_ID());
			}
		}
		else
		{
			AEnv.zoom(m_activity.getAD_Table_ID(), m_activity.getRecord_ID());	
		}
	}	//	cmd_zoom

	/**
	 * 	Answer Button
	 */
	private void cmd_button()
	{
		if (log.isLoggable(Level.CONFIG)) log.config("Activity=" + m_activity);
		if (m_activity == null)
			return;
		
		int[] rows = listbox.getSelectedIndices();
		if (m_isMultiselection && rows.length > 1)
		{
			for (int i=0; i<rows.length; i++)
			{
				MWFActivity activity = m_activities.get(rows[i]);
				cmd_button(activity);
			}
		}
		else
		{
			cmd_button(m_activity);
		}
		
	}	//	cmd_button
	
	
	private void cmd_button (MWFActivity activity)
	{
		MWFNode node = activity.getNode();
		if (MWFNode.ACTION_UserWindow.equals(node.getAction()))
		{
			int AD_Window_ID = node.getAD_Window_ID();		// Explicit Window
			String ColumnName = activity.getPO().get_TableName() + "_ID";
			int Record_ID = activity.getRecord_ID();
			MQuery query = MQuery.getEqualQuery(ColumnName, Record_ID);
			boolean IsSOTrx = activity.isSOTrx();
			//
			log.info("Zoom to AD_Window_ID=" + AD_Window_ID
				+ " - " + query + " (IsSOTrx=" + IsSOTrx + ")");

			AEnv.zoom(AD_Window_ID, query);
		}
		else if (MWFNode.ACTION_UserForm.equals(node.getAction()))
		{
			int AD_Form_ID = node.getAD_Form_ID();

			ADForm form = ADForm.openForm(AD_Form_ID);
			form.setAttribute(Window.MODE_KEY, form.getWindowMode());
			AEnv.showWindow(form);
		}else if (MWFNode.ACTION_UserInfo.equals(node.getAction())){
			SessionManager.getAppDesktop().openInfo(node.getAD_InfoWindow_ID());
		}
		else
			log.log(Level.SEVERE, "No User Action:" + node.getAction());
	}
	

	private void cmd_multi_ok ()
	{
		int[] rows = listbox.getSelectedIndices();
		for (int i=0; i<rows.length; i++)
		{
			MWFActivity activity = m_activities.get(rows[i]);
			doIt(activity);
		}
	}
	

	/**
	 * 	Save
	 */
	public void onOK()
	{
		if (log.isLoggable(Level.CONFIG)) log.config("Activity=" + m_activity);
		if (m_activity == null)
		{
			Clients.clearBusy();
			return;
		}
		
		if (m_isMultiselection && listbox.getSelectedIndices().length > 1)
		{
			cmd_multi_ok();
		}
		else
		{
			doIt(m_activity);
		}

		//	Next
		loadActivities();
		display(-1);

        boolean isMultiselection = listbox.isMultiSelection();
        System.out.println(getClass().getName() + " Multiselection " 
        			+ isMultiselection);
	}	//	onOK
	
	
	private void doIt (MWFActivity activity, Trx trx)
	{
		activity.getPO().setProcessInfo(getProcessInfo());
		int AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		String textMsg = fTextMsg.getValue();
		MWFNode node = activity.getNode();
		Object forward = fForward.getValue();

		try 
		{
			activity.set_TrxName(trx.getTrxName());

			if (forward != null)
			{
				if (log.isLoggable(Level.CONFIG)) log.config("Forward to " + forward);
				int fw = ((Integer)forward).intValue();
				if (fw == AD_User_ID || fw == 0)
				{
					log.log(Level.SEVERE, "Forward User=" + fw);
					trx.rollback();
					trx.close();
					return;
				}
				if (!activity.forwardTo(fw, textMsg))
				{
					FDialog.error(m_WindowNo, this, "CannotForward");
					trx.rollback();
					trx.close();
					return;
				}
			}
			//	User Choice - Answer
			else if (MWFNode.ACTION_UserChoice.equals(node.getAction()))
			{
				if (m_column == null)
					m_column = node.getColumn();
				//	Do we have an answer?
				int dt = m_column.getAD_Reference_ID();
				String value = fAnswerText.getText();
				if (dt == DisplayType.YesNo || dt == DisplayType.List)
				{
					ListItem li = fAnswerList.getSelectedItem();
					if(li != null) value = li.getValue().toString();
				}
				if (value == null || value.length() == 0)
				{
					FDialog.error(m_WindowNo, this, "FillMandatory", Msg.getMsg(Env.getCtx(), "Answer"));
					trx.rollback();
					trx.close();
					return;
				}
				//
				if (log.isLoggable(Level.CONFIG)) log.config("Answer=" + value + " - " + textMsg);
				try
				{
					activity.setUserChoice(AD_User_ID, value, dt, textMsg);
				}
				catch (Exception e)
				{
					log.log(Level.SEVERE, node.getName(), e);
					FDialog.error(m_WindowNo, this, "Error", e.toString());
					trx.rollback();
					trx.close();
					return;
				}
			}
			//	User Action
			else
			{
				if (log.isLoggable(Level.CONFIG)) log.config("Action=" + node.getAction() + " - " + textMsg);
				try
				{
					// ensure activity is ran within a transaction
					activity.setUserConfirmation(AD_User_ID, textMsg);
				}
				catch (Exception e)
				{
					log.log(Level.SEVERE, node.getName(), e);
					FDialog.error(m_WindowNo, this, "Error", e.toString());
					trx.rollback();
					trx.close();
					return;
				}

			}
			
			PO po = activity.getPO();
			if (po instanceof MPayment)
			{
				boolean ok = !po.get_ValueAsBoolean("IsReceipt") 
						&& po.get_Value("Reference_ID") != null;
				if (ok)
				{
					executeRefActivity(po.get_ValueAsInt("Reference_ID"), trx);
				}
			}
		}
		finally
		{
			Clients.clearBusy();
		}
	}
	
	public void loadDetailPane (PO po)
	{
		detailListBox.clear();
		detailTable = new ListModelTable();
		IUNSApprovalInfo appInfo = (IUNSApprovalInfo) po;
		String[] headerColumn = appInfo.getDetailTableHeader();
		WListItemRenderer renderer = new WListItemRenderer(
				Arrays.asList(headerColumn));
		List<Object[]> contents = appInfo.getDetailTableContent();
		for (int i=0; i<contents.size(); i++)
		{
			detailTable.add(Arrays.asList(contents.get(i)));
		}
		List<Object[]> accessColumn = appInfo.
				getApprovalInfoColumnClassAccessable();
		for (int i=0; i<accessColumn.size(); i++)
		{
			detailListBox.setColumnClass(i, (Class<?>) accessColumn.get(i)[0],
					(boolean) accessColumn.get(i)[1]);
		}
		detailTable.setNoColumns(headerColumn.length);
		detailListBox.setModel(detailTable);
		detailListBox.setItemRenderer(renderer);
		detailListBox.repaint();
		detailListBox.setSizedByContent(true);
		
//		detailListBox.addActionListener(new EventListener<Event>() {
//   			public void onEvent(Event event) throws Exception {
//   				PO po = m_activity.getPO();
//				IUNSApprovalInfo appInfo = (IUNSApprovalInfo) po;
//				boolean isShowAttachmentDetail = appInfo.isShowAttachmentDetail();
//				int tableIDDetail = appInfo.getTableIDDetail();
//				int row = detailListBox.getSelectedRow();
//	        	int column = detailListBox.getColumnCount() - 1;
//				if(isShowAttachmentDetail)
//		        {
//		        	int recordID = new Integer(detailListBox.getValueAt(row, column).toString());
//		        	int Attach_ID = MAttachment.getID(tableIDDetail, recordID);
//					new WAttachment(m_WindowNo, Attach_ID, tableIDDetail, recordID, null);
//		        }
//   			}
//   		}); 
	}
	
	private void doSearchActivity ()
	{
		String search = (String) fSearch.getValue();
		if (Util.isEmpty(search, true))
		{
			doRestore();
			return;
		}
		
		String[] searchs = search.split(";");
		
		for (int i=0; i<model.getSize(); i++)
		{
			@SuppressWarnings("unchecked")
			List<Object> rowData = (List<Object>)model.get(i);
			MWFActivity activity = m_activities.get(i);
			String value = (String) rowData.get(3);
			boolean isExists = false;
			
			for (int x=0; x<searchs.length; x++)
			{
				if (null != value && isMatch(searchs[x], value))
				{
					isExists = true;
					break;
				}
			}
			
			if (!isExists)
			{
				m_tmp.add(new ActivityMapping(activity, rowData));
				model.remove(i);
				m_activities.remove(i);
				i--;
				continue;
			}
		}
		
		for (int i=0; i<m_tmp.size(); i++)
		{
			ActivityMapping mapping = m_tmp.get(i);
			String value = (String) mapping.getRowData().get(3);
			boolean isExists = false;
			
			for (int x=0; x<searchs.length; x++)
			{
				if (null != value && isMatch(searchs[x], value))
				{
					isExists = true;
					break;
				}
			}
			
			if (isExists)
			{
				model.add(mapping.getRowData());
				m_activities.add(mapping.getActivity());
				m_tmp.remove(i);
				i--;
				continue;
			}
		}
	}
	
	private void doRestore ()
	{
		for (int i=0; i<m_tmp.size(); i++)
		{
			ActivityMapping mapping= m_tmp.get(i);
			model.add(mapping.getRowData());
			m_activities.add(mapping.getActivity());
		}
		
		m_tmp.removeAllElements();
		fSearch.setValue(null);
	}
	
	private boolean isMatch (String searchCriteria, String value)
	{
		Pattern patern = Pattern.compile(searchCriteria.toUpperCase());
		Matcher matcher = patern.matcher(value.toUpperCase());
		return matcher.find();
	}
	
	/**
	 * Special case (Payment from transfer bank @AR Payment@)
	 * @param ref_ID
	 */
	private void executeRefActivity (int ref_ID, Trx trx)
	{
		String sql = "SELECT * FROM AD_WF_Activity a "
				+ "WHERE " + getWhereActivities() + " AND Record_ID = ? AND "
				+ " AD_Table_ID = ? ORDER BY a.Priority DESC, Created";
		
		int AD_User_ID = Env.getAD_User_ID(Env.getCtx());
		int AD_Client_ID = Env.getAD_Client_ID(Env.getCtx());
		MRole role = MRole.get(Env.getCtx(), Env.getAD_Role_ID(Env.getCtx()));
		sql = role.addAccessSQL(sql, "a", true, false);
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try
		{
			pstmt = DB.prepareStatement (sql, trx.getTrxName());
			pstmt.setInt (1, AD_User_ID);
			pstmt.setInt (2, AD_User_ID);
			pstmt.setInt (3, AD_User_ID);
			pstmt.setInt (4, AD_User_ID);
			pstmt.setInt (5, AD_User_ID);
			pstmt.setInt (6, AD_User_ID);
			pstmt.setInt (7, AD_Client_ID);
			pstmt.setInt(8, ref_ID);
			pstmt.setInt(9, MPayment.Table_ID);
			rs = pstmt.executeQuery ();
			while (rs.next ())
			{
				MWFActivity activity = new MWFActivity(
						Env.getCtx(), rs, null);
				doIt(activity, trx);
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, sql, e);
			FDialog.error(m_WindowNo, this, "Unprocessed AR Receipt");
			trx.rollback();
			trx.close();
			return;
		}
		finally
		{
			DB.close(rs, pstmt);
			rs = null; pstmt = null;
		}
	}
	
	private void doIt(MWFActivity activity)
	{
		Trx trx = Trx.get(Trx.createTrxName("FWFA"), true);
		doIt(activity, trx);
		if (trx.isActive())
		{
			trx.commit();
			trx.close();
		}
	}
	
	private boolean enabledAttachment(int id)
	{
		if(id <= 0)
			return false;
		MWFActivity currActivity = new MWFActivity(Env.getCtx(),
				id, null);
		int Attach_ID = MAttachment.getID(currActivity.getAD_Table_ID(), currActivity.getRecord_ID());
		return Attach_ID > 0;
	}
}

class ActivityMapping 
{
	private MWFActivity m_activity;
	private List<Object> m_rowData;
	
	public ActivityMapping(MWFActivity activity, List<Object> rowData)
	{
		this.m_activity = activity;
		this.m_rowData = rowData;
	}
	
	public MWFActivity getActivity ()
	{
		return this.m_activity;
	}
	
	public List<Object> getRowData ()
	{
		return this.m_rowData;
	}
}
